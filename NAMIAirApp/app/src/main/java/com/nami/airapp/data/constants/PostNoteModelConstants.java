package com.nami.airapp.data.constants;

/**
 * Holds constants for the PostNote model.
 */
public class PostNoteModelConstants {

    public static final String FieldPost = "post";
    public static final String FieldUser = "user";
	public static final String FieldNotes = "notes";
}
