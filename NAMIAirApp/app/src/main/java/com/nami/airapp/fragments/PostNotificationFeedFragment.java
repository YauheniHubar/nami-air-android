package com.nami.airapp.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
import java.util.ArrayList;

import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.interfaces.PostRemovedListener;
import com.nami.airapp.utils.ActionBarManager;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import com.nami.airapp.R;
import com.nami.airapp.data.adapters.BaseParseAdapter.OnQueryRunListener;
import com.nami.airapp.data.adapters.PostNotificationFeedAdapter;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.data.models.PostNotificationModel;
import com.nami.airapp.utils.KeyboardManager;


public class PostNotificationFeedFragment extends ListFragment implements OnRefreshListener, OnQueryRunListener, PostRemovedListener {

    // Private Members.    
    private Context context = null;
    private PostNotificationFeedViewContainer viewContainer = null;
    private PostNotificationFeedAdapter adapter = null;
    private SwipeRefreshLayout swipeRefreshLayout = null;

    // Private Static Members.
    private static final String KEY_FRAGMENT_POST_NOTIFICATION_FEED_LIST = "KEY_FRAGMENT_POST_NOTIFICATION_FEED_LIST";

    // Public Static Members.
    public static final int TAB = 3;
    public static final String TAG = "PostNotificationFeedFragmentTag";

    /**
     * Default Constructor.
     */
    public PostNotificationFeedFragment() {}

    /**
     * Creates a new instance of a PostNotificationFeedFragment.
     * @return New instance of a PostNotificationFeedFragment.
     */
    public static PostNotificationFeedFragment newInstance() {

        // Create and return the new instance.
        PostNotificationFeedFragment instance = new PostNotificationFeedFragment();

        // Create the bundle.
        instance.setArguments(new Bundle());

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set options menu.
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_notification_feed, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ApplicationManager.getInstance().addPostRemovedListener(this);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Show the moderator message directly if applicable.
        this.showModeratorMessageDirectly();
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.post_feed, menu);

    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when the fragment is paused.
     */
    @Override
    public void onPause() {
        super.onPause();

        // Put the adapter list into the bundle.
        if (this.adapter.getDataSet() != null && this.adapter.getDataSet().size() > 0)
            getArguments().putParcelableArrayList(KEY_FRAGMENT_POST_NOTIFICATION_FEED_LIST, (ArrayList<PostNotificationModel>)this.adapter.getDataSet());
    }

    /**
     * Called when the activity is created.
     * @param savedInstanceState The saved instance state.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check the bundle.
        if (getArguments() != null && getArguments().containsKey(KEY_FRAGMENT_POST_NOTIFICATION_FEED_LIST)) {
            List<PostNotificationModel> dataSet = getArguments().getParcelableArrayList(KEY_FRAGMENT_POST_NOTIFICATION_FEED_LIST);

            // Create the adapter.
            this.adapter = new PostNotificationFeedAdapter(this, context, false);

            // Set the data set.
            this.adapter.setDataSet(dataSet);

        } else {

            // Create the adapter.
            this.adapter = new PostNotificationFeedAdapter(this, context, true);

            // Create and set the adapter.
            setListAdapter(this.adapter);
        }

        // Set the adapter.
        setListAdapter(this.adapter);
        this.adapter.setOnQueryRunListener(this);
    }

    /**
     * Called when a list view item is clicked.
     * @param listView List view containing the clicked item.
     * @param view The view.
     * @param position The position of the item.
     * @param id The identifier of the item.
     */
    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {

        // Get the notification.
        PostNotificationModel model = (PostNotificationModel)getListView().getItemAtPosition(position);

        // Set as read if not read.
        if (model.getRead() == false) {
            PostNotificationModel.SetNotifcationRead(context, model);
            model.setRead(true);

            // Decrease the badge count.
            ApplicationManager.getInstance().decreaseNotificationBadgeCount();
        }

        // Display the appropriate fragment.
        switch (model.getNotificationType()) {

            case SOCIAL:
            case NOTES:
            case PENDING:
            case APPROVED:
                this.showPostModelDetail(model.getPostModel());
                break;
            case DISAPPROVED:
            case DIRECT:
                this.showModeratorDirectMessage(model);
                break;
            case INSPIRED:
                this.showInspiredPost(model.getPostModel());
                break;
            case CHANGE_GROUP:
                this.adapter.setDataSet(this.adapter.getDataSet());
                break;

        }
    }

    /**
     * OnQueryRunListener Implementation
     */
    public void onLoaded() {

        if (!isAdded()) return;

        // Show / Hide the appropriate controls.
        if (this.adapter.getDataSet().size() > 0) {
            this.viewContainer.progress.setVisibility(View.GONE);
            this.viewContainer.listView.setVisibility(View.VISIBLE);
            this.viewContainer.textViewNoResults.setVisibility(View.GONE);
        } else {
            this.viewContainer.progress.setVisibility(View.GONE);
            this.viewContainer.listView.setVisibility(View.GONE);
            this.viewContainer.textViewNoResults.setVisibility(View.VISIBLE);
        }
    }

    /**
     * OnQueryRunListener Implementation
     */
    public void onLoading() {

        // Show / Hide the appropriate controls.
        this.viewContainer.progress.setVisibility(View.VISIBLE);
    }

    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.main_tab_text_notifications));
    }

    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = PostNotificationFeedViewContainer.with(view);

        // Get the swipe layout.
        this.swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.post_notification_feed_swipe_refresh_layout);

        // Set the swipe layout properties.
        this.swipeRefreshLayout.setOnRefreshListener(this);

        this.viewContainer.listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (viewContainer.listView == null || viewContainer.listView.getChildCount() == 0) ?
                                0 : viewContainer.listView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
            }
        });
    }

    @Override
    public void onRefresh() {

        // Reload the data.
        this.adapter.reloadData(this.swipeRefreshLayout);
    }

    /**
     * Implements the PostRemovedListener.
     * @param postModel
     */
    public void PostRemoved(PostModel postModel) {

        // Check the bundle.
        if (getArguments() != null && getArguments().containsKey(KEY_FRAGMENT_POST_NOTIFICATION_FEED_LIST)) {
            getArguments().remove(KEY_FRAGMENT_POST_NOTIFICATION_FEED_LIST);
        }
    }

    /**
     * Shows the post model detail.
     * @param postModel The post model.
     */
    private void showPostModelDetail(PostModel postModel) {

        // Determine the post owner.
        ParseUser postOwnerUser = postModel.getUser();
        if (TextUtils.equals(postOwnerUser.getObjectId(), ParseUser.getCurrentUser().getObjectId())) {

            // Create the fragment.
            MyPostDetailFragment postDetailFragment = MyPostDetailFragment.newInstance(postModel);

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, postDetailFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();

        } else {

            // Create the fragment.
            PostDetailFragment postDetailFragment = PostDetailFragment.newInstance(postModel);

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, postDetailFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();
        }
    }

    /**
     * Shows the inspired posts.
     * @param postModel The post model.
     */
    private void showInspiredPost(PostModel postModel) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        try {

            // Create the query factory.
            ParseQueryAdapter.QueryFactory<PostModel> queryFactory = PostModel.createPostInspiredByFeedAdapterQuery(postModel);

            // Create and execute the query.
            ParseQuery<PostModel> query = queryFactory.create();
            List<PostModel> inspiredPosts = query.find();

            // Return if there are no inspired posts.
            if (inspiredPosts == null || inspiredPosts.size() == 0)
                return;

            // Test the group.
            PostModel inspiredPostModel = inspiredPosts.get(0);
            if (isCorrectGroupType(inspiredPostModel) == false) {

                // Show the dialog and return.
                this.showIncorrectGroupDialog();
                return;
            }

            // Determine if single post or feed.
            if (inspiredPosts.size() > 1) {

                // Create the fragment.
                InspiredPostFeedFragment postFeedFragment = InspiredPostFeedFragment.newInstance(postModel);

                // Create the transaction.
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                transaction.replace(R.id.main_container, postFeedFragment);
                transaction.addToBackStack(null);

                // Commit the transaction.
                transaction.commit();

            } else if (inspiredPosts.size() == 1) {

                // Create the fragment.
                PostDetailFragment postDetailFragment = PostDetailFragment.newInstance(inspiredPosts.get(0));

                // Create the transaction.
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                transaction.replace(R.id.main_container, postDetailFragment);
                transaction.addToBackStack(null);

                // Commit the transaction.
                transaction.commit();
            }
        }
        catch (ParseException e) {

            // Show the message.
            ApplicationManager.getInstance().showConnectionError(context);
        }
    }

    /**
     * Checks the post model group type against the current user group type.
     * @param postModel
     * @return
     */
    private boolean isCorrectGroupType(PostModel postModel) {

        if (postModel.getUserType().getValue() == ParseUser.getCurrentUser().getInt(UserModelConstants.FieldUserType))
            return true;

        return false;
    }

    /**
     * Shows the incorrect group dialog.
     */
    private void showIncorrectGroupDialog() {

        // Create the dialog.
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // Set the properties.
        alertDialogBuilder.setTitle(getString(R.string.incorrect_group_dialog_title));
        alertDialogBuilder.setMessage(getString(R.string.incorrect_group_dialog_message));
        alertDialogBuilder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                // if this button is clicked, just close
                // the dialog box and do nothing
                dialog.cancel();
            }
        });

        // Create and show the dialog.
        alertDialogBuilder.create().show();

    }

    /**
     * Shows the moderator message directly if one exists.
     */
    private void showModeratorMessageDirectly() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Attempt to get the moderator message.
        String moderatorMessageId = ApplicationManager.getInstance().getModeratorMessageId();
        if (moderatorMessageId != null) {

            // Clear the message id.
            ApplicationManager.getInstance().setModeratorMessageId(null);

            // Fetch the notification model.
            ParseQuery<PostNotificationModel> query = PostNotificationModel.createPostNotificationModeratorMessageQuery(moderatorMessageId);

            // Run the query.
            query.getFirstInBackground(new GetCallback<PostNotificationModel>() {

                   @Override
                   public void done(PostNotificationModel postNotificationModel, ParseException e) {

                       // Check for an exception.
                       if (e != null) {
                           ApplicationManager.getInstance().showConnectionError(context);
                           return;
                       }

                       // Show the message.
                       showModeratorDirectMessage(postNotificationModel);
                   }
                }
            );
        }
    }


    /**
     * Shows the post model detail.
     * @param postNotificationModel The post notification model.
     */
    private void showModeratorDirectMessage(PostNotificationModel postNotificationModel) {

        // Set as read if not read.
        if (postNotificationModel.getRead() == false)
            PostNotificationModel.SetNotifcationRead(context, postNotificationModel);

        // Create the fragment.
        PostNotificationAdminFragment postNotificationAdminFragment = PostNotificationAdminFragment.newInstance(postNotificationModel);

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, postNotificationAdminFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Holds the view for the Post Feed Item.
     */
    private static class PostNotificationFeedViewContainer {

        // Private Members.
        private LinearLayout progress;
        private ListView listView;
        private TextView textViewNoResults;


        // Private Constructor.
        private PostNotificationFeedViewContainer() {}

        // Static Constructor.
        public static PostNotificationFeedViewContainer with(View view) {

            // Create the instance
            PostNotificationFeedViewContainer instance = new PostNotificationFeedViewContainer();

            // Get the items.
            instance.listView = (ListView)view.findViewById(android.R.id.list);
            instance.progress = (LinearLayout)view.findViewById(R.id.progress);
            instance.textViewNoResults = (TextView)view.findViewById(R.id.no_results);

            return instance;
        }
    }
}
