package com.nami.airapp.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Manages the keyboard state.
 */
public class KeyboardManager {

    /**
     * Hides the keyboard for an Activity.
     * @param context The Activity to hide the keyboard for.
     */
    public static void HideActivityKeyboard(Context context) {

        // Get the input manager.
        InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);

        // Check if the view has focus.
        View focusedView = ((FragmentActivity)context).getCurrentFocus();
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
