package com.nami.airapp.data.adapters;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.SwipeRefreshLayout;

import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import com.nami.airapp.data.models.PostNotificationModel;
import com.nami.airapp.views.PostNotificationFeedItemView;

public class PostNotificationFeedAdapter extends BaseParseAdapter {

    // Private Members
    private Fragment fragment = null;
    private Context context = null;
    private List<PostNotificationModel> dataSet = null;
    private SwipeRefreshLayout swipeRefreshLayout = null;

    /**
     * Default Constructor.
     * @param fragment The current fragment.
     * @param context Context which the adapter runs in.
     * @param reload Whether to reload the data.
     */
    public PostNotificationFeedAdapter(Fragment fragment, Context context, boolean reload) {
        super(context);

        // Set the members.
        this.fragment = fragment;
        this.context = context;

        // Load / Reload the data.
        if (reload)
            this.reloadData(null);
    }

    /**
     * Gets the number of items in the list.
     * @return Number of items in the list.
     */
    @Override
    public int getCount() {

        return this.dataSet.size();
    }

    /**
     * Gets the item at the position in the list.
     * @param position Position in the list.
     * @return Item at the position in the list.
     */
    @Override
    public PostNotificationModel getItem(int position) {

        return this.dataSet.get(position);
    }

    /**
     * Gets the item identifier at the position in the list.
     * @param position Position in the list.
     * @return Item identifier at the position in the list.
     */
    @Override
    public long getItemId(int position) {

        return position;
    }

    /**
     * Gets the view for the item in the position in the list.
     * @param position Position in the list.
     * @param convertView View of the item in the list.
     * @param parent Parent view of the item in the list.
     * @return Updated view of the item in the list.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Create the view if necessary.
        if (convertView == null) {
            convertView = new PostNotificationFeedItemView(context);
        }

        // Get the item and update the view.
        PostNotificationModel model = this.getItem(position);
        ((PostNotificationFeedItemView)convertView).updateFromModel(model, position);

        return convertView;
    }

    /**
     * Reloads the data.
     */
    public void reloadData(SwipeRefreshLayout swipeRefreshLayout) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            this.dataSet = new ArrayList<PostNotificationModel>();
            return;
        }

        // Set loading.
        super.setLoading(true);

        // Set the swipe layout.
        this.swipeRefreshLayout = swipeRefreshLayout;

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<PostNotificationModel> queryFactory = PostNotificationModel.createPostNotificationFeedAdapterQuery();

        // Create and execute the query.
        ParseQuery<PostNotificationModel> query = queryFactory.create();
        new NotificationFeedParseQueryTask(this.context, this).execute(query);
    }

    /**
     * Gets the data set.
     * @return List of data.
     */
    public List<PostNotificationModel> getDataSet() {

        return this.dataSet;
    }

    /**
     * Sets the data set.
     * @param dataSet List of data.
     */
    public void setDataSet(List<PostNotificationModel> dataSet) {

        // Set the data set.
        this.dataSet = dataSet;

        // Sort the data set.
        this.sortDataSet();

        // Check for the swipe layout.
        if (this.swipeRefreshLayout != null) {
            this.swipeRefreshLayout.setRefreshing(false);
            this.swipeRefreshLayout = null;
        }

        // Get the badge count.
        int badgeCount = 0;
        for (PostNotificationModel postNotificationModel : this.dataSet) {
            if (postNotificationModel.getRead() == false)
                badgeCount++;
        }

        // Set not loading.
        super.setLoading(false);

        // Notify that the data set has changed.
        this.notifyDataSetChanged();

        // Set the badge count.
        ApplicationManager.getInstance().setNotificationBadgeCount((MainActivity)this.context, badgeCount);
    }

    /**
     * Sort the data set correctly.
     */
    private void sortDataSet() {

        // Sort the data set.
        java.util.Collections.sort(this.dataSet);
        java.util.Collections.reverse(this.dataSet);
    }

    /**
     * Queries parse.
     */
    private class NotificationFeedParseQueryTask extends AsyncTask<ParseQuery<PostNotificationModel>, Void, List<PostNotificationModel>> {

        private Context context = null;
        private PostNotificationFeedAdapter adapter = null;
        private ParseException parseException = null;

        public NotificationFeedParseQueryTask(Context context, PostNotificationFeedAdapter adapter) {
            this.context = context;
            this.adapter = adapter;
        }

        @Override
        protected void onPreExecute() {

            // Create the posts.
            dataSet = new ArrayList<PostNotificationModel>();
        }

        @Override
        protected List<PostNotificationModel> doInBackground(ParseQuery<PostNotificationModel>... queries) {

            // Declare the return value.
            List<PostNotificationModel> data = null;

            // Get the query.
            ParseQuery<PostNotificationModel> query = queries[0];

            // Attempt to execute the query.
            try {
                data = query.find();
            }
            catch (ParseException e) {
                this.parseException = e;
                data = new ArrayList<PostNotificationModel>();
            }

            return data;
        }

        @Override
        protected void onPostExecute(List<PostNotificationModel> postsFound) {

            if (this.parseException != null) {

                if (this.adapter.swipeRefreshLayout != null) {
                    this.adapter.swipeRefreshLayout.setRefreshing(false);
                    this.adapter.swipeRefreshLayout = null;
                }

                ApplicationManager.getInstance().showConnectionError(this.context);
                return;
            }

            // Set the data set.
            setDataSet(postsFound);
        }
    }
}
