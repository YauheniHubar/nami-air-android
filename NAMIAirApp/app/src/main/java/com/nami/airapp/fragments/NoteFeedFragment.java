package com.nami.airapp.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.nami.airapp.R;
import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.adapters.BaseParseAdapter;
import com.nami.airapp.data.adapters.NoteFeedAdapter;
import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.enums.FeedType;
import com.nami.airapp.data.enums.UserType;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.AppFragmentManager;
import com.nami.airapp.utils.KeyboardManager;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class NoteFeedFragment extends ListFragment implements BaseParseAdapter.OnQueryRunListener {

    // Private Members.
    private Context context = null;
    private FeedType feedType = null;
    private UserType userType = null;
    private NoteFeedAdapter adapter = null;
    private String queryText = null;
    private PostFeedViewContainer viewContainer = null;

    // Private Static Members
    private static final String KEY_FRAGMENT_QUERY_TEXT = "KEY_FRAGMENT_QUERY_TEXT";
    private static final String KEY_FRAGMENT_SEARCH_FEED_LIST = "KEY_FRAGMENT_SEARCH_FEED_LIST";


    /**
     * Default Constructor.
     */
    public NoteFeedFragment() {}

    /**
     * Creates a new instance of a PostFeedFragment.
     * @return New instance of a PostFeedFragment.
     */
    public static NoteFeedFragment newInstance(String queryText) {

        // Create and return the new instance.
        NoteFeedFragment instance = new NoteFeedFragment();

        // Add the query text to the bundle.
        Bundle args = new Bundle();
        args.putString(KEY_FRAGMENT_QUERY_TEXT, queryText);
        instance.setArguments(args);

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {

            // Get the query text.
            this.queryText = args.getString(KEY_FRAGMENT_QUERY_TEXT);

            // Format the query text.
            this.queryText = this.queryText.replace("#", "");
            this.queryText = "#" + this.queryText;
        }
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set the action bar.
        MainActivity mainActivity = (MainActivity)context;
        mainActivity.setActionBarNoTabs();

        // Set options menu.
        setHasOptionsMenu(true);

        // Get the current user type and set the feed type.
        this.userType = UserType.fromValue(ParseUser.getCurrentUser().getInt(UserModelConstants.FieldUserType));
        this.feedType = FeedType.ALL_POSTS;

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_feed, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    AppFragmentManager.RemoveFromStack(that, context);
                    return true;
                }

                return false;
            }
        });
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, context);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when the fragment is paused.
     */
    @Override
    public void onPause() {
        super.onPause();

        // Put the adapter list into the bundle.
        getArguments().putParcelableArrayList(KEY_FRAGMENT_SEARCH_FEED_LIST, (ArrayList<PostModel>)this.adapter.getDataSet());
    }

    /**
     * Called when the activity is created.
     * @param savedInstanceState The saved instance state.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check the bundle.
        if (getArguments() != null && getArguments().containsKey(KEY_FRAGMENT_SEARCH_FEED_LIST)) {
            List<PostModel> dataSet = getArguments().getParcelableArrayList(KEY_FRAGMENT_SEARCH_FEED_LIST);

            // Create and set the adapter.
            this.adapter = new NoteFeedAdapter(this, context, this.feedType, this.userType, this.queryText, false);

            // Set the data set.
            this.adapter.setDataSet(dataSet);
        } else {

            // Create and set the adapter.
            this.adapter = new NoteFeedAdapter(this, context, this.feedType, this.userType, this.queryText, true);
        }

        // Set the adapter.
        this.adapter.setOnQueryRunListener(this);
        setListAdapter(this.adapter);
    }


    /**
     * Called when a list view item is clicked.
     * @param listView List view containing the clicked item.
     * @param view The view.
     * @param position The position of the item.
     * @param id The identifier of the item.
     */
    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {

        // Get the post model.
        PostModel selectedPostModel = (PostModel)getListView().getItemAtPosition(position);
        this.showPostModelDetail(selectedPostModel);
    }

    /**
     * OnQueryRunListener Implementation
     */
    public void onLoaded() {

        // Show / Hide the appropriate controls.
        this.viewContainer.progress.setVisibility(View.GONE);
        this.viewContainer.listView.setVisibility(View.VISIBLE);
    }

    /**
     * OnQueryRunListener Implementation
     */
    public void onLoading() {

        // Show / Hide the appropriate controls.
        this.viewContainer.progress.setVisibility(View.VISIBLE);
    }


    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, this.queryText);
    }

    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(final View view) {

        // Create the view container.
        this.viewContainer = PostFeedViewContainer.with(view);
    }

    /**
     * Shows the post model detail.
     * @param postModel The post model.
     */
    private void showPostModelDetail(PostModel postModel) {

        // Determine the post owner.
        ParseUser postOwnerUser = postModel.getUser();
        if (TextUtils.equals(postOwnerUser.getObjectId(), ParseUser.getCurrentUser().getObjectId())) {

            // Create the fragment.
            MyPostDetailFragment postDetailFragment = MyPostDetailFragment.newInstance(postModel);

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, postDetailFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();

        } else {

            // Create the fragment.
            PostDetailFragment postDetailFragment = PostDetailFragment.newInstance(postModel);

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, postDetailFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();
        }
    }

    /**
     * Holds the view for the Post Feed Item.
     */
    private static class PostFeedViewContainer {

        // Private Members.
        private LinearLayout progress;
        private ListView listView;


        // Private Constructor.
        private PostFeedViewContainer() {}

        // Static Constructor.
        public static PostFeedViewContainer with(View view) {

            // Create the instance
            PostFeedViewContainer instance = new PostFeedViewContainer();

            // Get the items.
            instance.listView = (ListView)view.findViewById(android.R.id.list);
            instance.progress = (LinearLayout)view.findViewById(R.id.progress);

            return instance;
        }
    }

}
