package com.nami.airapp.data.constants;

/**
 * Holds constants for the Red Flag Keyword model.
 */
public class RedFlagKeywordModelConstants {

    public static final String FieldName = "name";
}
