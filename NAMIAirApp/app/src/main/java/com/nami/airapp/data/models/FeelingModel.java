package com.nami.airapp.data.models;

import com.nami.airapp.data.constants.FeelingModelConstants;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

/**
 * Contains data about a Note.
 */
@ParseClassName("Feeling")
public class FeelingModel extends ParseObject {

    // Private Members.
    private boolean isSelected;

    /**
     * Gets the note name.
     * @return Note name.
     */
    public String getName() { return getString(FeelingModelConstants.FieldName); }

    /**
     * Sets the note name.
     * @param value Note name.
     */
    public void setName(String value) { put(FeelingModelConstants.FieldName, value); }

    /**
     * Gets the selected value.
     * @return True if selected, otherwise false;
     */
    public boolean getIsSelected() { return this.isSelected; }

    /**
     * Sets the selected value.
     * @param value Selected value.
     */
    public void setIsSelected(boolean value) { this.isSelected = value; }
	
	/**
     * Creates a query adapter to fetch the notes.
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<FeelingModel> createFeelingAdapterQuery() {


        return new ParseQueryAdapter.QueryFactory<FeelingModel>() {

            @Override
            public ParseQuery<FeelingModel> create() {

                // Create the query.
                ParseQuery<FeelingModel> query = new ParseQuery<FeelingModel>(
                        FeelingModel.class);

                // Set the sort.
                query.orderByAscending(FeelingModelConstants.FieldName);

                return query;
            }
        };
    }
}
