package com.nami.airapp.data.constants;

/**
 * Holds constants for the User model.
 */
public class UserModelConstants {

    public static final String FieldUserType = "userType";
    public static final String FieldPostsHidden = "postsHidden";
    public static final String FieldAuthorsBlocked = "authorsBlocked";
    public static final String FieldDisabled = "disabled";
}
