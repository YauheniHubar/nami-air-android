package com.nami.airapp.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.nami.airapp.data.models.FaqModel;

/**
 * Created by Brian on 12/30/2014.
 */
public class FaqSectionModel implements Parcelable {

    public String Question;
    public List<FaqModel> Questions;


    /**
     * Describes the contents of the parcel.
     * @return Hash code of the parcel.
     */
    @Override
    public int describeContents() {

        return super.hashCode();
    }

    /**
     * Writes the contents of the post to a parcel.
     * @param parcel Parcel to write the contents to.
     * @param flags Parcel flags.
     */
    @Override
    public void writeToParcel(Parcel parcel, int flags) {

        // Write the post contents.
        parcel.writeString(Question);
        parcel.writeList(Questions);
    }

    /**
     * Creates the parcel.
     */
    public static Creator<FaqSectionModel> CREATOR = new Creator<FaqSectionModel>() {

        @Override
        public FaqSectionModel createFromParcel(Parcel parcel) {

            // Create the post.
            FaqSectionModel model = new FaqSectionModel();

            // Get the post contents.
            model.Question = parcel.readString();
            parcel.readList(model.Questions, null);
            return model;
        }


        @Override
        public FaqSectionModel[] newArray(int size) {
            return new FaqSectionModel[size];
        }
    };

}
