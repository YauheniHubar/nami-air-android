package com.nami.airapp.views;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.nami.airapp.R;
import com.nami.airapp.data.enums.ApprovedStatus;
import com.nami.airapp.data.models.PostFlagModel;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.utils.DateFormatter;
import com.nami.airapp.utils.PostManager;

/**
 * Class containing logic for the view in the Post Feed.
 */
public class MyPostFeedItemView extends LinearLayout {

	// Private Members.
	private PostFeedItemViewContainer viewContainer = null;
    private Context context;

    /**
     * Default Constructor.
     * @param context Context which the view runs in.
     */
	public MyPostFeedItemView(Context context) {
		super(context);

		initialize(context);
	}

    /**
     * Default Constructor.
     * @param context Context which the view runs in.
     */
    public MyPostFeedItemView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initialize(context);
    }

    /**
     * Initializes the view.
     * @param context
     */
    public void initialize(Context context) {

        // Create the view.
        LayoutInflater.from(context).inflate(R.layout.view_my_post_feed_item, this);

        // Set the context.
        this.context = context;

        // Create the view container.
        this.viewContainer = PostFeedItemViewContainer.with(this);
    }

    /**
     * Updates the model.
     * @param fragment The current fragment.
     * @param model The model to update.
     * @param position The position of the model in the dataset.
     */
	public void updateFromModel(Fragment fragment, PostModel model, int position) {
		
		// Update the post.
		this.updatePostText(fragment, model, position);
	}

    /**
     * Updates the post text.
     * @param fragment The current fragment.
     * @param model The model to get the post text from.
     * @param position
     */
	private void updatePostText(Fragment fragment, PostModel model, int position) {

        // Create the text.
        SpannableString text = new SpannableString(model.getPostText());

        // Set the post styles.
        PostManager.setPostStyle(context, text);
        PostManager.setPostFirstSentenceStyle(context, text);
        PostManager.setPostHashTagsStyle(context, fragment, text);
        PostManager.setPostFeelingsStyle(context, text);
        PostManager.setPostPaddingCorners(context, this.viewContainer.layoutPostFeedLayout, this.viewContainer.layoutPostFeedItemContainer, position);
        this.setPostDate(model);

        // Set the counts.
        this.viewContainer.textViewPostLikeCount.setText(String.valueOf(model.getLikeCount()));
        this.viewContainer.textViewPostHugCount.setText(String.valueOf(model.getHugCount()));
        this.viewContainer.textViewPostMeTooCount.setText(String.valueOf(model.getMeTooCount()));
        this.viewContainer.textViewPostNoteCount.setText(String.valueOf(model.getNoteCount()));
        this.viewContainer.textViewPostInspiredByCount.setText(String.valueOf(model.getMeTooStoryCount()));

        // Set the post text.
        this.viewContainer.textViewPostText.setLineSpacing(4,1);
        this.viewContainer.textViewPostText.setText(text, BufferType.SPANNABLE);

        // Show / hide the not approved message.
        if (model.getApprovedStatus() == ApprovedStatus.APPROVED) {

            this.viewContainer.textViewPostNotApproved.setVisibility(View.GONE);
            this.viewContainer.textViewPostDisapproved.setVisibility(View.GONE);
            this.viewContainer.layoutPostFeedItemSpacer.setVisibility(View.VISIBLE);
        } else {

            if (model.getApprovedStatus() == ApprovedStatus.NOTAPPROVED) {
                this.viewContainer.textViewPostNotApproved.setVisibility(View.VISIBLE);
                this.viewContainer.textViewPostDisapproved.setVisibility(View.GONE);
            } else {
                this.viewContainer.textViewPostNotApproved.setVisibility(View.GONE);
                this.viewContainer.textViewPostDisapproved.setVisibility(View.VISIBLE);
            }

            this.viewContainer.layoutPostFeedItemSpacer.setVisibility(View.GONE);
        }

        // Show / hide the flagged message.
        // Set the flagged status.
        PostFlagModel flaggedPost = ApplicationManager.getInstance().findPostFlaggedStatus(model);
        if (flaggedPost != null) {

            // Create the flagged text.
            String flaggedText = "";
            switch (flaggedPost.getFlaggedStatus()) {
                case INAPPROPRIATE:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_inappropriate);
                    break;
                case SELF_HARM:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_self_harm);
                    break;
                case THREATENING:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_threatening);
                    break;
                case BULLYING:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_bullying);
                    break;
                case SPAM:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_spam);
                    break;
            }

            // Create the flagged text sentence.
            String flaggedTextSentence = String.format(this.context.getString(R.string.post_detail_flag_text), flaggedText);

            // Set the flagged text.
            this.viewContainer.textViewPostFlaggedText.setText(flaggedTextSentence);

            // Show the flagged layout.
            this.viewContainer.layoutPostFeedItemFlagged.setVisibility(View.VISIBLE);
            this.viewContainer.layoutPostFeedItemSpacer.setVisibility(View.GONE);

        } else {

            // Hide the flagged layout.
            this.viewContainer.layoutPostFeedItemFlagged.setVisibility(View.GONE);
            this.viewContainer.layoutPostFeedItemSpacer.setVisibility(View.VISIBLE);
        }
	}

    /**
     * Sets the post date.
     * @param model Post model.
     */
    private void setPostDate(PostModel model) {

        // Set the date.
        String date = DateFormatter.FormatPostDate(model.getPostDate(), this.context);
        this.viewContainer.textViewPostDate.setText(date);
    }

    /**
     * Holds the view for the Post Feed Item.
     */
	private static class PostFeedItemViewContainer {

		// Private Members.
        private LinearLayout layoutPostFeedLayout = null;
        private LinearLayout layoutPostFeedItemContainer = null;
        private LinearLayout layoutPostFeedItemFlagged = null;
        private LinearLayout layoutPostFeedItemSpacer = null;
        private LinearLayout textViewPostNotApproved = null;
        private LinearLayout textViewPostDisapproved = null;
        private TextView textViewPostText = null;
        private TextView textViewPostLikeCount = null;
        private TextView textViewPostHugCount = null;
        private TextView textViewPostMeTooCount = null;
        private TextView textViewPostNoteCount = null;
        private TextView textViewPostInspiredByCount = null;
        private TextView textViewPostDate = null;
        private TextView textViewPostFlaggedText = null;

		// Private Constructor.
		private PostFeedItemViewContainer() {}

		// Static Constructor.
		public static PostFeedItemViewContainer with(View view) {

			// Create the instance
			PostFeedItemViewContainer instance = new PostFeedItemViewContainer();

            // Get the items.
            instance.layoutPostFeedLayout = (LinearLayout)view.findViewById(R.id.post_feed_item_layout);
            instance.layoutPostFeedItemContainer = (LinearLayout)view.findViewById(R.id.post_feed_item_container);
            instance.layoutPostFeedItemFlagged = (LinearLayout)view.findViewById(R.id.post_feed_item_flagged);
            instance.layoutPostFeedItemSpacer = (LinearLayout)view.findViewById(R.id.post_feed_item_spacer);
            instance.textViewPostNotApproved = (LinearLayout)view.findViewById(R.id.post_feed_item_not_approved);
            instance.textViewPostDisapproved = (LinearLayout)view.findViewById(R.id.post_feed_item_disapproved);
            instance.textViewPostText = (TextView)view.findViewById(R.id.post_feed_item_post_text);
            instance.textViewPostLikeCount = (TextView)view.findViewById(R.id.post_feed_item_like_count);
            instance.textViewPostHugCount = (TextView)view.findViewById(R.id.post_feed_item_hug_count);
            instance.textViewPostMeTooCount = (TextView)view.findViewById(R.id.post_feed_item_metoo_count);
            instance.textViewPostNoteCount = (TextView)view.findViewById(R.id.post_feed_item_note_count);
            instance.textViewPostInspiredByCount = (TextView)view.findViewById(R.id.post_feed_item_inspired_by_count);
            instance.textViewPostDate = (TextView)view.findViewById(R.id.post_feed_item_date);
            instance.textViewPostFlaggedText = (TextView)view.findViewById(R.id.post_feed_item_flagged_text);

			return instance;
		}
	}
	
}
