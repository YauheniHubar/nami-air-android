package com.nami.airapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.nami.airapp.R;
import com.nami.airapp.activities.GuidedTourActivity;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.utils.AppFragmentManager;
import com.nami.airapp.utils.DisplayManager;
import com.nami.airapp.views.PostFeedItemView;

/**
 * Post Detail Fragment.
 */
public class GuidedTourFragment extends Fragment {

    // Private Static Members.
    private static final String KEY_FRAGMENT_GUIDED_PAGE = "KEY_FRAGMENT_GUIDED_PAGE";

    // Private Members
    private Context context = null;
    private int page = 0;

    /**
     * Default Constructor.
     */
    public GuidedTourFragment() {}

    /**
     * Creates a new instance of a PostDetailFragment.
     * @return New instance of a PostDetailFragment.
     */
    public static GuidedTourFragment newInstance(int page) {

        // Create the new instance.
        GuidedTourFragment instance = new GuidedTourFragment();

        // Add the post model to the bundle.
        Bundle args = new Bundle();
        args.putInt(KEY_FRAGMENT_GUIDED_PAGE, page + 1);
        instance.setArguments(args);

        return instance;
    }

    /**
     *
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            this.page = args.getInt(KEY_FRAGMENT_GUIDED_PAGE);
        }
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Declare the view.
        View view = null;

        switch (page) {

            case 1: {

                // Create the view.
                view = inflater.inflate(R.layout.fragment_guided_tour_1, container, false);

                // Get the skip button.
                Button buttonSkip = (Button)view.findViewById(R.id.guided_tour_skip);

                // Add the click handler.
                buttonSkip.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // Get the activity and close the guided tour.
                        ((GuidedTourActivity)context).closeGuidedTour();
                    }
                });


                break;
            }
            case 2: { view = inflater.inflate(R.layout.fragment_guided_tour_2, container, false); break; }
            case 3: { view = inflater.inflate(R.layout.fragment_guided_tour_3, container, false); break; }
            case 4: {

                // Create the view.
                view = inflater.inflate(R.layout.fragment_guided_tour_4, container, false);

                // Calculate the top offset.
                int listOffset = (int) DisplayManager.convertDpToPixel(40 + 50, this.context);

                // Get the last visible view item.
                int lastVisibleViewItem = ((GuidedTourActivity) this.context).getLastVisibleViewItem();

                // Get the item view and convert to offset.
                View listItemView = ((GuidedTourActivity) this.context).getViewByPosition(lastVisibleViewItem);
                int listItemViewOffset = listItemView.getTop(); //(int)DisplayManager.convertPixelsToDp(listItemView.getTop(), this.context);
                int listItemViewHeight = listItemView.getHeight(); //int)DisplayManager.convertPixelsToDp(listItemView.getHeight(), this.context);

                // Get the layouts.
                RelativeLayout topLayout = (RelativeLayout) view.findViewById(R.id.guided_layout_top);
                RelativeLayout bottomLayout = (RelativeLayout) view.findViewById(R.id.guided_layout_bottom);

                // Calculate the top layout height.
                RelativeLayout.LayoutParams topLayoutParams = new RelativeLayout.LayoutParams(listItemView.getWidth(), listOffset + listItemViewOffset);
                topLayout.setLayoutParams(topLayoutParams);

                // Calculate the border size.
                int borderSize = (int) DisplayManager.convertDpToPixel(10, this.context) - 1;

                // Calculate the bottom layout margin.
                RelativeLayout.LayoutParams bottomLayoutParams = (RelativeLayout.LayoutParams) bottomLayout.getLayoutParams();
                bottomLayoutParams.setMargins(0, listOffset + listItemViewOffset + listItemViewHeight + borderSize, 0, 0);
                bottomLayout.setLayoutParams(bottomLayoutParams);

                break;
            }
            case 5: { view = inflater.inflate(R.layout.fragment_guided_tour_5, container, false); break; }
            case 6: { view = inflater.inflate(R.layout.fragment_guided_tour_6, container, false); break; }
        }

        // Inflate the layout for this fragment
        return view;
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, context);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
