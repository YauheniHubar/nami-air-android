package com.nami.airapp.activities;


import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.content.pm.ActivityInfo;

import com.nami.airapp.data.constants.UserModelConstants;
import com.parse.CountCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import com.nami.airapp.R;
import com.nami.airapp.data.models.PostNotificationModel;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.fragments.GetHelpFragment;
import com.nami.airapp.fragments.MyPostFeedFragment;
import com.nami.airapp.fragments.PostComposeFragment;
import com.nami.airapp.fragments.PostFeedFragment;
import com.nami.airapp.fragments.PostNotificationFeedFragment;
import com.nami.airapp.providers.CachedFileProvider;
import com.nami.airapp.utils.AirAppPushBroadcastReceiver;
import com.nami.airapp.AirAppApplication;
import com.parse.ParseUser;
import com.parse.RefreshCallback;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;

import android.net.Uri;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import com.flurry.android.FlurryAgent;


/**
 * Main Activity.
 */
public class MainActivity extends ActionBarActivity implements TabListener {


    // Private Members.
    private PostFeedFragment fragmentPostFeed = null;
    private MyPostFeedFragment fragmentMyPostFeed = null;
    private PostNotificationFeedFragment fragmentPostNotificationFeed = null;
    private PostComposeFragment fragmentPostCompose =  null;
    private GetHelpFragment fragmentGetHelp = null;

    @Override
    public void onStart() {
        super.onStart();

        FlurryAgent.setCaptureUncaughtExceptions(true);
        FlurryAgent.onStartSession(this, AirAppApplication.getFlurryApplicationKey());
    }

    @Override
    public void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(this);
    }


        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the requested orientation.
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Initialize the activity and action bar.
        this.initializeActivity();
        this.initializeActionBar();

        // Set the content view.
        setContentView(R.layout.activity_main);

        // Initialize the layout content.
        this.initializeLayoutContent();

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this) == false) {
            return;
        }

        // Fetch unread messages.
        this.fetchUnreadMessages();
        this.fetchUnreadModeratorMessages();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this) == false) {
            return;
        }

        // Refresh the current user.
        final MainActivity that = this;
        ParseUser.getCurrentUser().refreshInBackground(new RefreshCallback() {

            @Override
            public void done(ParseObject parseObject, ParseException e) {

                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(that);
                }

                // Check for a disabled user.
                if (ParseUser.getCurrentUser().getBoolean(UserModelConstants.FieldDisabled) == true) {

                    // Log out.
                    handleLogout();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        // Get the position.
        int position = tab.getPosition();

        // Set the view.
        tab.setCustomView(null);
        tab.setCustomView(this.getSelectedViewForTab(position));

        // Get the fragment manager.
        FragmentManager fragmentManager = getSupportFragmentManager();

        // Update the stack and replace the fragment.
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentTransaction.replace(R.id.main_container, getFragmentForTab(position), getFragmentTagForTab(position));

        // Set the notification badge count.
        int badgeCount = ApplicationManager.getInstance().getNotificationBadgeCount();
        this.setNotificationBadgeCount(badgeCount);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        // Get the position.
        int position = tab.getPosition();

        // Set the view.
        tab.setCustomView(null);
        tab.setCustomView(this.getViewForTab(position));
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) { }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(new CalligraphyContextWrapper(newBase));
    }

    /**
     * Sets the action bar to have tabs.
     * @param position The tab to select
     */
    public void setActionBarTabs(int position) {

        // Get the action bar and set to tabs mode.
        ActionBar actionBar = getSupportActionBar();

        // Check the navigation mode.
        if (actionBar.getNavigationMode() == ActionBar.NAVIGATION_MODE_TABS) {

            // Set the notification badge count.
            int badgeCount = ApplicationManager.getInstance().getNotificationBadgeCount();
            this.setNotificationBadgeCount(badgeCount);
            return;
        }


        // Set the navigation mode.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);


        // Add the tabs.
        if (actionBar.getTabCount() == 0) {

            for (int index = 0; index < 5; index++) {

                // Create and add the tab.
                Tab tab = actionBar.newTab();
                tab.setCustomView(this.getViewForTab(index));
                tab.setTabListener(this);
                actionBar.addTab(tab);
            }
        }

        // Set the selected tab.
        actionBar.setSelectedNavigationItem(position);

        // Set the notification badge count.
        int badgeCount = ApplicationManager.getInstance().getNotificationBadgeCount();
        this.setNotificationBadgeCount(badgeCount);
    }

    /**
     * Sets the action bar to not have tabs.
     */
    public void setActionBarNoTabs() {

        // Get the action bar and set to tabs mode.
        ActionBar actionBar = getSupportActionBar();

        // Check the navigation mode.
        if (actionBar.getNavigationMode() == ActionBar.NAVIGATION_MODE_STANDARD)
            return;

        // Set the navigation mode.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    /**
     * Sets the notification badge count.
     * @param count
     */
    public void setNotificationBadgeCount(int count) {

        // Get the action bar.
        ActionBar actionBar = getSupportActionBar();

        // Return if the tab does not exist yet.
        if (actionBar.getTabCount() < PostNotificationFeedFragment.TAB)
            return;

        // Get the view for the notifications tab.
        Tab notificationsTab = actionBar.getTabAt(PostNotificationFeedFragment.TAB);
        View tabView = notificationsTab.getCustomView();
        TextView badgeView = (TextView)tabView.findViewById(R.id.tab_badge);
        if (count <= 0) {
            badgeView.setVisibility(View.GONE);
        } else {
            badgeView.setText(Integer.toString(count));
            badgeView.setVisibility(View.VISIBLE);
        }
    }

    /**
     Initializes the activity.
     */
    private void initializeActivity() {
    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar to have tabs.
        this.setActionBarTabs(PostFeedFragment.TAB);
    }

    /**
     * Initializes the layout content.
     */
    private void initializeLayoutContent() {

        // Test for popup data.
        ApplicationManager applicationManager = ApplicationManager.getInstance();
        if (applicationManager.hasPopupData()) {

            final HashMap<String, String> popupData = applicationManager.getPopupData();


            // Create the dialog
            final Dialog popupDialog = new Dialog(this);
            popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            popupDialog.setContentView(R.layout.dialog_popup);

            // Get the views.
            TextView titleTextView = (TextView)popupDialog.findViewById(R.id.dialog_popup_title);
            Button linkButton = (Button)popupDialog.findViewById(R.id.dialog_popup_link);
            Button cancelDialog  = (Button)popupDialog.findViewById(R.id.dialog_popup_close);
            View separator = (View)popupDialog.findViewById(R.id.dialog_popup_top_separator);

            /*
            String linkTitle = popupData.get("linkTitle");
            SpannableString linkTitleContent = new SpannableString(linkTitle);
            linkTitleContent.setSpan(new UnderlineSpan(), 0, linkTitle.length(), 0);
            */

            titleTextView.setText(popupData.get("popupTitle"));

            if (popupData.get("linkTitle") == null || popupData.get("linkTitle").length() == 0) {
                linkButton.setVisibility(View.INVISIBLE);
                separator.setVisibility(View.INVISIBLE);
            } else {
                linkButton.setText(popupData.get("linkTitle"));
                linkButton.setVisibility(View.VISIBLE);
                separator.setVisibility(View.VISIBLE);
            }

            // Add the listeners.
            cancelDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // Dismiss the dialog.
                    popupDialog.dismiss();
                }
            });
            linkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    popupDialog.dismiss();

                    Uri uri = Uri.parse(popupData.get("linkUrl"));
                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                }
            });

            // Show the dialog.
            popupDialog.show();
            applicationManager.clearPopupData();

        }

    }

    /**
     * Fetches unread moderator messages.
     */
    private void fetchUnreadModeratorMessages() {

        // Save the context.
        final Context context = this;

        // Get the intent and check if there is a message id.
        Intent mainActivityIntent = getIntent();
        String moderatorMessageId = AirAppPushBroadcastReceiver.ExtractModeratorMessageId(mainActivityIntent);
        if (TextUtils.isEmpty(moderatorMessageId)) {

            // Create the query.
            ParseQuery<PostNotificationModel> query = PostNotificationModel.createPostNotificationUnreadModeratorMessageQuery();

            // Run the query.
            query.getFirstInBackground(new GetCallback<PostNotificationModel>() {

                   @Override
                   public void done(PostNotificationModel postNotificationModel, ParseException e) {

                       // Check for an exception.
                       if (e != null && e.getCode() != ParseException.OBJECT_NOT_FOUND) {
                           ApplicationManager.getInstance().showConnectionError(context);
                           return;
                       }

                       // Check for a model.
                       if (postNotificationModel != null) {

                           // Get the message id.
                           String moderatorMessageId = postNotificationModel.getObjectId();

                           // Show the moderator message dialog.
                           showUnreadModeratorMessageDialog(moderatorMessageId);
                       }
                   }
               }

            );


        } else {

            // Show the moderator message dialog.
            this.showUnreadModeratorMessageDialog(moderatorMessageId);
        }
    }

    /**
     * Fetches unread messages.
     */
    private void fetchUnreadMessages() {

        // Save the context.
        final Context context = this;

        // Create the query.
        ParseQuery<PostNotificationModel> query = PostNotificationModel.createPostNotificationUnreadMessageQuery();

        // Get the count.
        final MainActivity that = this;
        query.countInBackground(new CountCallback() {

            @Override
            public void done(int i, ParseException e) {

                // Check for an exception.
                if (e != null && e.getCode() != ParseException.OBJECT_NOT_FOUND) {
                    ApplicationManager.getInstance().showConnectionError(that);
                    return;
                }

                // Set the badge count.
                ApplicationManager.getInstance().setNotificationBadgeCount(that, i);
            }
        });
    }

    /**
     * Handles the logout operation.
     */
    public void handleLogout() {

        // Logout the current Parse User.
        ParseUser.logOut();

        // Declare the intent.
        Intent intent = new Intent(this, WelcomeActivity.class);

        // Start the intent.
        startActivity(intent);
        finish();
    }


    /**
     * Shows the moderator message dialog.
     * @param messageId The message id.
     */
    protected void showUnreadModeratorMessageDialog(final String messageId) {

        // Create the dialog.
        final Dialog moderatorMessageDialog = new Dialog(this);
        moderatorMessageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        moderatorMessageDialog.setContentView(R.layout.dialog_moderator_message);

        // Get the views.
        Button moderatorMessageDialogIgnoreButton = (Button)moderatorMessageDialog.findViewById(R.id.dialog_moderator_message_ignore);
        Button moderatorMessageDialogOpenButton = (Button)moderatorMessageDialog.findViewById(R.id.dialog_moderator_message_open);

        // Add the listeners.
        moderatorMessageDialogIgnoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Dismiss.
                moderatorMessageDialog.dismiss();
            }
        });
        moderatorMessageDialogOpenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Set the moderator message id.
                ApplicationManager.getInstance().setModeratorMessageId(messageId);

                // Dismiss.
                moderatorMessageDialog.dismiss();

                // Get the action bar and select the posts tab.
                ActionBar actionBar = getSupportActionBar();
                actionBar.setSelectedNavigationItem(PostNotificationFeedFragment.TAB);
            }
        });

        // Show the dialog.
        moderatorMessageDialog.show();
    }

    /**
     * Gets the text for the tab.
     * @param position The position of the tab.
     * @return Text for the tab.
     */
    private int getTextForTab(int position) {

        // Determine the position.
        switch (position) {
            case PostFeedFragment.TAB: {
                return R.string.main_tab_text_posts;
            }
            case MyPostFeedFragment.TAB: {
                return R.string.main_tab_text_my_posts;
            }
            case PostComposeFragment.TAB: {
                return R.string.main_tab_text_compose;
            }
            case PostNotificationFeedFragment.TAB: {
                return R.string.main_tab_text_notifications;
            }
            case GetHelpFragment.TAB: {
                return R.string.main_tab_text_help;
            }
            default: {
                return -1;
            }
        }
    }


    /**
     * Gets the fragment at the tab position.
     * @param position The position of the tab.
     * @return Fragment at the tab position.
     */
    private Fragment getFragmentForTab(int position) {

        // Determine the position.
        switch (position) {
            case PostFeedFragment.TAB: {

                if (this.fragmentPostFeed == null)
                    this.fragmentPostFeed = PostFeedFragment.newInstance();

                return this.fragmentPostFeed;
            }
            case MyPostFeedFragment.TAB: {

                if (this.fragmentMyPostFeed == null)
                    this.fragmentMyPostFeed = MyPostFeedFragment.newInstance();

                return this.fragmentMyPostFeed;
            }
            case PostComposeFragment.TAB: {

                if (this.fragmentPostCompose == null)
                    this.fragmentPostCompose = PostComposeFragment.newInstance(null);

                return this.fragmentPostCompose;
            }
            case PostNotificationFeedFragment.TAB: {

                if (this.fragmentPostNotificationFeed == null)
                    this.fragmentPostNotificationFeed = PostNotificationFeedFragment.newInstance();

                return this.fragmentPostNotificationFeed;
            }
            case GetHelpFragment.TAB: {

                if (this.fragmentGetHelp == null)
                    this.fragmentGetHelp = GetHelpFragment.newInstance();

                return this.fragmentGetHelp;
            }
            default: {
                return null;
            }
        }
    }

    /**
     * Gets the fragment tag for the tab position.
     * @param position The position of the tab.
     * @return Fragment tag for the tab position.
     */
    private String getFragmentTagForTab(int position) {

        // Determine the position.
        switch (position) {
            case PostFeedFragment.TAB: {
                return PostFeedFragment.TAG;
            }
            case MyPostFeedFragment.TAB: {
                return MyPostFeedFragment.TAG;
            }
            case PostComposeFragment.TAB: {
                return PostComposeFragment.TAG;
            }
            case GetHelpFragment.TAB: {
                return GetHelpFragment.TAG;
            }
            default: {
                return null;
            }
        }
    }

    /**
     * Gets the view for a tab.
     * @param position The position of the tab.
     * @return Resource identifier of the tab.
     */
    private View getViewForTab(int position) {

        // Determine the tab.
        switch (position) {
            case PostFeedFragment.TAB: {
                return this.renderTabView(R.layout.tab_all_posts_off);
            }
            case MyPostFeedFragment.TAB: {
                return this.renderTabView(R.layout.tab_my_posts_off);
            }
            case PostComposeFragment.TAB: {
                return this.renderTabView(R.layout.tab_compose_off);
            }
            case PostNotificationFeedFragment.TAB: {
                return this.renderTabView(R.layout.tab_notifications_off);
            }
            case GetHelpFragment.TAB: {
                return this.renderTabView(R.layout.tab_help_off);
            }
            default: {
                return null;
            }
        }
    }

    /**
     * Gets the view for a tab.
     * @param position The position of the tab.
     * @return Resource identifier of the tab.
     */
    private View getSelectedViewForTab(int position) {

        // Determine the tab.
        switch (position) {
            case PostFeedFragment.TAB: {
                return this.renderTabView(R.layout.tab_all_posts_on);
            }
            case MyPostFeedFragment.TAB: {
                return this.renderTabView(R.layout.tab_my_posts_on);
            }
            case PostComposeFragment.TAB: {
                return this.renderTabView(R.layout.tab_compose_on);
            }
            case PostNotificationFeedFragment.TAB: {
                return this.renderTabView(R.layout.tab_notifications_on);
            }
            case GetHelpFragment.TAB: {
                return this.renderTabView(R.layout.tab_help_on);
            }
            default: {
                return null;
            }
        }
    }


    private View renderTabView(int resourceId) {

        // Get the view.
        View view = (View) LayoutInflater.from(this).inflate(resourceId, null);

        // Set the layout.
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        return view;
    }

    /**
     * Sends the exported data to an email.
     * @param data The exported data.
     */
    public void sendExportedDataToEmail(String data) {

        try {

            // Create the temporary file.
            File outputFile = File.createTempFile(getString(R.string.settings_export_filename), getString(R.string.settings_export_filename_extension), getCacheDir());

            // Write the text.
            FileWriter fileWriter = new FileWriter(outputFile);
            fileWriter.write(data);
            fileWriter.flush();
            fileWriter.close();

            // Create the URI.
            Uri attachmentUri = Uri.parse("content://" + CachedFileProvider.AUTHORITY + "/" + outputFile.getName());

            // Create the intent.
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("plain/text");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");
            emailIntent.putExtra(Intent.EXTRA_STREAM, attachmentUri);

            // Start the intent.
            this.startActivity(Intent.createChooser(emailIntent, "Creating Email ..."));
        }
        catch (Exception e) {

            // Show the message.
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Sends the feedback email.
     */
    public void sendFeedbackEmail() {

        // Create the intent.
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@nami.org"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");

        // Start the intent.
        this.startActivity(Intent.createChooser(emailIntent, "Creating Email ..."));
    }

}
