package com.nami.airapp.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.os.ParcelFileDescriptor;
import android.net.Uri;

import com.nami.airapp.AirAppApplication;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by Brian on 12/4/2014.
 */
public class CachedFileProvider extends ContentProvider {

    // Public Static Members
    public static final String AUTHORITY = "com.nami.airapp.email.provider";

    // Private Members.
    private UriMatcher uriMatcher = null;

    @Override
    public boolean onCreate() {

        this.uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        this.uriMatcher.addURI(AUTHORITY, "*", 1);
        return true;
    }

    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {

        switch (this.uriMatcher.match(uri)) {

            case 1:
                String fileLocation = getContext().getCacheDir() + File.separator + uri.getLastPathSegment();
                ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(new File(fileLocation), ParcelFileDescriptor.MODE_READ_ONLY);
                return parcelFileDescriptor;
            default:
                throw new FileNotFoundException("Unsupported URI : " + uri.toString());
        }
    }

    @Override public int update(Uri uri, ContentValues contentvalues, String s, String[] as) { return 0; }
    @Override public int delete(Uri uri, String s, String[] as) { return 0; }
    @Override public Uri insert(Uri uri, ContentValues contentvalues) { return null; }
    @Override public String getType(Uri uri) { return null; }
    @Override public Cursor query(Uri uri, String[] projection, String s, String[] as1, String s1) { return null; }
}
