package com.nami.airapp.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.TextAppearanceSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;

import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.enums.ApprovedStatus;
import com.nami.airapp.data.enums.FlaggedStatus;
import com.nami.airapp.data.enums.UserType;
import com.nami.airapp.data.models.FeelingModel;
import com.nami.airapp.data.models.PostNotificationModel;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.AppFragmentManager;
import com.nami.airapp.utils.KeyboardManager;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import com.nami.airapp.R;
import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.interfaces.FeelingItemsSelectedListener;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.data.models.RedFlagKeywordModel;
//import com.parse.codec.binary.StringUtils;

/**
 * Post Compose Fragment
 */
public class PostComposeFragment extends Fragment implements OnClickListener, FeelingItemsSelectedListener {

    // Private Members.
    private Context context = null;
    private static final String KEY_FRAGMENT_POST_INSPIRED_BY = "KEY_FRAGMENT_POST_INSPIRED_BY";
    private PostComposeViewContainer viewContainer = null;
    private PostModel inspiredByPostModel;
    private List<FeelingModel> selectedFeelings;
    private static final int MENU_POST = Menu.FIRST;
    private int endTextPosition = 0;
    private SpannableString updatedText = null;
    private boolean postMenuItemVisible = false;

    // Public Static Members.
    public static final int TAB = 2;
    public static final String TAG = "PostComposeFragmentTag";

    /**
     * Default Constructor.
     */
    public PostComposeFragment() {}


    /**
     * Creates a new instance of a PostComposeFragment.
     * @return New instance of a PostComposeFragment.
     */
    public static PostComposeFragment newInstance(PostModel inspiredByPost) {

        // Create and return the new instance.
        PostComposeFragment instance = new PostComposeFragment();

        // Add the inspired by post if necessary.
        if (inspiredByPost != null) {
            Bundle args = new Bundle();
            args.putParcelable(KEY_FRAGMENT_POST_INSPIRED_BY, inspiredByPost);
            instance.setArguments(args);
        }

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        // Set the action bar.
        MainActivity mainActivity = (MainActivity)context;
        mainActivity.setActionBarNoTabs();

        if (args != null && args.containsKey(KEY_FRAGMENT_POST_INSPIRED_BY)) {
            this.inspiredByPostModel = args.getParcelable(KEY_FRAGMENT_POST_INSPIRED_BY);
        }
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set options menu.
        setHasOptionsMenu(true);

        ((FragmentActivity)context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_compose, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    postCancel();
                    return true;
                }

                return false;
            }
        });

        // Initialize the layout content.
        this.initializeLayoutContent(view);
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // Create the post menu item.
        if (this.postMenuItemVisible) {
            MenuItem postMenuItem = menu.add(Menu.NONE, MENU_POST, Menu.NONE, "Post");
            MenuItemCompat.setShowAsAction(postMenuItem, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
            postMenuItem.setIcon(R.drawable.ic_menu_send);
        }
    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Check the id.
        switch (id) {

            case MENU_POST:
                this.attemptPost();
                break;
            case android.R.id.home:
                this.postCancel();
                return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();

        // Update the text.
        if (this.updatedText != null) {
            this.viewContainer.editTextPostText.setText(this.updatedText, TextView.BufferType.SPANNABLE);
            this.postMenuItemVisible = true;
            this.viewContainer.buttonFeelings.setEnabled(true);

            // Clear the updated text.
            this.updatedText = null;
        }
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (view == this.viewContainer.buttonFeelings) {
            this.showFeelingsFragment();
        }
    }

    /**
     * Called when the feelings are selected.
     * @param feelings
     */
    public void feelingItemsSelected(List<FeelingModel> feelings) {

        // Set the feelings.
        this.selectedFeelings = feelings;

        // Set the selected feelings text.
        this.setSelectedFeelingsText();
    }

    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.main_tab_text_compose));
    }

    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(final View view) {

        // Create the view container.
        this.viewContainer = PostComposeViewContainer.with(view);

        // Set the click events.
        this.viewContainer.buttonFeelings.setOnClickListener(this);
        this.viewContainer.buttonFeelings.setEnabled(false);
        this.viewContainer.editTextPostText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                // Enable / Disable controls if there is actual text.
                if (TextUtils.getTrimmedLength(editable.toString()) > 0) {

                    if (postMenuItemVisible == false) {
                        postMenuItemVisible = true;
                        ActivityCompat.invalidateOptionsMenu((Activity)context);
                    }
                    viewContainer.buttonFeelings.setEnabled(true);

                } else {

                    if (postMenuItemVisible == true) {
                        postMenuItemVisible = false;
                        ActivityCompat.invalidateOptionsMenu((Activity)context);
                    }
                    viewContainer.buttonFeelings.setEnabled(false);
                }
            }
        });
    }

    /**
     * Shows the feelings fragment.
     */
    private void showFeelingsFragment() {

        // Get the end of text position.
        this.getEndOfTextPosition();

        // Create the fragment.
        PostComposeFeelingFragment postComposeFeelingFragment = PostComposeFeelingFragment.newInstance(this);

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, postComposeFeelingFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Get the end of text position.
     */
    private void getEndOfTextPosition() {

        // Get the text.
        String text = this.viewContainer.editTextPostText.getText().toString();

        // Create the feeling text.
        String feelingText = String.format(" %s ", getString(R.string.post_compose_feeling));

        // Find the position of the feeling indicators if any.
        this.endTextPosition = text.lastIndexOf(feelingText);
        if (this.endTextPosition < 0)
            this.endTextPosition = text.length();
    }

    // Sets the selected feelings text.
    private void setSelectedFeelingsText() {

        // Get the text.
        String text = this.viewContainer.editTextPostText.getText().toString();

        // Remove any selected feelings.
        text = text.substring(0, this.endTextPosition);

        // Create the feeling text.
        String feelingText = String.format(" %s ", getString(R.string.post_compose_feeling));
        for (int index = 0; index < this.selectedFeelings.size(); index++) {
            feelingText += this.selectedFeelings.get(index).getName();
            if (index < (this.selectedFeelings.size() - 1))
                feelingText += ", ";
        }

        // Append the feeling text.
        text += feelingText;

        // Create the spannable text.
        this.updatedText = new SpannableString(text);

        // Create the span.s
        this.updatedText.setSpan(new TextAppearanceSpan(context, R.style.post_compose_edit_text_feelings_text), this.endTextPosition, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    /**
     * Cancels the post.
     */
    private void postCancel() {

        // Hide the keyboard.
        KeyboardManager.HideActivityKeyboard(context);

        // Determine if there is already text.
        if (this.viewContainer.editTextPostText.getText().toString().length() == 0) {

            // Determine if this is an inspired post.
            if (this.inspiredByPostModel != null) {

                // Pop back to the post detail.
                getFragmentManager().popBackStack();

                return;
            } else {

                // Select the posts tab.
                MainActivity mainActivity = (MainActivity)context;
                mainActivity.setActionBarTabs(PostFeedFragment.TAB);

                return;
            }

        }

        // Create the dialog
        final Dialog cancelPostDialog = new Dialog(context);
        cancelPostDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cancelPostDialog.setContentView(R.layout.dialog_post_cancel);

        // Get the fragment.
        final Fragment that = this;

        // Get the views.
        Button cancelPostDialogGoBack = (Button)cancelPostDialog.findViewById(R.id.dialog_post_cancel_go_back);
        Button cancelPostDialogCancel = (Button)cancelPostDialog.findViewById(R.id.dialog_post_cancel_cancel);

        // Add the listeners.
        cancelPostDialogGoBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                // Dismiss the dialog.
                cancelPostDialog.dismiss();
            }
        });
        cancelPostDialogCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                // Reset the post state.
                resetPostState();

                // Dismiss the dialog.
                cancelPostDialog.dismiss();

                // Determine if this is an inspired post.
                if (inspiredByPostModel != null) {

                    // Pop back to the post detail.
                    AppFragmentManager.RemoveFromStack(that, context);

                } else {

                    // Select the posts tab.
                    MainActivity mainActivity = (MainActivity)context;
                    mainActivity.setActionBarTabs(PostFeedFragment.TAB);

                    return;
                }
            }
        });

        // Show the dialog.
        cancelPostDialog.show();
    }

    /**
     * Attempts to post the post.
     */
    private void attemptPost() {

        // Validate the post.
        Boolean validated = true;
        List<RedFlagKeywordModel> redFlagKeywordModels = ApplicationManager.getInstance().getDataSetRedFlagKeywords();
        String postText = this.viewContainer.editTextPostText.getText().toString().toLowerCase();
        ArrayList<String> flaggedWords = new ArrayList<String>();
        BreakIterator breakIterator = BreakIterator.getWordInstance();
        breakIterator.setText(postText);
        ArrayList<String> postWords = new ArrayList<String>();
        int lastIndex = breakIterator.first();
        while (BreakIterator.DONE != lastIndex) {
            int firstIndex = lastIndex;
            lastIndex = breakIterator.next();
            if (lastIndex != BreakIterator.DONE && Character.isLetterOrDigit(postText.charAt(firstIndex))) {
                postWords.add(postText.substring(firstIndex, lastIndex));
            }
        }
        for (String postWord : postWords) {
            for (RedFlagKeywordModel keyword : redFlagKeywordModels) {
                if (TextUtils.equals(postWord.toLowerCase(), keyword.getName().toLowerCase())) {
                    flaggedWords.add(keyword.getName());
                    validated = false;
                }
            }
        }


        // Submit the post if validated.
        if (validated) {
            this.submitPost(true);
            return;
        }


        // Create the dialog.
        final Dialog postPendingDialog = new Dialog(context);
        postPendingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        postPendingDialog.setContentView(R.layout.dialog_post_pending);

        // Get the views.
        Button postPendingDialogPostAnyway = (Button)postPendingDialog.findViewById(R.id.dialog_post_pending_post_anyway);
        Button postPendingDialogEditPost = (Button)postPendingDialog.findViewById(R.id.dialog_post_pending_post_edit_post);
        TextView postPendingDialogMessage = (TextView)postPendingDialog.findViewById(R.id.dialog_post_pending_message);

        // Create the message.
        String message = getString(R.string.dialog_post_pending_message2);
        if (flaggedWords.size() == 1) {
            postPendingDialogMessage.setText(String.format(message, flaggedWords.get(0)));
        } else if (flaggedWords.size() == 2) {
            String pendingMessage = String.format("%s and %s", flaggedWords.get(0), flaggedWords.get(1));
            postPendingDialogMessage.setText(String.format(message, pendingMessage));
        } else {
            String lastKeyword = flaggedWords.get(flaggedWords.size() - 1);
            lastKeyword = String.format("and %s", lastKeyword);
            flaggedWords.set(flaggedWords.size() - 1, lastKeyword);
            String pendingMessage = TextUtils.join(", ", flaggedWords);
            postPendingDialogMessage.setText(String.format(message, pendingMessage));
        }

        // Add the listeners.
        postPendingDialogPostAnyway.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                // Dismiss the dialog.
                postPendingDialog.dismiss();

                // Submit the post.
                submitPost(false);
            }
        });
        postPendingDialogEditPost.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                // Dismiss the dialog.
                postPendingDialog.dismiss();
            }
        });

        // Show the dialog.
        postPendingDialog.show();
    }

    /**
     * Submits the post.
     * @param approved Whether the post should set to approved.
     */
    private void submitPost(boolean approved) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Return if there is no text.
        if (TextUtils.getTrimmedLength(this.viewContainer.editTextPostText.getText().toString()) == 0)
            return;

        // Disable post and show progress.
        this.postMenuItemVisible = false;
        this.viewContainer.layoutProgress.setVisibility(View.VISIBLE);
        ActivityCompat.invalidateOptionsMenu((Activity)this.context);

        // Create the PostModel.
        final PostModel postModel = new PostModel();
        postModel.setUser(ParseUser.getCurrentUser());
        postModel.setPostText(this.viewContainer.editTextPostText.getText().toString());
        postModel.setUserType(UserType.fromValue(ParseUser.getCurrentUser().getInt(UserModelConstants.FieldUserType)));
        postModel.setApprovedStatus(approved ? ApprovedStatus.APPROVED : ApprovedStatus.NOTAPPROVED);
        postModel.setFlaggedStatus(FlaggedStatus.NOT_FLAGGED);
        postModel.setIsPrivate(false);

        // Set the inspired by post if necessary.
        if (this.inspiredByPostModel != null) {
            postModel.setInspiredByPost(this.inspiredByPostModel);
        }

        // Save the Post
        final Fragment that = this;
        postModel.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                // Check for an exception.
                if (e != null) {

                    ApplicationManager.getInstance().showConnectionError(context);
                } else {

                    // Create the notification if necessary.
                    if (postModel.getApprovedStatus() == ApprovedStatus.NOTAPPROVED) {
                        PostNotificationModel.CreatePendingNotification(context, postModel);
                    }

                    // Update the post inspired count.
                    if (postModel.getInspiredByPost() != null) {
                        PostModel.UpdatePostInspiredCount(context, postModel.getInspiredByPost());
                    }

                    // Add the inspired notification.
                    if (postModel.getInspiredByPost() != null) {
                        PostNotificationModel.CreateInspiredNotification(context, postModel.getInspiredByPost());
                    }

                    // Reset the post state.
                    resetPostState();

                    // Pop back if inspired.
                    if (postModel.getInspiredByPost() != null) {

                        // Create the transaction.
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();

                        ApplicationManager.getInstance().addUpdatedPost(postModel.getInspiredByPost().getObjectId());
                        AppFragmentManager.RemoveFromStack(that, context);

                        // Create the fragment.
                        InspiredPostFeedFragment postFeedFragment = InspiredPostFeedFragment.newInstance(postModel.getInspiredByPost());

                        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                        transaction.replace(R.id.main_container, postFeedFragment);
                        transaction.addToBackStack(null);

                        // Commit the transaction.
                        transaction.commit();

                    } else {

                        // Select the posts tab.
                        MainActivity mainActivity = (MainActivity)context;
                        mainActivity.setActionBarTabs(PostFeedFragment.TAB);
                    }
                }

                // Enable post and hide progress.
                viewContainer.layoutProgress.setVisibility(View.GONE);
            }
        });
    }

    /**
     * Resets the post state.
     */
    private void resetPostState() {

        // Hide the keyboard.
        KeyboardManager.HideActivityKeyboard(context);

        // Clear the post text.
        this.viewContainer.editTextPostText.setText("");

        // Remove the post button.
        this.postMenuItemVisible = false;
        ActivityCompat.invalidateOptionsMenu((Activity)this.context);

    }


    /**
     * Holds the view for the Post Detail Item.
     */
    private static class PostComposeViewContainer {

        // Private Members.
        private EditText editTextPostText;
        private ImageButton buttonFeelings;
        private LinearLayout layoutProgress;

        // Private Constructor.
        private PostComposeViewContainer() {}

        // Static Constructor.
        public static PostComposeViewContainer with(View view) {

            // Create the instance
            PostComposeViewContainer instance = new PostComposeViewContainer();

            // Get the items.
            instance.editTextPostText = (EditText)view.findViewById(R.id.post_compose_edit_text_post_text);
            instance.buttonFeelings = (ImageButton)view.findViewById(R.id.post_compose_button_feelings);
            instance.layoutProgress = (LinearLayout)view.findViewById(R.id.progress);

            return instance;
        }
    }
}
