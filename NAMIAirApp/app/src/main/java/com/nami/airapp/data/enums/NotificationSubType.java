package com.nami.airapp.data.enums;

/**
 * Notification Sub Type enum
 */
public enum NotificationSubType {

    // Values
    LIKE (1),
    HUG (2),
	METOO (3);

    // Private Members.
    private final int id;

    /**
     * Constructor.
     * @param id Notification Type value.
     */
    NotificationSubType(int id) {

        // Set the members.
        this.id = id;
    }

    /**
     * Gets the Notification Type value.
     * @return Notification Type value.
     */
    public int getValue() {

        return this.id;
    }

    /**
     * Creates a NotificationSubType enum from a value.
     * @param id Notification Type value.
     * @return NotificationSubType enum.
     */
    public static NotificationSubType fromValue(int id) {

        // Determine the id.
        switch (id) {

            case 1 : { return LIKE; }
            case 2 : { return HUG; }
			case 3 : { return METOO; }
        };

        return null;
    }
}
