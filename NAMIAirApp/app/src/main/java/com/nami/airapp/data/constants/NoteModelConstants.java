package com.nami.airapp.data.constants;

/**
 * Holds constants for the Note model.
 */
public class NoteModelConstants {

    public static final String FieldName = "name";
}
