package com.nami.airapp.views;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.LinearLayout;

import com.nami.airapp.R;
import com.nami.airapp.data.models.FaqModel;

public class FaqListItemChildView extends LinearLayout {

    // Private Members.
    private FaqListItemChildViewContainer viewContainer = null;
    private Context context;

    /**
     * Default Constructor.
     * @param context Context which the view runs in.
     */
    public FaqListItemChildView(Context context) {
        super(context);

        // Create the view.
        LayoutInflater.from(context).inflate(R.layout.view_faq_list_item_child, this);

        // Set the context.
        this.context = context;

        // Create the view container.
        this.viewContainer = FaqListItemChildViewContainer.with(this);
    }

    /**
     * Updates the model.
     * @param model The model to update.
     * @param position The position of the model in the dataset.
     */
    public void updateFromModel(FaqModel model, int position) {

        // Set the detail.
        this.viewContainer.textViewDetail.setText(model.Answer);
    }


    /**
     * Holds the view for the FAQ List Item Child View
     */
    private static class FaqListItemChildViewContainer {

        // Private Members.
        private TextView textViewDetail;

        // Private Constructor.
        private FaqListItemChildViewContainer() {}

        // Static Constructor.
        public static FaqListItemChildViewContainer with(View view) {

            // Create the instance
            FaqListItemChildViewContainer instance = new FaqListItemChildViewContainer();

            // Get the items.
            instance.textViewDetail = (TextView)view.findViewById(R.id.faq_detail);

            return instance;
        }
    }

}
