package com.nami.airapp.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nami.airapp.R;
import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.enums.UserType;
import com.nami.airapp.data.models.PostNotificationModel;
import com.nami.airapp.data.models.UserModel;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.AppFragmentManager;
import com.nami.airapp.utils.KeyboardManager;
import com.nami.airapp.utils.ModelValidator;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * Post Detail Fragment.
 */
public class DeleteAccountFragment extends Fragment implements OnClickListener {


    // Private Members.
    private Context context = null;
    private DeleteAccountViewContainer viewContainer = null;

    /**
     * Default Constructor.
     */
    public DeleteAccountFragment() {}

    /**
     * Creates a new instance of a PostDetailFragment.
     * @return New instance of a PostDetailFragment.
     */
    public static DeleteAccountFragment newInstance() {

        // Create the new instance.
        DeleteAccountFragment instance = new DeleteAccountFragment();

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set the action bar.
        MainActivity mainActivity = (MainActivity)context;
        mainActivity.setActionBarNoTabs();

        // Set options menu.
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_delete_account, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        /*
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    AppFragmentManager.RemoveFromStack(that, context);
                    return true;
                }

                return false;
            }
        });
        */

        // Hide the keyboard.
        KeyboardManager.HideActivityKeyboard(context);
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, context);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (view == this.viewContainer.buttonDelete) {
            this.attemptUserDelete();
        }
    }

    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.delete_account_title));
    }


    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = DeleteAccountViewContainer.with(view);

        // Set the click events.
        this.viewContainer.buttonDelete.setOnClickListener(this);

        // Create the spans.
        SpannableString styledText = new SpannableString(this.viewContainer.textViewMessage.getText());
        TextAppearanceSpan styledSpan = new TextAppearanceSpan(context, R.style.delete_account_text_view_message);
        TextAppearanceSpan styledBoldSpan = new TextAppearanceSpan(context, R.style.delete_account_text_view_message_bold);
        int boldPosition = TextUtils.indexOf(this.viewContainer.textViewMessage.getText().toString(), getString(R.string.delete_account_text_view_message_bold));

        // Set the spans.
        styledText.setSpan(styledSpan, 0, styledText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        styledText.setSpan(styledBoldSpan, boldPosition, styledText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Set the text.
        this.viewContainer.textViewMessage.setText(styledText, TextView.BufferType.SPANNABLE);

    }

    /**
     * Attempts to delete a user.
     */
    private void attemptUserDelete() {

        // Hide the keyboard.
        KeyboardManager.HideActivityKeyboard(context);


        // Validate the user edit.
        ModelValidator<ParseUser> validator = this.validateUserDelete();
        if (!validator.isValid()) {

            // Set the error message.
            this.viewContainer.textViewError.setText(validator.getMessage());

            // Show the error message.
            this.viewContainer.layoutError.setVisibility(View.VISIBLE);

        } else {

            // Hide the error message.
            this.viewContainer.layoutError.setVisibility(View.GONE);

            // Get the current user.
            ParseUser user = ParseUser.getCurrentUser();

            // Start the Edit Account Task.
            new DeleteAccountTask().execute(user);
        }
    }


    /**
     * Attempts to validate the user edit.
     * @return ModelValidator.
     */
    private ModelValidator<ParseUser> validateUserDelete() {

        // Get the values.
        String password = this.viewContainer.editTextPassword.getText().toString();

        // Ensure there is a password.
        if (TextUtils.isEmpty(password)) {
            return new ModelValidator<ParseUser>(null, false, getString(R.string.edit_account_validation_password_missing));
        }

        // Return success.
        return new ModelValidator<ParseUser>(null, true, null);
    }

    /**
     * Task to Edit Account
     */
    private class DeleteAccountTask extends AsyncTask<ParseUser, Void, ParseException> {

        // Private Members.
        private ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {

            // Create the progress dialog.
            this.progressDialog = new ProgressDialog(context);
            this.progressDialog.setTitle(getString(R.string.delete_account_progress_title));
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        @Override
        protected ParseException doInBackground(ParseUser... users) {

            // Get the user.
            ParseUser user = users[0];

            // Attempt to create account.
            try
            {
                // Attempt to login with the current user.
                String username = user.getUsername();
                String password = viewContainer.editTextPassword.getText().toString();
                ParseUser.logIn(username, password);

                // Call the delete account.
                UserModel.DeleteAccount(context);
            }
            catch(ParseException e) {

                return e;
            }

            return null;
        }

        @Override
        protected void onPostExecute(ParseException e) {

            // Dismiss the progress dialog.
            this.progressDialog.dismiss();

            // Check for an exception.
            if (e != null) {

                // Set the error message.
                viewContainer.textViewError.setText(e.getLocalizedMessage());

                // Show the error message.
                viewContainer.layoutError.setVisibility(View.VISIBLE);

            } else {

                // Get the activity.
                MainActivity mainActivity = (MainActivity)context;

                // Perform the logout.
                mainActivity.handleLogout();
            }
        }
    }

    /**
     * Holds the view for the Delete Account
     */
    private static class DeleteAccountViewContainer {

        // Private Members.
        private TextView textViewMessage = null;
        private EditText editTextPassword = null;
        private Button buttonDelete = null;
        private LinearLayout layoutError = null;
        private TextView textViewError = null;

        // Private Constructor.
        private DeleteAccountViewContainer() {}

        // Static Constructor.
        public static DeleteAccountViewContainer with(View view) {

            // Create the instance
            DeleteAccountViewContainer instance = new DeleteAccountViewContainer();

            // Get the items.
            instance.textViewMessage = (TextView)view.findViewById(R.id.delete_account_text_view_message);
            instance.editTextPassword = (EditText)view.findViewById(R.id.delete_account_edit_text_password);
            instance.buttonDelete = (Button)view.findViewById(R.id.delete_account_button_delete);
            instance.layoutError = (LinearLayout)view.findViewById(R.id.delete_account_layout_error);
            instance.textViewError = (TextView)view.findViewById(R.id.delete_account_text_view_error);

            return instance;
        }
    }
}
