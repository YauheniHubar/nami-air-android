package com.nami.airapp.widgets;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.support.v4.view.MotionEventCompat;

/**
 * Created by Brian on 12/18/2014.
 */
public class SwipeOutViewPager extends ViewPager {

    private OnSwipeOutListener swipeOutListener;
    private float startDragX;

    public SwipeOutViewPager(Context context) {
        super(context);
    }

    public SwipeOutViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setSwipeOutListener(OnSwipeOutListener swipeOutListener) {
        this.swipeOutListener = swipeOutListener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev){
        if(getCurrentItem()==getAdapter().getCount()-1){
            final int action = ev.getAction();
            float x = ev.getX();
            switch(action & MotionEventCompat.ACTION_MASK){
                case MotionEvent.ACTION_DOWN:
                    startDragX = x;
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    if (x<startDragX){
                        swipeOutListener.onSwipeOutAtEnd();
                    }else{
                        startDragX = 0;
                    }
                    break;
            }
        }else{
            startDragX=0;
        }
        return super.onTouchEvent(ev);
    }

    public interface OnSwipeOutListener {
        public void onSwipeOutAtEnd();
    }
}
