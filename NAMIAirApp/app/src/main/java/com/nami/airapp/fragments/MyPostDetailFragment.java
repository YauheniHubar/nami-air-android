package com.nami.airapp.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.enums.ApprovedStatus;
import com.nami.airapp.data.models.PostFlagModel;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.KeyboardManager;
import com.nami.airapp.utils.PostManager;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import com.nami.airapp.R;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.data.models.PostNoteModel;
import com.nami.airapp.views.PostNoteListItemView;
import com.nami.airapp.utils.DateFormatter;
import com.nami.airapp.utils.AppFragmentManager;
import com.parse.ParseUser;


/**
 * My Post Detail Fragment.
 */
public class MyPostDetailFragment extends Fragment implements OnClickListener {

    // Private Static Members.
    private static final String KEY_FRAGMENT_MY_POST_DETAIL = "KEY_FRAGMENT_MY_POST_DETAIL";
    private static final String KEY_FRAGMENT_MY_POST_DETAIL_LIST = "KEY_FRAGMENT_MY_POST_DETAIL_LIST";
    private static final int MENU_OPTIONS = Menu.FIRST;
    private static final int MENU_MENU_POST_CANCEL = 2;
    private static final int FLAGGED_MENU_TIMEOUT = 5000;

    // Private Members.
    private Context context = null;
    private MyPostDetailViewContainer viewContainer = null;
    private PostModel postModel = null;
    private List<PostNoteModel> postNotes = null;
    private List<PostModel> inspiredPosts = null;
    private List<PostModel> postItems = null;
    private InitializePageAsyncTask initializer = null;

    /**
     * Default Constructor.
     */
    public MyPostDetailFragment() {}

    /**
     * Creates a new instance of a MyPostDetailFragment.
     * @param postModel The post model.
     * @return New instance of a MyPostDetailFragment.
     */
    public static MyPostDetailFragment newInstance(PostModel postModel) {

        // Create the new instance.
        MyPostDetailFragment instance = new MyPostDetailFragment();

        // Add the post model to the bundle.
        Bundle args = new Bundle();
        args.putParcelable(KEY_FRAGMENT_MY_POST_DETAIL, postModel);
        instance.setArguments(args);

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            this.postModel = args.getParcelable(KEY_FRAGMENT_MY_POST_DETAIL);
        }
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set the action bar.
        MainActivity mainActivity = (MainActivity)context;
        mainActivity.setActionBarNoTabs();

        // Set options menu.
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_post_detail, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    AppFragmentManager.RemoveFromStack(that, context);
                    return true;
                }

                return false;
            }
        });

        // Hide the keyboard.
        KeyboardManager.HideActivityKeyboard(context);

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Initialize the page with the data.
        this.initializer = new InitializePageAsyncTask(this.context);
        this.initializer.execute();
    }

    @Override
    public void onPause() {
        super.onPause();

        // Cancel the initializer.
        if (this.initializer.getStatus() != AsyncTask.Status.FINISHED)
            this.initializer.cancel(true);

        // Remove the key listener.
        this.getView().setOnKeyListener(null);
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // Create the search menu item.
        MenuItem search = menu.add(Menu.NONE, MENU_OPTIONS, Menu.NONE, getString(R.string.options_menu_options));
        MenuItemCompat.setShowAsAction(search, MenuItemCompat.SHOW_AS_ACTION_ALWAYS | MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        search.setIcon(R.drawable.ic_menu_options);
    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case MENU_OPTIONS:
                showOptionsMenu();
                break;
            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, context);
                return true;            
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        if (view == this.viewContainer.textViewPostInspiredByPost) {
            this.showInspiredByPost();
        } else if (view == this.viewContainer.textViewPostInspiredByCountLink) {
            this.showInspiredPost();
        }
    }

    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, "");
    }

    /**
     * Fetches all the post details.
     */
    private void fetchPostDetails() throws ParseException {

        // Fetch the inspired posts.
        this.fetchInspiredPosts();

        // Fetch the post notes.
        if (this.postModel.getApprovedStatus() == ApprovedStatus.APPROVED) {
            this.fetchPostNotes();
        }
    }

    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = MyPostDetailViewContainer.with(view);

        this.viewContainer.textViewPostInspiredByPost.setOnClickListener(this);
        this.viewContainer.textViewPostInspiredByCountLink.setOnClickListener(this);

        // Hide the appropriate controls.
        this.viewContainer.layoutPostDetail.setVisibility(View.INVISIBLE);
        this.viewContainer.layoutProgress.setVisibility(View.VISIBLE);
    }

    /**
     * Updates the post content.
     */
    private void updatePostContent() {

        // Create the text.
        SpannableString text = new SpannableString(this.postModel.getPostText());

        // Set the post styles.
        PostManager.setPostStyle(context, text);
        PostManager.setPostFirstSentenceStyle(context, text);
        PostManager.setPostHashTagsStyle(context, this, text);
        PostManager.setPostFeelingsStyle(context, text);
        this.setPostDate(this.postModel);

        // Set the inspired by.
        if (this.postModel.getInspiredByPost() != null)
            this.viewContainer.layoutInspired.setVisibility(View.VISIBLE);
        else this.viewContainer.layoutInspired.setVisibility(View.GONE);

        // Add the click events.
        this.viewContainer.textViewPostInspiredByPost.setOnClickListener(this);

        // Set the button properties.
        this.viewContainer.buttonPostLike.setChecked(false);
        this.viewContainer.buttonPostHug.setChecked(false);
        this.viewContainer.buttonPostMeToo.setChecked(false);
        this.viewContainer.buttonPostLike.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
        this.viewContainer.buttonPostHug.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
        this.viewContainer.buttonPostMeToo.setTextAppearance(this.context, R.style.post_item_button_social_text_off);

        this.viewContainer.buttonPostLike.setText(String.valueOf(this.postModel.getLikeCount()));
        this.viewContainer.buttonPostHug.setText(String.valueOf(this.postModel.getHugCount()));
        this.viewContainer.buttonPostMeToo.setText(String.valueOf(this.postModel.getMeTooCount()));

        this.viewContainer.buttonPostLike.setOnClickListener(null);
        this.viewContainer.buttonPostHug.setOnClickListener(null);
        this.viewContainer.buttonPostMeToo.setOnClickListener(null);
        this.viewContainer.buttonPostLike.setClickable(false);
        this.viewContainer.buttonPostHug.setClickable(false);
        this.viewContainer.buttonPostMeToo.setClickable(false);
        this.viewContainer.buttonPostLike.setEnabled(false);
        this.viewContainer.buttonPostHug.setEnabled(false);
        this.viewContainer.buttonPostMeToo.setEnabled(false);


        // Determine if the post is approved.
        if (this.postModel.getApprovedStatus() == ApprovedStatus.APPROVED) {

            // Hide the not approved.
            this.viewContainer.textViewNotApproved.setVisibility(View.GONE);
            this.viewContainer.textViewDisapproved.setVisibility(View.GONE);

            // Set the inspired by link count.
            if (this.postModel.getMeTooStoryCount() > 0) {

                // Set the text.
                String inspiredCountText = null;
                if (this.postModel.getMeTooStoryCount() > 1)
                    inspiredCountText = String.format(getString(R.string.post_detail_post_inspired_by_count_link).toString(), this.postModel.getMeTooStoryCount());
                else inspiredCountText = String.format(getString(R.string.post_detail_post_inspired_by_count_link_single).toString(), this.postModel.getMeTooStoryCount());
                this.viewContainer.textViewPostInspiredByCountLink.setText(inspiredCountText);

                // Show the link.
                this.viewContainer.layoutInspiredPosts.setVisibility(View.VISIBLE);


            } else {

                // Hide the link.
                this.viewContainer.layoutInspiredPosts.setVisibility(View.GONE);
            }

        } else {


            if (this.postModel.getApprovedStatus() == ApprovedStatus.NOTAPPROVED) {

                this.viewContainer.textViewNotApproved.setVisibility(View.VISIBLE);
                this.viewContainer.textViewDisapproved.setVisibility(View.GONE);
            } else {
                this.viewContainer.textViewNotApproved.setVisibility(View.GONE);
                this.viewContainer.textViewDisapproved.setVisibility(View.VISIBLE);
            }
        }


        PostFlagModel moderatorFlaggedPost = ApplicationManager.getInstance().findModeratorPostFlaggedStatus(this.postModel);
        if (false /*moderatorFlaggedPost != null*/) {

            // Create the flagged text.
            String flaggedText = "";
            switch (moderatorFlaggedPost.getFlaggedStatus()) {
                case INAPPROPRIATE:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_inappropriate);
                    break;
                case SELF_HARM:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_self_harm);
                    break;
                case THREATENING:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_threatening);
                    break;
                case BULLYING:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_bullying);
                    break;
                case SPAM:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_spam);
                    break;
            }

            // Create the flagged text sentence.
            String flaggedTextSentence = String.format(this.context.getString(R.string.post_detail_moderator_flag_text), flaggedText);

            // Set the flagged text.
            this.viewContainer.textViewPostModeratorFlagged.setText(flaggedTextSentence);
            this.viewContainer.textViewPostModeratorFlagged.setVisibility(View.VISIBLE);

            // Show the flagged layout.
            this.viewContainer.textViewNotApproved.setVisibility(View.GONE);
            this.viewContainer.textViewDisapproved.setVisibility(View.GONE);
        } else {

            this.viewContainer.textViewPostModeratorFlagged.setVisibility(View.GONE);
        }

        this.viewContainer.textViewPostModeratorFlagged.setVisibility(View.GONE);

        // Set the post text.
        this.viewContainer.textViewPostText.setLineSpacing(4,1);
        this.viewContainer.textViewPostText.setText(text, TextView.BufferType.SPANNABLE);
        this.viewContainer.textViewPostText.setMovementMethod(LinkMovementMethod.getInstance());

        // Set the counts.
        this.viewContainer.layoutNoteLine.setVisibility(View.VISIBLE);
        if (this.postModel.getNoteCount() > 0) {

            this.viewContainer.textViewPostNoteCount.setText(String.valueOf(this.postModel.getNoteCount()));
            if (this.postModel.getNoteCount() > 1)
                this.viewContainer.textViewPostNoteText.setText(getString(R.string.post_detail_hash_notes));
            else this.viewContainer.textViewPostNoteText.setText(getString(R.string.post_detail_hash_note));
        }

        // Update the notes.
        this.updatePostNotes(this.postNotes);
    }

    /**
     * Fetches the inspired by posts.
     */
    private void fetchInspiredPosts() throws ParseException {

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<PostModel> queryFactory = PostModel.createPostInspiredByFeedAdapterQuery(this.postModel);

        // Create and execute the query.
        ParseQuery<PostModel> query = queryFactory.create();
        this.inspiredPosts = query.find();
    }

    /**
     * Fetches the post notes.
     */
    private void fetchPostNotes() throws ParseException {

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<PostNoteModel> queryFactory = PostNoteModel.createPostNoteAdapterQuery(this.postModel);

        // Create and execute the query.
        ParseQuery<PostNoteModel> query = queryFactory.create();
        this.postNotes = query.find();
    }


    /**
     * Updates the post notes.
     */
    private void updatePostNotes(List<PostNoteModel> postNotes) {

        // Clear all the previous notes.
        this.viewContainer.layoutPostNotes.removeAllViews();

        // Loop through the notes.
        if (postNotes != null) {
            for (PostNoteModel note : postNotes) {

                // Create the view and update.
                PostNoteListItemView noteView = new PostNoteListItemView(context);
                noteView.updateFromModel(this, note);

                // Add the view.
                this.viewContainer.layoutPostNotes.addView(noteView);
            }
        }
    }


    /**
     * Sets the post date.
     * @param model Post model.
     */
    private void setPostDate(PostModel model) {

        // Set the date.
        String date = DateFormatter.FormatPostDate(model.getPostDate(), context);
        this.viewContainer.textViewPostDate.setText(date);
    }

    /**
     * Shows the inspired post.
     */
    private void showInspiredByPost() {

        if (isCorrectGroupType(postModel.getInspiredByPost()) == false) {

            // Show the dialog and return.
            this.showIncorrectGroupDialog();
            return;
        }

        // Create the fragment.
        PostDetailFragment postDetailFragment = PostDetailFragment.newInstance(postModel.getInspiredByPost());

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, postDetailFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Shows the inspired posts.
     */
    private void showInspiredPost() {

        // Return if there are no inspired posts.
        if (this.inspiredPosts == null || this.inspiredPosts.size() == 0)
            return;

        // Test the group.
        PostModel inspiredPostModel = this.inspiredPosts.get(0);
        if (isCorrectGroupType(postModel) == false) {

            // Show the dialog and return.
            this.showIncorrectGroupDialog();
            return;
        }

        // Determine if single post or feed.
        if (this.inspiredPosts.size() > 1) {

            // Create the fragment.
            InspiredPostFeedFragment postFeedFragment = InspiredPostFeedFragment.newInstance(this.postModel);

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, postFeedFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();

        } else if (this.inspiredPosts.size() == 1) {

            // Create the fragment.
            PostDetailFragment postDetailFragment = PostDetailFragment.newInstance(this.inspiredPosts.get(0));

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, postDetailFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();
        }
    }

    /**
     * Checks the post model group type against the current user group type.
     * @param postModel
     * @return
     */
    private boolean isCorrectGroupType(PostModel postModel) {

        if (postModel.getUserType().getValue() == ParseUser.getCurrentUser().getInt(UserModelConstants.FieldUserType))
            return true;

        return false;
    }

    /**
     * Shows the incorrect group dialog.
     */
    private void showIncorrectGroupDialog() {

        // Create the dialog.
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // Set the properties.
        alertDialogBuilder.setTitle(getString(R.string.incorrect_group_dialog_title));
        alertDialogBuilder.setMessage(getString(R.string.incorrect_group_dialog_message));
        alertDialogBuilder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                // if this button is clicked, just close
                // the dialog box and do nothing
                dialog.cancel();
            }
        });

        // Create and show the dialog.
        alertDialogBuilder.create().show();

    }

    /**
     * Shows the options menu.
     */
    private void showOptionsMenu() {

        // Determine the items to show.
        int items = 0;
        if (this.postModel.getIsPrivate())
            items = R.array.my_post_detail_menu_private;
        else items = R.array.my_post_detail_menu;

        // Create the dialog builder.
        AlertDialog.Builder menuPostDialogBuilder = new AlertDialog.Builder(context);
        menuPostDialogBuilder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                // Determine the action.
                switch (i) {
                    /*case 1:
                        togglePostVisibility();
                        break; */
                    case 0:
                        deletePost();
                        break;
                    case MENU_MENU_POST_CANCEL:
                        break;
                }
            }
        });


        // Create and show the dialog.
        AlertDialog menuPostDialog = menuPostDialogBuilder.create();
        menuPostDialog.show();
    }

    /**
     * Toggles the post visibility.
     */
    private void togglePostVisibility() {

        // Determine the current state.
        if (this.postModel.getIsPrivate()) {
            this.postModel.setIsPrivate(false);
            PostModel.SetPostPublic(this.context, this.postModel);
        }
        else {
            this.postModel.setIsPrivate(true);
            PostModel.SetPostPrivate(this.context, this.postModel);
            ApplicationManager.getInstance().notifyPostRemoveListeners(this.postModel);
        }
    }

    /**
     * Deletes a post.
     */
    private void deletePost() {

        // Delete the post.
        PostModel.DeletePost(this.context, this.postModel);
        ApplicationManager.getInstance().notifyPostRemoveListeners(this.postModel);

        // Pop the stack.
        AppFragmentManager.RemoveFromStack(this, context);

    }

    /**
     * Holds the view for the Post Detail Item.
     */
    private static class MyPostDetailViewContainer {

        // Private Members.
        private RelativeLayout layoutFullContainer;
        private TextView textViewPostText = null;
        private TextView textViewPostLikeCount = null;
        private TextView textViewPostHugCount = null;
        private TextView textViewPostMeTooCount = null;
        private TextView textViewPostNoteCount = null;
        private TextView textViewPostInspiredByCount = null;
        private TextView textViewPostInspiredByCountLink = null;
        private TextView textViewPostDate = null;
        private TextView textViewNotApproved = null;
        private TextView textViewDisapproved = null;
        private TextView textViewPostInspiredByPost = null;
        private LinearLayout layoutPostCounts = null;
        private LinearLayout layoutPostNotes = null;
        private LinearLayout layoutInspired = null;
        private LinearLayout layoutInspiredPosts = null;
        private ScrollView layoutPostDetail = null;
        private LinearLayout layoutProgress = null;
        private LinearLayout layoutMain = null;
        private TextView textViewPostModeratorFlagged = null;
        private LinearLayout layoutNoteLine = null;
        private TextView textViewPostNoteText = null;

        private ToggleButton buttonPostLike = null;
        private ToggleButton buttonPostHug = null;
        private ToggleButton buttonPostMeToo = null;

        // Private Constructor.
        private MyPostDetailViewContainer() {}

        // Static Constructor.
        public static MyPostDetailViewContainer with(View view) {

            // Create the instance
            MyPostDetailViewContainer instance = new MyPostDetailViewContainer();

            // Get the items.
            instance.layoutFullContainer = (RelativeLayout)view.findViewById(R.id.post_full_container);
            instance.textViewPostText = (TextView)view.findViewById(R.id.my_post_detail_post_text);
            instance.textViewPostNoteCount = (TextView)view.findViewById(R.id.my_post_detail_post_note_count);
            instance.textViewPostInspiredByCountLink = (TextView)view.findViewById(R.id.my_post_detail_post_inspired_by_count_link);
            instance.textViewPostDate = (TextView)view.findViewById(R.id.my_post_detail_post_date);
            instance.textViewNotApproved = (TextView)view.findViewById(R.id.my_post_detail_text_view_not_approved);
            instance.textViewDisapproved = (TextView)view.findViewById(R.id.my_post_detail_text_view_disapproved);
            instance.textViewPostInspiredByPost = (TextView)view.findViewById(R.id.my_post_detail_inspired_by_this_post);
            instance.layoutPostNotes = (LinearLayout)view.findViewById(R.id.my_post_detail_list_notes);
            instance.layoutInspired = (LinearLayout)view.findViewById(R.id.my_post_detail_inspired);
            instance.layoutInspiredPosts = (LinearLayout)view.findViewById(R.id.my_post_detail_post_inspired_container);
            instance.layoutPostDetail = (ScrollView)view.findViewById(R.id.my_post_detail);
            instance.layoutProgress = (LinearLayout)view.findViewById(R.id.progress);
            instance.layoutMain = (LinearLayout)view.findViewById(R.id.post_detail_main);
            instance.textViewPostModeratorFlagged = (TextView)view.findViewById(R.id.my_post_detail_text_view_flagged);
            instance.textViewPostNoteText = (TextView)view.findViewById(R.id.my_post_detail_post_note_text);


            instance.buttonPostLike = (ToggleButton)view.findViewById(R.id.my_post_detail_button_like);
            instance.buttonPostHug = (ToggleButton)view.findViewById(R.id.my_post_detail_button_hug);
            instance.buttonPostMeToo = (ToggleButton)view.findViewById(R.id.my_post_detail_button_metoo);
            instance.layoutNoteLine = (LinearLayout)view.findViewById(R.id.my_post_detail_notes_line);

            return instance;
        }
    }

    /**
     * Initializes the page with data.
     */
    private class InitializePageAsyncTask extends AsyncTask<Void, Void, Void> {

        private Context context;
        private ParseException parseException;

        public InitializePageAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Fetch the model if needed.
            try
            {
                // Fetch the post.
                postModel.fetch();

                // Fetch the post details.
                fetchPostDetails();

            }
            catch (ParseException e)
            {

                this.parseException = e;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void voids) {

            if (this.parseException != null) {
                ApplicationManager.getInstance().showConnectionError(this.context);
                return;
            }

            // Return if cancelled.
            if (isCancelled())
                return;

            // Set the content.
            updatePostContent();

            // Show the appropriate controls.
            viewContainer.layoutProgress.setVisibility(View.GONE);
            viewContainer.layoutPostDetail.setVisibility(View.VISIBLE);
        }
    }
}
