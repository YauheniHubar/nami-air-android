package com.nami.airapp.data.enums;

/**
 * User Type enum.
 */
public enum UserType {

    // Values.
    USER (1),
    CAREGIVER (2),
    MODERATOR (3);

    // Private Members.
    private final int id;

    /**
     * Constructor.
     * @param id User Type value.
     */
    UserType(int id) {

        // Set the members.
        this.id = id;
    }

    /**
     * Gets the User Type value.
     * @return User Type value.
     */
    public int getValue() {

        return this.id;
    }

    /**
     * Creates a UserType enum from a value.
     * @param id User Type value.
     * @return UserType enum.
     */
    public static UserType fromValue(int id) {

        // Determine the id.
        switch (id) {

            case 1 : { return USER; }
            case 2 : { return CAREGIVER; }
            case 3 : { return MODERATOR; }
        };

        return null;
    }
}
