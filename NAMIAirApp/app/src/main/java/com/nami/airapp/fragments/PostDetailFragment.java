package com.nami.airapp.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.enums.FlaggedStatus;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.KeyboardManager;
import com.nami.airapp.utils.PostManager;
import com.parse.ParseCloud;
import com.parse.FunctionCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import com.nami.airapp.R;
import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.models.PostFlagModel;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.data.models.PostNoteModel;
import com.nami.airapp.data.models.PostUserSocialStatus;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.interfaces.NoteSavedListener;
import com.nami.airapp.views.PostNoteListItemView;
import com.nami.airapp.utils.DateFormatter;
import com.nami.airapp.utils.AppFragmentManager;

/**
 * Post Detail Fragment.
 */
public class PostDetailFragment extends Fragment implements OnClickListener, NoteSavedListener, PostUserSocialStatus.PostUserSocialStatusListener{

    // Private Static Members.
    private static final String KEY_FRAGMENT_POST_DETAIL = "KEY_FRAGMENT_POST_DETAIL";
    private static final String KEY_FRAGMENT_POST_DETAIL_LIST = "KEY_FRAGMENT_POST_DETAIL_LIST";
    private static final int MENU_OPTIONS = Menu.FIRST;
    private static final int MENU_FLAG_POST_CANCEL = 5;
    private static final int MENU_MENU_POST_CANCEL = 3;
    private static final int FLAGGED_MENU_TIMEOUT = 5000;

    // Private Members.
    private Context context = null;
    private PostDetailViewContainer viewContainer = null;
    private PostModel postModel = null;
    private List<PostNoteModel> postNotes = null;
    private List<PostModel> inspiredPosts = null;
    private PostUserSocialStatus postUserSocialStatus = null;
    private List<PostModel> postItems = null;
    private InitializePageAsyncTask initializer = null;

    /**
     * Default Constructor.
     */
    public PostDetailFragment() {}

    /**
     * Creates a new instance of a PostDetailFragment.
     * @param postModel The post model.
     * @return New instance of a PostDetailFragment.
     */
    public static PostDetailFragment newInstance(PostModel postModel) {

        // Create the new instance.
        PostDetailFragment instance = new PostDetailFragment();

        // Add the post model to the bundle.
        Bundle args = new Bundle();
        args.putParcelable(KEY_FRAGMENT_POST_DETAIL, postModel);
        instance.setArguments(args);

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            this.postModel = args.getParcelable(KEY_FRAGMENT_POST_DETAIL);
        }
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Set the action bar.
        MainActivity mainActivity = (MainActivity)context;
        mainActivity.setActionBarNoTabs();

        // Set options menu.
        setHasOptionsMenu(true);


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_detail, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    AppFragmentManager.RemoveFromStack(that, context);
                    return true;
                }

                return false;
            }
        });

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Initialize the page with the data.
        this.initializer = new InitializePageAsyncTask(this.context);
        this.initializer.execute();
    }

    @Override
    public void onPause() {
        super.onPause();

        // Cancel the initializer.
        if (this.initializer.getStatus() != AsyncTask.Status.FINISHED)
            this.initializer.cancel(true);

        // Remove the key listener.
        this.getView().setOnKeyListener(null);
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // Create the search menu item.
        MenuItem search = menu.add(Menu.NONE, MENU_OPTIONS, Menu.NONE, getString(R.string.options_menu_options));
        MenuItemCompat.setShowAsAction(search, MenuItemCompat.SHOW_AS_ACTION_ALWAYS | MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        search.setIcon(R.drawable.ic_menu_options);
    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case MENU_OPTIONS:
                showOptionsMenu();
                break;
            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, context);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (view == this.viewContainer.buttonLike) {
            this.viewContainer.buttonLike.setSelected(true);
            this.togglePostLike();
        } else if (view == this.viewContainer.buttonHug) {
            this.viewContainer.buttonHug.setSelected(true);
            this.togglePostHug();
        } else if (view == this.viewContainer.buttonMeToo) {
            this.viewContainer.buttonMeToo.setSelected(true);
            this.togglePostMeToo();
        } else if (view == this.viewContainer.layoutAddNote) {
            this.showPostNoteList();
        } else if (view == this.viewContainer.textViewPostInspiredByPost) {
            this.showInspiredByPost();
        } else if (view == this.viewContainer.textViewPostInspiredByCountLink) {
            this.showInspiredPost();
        }
    }



    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, "");
    }

    /**
     * Shows the options menu.
     */
    private void showOptionsMenu() {

        // Create the dialog builder.
        AlertDialog.Builder menuPostDialogBuilder = new AlertDialog.Builder(context);
        menuPostDialogBuilder.setItems(R.array.post_detail_menu, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                // Determine the action.
                switch (i) {
                    case 0:
                        flagPost();
                        break;
                    case 1:
                        hidePost();
                        break;
                    case 2:
                        blockAuthor();
                        break;
                    case MENU_MENU_POST_CANCEL:
                        break;
                }
            }
        });


        // Create and show the dialog.
        AlertDialog menuPostDialog = menuPostDialogBuilder.create();
        menuPostDialog.show();
    }

    /**
     * Toggles the post like.
     */
    private void togglePostLike() {

        // Toggle the status.
        this.viewContainer.buttonLike.setEnabled(false);
        this.postUserSocialStatus.togglePostLike();
    }

    /**
     * Toggles the post hug.
     */
    private void togglePostHug() {

        // Toggle the status.
        this.viewContainer.buttonHug.setEnabled(false);
        this.postUserSocialStatus.togglePostHug();
    }

    /**
     * Toggles the post me too.
     */
    private void togglePostMeToo() {

        // Toggle the status.
        this.viewContainer.buttonMeToo.setEnabled(false);
        this.postUserSocialStatus.togglePostMeToo();

        // Create the dialog.
        final Dialog relateDialog = new Dialog(context);
        relateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        relateDialog.setContentView(R.layout.dialog_post_relate);

        // Get the views.
        Button relateDialogNoButton = (Button)relateDialog.findViewById(R.id.dialog_post_relate_button_no);
        Button relateDialogYesButton = (Button)relateDialog.findViewById(R.id.dialog_post_relate_button_yes);

        // Add the listeners.
        relateDialogNoButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {

                // Dismiss.
                relateDialog.dismiss();
            }
        });
        relateDialogYesButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {

                // Dismiss.
                relateDialog.dismiss();

                // Show the compose post.
                showComposeFragment(postModel);
            }
        });

        // Show the dialog.
        relateDialog.show();
    }

    /**
     * Shows the compose fragment.
     * @param postModel
     */
    private void showComposeFragment(PostModel postModel) {

        // Create the fragment.
        PostComposeFragment postComposeFragment = PostComposeFragment.newInstance(postModel);

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, postComposeFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Shows the inspired post.
     */
    private void showInspiredByPost() {

        // Create the fragment.
        PostDetailFragment postDetailFragment = PostDetailFragment.newInstance(postModel.getInspiredByPost());

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, postDetailFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Shows the inspired posts.
     */
    private void showInspiredPost() {

        // Return if there are no inspired posts.
        if (this.inspiredPosts == null || this.inspiredPosts.size() == 0)
            return;

        // Determine if single post or feed.
        if (this.inspiredPosts.size() > 1) {

            // Create the fragment.
            InspiredPostFeedFragment postFeedFragment = InspiredPostFeedFragment.newInstance(this.postModel);

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, postFeedFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();

        } else if (this.inspiredPosts.size() == 1) {

            // Create the fragment.
            PostDetailFragment postDetailFragment = PostDetailFragment.newInstance(this.inspiredPosts.get(0));

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, postDetailFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();
        }
    }

    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = PostDetailViewContainer.with(view);

        // Add the click events.
        this.viewContainer.buttonLike.setOnClickListener(this);
        this.viewContainer.buttonHug.setOnClickListener(this);
        this.viewContainer.buttonMeToo.setOnClickListener(this);
        this.viewContainer.layoutAddNote.setOnClickListener(this);
        this.viewContainer.textViewPostInspiredByPost.setOnClickListener(this);
        this.viewContainer.textViewPostInspiredByCountLink.setOnClickListener(this);

        // Hide the appropriate controls.
        viewContainer.layoutPostDetail.setVisibility(View.INVISIBLE);
        viewContainer.layoutAddNote.setVisibility(View.INVISIBLE);
        viewContainer.layoutProgress.setVisibility(View.VISIBLE);
    }

    /**
     * Fetches all the post details.
     */
    private void fetchPostDetails() throws ParseException {

        // Fetch the inspired posts.
        this.fetchInspiredPosts();

        // Fetch the post notes.
        this.fetchPostNotes();

        // Create the social status and fetch the status.
        this.postUserSocialStatus = new PostUserSocialStatus(context, this, this.postModel);
    }

    /**
     * Called when a note is saved.
     */
    public void noteSaved() {

        // Update the note count.
        this.postModel.setNoteCount(this.postModel.getNoteCount() + 1);

        // Update the post content.
        this.updatePostContent();
    }

    /**
     * Updates the content based on the social status.
     * @param socialPostModel The post model.
     * @param hasLike Whether the user has liked a post.
     * @param hasHug Whether the user has hugged a post.
     * @param hasMeToo Whether the user has me tooed a post.
     */
    public void UpdatePostUserSocialStatus(PostModel socialPostModel, boolean hasLike, boolean hasHug, boolean hasMeToo, boolean updateContent)
    {

        // Enable the buttons.
        this.viewContainer.buttonLike.setEnabled(true);
        this.viewContainer.buttonHug.setEnabled(true);
        this.viewContainer.buttonMeToo.setEnabled(true);

        // Set the like button state.
        if (hasLike) {
            this.viewContainer.buttonLike.setChecked(true);
            this.viewContainer.buttonLike.setTextAppearance(this.context, R.style.post_item_button_social_text_on);
        } else {
            this.viewContainer.buttonLike.setChecked(false);
            this.viewContainer.buttonLike.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
        }

        // Set the hug button state.
        if (hasHug) {
            this.viewContainer.buttonHug.setChecked(true);
            this.viewContainer.buttonHug.setTextAppearance(this.context, R.style.post_item_button_social_text_on);
        } else {
            this.viewContainer.buttonHug.setChecked(false);
            this.viewContainer.buttonHug.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
        }

        // Set the me too button state.
        if (hasMeToo) {
            this.viewContainer.buttonMeToo.setChecked(true);
            this.viewContainer.buttonMeToo.setTextAppearance(this.context, R.style.post_item_button_social_text_on);
        } else {
            this.viewContainer.buttonMeToo.setChecked(false);
            this.viewContainer.buttonMeToo.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
        }

        // Set the post model.
        this.postModel = socialPostModel;

        this.viewContainer.buttonLike.setText(String.valueOf(this.postModel.getLikeCount()));
        this.viewContainer.buttonHug.setText(String.valueOf(this.postModel.getHugCount()));
        this.viewContainer.buttonMeToo.setText(String.valueOf(this.postModel.getMeTooCount()));

        // Update the post content.
        if (updateContent)
            this.updatePostContent();
    }

    /**
     * Updates the post content.
     */
    private void updatePostContent() {

        // Create the text.
        SpannableString text = new SpannableString(this.postModel.getPostText());

        // Set the post styles.
        PostManager.setPostStyle(context, text);
        PostManager.setPostFirstSentenceStyle(context, text);
        PostManager.setPostHashTagsStyle(context, this, text);
        PostManager.setPostFeelingsStyle(context, text);
        this.setPostDate(this.postModel);

        // Set the inspired by.
        if (this.postModel.getInspiredByPost() != null)
            this.viewContainer.layoutInspired.setVisibility(View.VISIBLE);
        else this.viewContainer.layoutInspired.setVisibility(View.GONE);

        // Set the counts.
        this.viewContainer.layoutNoteLine.setVisibility(View.VISIBLE);
        if (this.postModel.getNoteCount() > 0) {
            this.viewContainer.textViewPostNoteCount.setText(String.valueOf(this.postModel.getNoteCount()));
            if (this.postModel.getNoteCount() > 1)
                this.viewContainer.textViewPostNoteText.setText(getString(R.string.post_detail_hash_notes));
            else this.viewContainer.textViewPostNoteText.setText(getString(R.string.post_detail_hash_note));
        }


        // Set the inspired by link count.
        if (this.postModel.getMeTooStoryCount() > 0) {

            // Set the text.
            String inspiredCountText = null;
            if (this.postModel.getMeTooStoryCount() > 1)
                inspiredCountText = String.format(getString(R.string.post_detail_post_inspired_by_count_link).toString(), this.postModel.getMeTooStoryCount());
            else inspiredCountText = String.format(getString(R.string.post_detail_post_inspired_by_count_link_single).toString(), this.postModel.getMeTooStoryCount());
            this.viewContainer.textViewPostInspiredByCountLink.setText(inspiredCountText);

            // Show the link.
            this.viewContainer.layoutInspiredPosts.setVisibility(View.VISIBLE);


        } else {

            // Hide the link.
            this.viewContainer.layoutInspiredPosts.setVisibility(View.GONE);
        }

        // Set the post text.
        this.viewContainer.textViewPostText.setLineSpacing(4,1);
        this.viewContainer.textViewPostText.setText(text, TextView.BufferType.SPANNABLE);
        this.viewContainer.textViewPostText.setMovementMethod(LinkMovementMethod.getInstance());

        // Set the flagged status.
        PostFlagModel flaggedPost = ApplicationManager.getInstance().findPostFlaggedStatus(this.postModel);
        if (flaggedPost != null) {

            // Create the flagged text.
            String flaggedText = "";
            switch (flaggedPost.getFlaggedStatus()) {
                case INAPPROPRIATE:
                    flaggedText = getString(R.string.post_detail_flag_text_inappropriate);
                    break;
                case SELF_HARM:
                    flaggedText = getString(R.string.post_detail_flag_text_self_harm);
                    break;
                case THREATENING:
                    flaggedText = getString(R.string.post_detail_flag_text_threatening);
                    break;
                case BULLYING:
                    flaggedText = getString(R.string.post_detail_flag_text_bullying);
                    break;
                case SPAM:
                    flaggedText = getString(R.string.post_detail_flag_text_spam);
                    break;
            }

            // Create the flagged text sentence.
            String flaggedTextSentence = String.format(getString(R.string.post_detail_flag_text), flaggedText);

            // Set the flagged text.
            this.viewContainer.textViewPostFlaggedText.setText(flaggedTextSentence);

            // Show the flagged layout.
            this.viewContainer.layoutFlagged.setVisibility(View.VISIBLE);


        } else {

            // Hide the flagged layout.
            this.viewContainer.layoutFlagged.setVisibility(View.GONE);
        }

        // Update the notes.
        this.updatePostNotes(this.postNotes);

        // Set the social status.
        UpdatePostUserSocialStatus(postModel, postUserSocialStatus.hasPostLike(), postUserSocialStatus.hasPostHug(), postUserSocialStatus.hasPostMeToo(), false);
    }

    /**
     * Fetches the inspired by posts.
     */
    private void fetchInspiredPosts() throws ParseException {

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<PostModel> queryFactory = PostModel.createPostInspiredByFeedAdapterQuery(this.postModel);

        // Create and execute the query.
        ParseQuery<PostModel> query = queryFactory.create();
        this.inspiredPosts = query.find();
    }

    /**
     * Fetches the post notes.
     */
    private void fetchPostNotes() throws ParseException {

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<PostNoteModel> queryFactory = PostNoteModel.createPostNoteAdapterQuery(this.postModel);

        // Create and execute the query.
        ParseQuery<PostNoteModel> query = queryFactory.create();
        this.postNotes = query.find();
    }

    /**
     * Updates the post notes.
     */
    private void updatePostNotes(List<PostNoteModel> postNotes) {

        // Clear all the previous notes.
        this.viewContainer.layoutPostNotes.removeAllViews();

        // Loop through the notes.
        if (postNotes != null) {
            for (PostNoteModel note : postNotes) {

                // Create the view and update.
                PostNoteListItemView noteView = new PostNoteListItemView(context);
                noteView.updateFromModel(this, note);

                // Add the view.
                this.viewContainer.layoutPostNotes.addView(noteView);
            }
        }

        // Add room for the add note button.
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, this.viewContainer.layoutAddNote.getMeasuredHeight() + 10);
        LinearLayout spacer = new LinearLayout(context);
        spacer.setLayoutParams(layoutParams);
        this.viewContainer.layoutPostNotes.addView(spacer);
    }

    /**
     * Sets the post date.
     * @param model Post model.
     */
    private void setPostDate(PostModel model) {

        // Set the date.
        String date = DateFormatter.FormatPostDate(model.getPostDate(), context);
        this.viewContainer.textViewPostDate.setText(date);
    }

    /**
     * Shows the post note list.
     */
    private void showPostNoteList() {

        // Create the fragment.
        PostDetailNoteFragment postDetailNoteFragment = PostDetailNoteFragment.newInstance(this, this.postModel);

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, postDetailNoteFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Flags a post.
     */
    private void flagPost() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Create the dialog builder.
        AlertDialog.Builder flagPostDialogBuilder = new AlertDialog.Builder(context);
        flagPostDialogBuilder.setItems(R.array.post_detail_flags, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                // Check for cancel.
                if (i == MENU_FLAG_POST_CANCEL) return;

                // Create the appropriate flagged type.
                final FlaggedStatus flaggedStatus = FlaggedStatus.fromValue(i + 1);

                // Create the parameters.
                HashMap<String, Object> params = new HashMap<String, Object>();
                params.put("postId", postModel.getObjectId());
                params.put("flaggedStatus", flaggedStatus.getValue());

                // Call the cloud function.
                ParseCloud.callFunctionInBackground("flagPost", params, new FunctionCallback<Object>() {

                    @Override
                    public void done(Object o, ParseException e) {

                        if (e != null) {

                            ApplicationManager.getInstance().showConnectionError(context);
                        } else {

                            // Update the list of flagged posts.
                            ApplicationManager.getInstance().fetchUserFlaggedPosts();

                            // Show the flagged dialog.
                            showFlaggedDialog();

                            // Update the post content.
                            updatePostContent();
                        }
                    }
                });
            }
        });

        // Create and show the dialog.
        AlertDialog flagPostDialog = flagPostDialogBuilder.create();
        flagPostDialog.show();
    }

    /**
     * Hides a post.
     */
    private void hidePost() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Add the post id to the posts hidden.
        ArrayList<String> postsHidden = (ArrayList<String>)ParseUser.getCurrentUser().get(UserModelConstants.FieldPostsHidden);
        if (postsHidden == null) postsHidden = new ArrayList<String>();
        postsHidden.add(this.postModel.getObjectId());

        // Set the posts hidden.
        ParseUser.getCurrentUser().put(UserModelConstants.FieldPostsHidden, postsHidden);

        // Reference the fragment.
        final Fragment that = this;

        // Save the user.
        ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {

                if (e != null) {

                    ApplicationManager.getInstance().showConnectionError(context);
                } else {

                    // Call the cloud method.
                    PostModel.HidePost(context, postModel);

                    // Show the dialog.
                    final Dialog hidePostDialog = new Dialog(context);
                    hidePostDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    hidePostDialog.setContentView(R.layout.dialog_post_hide_post);

                    // Get the views.
                    Button hidePostDialogOkButton = (Button)hidePostDialog.findViewById(R.id.dialog_post_hide_post_ok);

                    // Add the listeners.
                    hidePostDialogOkButton.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View view) {

                            // Dismiss.
                            hidePostDialog.dismiss();

                            AppFragmentManager.RemoveFromStack(that, context);
                        }
                    });

                    hidePostDialog.show();

                    // Notify the listener.
                    ApplicationManager.getInstance().notifyPostHideListeners(postModel.getObjectId());
                }
            }
        });

    }

    /**
     * Blocks an author.
     */
    private void blockAuthor() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Add the author to the blocked authors.
        ArrayList<ParseUser> authorsBlocked = (ArrayList<ParseUser>)ParseUser.getCurrentUser().get(UserModelConstants.FieldPostsHidden);
        if (authorsBlocked == null) authorsBlocked = new ArrayList<ParseUser>();
        authorsBlocked.add(this.postModel.getUser());

        // Set the posts hidden.
        ParseUser.getCurrentUser().put(UserModelConstants.FieldAuthorsBlocked, authorsBlocked);

        // Reference the fragment.
        final Fragment that = this;

        // Save the user.
        ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {

                if (e != null) {

                    ApplicationManager.getInstance().showConnectionError(context);
                } else {

                    // Call the cloud method.
                    PostModel.AuthorBlock(context, postModel);

                    // Show the dialog.
                    final Dialog blockAuthorDialog = new Dialog(context);
                    blockAuthorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    blockAuthorDialog.setContentView(R.layout.dialog_post_block_author);

                    // Get the views.
                    Button blockAuthorDialogOkButton = (Button)blockAuthorDialog.findViewById(R.id.dialog_post_block_author_ok);

                    // Add the listeners.
                    blockAuthorDialogOkButton.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View view) {

                            // Dismiss.
                            blockAuthorDialog.dismiss();

                            AppFragmentManager.RemoveFromStack(that, context);
                        }
                    });

                    blockAuthorDialog.show();

                    // Notify the listener.
                    ApplicationManager.getInstance().notifyAuthorBlockListeners(postModel.getUser().getObjectId());
                }
            }
        });
    }

    /**
     * Shows the flagged dialog.
     */
    private void showFlaggedDialog() {

        // Create the dialog.
        final Dialog postFlaggedDialog = new Dialog(context);
        postFlaggedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        postFlaggedDialog.setContentView(R.layout.dialog_post_flagged);

        // Show the dialog.
        postFlaggedDialog.show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                // Hide the dialog.
                postFlaggedDialog.dismiss();
            }
        }, FLAGGED_MENU_TIMEOUT);
    }



    /**
     * Holds the view for the Post Detail Item.
     */
    private static class PostDetailViewContainer {

        // Private Members.
        private RelativeLayout layoutFullContainer = null;
        private ScrollView layoutPostDetail = null;
        private LinearLayout layoutPostItemContainer = null;
        private TextView textViewPostText = null;
        private TextView textViewPostLikeCount = null;
        private TextView textViewPostHugCount = null;
        private TextView textViewPostMeTooCount = null;
        private TextView textViewPostNoteCount = null;
        private TextView textViewPostInspiredByCount = null;
        private TextView textViewPostInspiredByCountLink = null;
        private TextView textViewPostDate = null;
        private TextView textViewPostInspiredByPost = null;
        private TextView textViewPostFlaggedText = null;
        private ToggleButton buttonLike = null;
        private ToggleButton buttonHug = null;
        private ToggleButton buttonMeToo = null;
        private LinearLayout layoutPostNotes = null;
        private LinearLayout layoutAddNote = null;
        private LinearLayout layoutInspired = null;
        private LinearLayout layoutFlagged = null;
        private LinearLayout layoutInspiredPosts = null;
        private LinearLayout layoutProgress = null;
        private ProgressBar progressBar;
        private TextView progressBarText;
        private LinearLayout layoutMain = null;
        private LinearLayout layoutNoteLine = null;
        private TextView textViewPostNoteText = null;

        // Private Constructor.
        private PostDetailViewContainer() {}

        // Static Constructor.
        public static PostDetailViewContainer with(View view) {

            // Create the instance
            PostDetailViewContainer instance = new PostDetailViewContainer();

            // Get the items.
            instance.layoutFullContainer = (RelativeLayout)view.findViewById(R.id.post_full_container);
            instance.layoutPostItemContainer = (LinearLayout)view.findViewById(R.id.post_detail_post_container);
            instance.textViewPostText = (TextView)view.findViewById(R.id.post_detail_post_text);
            instance.textViewPostNoteCount = (TextView)view.findViewById(R.id.post_detail_post_note_count);
            instance.textViewPostInspiredByPost = (TextView)view.findViewById(R.id.post_detail_inspired_by_this_post);
            instance.textViewPostInspiredByCountLink = (TextView)view.findViewById(R.id.post_detail_post_inspired_by_count_link);
            instance.textViewPostDate = (TextView)view.findViewById(R.id.post_detail_post_date);
            instance.textViewPostFlaggedText = (TextView)view.findViewById(R.id.post_detail_flagged_text_view_message);
            instance.buttonLike = (ToggleButton)view.findViewById(R.id.post_detail_button_like);
            instance.buttonHug = (ToggleButton)view.findViewById(R.id.post_detail_button_hug);
            instance.buttonMeToo = (ToggleButton)view.findViewById(R.id.post_detail_button_metoo);
            instance.layoutPostNotes = (LinearLayout)view.findViewById(R.id.post_detail_list_notes);
            instance.layoutAddNote = (LinearLayout)view.findViewById(R.id.post_detail_add_note);
            instance.layoutInspired = (LinearLayout)view.findViewById(R.id.post_detail_inspired);
            instance.layoutFlagged = (LinearLayout)view.findViewById(R.id.post_detail_flagged_container);
            instance.layoutInspiredPosts = (LinearLayout)view.findViewById(R.id.post_detail_post_inspired_container);
            instance.layoutPostDetail = (ScrollView)view.findViewById(R.id.post_detail);
            instance.layoutProgress = (LinearLayout)view.findViewById(R.id.progress);
            instance.progressBarText = (TextView)view.findViewById(R.id.progress_bar_text);
            instance.layoutMain = (LinearLayout)view.findViewById(R.id.post_detail_main);
            instance.layoutNoteLine = (LinearLayout)view.findViewById(R.id.post_detail_notes_line);
            instance.textViewPostNoteText = (TextView)view.findViewById(R.id.post_detail_post_note_text);

            return instance;
        }
    }

    /**
     * Initializes the page with data.
     */
    private class InitializePageAsyncTask extends AsyncTask<Void, Void, Void> {

        private Context context = null;
        private ParseException parseException = null;

        public InitializePageAsyncTask(Context context) {
            this.context = context;
        }


        @Override
        protected Void doInBackground(Void... params) {

            // Fetch the model if needed.
            try
            {
                // Fetch the post.
                postModel.fetch();

                // Fetch the post details.
                fetchPostDetails();

            }
            catch (ParseException e)
            {

                this.parseException = e;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void voids) {

            if (this.parseException != null) {
                ApplicationManager.getInstance().showConnectionError(this.context);
            }

            // Return if cancelled.
            if (isCancelled())
                return;

            // Set the content.
            updatePostContent();

            // Show the appropriate controls.
            viewContainer.layoutProgress.setVisibility(View.GONE);
            viewContainer.layoutPostDetail.setVisibility(View.VISIBLE);
            viewContainer.layoutAddNote.setVisibility(View.VISIBLE);

        }
    }

}
