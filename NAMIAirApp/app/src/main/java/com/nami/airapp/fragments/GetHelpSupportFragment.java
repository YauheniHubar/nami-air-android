package com.nami.airapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.nami.airapp.R;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.AppFragmentManager;

/**
 * Post Compose Fragment
 */
public class GetHelpSupportFragment extends Fragment implements OnClickListener {

    // Private Members.
    private Context context = null;
    private GetHelpSupportViewContainer viewContainer = null;
    /**
     * Default Constructor.
     */
    public GetHelpSupportFragment() {}


    /**
     * Creates a new instance of a PostComposeFragment.
     * @return New instance of a PostComposeFragment.
     */
    public static GetHelpSupportFragment newInstance() {

        // Create and return the new instance.
        GetHelpSupportFragment instance = new GetHelpSupportFragment();
        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set options menu.
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_get_help_support, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    AppFragmentManager.RemoveFromStack(that, context);
                    return true;
                }

                return false;
            }
        });
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {


    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, context);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (this.viewContainer.layoutFind == view) {
            this.handleFind();
        } else if (this.viewContainer.layoutLearn == view) {
            this.handleLearn();
        } else if (this.viewContainer.buttonAsk == view) {
            this.handleAsk();
        }
    }

    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.get_help_support_title));
    }

    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = GetHelpSupportViewContainer.with(view);

        // Add the events.
        this.viewContainer.layoutLearn.setOnClickListener(this);
        this.viewContainer.layoutFind.setOnClickListener(this);
        this.viewContainer.buttonAsk.setOnClickListener(this);
    }


    /**
     * Handles the find
     */
    private void handleFind() {

        // Launch the site.
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.get_help_support_find_link)));
        startActivity(intent);
    }

    /**
     * Handles the learn
     */
    private void handleLearn() {

        // Launch the site.
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.get_help_support_learn_link)));
        startActivity(intent);
    }

    /**
     * Handles the ask.
     */
    private void handleAsk() {

        // Launch the site.
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.get_help_support_ask_link)));
        startActivity(intent);
    }

    /**
     * Holds the view for the Get Help
     */
    private static class GetHelpSupportViewContainer {

        // Private Members.
        private LinearLayout layoutFind = null;
        private LinearLayout layoutLearn = null;
        private Button buttonAsk = null;

        // Private Constructor.
        private GetHelpSupportViewContainer() {}

        // Static Constructor.
        public static GetHelpSupportViewContainer with(View view) {

            // Create the instance
            GetHelpSupportViewContainer instance = new GetHelpSupportViewContainer();

            // Get the items.
            instance.layoutFind = (LinearLayout)view.findViewById(R.id.get_help_support_find);
            instance.layoutLearn = (LinearLayout)view.findViewById(R.id.get_help_support_learn);
            instance.buttonAsk = (Button)view.findViewById(R.id.get_help_support_button_ask);

            return instance;
        }
    }
}
