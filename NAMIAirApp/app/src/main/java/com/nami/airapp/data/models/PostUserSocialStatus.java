package com.nami.airapp.data.models;

import android.widget.Toast;
import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;

import com.nami.airapp.data.enums.NotificationSubType;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.parse.DeleteCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by Brian on 11/17/2014.
 */
public class PostUserSocialStatus {

    // Private Members.
    private Context context;
    private PostUserSocialStatusListener listener = null;
    private PostModel postModel = null;
    private PostLikeModel socialPostLike = null;
    private PostHugModel socialPostHug = null;
    private PostMeTooModel socialPostMeToo = null;

    /**
     * PostUserSocialStatusListener
     */
    public interface PostUserSocialStatusListener
    {
        void UpdatePostUserSocialStatus(PostModel socialPostModel, boolean hasLike, boolean hasHug, boolean hasMeToo, boolean updateContent);
    }

    /**
     * Constructor
     * @param postModel The post.
     */
    public PostUserSocialStatus(Context context, PostUserSocialStatusListener listener, PostModel postModel)
    {

        // Set the member values.
        this.context = context;
        this.listener = listener;
        this.postModel = postModel;

        this.socialPostLike = ApplicationManager.getInstance().hasPostLike(this.postModel.getObjectId());
        this.socialPostHug = ApplicationManager.getInstance().hasPostHug(this.postModel.getObjectId());
        this.socialPostMeToo = ApplicationManager.getInstance().hasPostMeToo(this.postModel.getObjectId());
    }

    /**
     * Fetches the user social status (like, hug, me too)
     * @param background Whether to run in the background.
     */
    public void fetchUserSocialStatus(boolean background) {

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postId", this.postModel.getObjectId());


        try {

            // Get the hash map.
            HashMap<String, Object> socialStatus = (HashMap<String, Object>) ParseCloud.callFunction("fetchUserPostSocialStatus", params);

            // Get the social items.
            socialPostLike = (PostLikeModel) socialStatus.get("PostLike");
            socialPostHug = (PostHugModel) socialStatus.get("PostHug");
            socialPostMeToo = (PostMeTooModel) socialStatus.get("PostMeToo");
        }
        catch(ParseException e) {}
    }

    /**
     * Returns true if there is a post like.
     * @return
     */
    public boolean hasPostLike() {
        return this.socialPostLike != null;
    }

    /**
     * Returns true if there is post hug.
     * @return
     */
    public boolean hasPostHug() {
        return this.socialPostHug != null;
    }

    /**
     * Returns true if there is a post me too.
     * @return
     */
    public boolean hasPostMeToo() {
        return this.socialPostMeToo != null;
    }

    /**
     *
     */
    public void togglePostLike()
    {

        // Determine the status.
        if (this.socialPostLike == null)
            this.createPostLike();
        else this.deletePostLike();
    }

    /**
     *
     */
    public void togglePostHug()
    {

        // Determine the status.
        if (this.socialPostHug == null)
            this.createPostHug();
        else this.deletePostHug();
    }

    /**
     *
     */
    public void togglePostMeToo()
    {

        // Determine the status.
        if (this.socialPostMeToo == null)
            this.createPostMeToo();
        else updateListener();
    }

    /**
     * Creates the post like.
     */
    private void createPostLike() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Create the PostLike.
        final PostLikeModel postLike = new PostLikeModel();
        postLike.setPostModel(this.postModel);
        postLike.setPostUser(ParseUser.getCurrentUser());

        // Save the PostLike.
        postLike.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                // Check for an exception.
                if (e != null) {

                    ApplicationManager.getInstance().showConnectionError(context);
                } else {

                    // Update the post.
                    postModel.setLikeCount(postModel.getLikeCount() + 1);
                    socialPostLike = postLike;

                    // Create the notification.
                    PostNotificationModel.CreateSocialNotification(context, postModel, NotificationSubType.LIKE);
                    
                    // Add the post like.
                    ArrayList<PostLikeModel> postLikes = new ArrayList<PostLikeModel>();
                    postLikes.add(postLike);
                    ApplicationManager.getInstance().addPostLikes(postLikes);

                    // Update the listener.
                    updateListener();
                }
            }
        });
    }

    /**
     * Deletes a post like.
     */
    private void deletePostLike() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Delete the post like.
        this.socialPostLike.deleteInBackground(new DeleteCallback() {

            @Override
            public void done(ParseException e) {

                // Check for an exception.
                if (e != null) {

                    ApplicationManager.getInstance().showConnectionError(context);
                } else {

                    // Update the post.
                    postModel.setLikeCount(postModel.getLikeCount() - 1);

                    // Delete the notification.
                    PostNotificationModel.DeleteSocialNotification(context, postModel, NotificationSubType.LIKE);

                    // Remove the post like.
                    ApplicationManager.getInstance().removePostLike(postModel.getObjectId());

                    // Clear the post like.
                    socialPostLike = null;

                    // Update the listener.
                    updateListener();
                }
            }
        });
    }

    /**
     * Creates the post hug.
     */
    private void createPostHug() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Create the PostHug.
        final PostHugModel postHug = new PostHugModel();
        postHug.setPostModel(this.postModel);
        postHug.setPostUser(ParseUser.getCurrentUser());

        // Save the PostHug
        postHug.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                // Check for an exception.
                if (e != null) {

                    ApplicationManager.getInstance().showConnectionError(context);
                } else {

                    // Update the post.
                    postModel.setHugCount(postModel.getHugCount() + 1);
                    socialPostHug = postHug;

                    // Create the notification.
                    PostNotificationModel.CreateSocialNotification(context, postModel, NotificationSubType.HUG);

                    // Add the post hugs.
                    ArrayList<PostHugModel> postHugs = new ArrayList<PostHugModel>();
                    postHugs.add(postHug);
                    ApplicationManager.getInstance().addPostHugs(postHugs);

                    // Update the listener.
                    updateListener();
                }
            }
        });
    }

    /**
     * Deletes a post hug.
     */
    private void deletePostHug() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Delete the post hug.
        this.socialPostHug.deleteInBackground(new DeleteCallback() {

            @Override
            public void done(ParseException e) {

                // Check for an exception.
                if (e != null) {

                    ApplicationManager.getInstance().showConnectionError(context);
                } else {

                    // Update the post.
                    postModel.setHugCount(postModel.getHugCount() - 1);

                    // Delete the notification.
                    PostNotificationModel.DeleteSocialNotification(context, postModel, NotificationSubType.HUG);

                    // Remove the post hug.
                    ApplicationManager.getInstance().removePostHug(postModel.getObjectId());

                    // Clear the post hug.
                    socialPostHug = null;

                    // Update the listener.
                    updateListener();
                }
            }
        });
    }

    /**
     * Creates the post me too.
     */
    private void createPostMeToo() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Create the PostMeToo.
        final PostMeTooModel postMeToo = new PostMeTooModel();
        postMeToo.setPostModel(this.postModel);
        postMeToo.setPostUser(ParseUser.getCurrentUser());

        // Save the PostMeToo
        postMeToo.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                // Check for an exception.
                if (e != null) {

                    ApplicationManager.getInstance().showConnectionError(context);
                } else {

                    // Update the post.
                    postModel.setMeTooCount(postModel.getMeTooCount() + 1);
                    socialPostMeToo = postMeToo;

                    // Create the notification.
                    PostNotificationModel.CreateSocialNotification(context, postModel, NotificationSubType.METOO);

                    // Add the post me too.
                    ArrayList<PostMeTooModel> postMeToos = new ArrayList<PostMeTooModel>();
                    postMeToos.add(postMeToo);
                    ApplicationManager.getInstance().addPostMeToos(postMeToos);

                    // Update the listener.
                    updateListener();
                }
            }
        });
    }

    /**
     * Deletes a post me too.
     */
    private void deletePostMeToo() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Delete the post like.
        this.socialPostMeToo.deleteInBackground(new DeleteCallback() {

            @Override
            public void done(ParseException e) {

                // Check for an exception.
                if (e != null) {

                    ApplicationManager.getInstance().showConnectionError(context);
                } else {

                    // Update the post.
                    postModel.setMeTooCount(postModel.getMeTooCount() - 1);

                    // Delete the notification.
                    PostNotificationModel.DeleteSocialNotification(context, postModel, NotificationSubType.METOO);

                    // Remove the post me too.
                    ApplicationManager.getInstance().removePostMeToo(postModel.getObjectId());

                    // Clear the post me too.
                    socialPostMeToo = null;

                    // Update the listener.
                    updateListener();
                }
            }
        });
    }

    /**
     * Updates the listener.
     */
    private void updateListener() {

        // Check for a valid listener.
        if (this.listener == null) return;

        // Call the callback.
        this.listener.UpdatePostUserSocialStatus(this.postModel, this.hasPostLike(), this.hasPostHug(), this.hasPostMeToo(), true);
    }

}
