package com.nami.airapp.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.nami.airapp.R;
import com.nami.airapp.data.models.*;

/**
 * Class containing logic for the view in the Note List
 */
public class NoteListItemView extends LinearLayout {

	// Private Members.
	private NoteListItemViewContainer viewContainer = null;
    private Context context;

    /**
     * Default Constructor.
     * @param context Context which the view runs in.
     */
	public NoteListItemView(Context context) {
		super(context);
		
		// Create the view.
		LayoutInflater.from(context).inflate(R.layout.view_note_list_item, this);

        // Set the context.
        this.context = context;
		
		// Create the view container.
		this.viewContainer = NoteListItemViewContainer.with(this);
	}

    /**
     * Updates the model.
     * @param model The model to update.
     * @param position The position of the model in the dataset.
     */
	public void updateFromModel(NoteModel model, int position) {
		
		// Set the note text.
        this.viewContainer.checkBoxNoteText.setText(model.getName());
	}

    /**
     * Gets the checkbox.
     * @return Checkbox.
     */
    public CheckBox getCheckBox() {

        return this.viewContainer.checkBoxNoteText;
    }

   
    /**
     * Holds the view for the Post Feed Item.
     */
	private static class NoteListItemViewContainer {

		// Private Members.
        private CheckBox checkBoxNoteText;

		// Private Constructor.
		private NoteListItemViewContainer() {}

		// Static Constructor.
		public static NoteListItemViewContainer with(View view) {

			// Create the instance
			NoteListItemViewContainer instance = new NoteListItemViewContainer();

            // Get the items.
            instance.checkBoxNoteText = (CheckBox)view.findViewById(R.id.note_list_checkbox);

			return instance;
		}
	}
	
}
