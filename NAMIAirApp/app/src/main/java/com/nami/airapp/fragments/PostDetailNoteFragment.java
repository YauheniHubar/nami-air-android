package com.nami.airapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import java.util.ArrayList;

import com.nami.airapp.data.models.PostNotificationModel;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.KeyboardManager;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import com.nami.airapp.R;
import com.nami.airapp.data.adapters.NoteAdapter;
import com.nami.airapp.data.models.NoteModel;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.data.models.PostNoteModel;
import com.nami.airapp.interfaces.NoteSavedListener;
import com.nami.airapp.interfaces.ListItemSelectedListener;
import com.nami.airapp.utils.AppFragmentManager;



/**
 * Post Detail Note Fragment.
 */
public class PostDetailNoteFragment extends ListFragment implements ListItemSelectedListener {

    // Private Static Members.
    private static final String KEY_FRAGMENT_POST_DETAIL = "KEY_FRAGMENT_POST_DETAIL";
    private static final int MENU_DONE = Menu.FIRST;

    // Private Members.
    private Context context = null;
    private NoteSavedListener noteSavedListener;
    private PostModel postModel = null;
    private boolean doneMenuItemVisible = false;

    /**
     * Default Constructor.
     */
    public PostDetailNoteFragment() {}

    /**
     * Creates a new instance of a PostDetailNoteFragment.
     * @param noteSavedListener The note saved listener.
     * @param postModel The post model.
     * @return New instance of a PostDetailNoteFragment.
     */
    public static PostDetailNoteFragment newInstance(NoteSavedListener noteSavedListener, PostModel postModel) {

        // Create the new instance.
        PostDetailNoteFragment instance = new PostDetailNoteFragment();

        // Set the members.
        instance.noteSavedListener = noteSavedListener;

        // Add the post model to the bundle.
        Bundle args = new Bundle();
        args.putParcelable(KEY_FRAGMENT_POST_DETAIL, postModel);
        instance.setArguments(args);

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            this.postModel = args.getParcelable(KEY_FRAGMENT_POST_DETAIL);
        }
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set options menu.
        setHasOptionsMenu(true);

		// Create and set the adapter.
        setListAdapter(new NoteAdapter(context, this));

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_note, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    AppFragmentManager.RemoveFromStack(that, context);
                    return true;
                }

                return false;
            }
        });

        // Hide the keyboard.
        KeyboardManager.HideActivityKeyboard(context);

        // Initialize the layout content.
        this.initializeLayoutContent(view);
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.post_feed, menu);

        // Create the done menu item.
        if (this.doneMenuItemVisible) {
            MenuItem doneMenuItem = menu.add(Menu.NONE, MENU_DONE, Menu.NONE, getString(R.string.post_detail_note_menu_done).toString());
            MenuItemCompat.setShowAsAction(doneMenuItem, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
            doneMenuItem.setIcon(R.drawable.ic_menu_check);
        }
    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                this.cancelPostNotes();
                return true;
            case MENU_DONE:
                this.savePostNotes();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when a list item is selected.
     * @param model
     * @param selected
     */
    public void listItemSelected(Object model, boolean selected) {

        // Get the adapter.
        NoteAdapter adapter = (NoteAdapter)getListAdapter();

        // Loop through the notes.
        boolean isSelected = false;
        for (NoteModel noteModel : adapter.getDataSet()) {

            // Add the note.
            if (noteModel.getIsSelected()) {
                isSelected = true;
                break;
            }
        }

        // Enable the menu item if any are selected.
        if (isSelected && this.doneMenuItemVisible == false) {
            this.doneMenuItemVisible = true;
            ActivityCompat.invalidateOptionsMenu((Activity)this.context);
        } else if (isSelected == false && this.doneMenuItemVisible == true) {
            this.doneMenuItemVisible = false;
            ActivityCompat.invalidateOptionsMenu((Activity)this.context);
        }
    }

    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.post_detail_note_title));
    }

    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

       
    }

    /**
     * Cancels the post notes.
     */
    private void cancelPostNotes() {

        // Get the adapter.
        NoteAdapter adapter = (NoteAdapter)getListAdapter();

        // Loop through the notes and set as not selected.
        for (NoteModel noteModel : adapter.getDataSet()) {

            // Clear the selected flag.
            noteModel.setIsSelected(false);
        }

        AppFragmentManager.RemoveFromStack(this, context);
    }

    /**
     * Saves the post notes.
     */
    private void savePostNotes() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Get the adapter.
        NoteAdapter adapter = (NoteAdapter)getListAdapter();

        // Get the notes that are selected.
        ArrayList<NoteModel> selectedNotes = new ArrayList<NoteModel>();
        final ArrayList<String> selectedNoteValues = new ArrayList<String>();
        for (NoteModel noteModel : adapter.getDataSet()) {

            // Add the note.
            if (noteModel.getIsSelected()) {
                selectedNotes.add(noteModel);
                selectedNoteValues.add(noteModel.getName());
            }

            // Clear the selected flag.
            noteModel.setIsSelected(false);
        }

        // Return if there are no selected notes.
        if (selectedNotes.size() == 0) {

            AppFragmentManager.RemoveFromStack(this, context);
            return;
        }

        // Create the PostNote model.
        PostNoteModel postNoteModel = new PostNoteModel();
        postNoteModel.setPostModel(this.postModel);
        postNoteModel.setPostUser(ParseUser.getCurrentUser());
        postNoteModel.setPostNotes(selectedNoteValues);

        // Reference the fragment.
        final Fragment that = this;

        // Save the PostNote model.
        postNoteModel.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {

                // Check for an exception.
                if (e != null) {

                    ApplicationManager.getInstance().showConnectionError(context);
                } else {

                    // Notify the listener.
                    if (noteSavedListener != null)
                        noteSavedListener.noteSaved();

                    // Create the note notification.
                    PostNotificationModel.CreateNoteNotification(context, postModel);

                    // Update the post notes.
                    PostModel.UpdatePostNotes(context, postModel, selectedNoteValues);

                    // Remove from the stack.
                    AppFragmentManager.RemoveFromStack(that, context);
                }
            }
        });

    }
}
