package com.nami.airapp.interfaces;

/**
 * Created by Brian on 12/8/2014.
 */
public interface AuthorBlockListener {


    public void authorBlocked(String objectId);
}
