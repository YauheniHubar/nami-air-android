package com.nami.airapp.data.models;

import com.parse.ParseObject;
import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import com.nami.airapp.data.constants.RedFlagKeywordModelConstants;
/**
 * Contains data about a RedFlagKeyword.
 */
@ParseClassName("RedFlagKeyword")
public class RedFlagKeywordModel extends ParseObject {

    // Private Members.
    private boolean isSelected;

    /**
     * Gets the note name.
     * @return RedFlagKeyword name.
     */
    public String getName() { return getString(RedFlagKeywordModelConstants.FieldName); }

    /**
     * Sets the note name.
     * @param value RedFlagKeyword name.
     */
    public void setName(String value) { put(RedFlagKeywordModelConstants.FieldName, value); }

	
	/**
     * Creates a query adapter to fetch the notes.
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<RedFlagKeywordModel> createRedFlagKeywordAdapterQuery() {


        return new ParseQueryAdapter.QueryFactory<RedFlagKeywordModel>() {

            @Override
            public ParseQuery<RedFlagKeywordModel> create() {

                // Create the query.
                ParseQuery<RedFlagKeywordModel> query = new ParseQuery<RedFlagKeywordModel>(
                        RedFlagKeywordModel.class);                

                return query;
            }
        };
    }
}
