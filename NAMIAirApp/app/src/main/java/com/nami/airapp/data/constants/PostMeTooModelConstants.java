package com.nami.airapp.data.constants;

/**
 * Holds constants for the PostMeToo model.
 */
public class PostMeTooModelConstants {

    public static final String FieldPost = "post";
    public static final String FieldUser = "user";
}
