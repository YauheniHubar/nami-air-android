package com.nami.airapp.views;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.*;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.util.AttributeSet;
import android.widget.ToggleButton;

import com.nami.airapp.R;
import com.nami.airapp.data.enums.ApprovedStatus;
import com.nami.airapp.data.models.*;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.fragments.PostComposeFragment;
import com.nami.airapp.utils.DateFormatter;
import com.nami.airapp.utils.PostManager;
import com.parse.ParseUser;

/**
 * Class containing logic for the view in the Post Feed.
 */
public class PostFeedItemView extends LinearLayout implements OnClickListener, PostUserSocialStatus.PostUserSocialStatusListener {

	// Private Members.
	private PostFeedItemViewContainer viewContainer = null;
    private PostUserSocialStatus postUserSocialStatus = null;
    private PostModel postModel = null;
    private Fragment fragment = null;
    private Context context;

    /**
     * Default Constructor.
     * @param context Context which the view runs in.
     */
	public PostFeedItemView(Context context) {
		super(context);
		
		initialize(context);
	}

    /**
     * Default Constructor.
     * @param context Context which the view runs in.
     */
    public PostFeedItemView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initialize(context);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (view == this.viewContainer.buttonPostLike) {
            this.viewContainer.buttonPostLike.setSelected(true);
            this.togglePostLike();
        } else if (view == this.viewContainer.buttonPostHug) {
            this.viewContainer.buttonPostHug.setSelected(true);
            this.togglePostHug();
        } else if (view == this.viewContainer.buttonPostMeToo) {
            this.viewContainer.buttonPostMeToo.setSelected(true);
            this.togglePostMeToo();
        }
    }

    /**
     * Initializes the view.
     * @param context
     */
    public void initialize(Context context) {

        // Create the view.
        LayoutInflater.from(context).inflate(R.layout.view_post_feed_item, this);

        // Set the context.
        this.context = context;

        // Create the view container.
        this.viewContainer = PostFeedItemViewContainer.with(this);
    }

    /**
     * Updates the model.
     * @param fragment The current fragment.
     * @param model The model to update.
     * @param position The position of the model in the dataset.
     */
	public void updateFromModel(Fragment fragment, PostModel model, int position) {

        // Set the post model and fragment.
        this.postModel = model;
        this.fragment = fragment;

        // Create the social status.
        this.postUserSocialStatus = new PostUserSocialStatus(this.context, this, model);


        // Add the click events.
        if (model.getUser() != null && TextUtils.equals(model.getUser().getObjectId(), ParseUser.getCurrentUser().getObjectId())) {

            this.viewContainer.buttonPostLike.setOnClickListener(null);
            this.viewContainer.buttonPostHug.setOnClickListener(null);
            this.viewContainer.buttonPostMeToo.setOnClickListener(null);
            this.viewContainer.buttonPostLike.setClickable(false);
            this.viewContainer.buttonPostHug.setClickable(false);
            this.viewContainer.buttonPostMeToo.setClickable(false);
            this.viewContainer.buttonPostLike.setEnabled(false);
            this.viewContainer.buttonPostHug.setEnabled(false);
            this.viewContainer.buttonPostMeToo.setEnabled(false);

            this.viewContainer.buttonPostMeToo.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
            this.viewContainer.buttonPostHug.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
            this.viewContainer.buttonPostMeToo.setTextAppearance(this.context, R.style.post_item_button_social_text_off);

        } else {

            this.viewContainer.buttonPostLike.setOnClickListener(this);
            this.viewContainer.buttonPostHug.setOnClickListener(this);
            this.viewContainer.buttonPostMeToo.setOnClickListener(this);
            this.viewContainer.buttonPostLike.setClickable(true);
            this.viewContainer.buttonPostHug.setClickable(true);
            this.viewContainer.buttonPostMeToo.setClickable(true);
            this.viewContainer.buttonPostLike.setEnabled(true);
            this.viewContainer.buttonPostHug.setEnabled(true);
            this.viewContainer.buttonPostMeToo.setEnabled(true);
        }

        // Update the post.
        this.updatePostText(fragment, model, position);
        this.updatePostSocialContent(model);
	}

    /**
     * Updates the post text.
     * @param fragment The current fragment.
     * @param model The model to get the post text from.
     * @param position
     */
	private void updatePostText(Fragment fragment, PostModel model, int position) {

        // Create the text.
        SpannableString text = new SpannableString(model.getPostText());

        // Set the post styles.
        PostManager.setPostStyle(context, text);
        PostManager.setPostFirstSentenceStyle(context, text);
        PostManager.setPostHashTagsStyle(context, fragment, text);
        PostManager.setPostFeelingsStyle(context, text);
        PostManager.setPostPaddingCorners(context, this.viewContainer.layoutPostFeedLayout, this.viewContainer.layoutPostFeedItemContainer, position);
        this.setPostDate(model);

        // Set the counts.
        this.viewContainer.textViewPostLikeCount.setText(String.valueOf(model.getLikeCount()));
        this.viewContainer.textViewPostHugCount.setText(String.valueOf(model.getHugCount()));
        this.viewContainer.textViewPostMeTooCount.setText(String.valueOf(model.getMeTooCount()));
        this.viewContainer.textViewPostNoteCount.setText(String.valueOf(model.getNoteCount()));
        this.viewContainer.textViewPostInspiredByCount.setText(String.valueOf(model.getMeTooStoryCount()));

        // Set the post text.
        this.viewContainer.textViewPostText.setLineSpacing(4,1);
        this.viewContainer.textViewPostText.setText(text, BufferType.SPANNABLE);

        // Show / hide the not approved message.
        if (model.getApprovedStatus() == ApprovedStatus.APPROVED) {

            this.viewContainer.textViewPostNotApproved.setVisibility(View.GONE);
            this.viewContainer.textViewPostDisapproved.setVisibility(View.GONE);
            this.viewContainer.layoutPostFeedItemSpacer.setVisibility(View.VISIBLE);
        } else {

            if (model.getApprovedStatus() == ApprovedStatus.NOTAPPROVED) {
                this.viewContainer.textViewPostNotApproved.setVisibility(View.VISIBLE);
                this.viewContainer.textViewPostDisapproved.setVisibility(View.GONE);
            } else {
                this.viewContainer.textViewPostNotApproved.setVisibility(View.GONE);
                this.viewContainer.textViewPostDisapproved.setVisibility(View.VISIBLE);
            }

            this.viewContainer.layoutPostFeedItemSpacer.setVisibility(View.GONE);
        }

        // Show / hide the flagged message.
        // Set the flagged status.
        PostFlagModel flaggedPost = ApplicationManager.getInstance().findPostFlaggedStatus(model);
        PostFlagModel moderatorFlaggedPost = ApplicationManager.getInstance().findModeratorPostFlaggedStatus(model);
        if (false /*moderatorFlaggedPost != null*/) {

            // Create the flagged text.
            String flaggedText = "";
            switch (moderatorFlaggedPost.getFlaggedStatus()) {
                case INAPPROPRIATE:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_inappropriate);
                    break;
                case SELF_HARM:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_self_harm);
                    break;
                case THREATENING:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_threatening);
                    break;
                case BULLYING:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_bullying);
                    break;
                case SPAM:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_spam);
                    break;
            }

            // Create the flagged text sentence.
            String flaggedTextSentence = String.format(this.context.getString(R.string.post_detail_moderator_flag_text), flaggedText);

            // Set the flagged text.
            this.viewContainer.textViewPostFlaggedText.setText(flaggedTextSentence);

            // Show the flagged layout.
            this.viewContainer.layoutPostFeedItemFlagged.setVisibility(View.VISIBLE);
            this.viewContainer.layoutPostFeedItemSpacer.setVisibility(View.GONE);
            this.viewContainer.textViewPostNotApproved.setVisibility(View.GONE);
            this.viewContainer.textViewPostDisapproved.setVisibility(View.GONE);

        } else if (flaggedPost != null) {

            // Create the flagged text.
            String flaggedText = "";
            switch (flaggedPost.getFlaggedStatus()) {
                case INAPPROPRIATE:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_inappropriate);
                    break;
                case SELF_HARM:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_self_harm);
                    break;
                case THREATENING:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_threatening);
                    break;
                case BULLYING:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_bullying);
                    break;
                case SPAM:
                    flaggedText = this.context.getString(R.string.post_detail_flag_text_spam);
                    break;
            }

            // Create the flagged text sentence.
            String flaggedTextSentence = String.format(this.context.getString(R.string.post_detail_flag_text), flaggedText);

            // Set the flagged text.
            this.viewContainer.textViewPostFlaggedText.setText(flaggedTextSentence);

            // Show the flagged layout.
            this.viewContainer.layoutPostFeedItemFlagged.setVisibility(View.VISIBLE);
            this.viewContainer.layoutPostFeedItemSpacer.setVisibility(View.GONE);


        } else {

            // Hide the flagged layout.
            this.viewContainer.layoutPostFeedItemFlagged.setVisibility(View.GONE);
            this.viewContainer.layoutPostFeedItemSpacer.setVisibility(View.VISIBLE);
        }

        this.viewContainer.textViewPostFlaggedText.setVisibility(View.GONE);

        if (position % 2 == 0) {
            viewContainer.layoutContainerSpacerLeft.setVisibility(View.VISIBLE);
            viewContainer.layoutContainerSpacerRight.setVisibility(View.GONE);
        } else {
            viewContainer.layoutContainerSpacerLeft.setVisibility(View.GONE);
            viewContainer.layoutContainerSpacerRight.setVisibility(View.VISIBLE);
        }
	}

    /**
     * Updates the social content.
     * @param model
     */
    private void updatePostSocialContent(PostModel model) {

        // Determine if it is the user's own post.
        if (model.getUser() != null && TextUtils.equals(model.getUser().getObjectId(), ParseUser.getCurrentUser().getObjectId())) {

            this.viewContainer.buttonPostLike.setChecked(false);
            this.viewContainer.buttonPostHug.setChecked(false);
            this.viewContainer.buttonPostMeToo.setChecked(false);

            this.viewContainer.buttonPostLike.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
            this.viewContainer.buttonPostHug.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
            this.viewContainer.buttonPostMeToo.setTextAppearance(this.context, R.style.post_item_button_social_text_off);

        } else {


            // Determine if the user has social interaction with the post.
            if (ApplicationManager.getInstance().hasPostLike(model.getObjectId()) != null) {
                this.viewContainer.buttonPostLike.setChecked(true);
                this.viewContainer.buttonPostLike.setTextAppearance(this.context, R.style.post_item_button_social_text_on);
            } else {
                this.viewContainer.buttonPostLike.setChecked(false);
                this.viewContainer.buttonPostLike.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
            }
            if (ApplicationManager.getInstance().hasPostHug(model.getObjectId()) != null) {
                this.viewContainer.buttonPostHug.setChecked(true);
                this.viewContainer.buttonPostHug.setTextAppearance(this.context, R.style.post_item_button_social_text_on);
            } else {
                this.viewContainer.buttonPostHug.setChecked(false);
                this.viewContainer.buttonPostHug.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
            }
            if (ApplicationManager.getInstance().hasPostMeToo(model.getObjectId()) != null) {
                this.viewContainer.buttonPostMeToo.setChecked(true);
                this.viewContainer.buttonPostMeToo.setTextAppearance(this.context, R.style.post_item_button_social_text_on);
            } else {
                this.viewContainer.buttonPostMeToo.setChecked(false);
                this.viewContainer.buttonPostMeToo.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
            }
        }

        this.viewContainer.buttonPostLike.setText(String.valueOf(model.getLikeCount()));
        this.viewContainer.buttonPostHug.setText(String.valueOf(model.getHugCount()));
        this.viewContainer.buttonPostMeToo.setText(String.valueOf(model.getMeTooCount()));
    }

    /**
     * Sets the post date.
     * @param model Post model.
     */
    private void setPostDate(PostModel model) {

        // Set the date.
        String date = DateFormatter.FormatPostDate(model.getPostDate(), this.context);
        this.viewContainer.textViewPostDate.setText(date);
    }

    /**
     * Toggles the post like.
     */
    private void togglePostLike() {

        // Toggle the status.
        this.viewContainer.buttonPostLike.setEnabled(false);
        this.postUserSocialStatus.togglePostLike();
    }

    /**
     * Toggles the post hug.
     */
    private void togglePostHug() {

        // Toggle the status.
        this.viewContainer.buttonPostHug.setEnabled(false);
        this.postUserSocialStatus.togglePostHug();
    }

    /**
     * Toggles the post me too.
     */
    private void togglePostMeToo() {

        // Toggle the status.
        this.viewContainer.buttonPostMeToo.setEnabled(false);
        this.postUserSocialStatus.togglePostMeToo();

        // Create the dialog.
        final Dialog relateDialog = new Dialog(context);
        relateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        relateDialog.setContentView(R.layout.dialog_post_relate);

        // Get the views.
        Button relateDialogNoButton = (Button)relateDialog.findViewById(R.id.dialog_post_relate_button_no);
        Button relateDialogYesButton = (Button)relateDialog.findViewById(R.id.dialog_post_relate_button_yes);

        // Add the listeners.
        relateDialogNoButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {

                // Dismiss.
                relateDialog.dismiss();
            }
        });
        relateDialogYesButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {

                // Dismiss.
                relateDialog.dismiss();

                // Show the compose post.
                showComposeFragment(postModel);
            }
        });

        // Show the dialog.
        relateDialog.show();
    }

    /**
     * Shows the compose fragment.
     * @param postModel
     */
    private void showComposeFragment(PostModel postModel) {

        // Create the fragment.
        PostComposeFragment postComposeFragment = PostComposeFragment.newInstance(postModel);

        // Create the transaction.
        FragmentTransaction transaction = this.fragment.getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, postComposeFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Updates the content based on the social status.
     * @param socialPostModel The post model.
     * @param hasLike Whether the user has liked a post.
     * @param hasHug Whether the user has hugged a post.
     * @param hasMeToo Whether the user has me tooed a post.
     */
    public void UpdatePostUserSocialStatus(PostModel socialPostModel, boolean hasLike, boolean hasHug, boolean hasMeToo, boolean updateContent)
    {

        // Determine if it is the user's own post.
        if (socialPostModel.getUser() != null && TextUtils.equals(socialPostModel.getUser().getObjectId(), ParseUser.getCurrentUser().getObjectId())) {

            this.viewContainer.buttonPostLike.setChecked(false);
            this.viewContainer.buttonPostHug.setChecked(false);
            this.viewContainer.buttonPostMeToo.setChecked(false);

            this.viewContainer.buttonPostLike.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
            this.viewContainer.buttonPostHug.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
            this.viewContainer.buttonPostMeToo.setTextAppearance(this.context, R.style.post_item_button_social_text_off);

        } else {


            // Set the like button state.
            if (hasLike) {
                this.viewContainer.buttonPostLike.setChecked(true);
                this.viewContainer.buttonPostLike.setTextAppearance(this.context, R.style.post_item_button_social_text_on);
            } else {
                this.viewContainer.buttonPostLike.setChecked(false);
                this.viewContainer.buttonPostLike.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
            }

            // Set the hug button state.
            if (hasHug) {
                this.viewContainer.buttonPostHug.setChecked(true);
                this.viewContainer.buttonPostHug.setTextAppearance(this.context, R.style.post_item_button_social_text_on);
            } else {
                this.viewContainer.buttonPostHug.setChecked(false);
                this.viewContainer.buttonPostHug.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
            }

            // Set the me too button state.
            if (hasMeToo) {
                this.viewContainer.buttonPostMeToo.setChecked(true);
                this.viewContainer.buttonPostMeToo.setTextAppearance(this.context, R.style.post_item_button_social_text_on);
            } else {
                this.viewContainer.buttonPostMeToo.setChecked(false);
                this.viewContainer.buttonPostMeToo.setTextAppearance(this.context, R.style.post_item_button_social_text_off);
            }

            this.viewContainer.buttonPostLike.setEnabled(true);
            this.viewContainer.buttonPostHug.setEnabled(true);
            this.viewContainer.buttonPostMeToo.setEnabled(true);
        }


        // Update the post content.
        if (updateContent)
            this.updatePostSocialContent(socialPostModel);
    }

    /**
     * Holds the view for the Post Feed Item.
     */
	private static class PostFeedItemViewContainer {

		// Private Members.
        private LinearLayout layoutPostFeedLayout = null;
        private LinearLayout layoutPostFeedItemContainer = null;
        private LinearLayout layoutPostFeedItemFlagged = null;
        private LinearLayout layoutPostFeedItemSpacer = null;
        private LinearLayout textViewPostNotApproved = null;
        private LinearLayout textViewPostDisapproved = null;
        private View layoutContainerSpacerLeft = null;
        private View layoutContainerSpacerRight = null;
        private TextView textViewPostText = null;
        private TextView textViewPostLikeCount = null;
        private TextView textViewPostHugCount = null;
        private TextView textViewPostMeTooCount = null;
        private TextView textViewPostNoteCount = null;
        private TextView textViewPostInspiredByCount = null;
        private TextView textViewPostDate = null;
        private TextView textViewPostFlaggedText = null;

        private ToggleButton buttonPostLike = null;
        private ToggleButton buttonPostHug = null;
        private ToggleButton buttonPostMeToo = null;

		// Private Constructor.
		private PostFeedItemViewContainer() {}

		// Static Constructor.
		public static PostFeedItemViewContainer with(View view) {

			// Create the instance
			PostFeedItemViewContainer instance = new PostFeedItemViewContainer();

            // Get the items.
            instance.layoutPostFeedLayout = (LinearLayout)view.findViewById(R.id.post_feed_item_layout);
            instance.layoutPostFeedItemContainer = (LinearLayout)view.findViewById(R.id.post_feed_item_container);
            instance.layoutPostFeedItemFlagged = (LinearLayout)view.findViewById(R.id.post_feed_item_flagged);
            instance.layoutPostFeedItemSpacer = (LinearLayout)view.findViewById(R.id.post_feed_item_spacer);
            instance.textViewPostNotApproved = (LinearLayout)view.findViewById(R.id.post_feed_item_not_approved);
            instance.textViewPostDisapproved = (LinearLayout)view.findViewById(R.id.post_feed_item_disapproved);
            instance.textViewPostText = (TextView)view.findViewById(R.id.post_feed_item_post_text);
            instance.textViewPostLikeCount = (TextView)view.findViewById(R.id.post_feed_item_like_count);
            instance.textViewPostHugCount = (TextView)view.findViewById(R.id.post_feed_item_hug_count);
            instance.textViewPostMeTooCount = (TextView)view.findViewById(R.id.post_feed_item_metoo_count);
            instance.textViewPostNoteCount = (TextView)view.findViewById(R.id.post_feed_item_note_count);
            instance.textViewPostInspiredByCount = (TextView)view.findViewById(R.id.post_feed_item_inspired_by_count);
            instance.textViewPostDate = (TextView)view.findViewById(R.id.post_feed_item_date);
            instance.textViewPostFlaggedText = (TextView)view.findViewById(R.id.post_feed_item_flagged_text);

            instance.buttonPostLike = (ToggleButton)view.findViewById(R.id.post_feed_button_like);
            instance.buttonPostHug = (ToggleButton)view.findViewById(R.id.post_feed_button_hug);
            instance.buttonPostMeToo = (ToggleButton)view.findViewById(R.id.post_feed_button_me_too);

            instance.layoutContainerSpacerLeft = (View)view.findViewById(R.id.post_feed_item_container_spacer_left);
            instance.layoutContainerSpacerRight = (View)view.findViewById(R.id.post_feed_item_container_spacer_right);

			return instance;
		}
	}
	
}
