package com.nami.airapp.activities;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.Canvas;

import com.nami.airapp.R;
import com.nami.airapp.data.adapters.PostFeedAdapter;
import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.enums.FeedType;
import com.nami.airapp.data.enums.UserType;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.fragments.GuidedTourFragment;
import com.nami.airapp.handlers.PostXmlHandler;
import com.nami.airapp.utils.ListViewScrollTracker;
import com.nami.airapp.widgets.SwipeOutViewPager;
import com.nami.airapp.utils.ActionBarManager;
import com.parse.ParseUser;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class GuidedTourActivity extends ActionBarActivity implements SwipeOutViewPager.OnSwipeOutListener {

    // Private Static Members
    private static int NUM_PAGES = 6;

    // Private Members
    private GuidedTourViewContainer viewContainer = null;
    private PostFeedAdapter adapter = null;
    private PagerAdapter pagerAdapter;
    private ListViewScrollTracker scrollTracker = null;
    private ArrayList<PostModel> postItems = null;
    private ArrayList<View> pagerIndicators = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the requested orientation.
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Initialize the activity and action bar.
        this.initializeActivity();
        this.initializeActionBar();

        // Set the content view.
        setContentView(R.layout.activity_guided_tour);

        // Create the view container.
        this.viewContainer = GuidedTourViewContainer.with(this);

        // Load the sample posts.
        this.loadSamplePosts();

        // Instantiate a ViewPager and a PagerAdapter.
        this.pagerAdapter = new GuidedTourPagerAdapter(getSupportFragmentManager());
        this.viewContainer.viewPager.setAdapter(this.pagerAdapter);

        // Create the data adapter.
        this.adapter = new PostFeedAdapter(null, this, FeedType.ALL_POSTS, UserType.USER, false);
        this.adapter.setDataSet(this.postItems);

        // Set the adapter.
        this.viewContainer.listView.setAdapter(this.adapter);

        // Create the scroll tracker.
        this.scrollTracker = new ListViewScrollTracker(this.viewContainer.listView);

        // Handle the offsets.
        this.viewContainer.listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int incrementalOffset = scrollTracker.calculateIncrementalOffset(firstVisibleItem, visibleItemCount);
            }
        });

        // Create the pager indicators.
        this.createPagerIndicators();

        // Set the swipeout listener.
        this.viewContainer.viewPager.setSwipeOutListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(new CalligraphyContextWrapper(newBase));
    }

    @Override
    public void onBackPressed() {
        if (this.viewContainer.viewPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            this.viewContainer.viewPager.setCurrentItem(this.viewContainer.viewPager.getCurrentItem() - 1);
        }
    }


    /**
     Initializes the activity.
     */
    private void initializeActivity() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.hide();
    }

    /**
     * Loads the sample posts.
     */
    private void loadSamplePosts() {

        // Get the asset manager.
        AssetManager assetManager = getBaseContext().getAssets();

        try
        {
            // Create the input stream and reader.
            InputStream inputStream = assetManager.open("xml/postSample.xml");
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser parser = parserFactory.newSAXParser();
            XMLReader reader = parser.getXMLReader();

            // Create the handler and parse the contents.
            PostXmlHandler xmlHandler = new PostXmlHandler();
            reader.setContentHandler(xmlHandler);
            InputSource inputSource = new InputSource(inputStream);
            reader.parse(inputSource);

            // Set the items.
            this.postItems = xmlHandler.getItems();

            // Close the stream.
            inputStream.close();

        }
        catch (Exception e) {
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Gets the view at the position.
     * @param position
     * @return
     */
    public View getViewByPosition(int position) {
        final int firstListItemPosition = this.viewContainer.listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + this.viewContainer.listView.getChildCount() - 1;

        if (position < firstListItemPosition || position > lastListItemPosition ) {
            return this.viewContainer.listView.getAdapter().getView(position, null, this.viewContainer.listView);
        } else {
            final int childIndex = position - firstListItemPosition;
            return this.viewContainer.listView.getChildAt(childIndex);
        }
    }

    /**
     * Gets the last visible view item.
     * @return
     */
    public int getLastVisibleViewItem() {

        if (this.scrollTracker.getPositions().size() <= 2)
            return this.scrollTracker.getPositions().size() - 1;

        return this.scrollTracker.getPositions().size() - 2;
    }

    /**
     * Closes the guided tour and starts the main activity.
     */
    public void closeGuidedTour() {

        // Set to have shown the guided tour.
        ApplicationManager.getInstance().setShownGuidedTour(this);

        // Create and start the activity.
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Called when the view pager swipes past the last page.
     */
    public void onSwipeOutAtEnd() {

        // Close the guided tour.
        this.closeGuidedTour();
    }

    /**
     * Creates the pager indicators.
     */
    private void createPagerIndicators() {

        // Create the view list.
        this.pagerIndicators = new ArrayList<View>();

        // Get the page indicator size and margin.
        int pageIndicatorSize = (int)getResources().getDimension(R.dimen.page_indicator_size);
        int pageIndicatorMargin = (int)getResources().getDimension(R.dimen.page_indicator_margin);

        // Create the first and last views.
        LinearLayout startView = new LinearLayout(this);
        LinearLayout endView = new LinearLayout(this);
        startView.setLayoutParams(new LinearLayout.LayoutParams(0, 0, 1f));
        endView.setLayoutParams(new LinearLayout.LayoutParams(0, 0, 1f));

        // Add the first view.
        this.pagerIndicators.add(startView);

        // Loop through the number of pages.
        for (int index = 0; index < NUM_PAGES; index++) {

            // Create the view.
            LinearLayout pagerIndicator = new LinearLayout(this);
            LinearLayout.LayoutParams pagerIndicatorParams = new LinearLayout.LayoutParams(pageIndicatorSize, (pageIndicatorSize));
            pagerIndicatorParams.gravity = Gravity.CENTER;
            pagerIndicatorParams.setMargins(pageIndicatorMargin, 0, pageIndicatorMargin, 0);
            pagerIndicator.setLayoutParams(pagerIndicatorParams);
            pagerIndicator.setBackgroundResource(R.drawable.page_indicator_off);

            // Add the view.
            this.pagerIndicators.add(pagerIndicator);
        }

        // Add the last view.
        this.pagerIndicators.add(endView);

        // Loop through the views and add to the layout.
        for (int index = 0; index < this.pagerIndicators.size(); index++) {
            this.viewContainer.layoutPagerIndicator.addView(this.pagerIndicators.get(index));
        }
    }

    /**
     * Sets the selected page.
     * @param page
     */
    private void setSelectedPage(int page) {

        // Loop through the views.
        for (int index = 1; index < this.pagerIndicators.size() - 1; index++) {

            if (index == page)
                this.pagerIndicators.get(index).setBackgroundResource(R.drawable.page_indicator_on);
            else this.pagerIndicators.get(index).setBackgroundResource(R.drawable.page_indicator_off);
        }
    }

    /**
     * Holds the view for the Post Feed Item.
     */
    private static class GuidedTourViewContainer {

        // Private Members.
        private LinearLayout layoutTabs = null;
        private SwipeOutViewPager viewPager = null;
        private ListView listView = null;
        private LinearLayout layoutPagerIndicator = null;

        // Private Constructor.
        private GuidedTourViewContainer() {}

        // Static Constructor.
        public static GuidedTourViewContainer with(Activity activity) {

            // Create the instance
            GuidedTourViewContainer instance = new GuidedTourViewContainer();

            // Get the items.
            instance.layoutTabs = (LinearLayout)activity.findViewById(R.id.guided_tour_tabs);
            instance.viewPager = (SwipeOutViewPager)activity.findViewById(R.id.guided_tour_pager);
            instance.listView = (ListView)activity.findViewById(R.id.guided_tour_list);
            instance.layoutPagerIndicator = (LinearLayout)activity.findViewById(R.id.guided_tour_pager_indicator);

            return instance;
        }
    }

    private class GuidedTourPagerAdapter extends FragmentStatePagerAdapter {
        public GuidedTourPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return GuidedTourFragment.newInstance(position);
        }

        @Override
        public void setPrimaryItem (ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);

            setSelectedPage(position + 1);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
