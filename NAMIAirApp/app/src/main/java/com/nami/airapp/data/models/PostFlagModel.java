package com.nami.airapp.data.models;

import com.nami.airapp.data.constants.PostModelConstants;
import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.enums.UserType;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import com.nami.airapp.data.constants.BaseModelConstants;
import com.nami.airapp.data.constants.PostFlagModelConstants;
import com.nami.airapp.data.enums.FlaggedStatus;


/**
 * Contains data about a Post Flag.
 */
@ParseClassName("PostFlag")
public class PostFlagModel extends ParseObject {

    /**
     * Gets the post model.
     * @return Post model.
     */
    public PostModel getPostModel() { return (PostModel)get(PostFlagModelConstants.FieldPost); }

    /**
     * Sets the post model.
     * @param postModel Post model.
     */
    public void setPostModel(PostModel postModel) { put(PostFlagModelConstants.FieldPost, postModel); }

    /**
     * Gets the post user.
     * @return ParseUser
     */
    public ParseUser getPostUser() { return (ParseUser)get(PostFlagModelConstants.FieldUser); }

    /**
     * Sets the post user.
     * @param postUser ParseUser
     */
    public void setPostUser(ParseUser postUser) { put(PostFlagModelConstants.FieldUser, postUser); }

    /**
     * Gets the Flagged Status.
     * @return FlaggedStatus enum.
     */
    public FlaggedStatus getFlaggedStatus() { return FlaggedStatus.fromValue(getInt(PostFlagModelConstants.FieldFlaggedStatus)); }

    /**
     * Sets the Flagged Status.
     * @param value FlaggedStatus enum.
     */
    public void setFlaggedStatus(FlaggedStatus value) { put(PostFlagModelConstants.FieldFlaggedStatus, value.getValue()); }

    /**
     * Creates a query adapter to fetch the flags.
     * @param postModel The Post model.
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<PostFlagModel> createPostFlagAdapterQuery(PostModel postModel) {

        // Declare the filters.
        final PostModel filterPostModel = postModel;

        return new ParseQueryAdapter.QueryFactory<PostFlagModel>() {

            @Override
            public ParseQuery<PostFlagModel> create() {

                // Create the query.
                ParseQuery<PostFlagModel> query = new ParseQuery<PostFlagModel>(PostFlagModel.class);

                // Set the filters.
                query.whereEqualTo(PostFlagModelConstants.FieldPost, filterPostModel);
                query.whereEqualTo(PostFlagModelConstants.FieldUser, ParseUser.getCurrentUser());

                return query;
            }
        };
    }

    /**
     * Creates a query adapter to fetch the flags.
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<PostFlagModel> createPostFlagAdapterQuery() {

        return new ParseQueryAdapter.QueryFactory<PostFlagModel>() {

            @Override
            public ParseQuery<PostFlagModel> create() {

                // Create the query.
                ParseQuery<PostFlagModel> query = new ParseQuery<PostFlagModel>(PostFlagModel.class);

                // Set the includes.
                query.include(PostFlagModelConstants.FieldPost);

                // Set the filters.
                query.whereEqualTo(PostFlagModelConstants.FieldUser, ParseUser.getCurrentUser());

                return query;
            }
        };
    }

    /**
     * Creates a query adapter to fetch the flags.
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<PostFlagModel> createModeratorPostFlagAdapterQuery() {

        return new ParseQueryAdapter.QueryFactory<PostFlagModel>() {

            @Override
            public ParseQuery<PostFlagModel> create() {

                // Create Moderator Query.
                ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
                userQuery.whereEqualTo(UserModelConstants.FieldUserType, UserType.MODERATOR.getValue());

                // Create the Post Query.
                ParseQuery<PostModel> postQuery = new ParseQuery<PostModel>(PostModel.class);
                postQuery.whereEqualTo(PostModelConstants.FieldUser, ParseUser.getCurrentUser());

                // Create the query.
                ParseQuery<PostFlagModel> query = new ParseQuery<PostFlagModel>(PostFlagModel.class);

                // Set the includes.
                query.include(PostFlagModelConstants.FieldPost);

                // Set the filters.
                query.whereMatchesQuery(PostFlagModelConstants.FieldUser, userQuery);
                query.whereMatchesQuery(PostFlagModelConstants.FieldPost, postQuery);

                return query;
            }
        };
    }
}
