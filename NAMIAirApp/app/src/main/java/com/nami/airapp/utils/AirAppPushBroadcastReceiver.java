package com.nami.airapp.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import org.json.JSONObject;

import com.parse.ParsePushBroadcastReceiver;

import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.singletons.ApplicationManager;

/**
 * Created by Brian on 11/25/2014.
 */
public class AirAppPushBroadcastReceiver extends ParsePushBroadcastReceiver {

    // Public Static Members.
    public static String KEY_PUSH_EXTRAS_KEY = "com.parse.Data";
    public static String KEY_PUSH_TYPE= "type";
    public static String KEY_PUSH_MODERATOR_TYPE_MODERATOR= "moderator";
    public static String KEY_PUSH_MODERATOR_MESSAGE_FIELD = "messageId";

    public static String KEY_PUSH_POPUP_TYPE_POPUP = "popup";
    public static String KEY_PUSH_POPUP_TITLE = "pt";
    public static String KEY_PUSH_POPUP_LINK_TITLE = "lt";
    public static String KEY_PUSH_POPUP_LINK_URL = "lu";


    @Override
    public void onPushReceive(Context context, Intent intent) {

        // Handle the push.
        HandlePush(context, intent);
    }

    @Override
    public void onPushOpen(Context context, Intent intent) {

        //Handle the push.
        HandlePush(context, intent);
    }

    /**
     * Handles the push notification.
     * @param intent
     * @return
     */
    public static void HandlePush(Context context, Intent intent) {

        try {

            // Get the intent extras.
            String extras = intent.getExtras().getString(KEY_PUSH_EXTRAS_KEY);

            // Create the JSON object.
            JSONObject jsonObject = new JSONObject(extras);

            // Check for the type.
            if (jsonObject.has(KEY_PUSH_TYPE) && TextUtils.equals(jsonObject.getString(KEY_PUSH_TYPE), KEY_PUSH_MODERATOR_TYPE_MODERATOR)) {

                // Attempt to get the moderator message id.
                String messageId = ExtractModeratorMessageId(intent);

                // Create and start the intent.
                Intent mainActivityIntent = new Intent(context, MainActivity.class);
                mainActivityIntent.putExtras(intent.getExtras());
                mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(mainActivityIntent);

            } else if (jsonObject.has(KEY_PUSH_TYPE) && TextUtils.equals(jsonObject.getString(KEY_PUSH_TYPE), KEY_PUSH_POPUP_TYPE_POPUP)) {

                ExtractPopupData(intent);

                // Create and start the intent.
                Intent mainActivityIntent = new Intent(context, MainActivity.class);
                mainActivityIntent.putExtras(intent.getExtras());
                mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(mainActivityIntent);
            }

        } catch (Exception ex){

        }

    }

    /**
     * Extracts a moderator message id if found in the intent.
     * @param intent The intent to check for the message in.
     * @return PostNotificationModel id.
     */
    public static String ExtractModeratorMessageId(Intent intent) {

        // Declare the message id.
        String messageId = "";

        try {

            // Get the intent extras.
            String extras = intent.getExtras().getString(KEY_PUSH_EXTRAS_KEY);

            // Create the JSON object.
            JSONObject jsonObject = new JSONObject(extras);

            // Check for the type.
            if (jsonObject.has(KEY_PUSH_TYPE) && TextUtils.equals(jsonObject.getString(KEY_PUSH_TYPE), KEY_PUSH_MODERATOR_TYPE_MODERATOR)) {

                // Get the message id.
                messageId = jsonObject.getString(KEY_PUSH_MODERATOR_MESSAGE_FIELD);
            }

        } catch (Exception ex){

        }

        return messageId;
    }

    /**
     * Extracts popup data if found in the intent.
     * @param intent The intent to check for the message in.
     * @return PostNotificationModel id.
     */
    public static void ExtractPopupData(Intent intent) {


        try {

            // Get the intent extras.
            String extras = intent.getExtras().getString(KEY_PUSH_EXTRAS_KEY);

            // Create the JSON object.
            JSONObject jsonObject = new JSONObject(extras);

            // Check for the type.
            if (jsonObject.has(KEY_PUSH_TYPE) && TextUtils.equals(jsonObject.getString(KEY_PUSH_TYPE), KEY_PUSH_POPUP_TYPE_POPUP)) {

                String popupTitle = jsonObject.getString(KEY_PUSH_POPUP_TITLE);
                String linkTitle = jsonObject.getString(KEY_PUSH_POPUP_LINK_TITLE);
                String linkUrl = jsonObject.getString(KEY_PUSH_POPUP_LINK_URL);

                ApplicationManager applicationManager = ApplicationManager.getInstance();
                applicationManager.setPopupData(popupTitle, linkTitle, linkUrl);
            }

        } catch (Exception ex){

        }
    }
}
