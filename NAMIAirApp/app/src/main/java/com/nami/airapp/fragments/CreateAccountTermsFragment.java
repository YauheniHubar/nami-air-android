package com.nami.airapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.nami.airapp.R;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.interfaces.ShowPrivacyPolicyListener;
import com.nami.airapp.interfaces.TermsAcceptedListener;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.AppFragmentManager;

import java.io.InputStream;

/**
 * Post Detail Fragment.
 */
public class CreateAccountTermsFragment extends Fragment implements View.OnClickListener {

    // Private Members
    private CreateAccountTermsActivityViewContainer viewContainer = null;
    private Context context = null;
    private String terms = null;

    // Public Members
    public TermsAcceptedListener termsAcceptedListener = null;
    public ShowPrivacyPolicyListener showPrivacyPolicyListener = null;

    /**
     * Default Constructor.
     */
    public CreateAccountTermsFragment() {}

    /**
     * Creates a new instance of a PostDetailFragment.
     * @return New instance of a PostDetailFragment.
     */
    public static CreateAccountTermsFragment newInstance() {

        // Create the new instance.
        CreateAccountTermsFragment instance = new CreateAccountTermsFragment();

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set options menu.
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_account_terms, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Load the terms.
        this.loadTerms();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        /*
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    AppFragmentManager.RemoveFromStack(that, context);
                    return true;
                }

                return false;
            }
        });
        */
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, context);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (view == this.viewContainer.buttonContinue) {
            this.attemptSetTerms();
        }
    }

    /**
     * Attempts to set that the terms were accepted.
     */
    private void attemptSetTerms() {

        // Set that the terms were accepted.
        ApplicationManager.getInstance().setAcceptedTermsOfUse(true);

        // Notify the listener.
        if (this.termsAcceptedListener != null)
            this.termsAcceptedListener.termsAccepted();
    }

    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.create_account_title));
    }


    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = CreateAccountTermsActivityViewContainer.with(view);

        // Add the click events.
        this.viewContainer.buttonContinue.setOnClickListener(this);

        // Check if already agreed to the terms.
        if (ApplicationManager.getInstance().getAcceptedTermsOfUse() == false) {
            this.viewContainer.buttonContinue.setEnabled(false);
            this.viewContainer.radioAccept.setChecked(false);
        } else {
            this.viewContainer.buttonContinue.setEnabled(true);
            this.viewContainer.radioAccept.setChecked(true);
        }

        // Add the radio event.
        this.viewContainer.radioAccept.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                // Test if checked.
                if (viewContainer.radioAccept.isChecked()) {
                    viewContainer.buttonContinue.setEnabled(true);
                }
            }
        });

        // Set the terms.
        //TextView textViewTerms = (TextView)view.findViewById(R.id.create_account_terms_text);
        //textViewTerms.setText(Html.fromHtml(this.terms));

        WebView webViewTerms = (WebView)view.findViewById(R.id.create_account_terms_text);
        webViewTerms.setBackgroundColor(0x00000000);
        webViewTerms.loadDataWithBaseURL(null, this.terms, "text/html", "utf-8", null);
        webViewTerms.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return true;
            }
        });
        webViewTerms.setLongClickable(false);
        webViewTerms.setClickable(false);
        webViewTerms.setOnTouchListener(new View.OnTouchListener(){
            public boolean onTouch(View v, MotionEvent event) {
                WebView.HitTestResult hr = ((WebView)v).getHitTestResult();

                if (event.getAction() == MotionEvent.ACTION_UP &&
                        (hr.getType() == WebView.HitTestResult.ANCHOR_TYPE || hr.getType() == WebView.HitTestResult.SRC_ANCHOR_TYPE)) {

                    // Check the extra.
                    if (hr.getExtra().indexOf("#_pp") >= 0) {

                        // Show the privacy policy.
                        showPrivacyPolicy();;
                    }

                }

                return false;
            }
        });
    }

    /**
     * Shows the privacy policy.
     */
    private void showPrivacyPolicy() {

        if (this.showPrivacyPolicyListener != null) {
            this.showPrivacyPolicyListener.showPrivacyPolicy();
        }
    }

    /**
     * Loads the terms from the assets.
     */
    private void loadTerms() {

        // Get the asset manager.
        AssetManager assetManager = this.context.getAssets();

        try {

            // Create the input stream.
            InputStream inputStream = assetManager.open("copy/terms.txt");

            // Read the input stream.
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);

            // Close the stream.
            inputStream.close();

            // Create the text.
            this.terms = new String(buffer);


        }
        catch (Exception e) {

            Toast.makeText(this.context, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Holds the view for the Create Account Terms Activity.
     */
    private static class CreateAccountTermsActivityViewContainer {

        // Private Members
        private Button buttonContinue = null;
        private RadioButton radioAccept = null;

        /**
         * Private Constructor
         */
        private CreateAccountTermsActivityViewContainer() {}

        /**
         * Public Static Constructor
         * @param view Activity to create the view from
         * @return CreateAccountActivityViewContainer
         */
        public static CreateAccountTermsActivityViewContainer with(View view) {

            // Create the instance.
            CreateAccountTermsActivityViewContainer instance = new CreateAccountTermsActivityViewContainer();

            // Get the items.
            instance.buttonContinue = (Button)view.findViewById(R.id.create_account_terms_continue);
            instance.radioAccept = (RadioButton)view.findViewById(R.id.create_account_terms_accept);

            return instance;
        }
    }

}
