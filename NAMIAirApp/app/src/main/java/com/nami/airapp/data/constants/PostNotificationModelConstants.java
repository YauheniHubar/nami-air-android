package com.nami.airapp.data.constants;

/**
 * Holds constants for the PostNotification model.
 */
public class PostNotificationModelConstants {

    public static final String FieldPost = "post";
    public static final String FieldUser = "user";
    public static final String FieldNotificationType = "notificationType";
	public static final String FieldLikeCount = "likeCount";
    public static final String FieldHugCount = "hugCount";
    public static final String FieldMeTooCount = "meTooCount";
    public static final String FieldNoteCount = "noteCount";
    public static final String FieldInspiredByCount = "inspiredByCount";
	public static final String FieldRead = "read";
    public static final String FieldMessage = "message";
    public static final String FieldModifiedAt = "modifiedAt";
}
