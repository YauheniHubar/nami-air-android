package com.nami.airapp.data.constants;

/**
 * Holds constants for all model types.
 */
public class BaseModelConstants {

    public static final String FieldObjectId = "objectId";
    public static final String FieldCreatedAt = "createdAt";
    public static final String FieldUpdatedAt = "updatedAt";
}
