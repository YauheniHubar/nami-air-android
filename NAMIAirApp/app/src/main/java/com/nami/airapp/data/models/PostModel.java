package com.nami.airapp.data.models;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.Toast;


import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.enums.NotificationType;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import com.nami.airapp.data.constants.BaseModelConstants;
import com.nami.airapp.data.constants.PostModelConstants;
import com.nami.airapp.data.enums.ApprovedStatus;
import com.nami.airapp.data.enums.FeedType;
import com.nami.airapp.data.enums.FlaggedStatus;
import com.nami.airapp.data.enums.UserType;

/**
 * Contains data about a Post
 */
@ParseClassName("Post")
public class PostModel extends ParseObject implements Parcelable {

    // Private Members.
    private Date postDate = null;

    /**
     * Gets the User.
     * @return ParseUser.
     */
    public ParseUser getUser() { return (ParseUser)get(PostModelConstants.FieldUser); }

    /**
     * Sets the User.
     * @param value ParseUser.
     */
    public void setUser(ParseUser value) { put(PostModelConstants.FieldUser, value); }

    /**
     * Gets the User Type.
     * @return UserType
     */
    public UserType getUserType() { return UserType.fromValue(getInt(PostModelConstants.FieldUserType)); }

    /**
     * Sets the User Type.
     * @param value UserType enum.
     */
    public void setUserType(UserType value) { put(PostModelConstants.FieldUserType, value.getValue()); }

    /**
     * Gets the Approved Status
     * @return ApprovedStatus.
     */
    public ApprovedStatus getApprovedStatus() { return ApprovedStatus.fromValue(getInt(PostModelConstants.FieldApprovedStatus)); }

    /**
     * Sets the Approved Status.
     * @param value ApprovedStatus enum.
     */
    public void setApprovedStatus(ApprovedStatus value) { put(PostModelConstants.FieldApprovedStatus, value.getValue()); }

    /**
     * Gets the Flagged Status.
     * @return FlaggedStatus enum.
     */
    public FlaggedStatus getFlaggedStatus() { return FlaggedStatus.fromValue(getInt(PostModelConstants.FieldFlaggedStatus)); }

    /**
     * Sets the Flagged Status.
     * @param value FlaggedStatus enum.
     */
    public void setFlaggedStatus(FlaggedStatus value) { put(PostModelConstants.FieldFlaggedStatus, value.getValue()); }

    /**
     * Gets the Post text.
     * @return Post text.
     */
    public String getPostText() {
        return getString(PostModelConstants.FieldText);
    }

    /**
     * Sets the Post text.
     * @param value Post text.
     */
    public void setPostText(String value) {
        put(PostModelConstants.FieldText, value);
    }

    /**
     * Gets whether the post is private.
     * @return True if private, otherwise false.
     */
    public boolean getIsPrivate() {
        return getBoolean(PostModelConstants.FieldIsPrivate);
    }

    /**
     * Sets whether the post is private.
     * @param value True if private, otherwise false.
     */
    public void setIsPrivate(boolean value) {
        put(PostModelConstants.FieldIsPrivate, value);
    }

    /**
     * Gets the number of likes.
     * @return Number of likes.
     */
    public int getLikeCount() {
        return getInt(PostModelConstants.FieldLikeCount);
    }

    /**
     * Sets the number of likes.
     * @param value Number of likes.
     */
    public void setLikeCount(int value) {
        put(PostModelConstants.FieldLikeCount, value);
    }

    /**
     * Gets the number of hugs.
     * @return Number of hugs.
     */
    public int getHugCount() {
        return getInt(PostModelConstants.FieldHugCount);
    }

    /**
     * Sets the number of hugs.
     * @param value Number of hugs.
     */
    public void setHugCount(int value) {
        put(PostModelConstants.FieldHugCount, value);
    }

    /**
     * Gets the number of me-toos.
     * @return Number of me-toos.
     */
    public int getMeTooCount() {
        return getInt(PostModelConstants.FieldMeTooCount);
    }

    /**
     * Sets the number of me-toos.
     * @param value Number of me-toos.
     */
    public void setMeTooCount(int value) {
        put(PostModelConstants.FieldMeTooCount, value);
    }

    /**
     * Gets the number of stories inspired by the post.
     * @return Number of stories inspired by the post.
     */
    public int getMeTooStoryCount() {
        return getInt(PostModelConstants.FieldMeTooStoryCount);
    }

    /**
     * Sets the number of stories inspired by the post.
     * @param value Number of stories inspired by the post.
     */
    public void setMeTooStoryCount(int value) { put(PostModelConstants.FieldMeTooStoryCount, value); }

    /**
     * Gets the the number of notes for the post.
     * @return Number of notes for the post.
     */
    public int getNoteCount() { return getInt(PostModelConstants.FieldNoteCount); }

    /**
     * Sets the number of notes for the post.
     * @param count Number of notes for the post.
     */
    public void setNoteCount(int count) { put(PostModelConstants.FieldNoteCount, count); }

    /**
     * Gets the inspired by post.
     * @return Post Model
     */
    public PostModel getInspiredByPost() { return (PostModel)get(PostModelConstants.FieldInspiredByPost); }

    /**
     * Sets the inspired by post.
     * @param value Post Model
     */
    public void setInspiredByPost(PostModel value) { put(PostModelConstants.FieldInspiredByPost, value); }

    /**
     * Gets the post date.
     * @return Post date.
     */
    public Date getPostDate() {
        if (this.postDate == null)
            this.postDate = this.getCreatedAt();
        return this.postDate;
    }

    /**
     * Sets the post date.
     * @param value Post date.
     */
    public void setPostDate(Date value) {
        this.postDate = value;
    }


    /**
     * Updates a post inspired count.
     * @param context The context.
     * @param postModel The post to update the inspired count for.
     */
    public static void UpdatePostInspiredCount(final Context context, PostModel postModel) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postId", postModel.getObjectId());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("updateInspiredPostCount", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }


    /**
     * Describes the contents of the parcel.
     * @return Hash code of the parcel.
     */
    @Override
    public int describeContents() {

        return super.hashCode();
    }

    /**
     * Writes the contents of the post to a parcel.
     * @param parcel Parcel to write the contents to.
     * @param flags Parcel flags.
     */
    @Override
    public void writeToParcel(Parcel parcel, int flags) {

        // Write the post contents.
        parcel.writeString(getObjectId());
        parcel.writeInt(getUserType().getValue());
        parcel.writeInt(getApprovedStatus().getValue());
        parcel.writeInt(getFlaggedStatus().getValue());
        parcel.writeString(getPostText());
        parcel.writeInt(getIsPrivate() == true ? 1 : 0);
        parcel.writeInt(getLikeCount());
        parcel.writeInt(getHugCount());
        parcel.writeInt(getMeTooCount());
        parcel.writeInt(getNoteCount());
        parcel.writeInt(getMeTooStoryCount());
        parcel.writeLong(getPostDate().getTime());
    }

    /**
     * Creates the parcel.
     */
    public static Creator<PostModel> CREATOR = new Creator<PostModel>() {

        @Override
        public PostModel createFromParcel(Parcel parcel) {

            // Create the post.
            PostModel model = new PostModel();

            // Get the post contents.
            model.setObjectId(parcel.readString());
            model.setUserType(UserType.fromValue(parcel.readInt()));
            model.setApprovedStatus(ApprovedStatus.fromValue(parcel.readInt()));
            model.setFlaggedStatus(FlaggedStatus.fromValue(parcel.readInt()));
            model.setIsPrivate(parcel.readInt() == 1 ? true : false);
            model.setLikeCount(parcel.readInt());
            model.setHugCount(parcel.readInt());
            model.setMeTooCount(parcel.readInt());
            model.setNoteCount(parcel.readInt());
            model.setMeTooStoryCount(parcel.readInt());
            model.setPostDate(new Date(parcel.readLong()));

            return model;
        }


        @Override
        public PostModel[] newArray(int size) {
            return new PostModel[size];
        }
    };


    /**
     * Creates a post notification.
     * @param context The context.
     * @param postModel The post to update the notes for.
     */
    public static void UpdatePostNotes(final Context context, PostModel postModel, List<String> hashNotes) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postId", postModel.getObjectId());
        params.put("notes", hashNotes);

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("postUpdateNotes", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }

    /**
     * Sets a post as private.
     * @param context The context.
     * @param postModel The post.
     */
    public static void SetPostPrivate(final Context context, PostModel postModel) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postId", postModel.getObjectId());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("postSetPrivate", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }

    /**
     * Sets a post as public.
     * @param context The context.
     * @param postModel The post.
     */
    public static void SetPostPublic(final Context context, PostModel postModel) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postId", postModel.getObjectId());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("postSetPrivate", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }

    /**
     * Deletes a post.
     * @param context The context.
     * @param postModel The post.
     */
    public static void DeletePost(final Context context, PostModel postModel) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postId", postModel.getObjectId());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("deletePost", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }

    /**
     * Calls the cloud function hide post.
     * @param context
     * @param postModel
     */
    public static void HidePost(final Context context, PostModel postModel) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postId", postModel.getObjectId());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("hidePost", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }

    /**
     * Calls the cloud function author block
     * @param context
     * @param postModel
     */
    public static void AuthorBlock(final Context context, PostModel postModel) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("userId", postModel.getUser().getObjectId());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("blockAuthor", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }

    /**
     *
     * @param feedType
     * @param userType
     * @return
     */
    public static  ParseQuery<PostModel> createBasePostFeedQuery(final FeedType feedType, final UserType userType) {

        // Create the query.
        ParseQuery<PostModel> query = new ParseQuery<PostModel>(
                PostModel.class);

        // Set the filters.
        if (feedType == FeedType.MY_POSTS) {

            // Only show posts from the current user.
            query.whereEqualTo(PostModelConstants.FieldUser, ParseUser.getCurrentUser());
        } else {

            // Set the user type.
            query.whereEqualTo(PostModelConstants.FieldUserType, userType.getValue());

            // Hide any private posts.
            query.whereEqualTo(PostModelConstants.FieldIsPrivate, false);

            // Show only approved posts.
            query.whereEqualTo(PostModelConstants.FieldApprovedStatus, ApprovedStatus.APPROVED.getValue());

            // Hide any hidden posts.
            ArrayList<String> postsHidden = (ArrayList<String>)ParseUser.getCurrentUser().get(UserModelConstants.FieldPostsHidden);
            if (postsHidden != null && postsHidden.size() > 0) {
                query.whereNotContainedIn(BaseModelConstants.FieldObjectId, postsHidden);
            }

            // Hide any blocked authors.
            ArrayList<ParseUser> authorsBlocked = (ArrayList<ParseUser>)ParseUser.getCurrentUser().get(UserModelConstants.FieldAuthorsBlocked);
            if (authorsBlocked != null && authorsBlocked.size() > 0) {
                query.whereNotContainedIn(PostModelConstants.FieldUser, authorsBlocked);
            }

        }

        // Set not hidden.
        query.whereNotEqualTo(PostModelConstants.FieldIsHidden, true);

        return query;
    }

    /**
     * Creates a query adapter to fetch the posts.
     * @param feedType The feed type (All Posts, My Posts)
     * @param userType The user type (User, Caregiver)
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<PostModel> createPostFeedAdapterQuery(final FeedType feedType, final UserType userType) {

        return new ParseQueryAdapter.QueryFactory<PostModel>() {

            @Override
            public ParseQuery<PostModel> create() {

                // Create the query.
                ParseQuery<PostModel> query = new ParseQuery<PostModel>(
                        PostModel.class);

                // Set the filters.
                if (feedType == FeedType.MY_POSTS) {

                    // Only show posts from the current user.
                    query.whereEqualTo(PostModelConstants.FieldUser, ParseUser.getCurrentUser());
                } else {

                    // Set the user type.
                    query.whereEqualTo(PostModelConstants.FieldUserType, userType.getValue());

                    // Hide any private posts.
                    query.whereEqualTo(PostModelConstants.FieldIsPrivate, false);

                    // Show only approved posts.
                    query.whereEqualTo(PostModelConstants.FieldApprovedStatus, ApprovedStatus.APPROVED.getValue());

                    // Hide any hidden posts.
                    ArrayList<String> postsHidden = (ArrayList<String>)ParseUser.getCurrentUser().get(UserModelConstants.FieldPostsHidden);
                    if (postsHidden != null && postsHidden.size() > 0) {
                        query.whereNotContainedIn(BaseModelConstants.FieldObjectId, postsHidden);
                    }

                    // Hide any blocked authors.
                    ArrayList<ParseUser> authorsBlocked = (ArrayList<ParseUser>)ParseUser.getCurrentUser().get(UserModelConstants.FieldAuthorsBlocked);
                    if (authorsBlocked != null && authorsBlocked.size() > 0) {
                        query.whereNotContainedIn(PostModelConstants.FieldUser, authorsBlocked);
                    }

                    // Set not hidden.
                    query.whereNotEqualTo(PostModelConstants.FieldIsHidden, true);

                }

                // Add the includes.
                query.include(PostModelConstants.FieldInspiredByPost);

                // Set the sort.
                query.orderByDescending(BaseModelConstants.FieldCreatedAt);

                return query;
            }
        };
    }

    /**
     * Creates a query adapter to fetch the posts.
     * @param feedType The feed type (All Posts, My Posts)
     * @param userType The user type (User, Caregiver)
     * @param date The date to get the newer items of
     * @param earliestDate The earliest date.
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<PostModel> createPostFeedNewItemsAdapterQuery(final FeedType feedType, final UserType userType, final Date date, final Date earliestDate) {

        return new ParseQueryAdapter.QueryFactory<PostModel>() {

            @Override
            public ParseQuery<PostModel> create() {

                // Create the query.
                ParseQuery<PostModel> query = new ParseQuery<PostModel>(
                        PostModel.class);

                // Set the filters.
                if (feedType == FeedType.MY_POSTS) {

                    // Only show posts from the current user.
                    query.whereEqualTo(PostModelConstants.FieldUser, ParseUser.getCurrentUser());
                } else {

                    // Set the user type.
                    query.whereEqualTo(PostModelConstants.FieldUserType, userType.getValue());

                    // Hide any private posts.
                    query.whereEqualTo(PostModelConstants.FieldIsPrivate, false);

                    // Show only approved posts.
                    query.whereEqualTo(PostModelConstants.FieldApprovedStatus, ApprovedStatus.APPROVED.getValue());

                    // Hide any hidden posts.
                    ArrayList<String> postsHidden = (ArrayList<String>)ParseUser.getCurrentUser().get(UserModelConstants.FieldPostsHidden);
                    if (postsHidden != null && postsHidden.size() > 0) {
                        query.whereNotContainedIn(BaseModelConstants.FieldObjectId, postsHidden);
                    }

                    // Hide any blocked authors.
                    ArrayList<ParseUser> authorsBlocked = (ArrayList<ParseUser>)ParseUser.getCurrentUser().get(UserModelConstants.FieldAuthorsBlocked);
                    if (authorsBlocked != null && authorsBlocked.size() > 0) {
                        query.whereNotContainedIn(PostModelConstants.FieldUser, authorsBlocked);
                    }
                }

                // Add the date filter.
                query.whereGreaterThan(BaseModelConstants.FieldUpdatedAt, new Date(earliestDate.getTime() - 60 * 1000 * 10));

                // Add the includes.
                query.include(PostModelConstants.FieldInspiredByPost);

                // Set the sort.
                query.orderByDescending(BaseModelConstants.FieldCreatedAt);

                return query;
            }
        };
    }

    /**
     * Creates a query adapter to fetch the posts.
     * @param feedType The feed type (All Posts, My Posts)
     * @param userType The user type (User, Caregiver)
     * @param date The date to get the earlier items of
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<PostModel> createPostFeedEarlierAdapterQuery(final FeedType feedType, final UserType userType, final Date date) {

        return new ParseQueryAdapter.QueryFactory<PostModel>() {

            @Override
            public ParseQuery<PostModel> create() {

                // Create the query.
                ParseQuery<PostModel> query = new ParseQuery<PostModel>(
                        PostModel.class);

                // Set the filters.
                if (feedType == FeedType.MY_POSTS) {

                    // Only show posts from the current user.
                    query.whereEqualTo(PostModelConstants.FieldUser, ParseUser.getCurrentUser());
                } else {

                    // Set the user type.
                    query.whereEqualTo(PostModelConstants.FieldUserType, userType.getValue());

                    // Hide any private posts.
                    query.whereEqualTo(PostModelConstants.FieldIsPrivate, false);

                    // Show only approved posts.
                    query.whereEqualTo(PostModelConstants.FieldApprovedStatus, ApprovedStatus.APPROVED.getValue());

                    // Hide any hidden posts.
                    ArrayList<String> postsHidden = (ArrayList<String>)ParseUser.getCurrentUser().get(UserModelConstants.FieldPostsHidden);
                    if (postsHidden != null && postsHidden.size() > 0) {
                        query.whereNotContainedIn(BaseModelConstants.FieldObjectId, postsHidden);
                    }

                    // Hide any blocked authors.
                    ArrayList<ParseUser> authorsBlocked = (ArrayList<ParseUser>)ParseUser.getCurrentUser().get(UserModelConstants.FieldAuthorsBlocked);
                    if (authorsBlocked != null && authorsBlocked.size() > 0) {
                        query.whereNotContainedIn(PostModelConstants.FieldUser, authorsBlocked);
                    }

                    // Set not hidden.
                    query.whereNotEqualTo(PostModelConstants.FieldIsHidden, true);
                }

                // Add the date filter.
                query.whereLessThan(BaseModelConstants.FieldCreatedAt, date);

                // Add the includes.
                query.include(PostModelConstants.FieldInspiredByPost);

                // Set the sort.
                query.orderByDescending(BaseModelConstants.FieldCreatedAt);

                return query;
            }
        };
    }

    /**
     * Creates a query adapter to fetch the posts.
     * @param feedType The feed type (All Posts, My Posts)
     * @param userType The user type (User, Caregiver)
     * @param queryText Search query
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<PostModel> createSearchFeedAdapterQuery(final FeedType feedType, final UserType userType, final String queryText) {

        return new ParseQueryAdapter.QueryFactory<PostModel>() {

            @Override
            public ParseQuery<PostModel> create() {

                // Create the hash tag query.
                ParseQuery hashTagQuery = PostModel.createBasePostFeedQuery(feedType, userType);
                String hashTagQueryText = queryText.toLowerCase();
                hashTagQueryText = hashTagQueryText.replace("#", "");
                hashTagQueryText = "#" + hashTagQueryText;
                hashTagQuery.whereEqualTo(PostModelConstants.FieldHashTags, hashTagQueryText);

                // Create the hash note query.
                ParseQuery hashNoteQuery = PostModel.createBasePostFeedQuery(feedType, userType);
                String hashNoteQueryText = queryText.toLowerCase();
                hashNoteQueryText = hashNoteQueryText.replace("#", "");
                hashNoteQueryText = "#" + hashNoteQueryText;
                hashNoteQuery.whereEqualTo(PostModelConstants.FieldHashNotes, hashTagQueryText);

                // Create the hash queries.
                List<ParseQuery<PostModel>> hashQueries = new ArrayList<ParseQuery<PostModel>>();
                hashQueries.add(hashTagQuery);
                hashQueries.add(hashNoteQuery);

                // Create the main query.
                ParseQuery<PostModel> query = ParseQuery.or(hashQueries);

                // Add the includes.
                query.include(PostModelConstants.FieldInspiredByPost);

                // Set the sort.
                query.orderByDescending(BaseModelConstants.FieldCreatedAt);

                return query;
            }
        };
    }


    /**
     * Creates a query adapter to fetch the posts.
     * @param feedType The feed type (All Posts, My Posts)
     * @param userType The user type (User, Caregiver)
     * @param queryText Search query
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<PostModel> createNoteFeedAdapterQuery(final FeedType feedType, final UserType userType, final String queryText) {

        return new ParseQueryAdapter.QueryFactory<PostModel>() {

            @Override
            public ParseQuery<PostModel> create() {

                // Create the query.
                ParseQuery<PostModel> query = new ParseQuery<PostModel>(
                        PostModel.class);

                // Set the filters.
                if (feedType == FeedType.MY_POSTS) {

                    // Only show posts from the current user.
                    query.whereEqualTo(PostModelConstants.FieldUser, ParseUser.getCurrentUser());
                } else {

                    // Set the user type.
                    query.whereEqualTo(PostModelConstants.FieldUserType, userType.getValue());

                    // Hide any private posts.
                    query.whereEqualTo(PostModelConstants.FieldIsPrivate, false);

                    // Show only approved posts.
                    query.whereEqualTo(PostModelConstants.FieldApprovedStatus, ApprovedStatus.APPROVED.getValue());

                    // Hide any hidden posts.
                    ArrayList<String> postsHidden = (ArrayList<String>)ParseUser.getCurrentUser().get(UserModelConstants.FieldPostsHidden);
                    if (postsHidden != null && postsHidden.size() > 0) {
                        query.whereNotContainedIn(BaseModelConstants.FieldObjectId, postsHidden);
                    }

                    // Hide any blocked authors.
                    ArrayList<ParseUser> authorsBlocked = (ArrayList<ParseUser>)ParseUser.getCurrentUser().get(UserModelConstants.FieldAuthorsBlocked);
                    if (authorsBlocked != null && authorsBlocked.size() > 0) {
                        query.whereNotContainedIn(PostModelConstants.FieldUser, authorsBlocked);
                    }

                }

                // Set not hidden.
                query.whereNotEqualTo(PostModelConstants.FieldIsHidden, true);

                // Add the hash tag query.
                String hashTagQueryText = queryText.toLowerCase();
                hashTagQueryText = hashTagQueryText.replace("#", "");
                hashTagQueryText = "#" + hashTagQueryText;
                query.whereEqualTo(PostModelConstants.FieldHashNotes, hashTagQueryText);

                // Add the includes.
                query.include(PostModelConstants.FieldInspiredByPost);

                // Set the sort.
                query.orderByDescending(BaseModelConstants.FieldCreatedAt);

                return query;
            }
        };
    }

    /**
     * Creates a query adapter to fetch the posts inspired by a post.
     * @param postModel Post Model
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<PostModel> createPostInspiredByFeedAdapterQuery(PostModel postModel) {

        // Declare the filters.
        final PostModel filterPostModel = postModel;

        return new ParseQueryAdapter.QueryFactory<PostModel>() {

            @Override
            public ParseQuery<PostModel> create() {

                // Create the query.
                ParseQuery<PostModel> query = new ParseQuery<PostModel>(
                        PostModel.class);

                // Set not hidden.
                query.whereNotEqualTo(PostModelConstants.FieldIsHidden, true);

                // Set the filters.
                query.whereEqualTo(PostModelConstants.FieldInspiredByPost, filterPostModel);

                // Show only approved posts.
                query.whereEqualTo(PostModelConstants.FieldApprovedStatus, ApprovedStatus.APPROVED.getValue());

                // Add the includes.
                query.include(PostModelConstants.FieldInspiredByPost);

                // Set the sort.
                query.orderByDescending(BaseModelConstants.FieldCreatedAt);

                return query;
            }
        };
    }
}
