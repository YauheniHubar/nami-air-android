package com.nami.airapp.data.adapters;

import java.util.List;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;

import com.nami.airapp.R;
import com.nami.airapp.data.models.NoteModel;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.interfaces.ListItemSelectedListener;
import com.nami.airapp.views.NoteListItemView;

public class NoteAdapter extends BaseParseAdapter {

    // Private Members
    private List<NoteModel> dataSet = null;
    private ListItemSelectedListener listItemSelectedListener;

    /**
     * Default Constructor.
     * @param context Context which the adapter runs in.
     */
    public NoteAdapter(Context context, ListItemSelectedListener listItemSelectedListener) {
        super(context);

        // Set the members.
        this.listItemSelectedListener = listItemSelectedListener;

        // Load / Reload the data.
        this.reloadData();
    }

    /**
     * Gets the number of items in the list.
     * @return Number of items in the list.
     */
    @Override
    public int getCount() {

        return this.dataSet.size();
    }

    /**
     * Gets the item at the position in the list.
     * @param position Position in the list.
     * @return Item at the position in the list.
     */
    @Override
    public NoteModel getItem(int position) {

        return this.dataSet.get(position);
    }

    /**
     * Gets the item identifier at the position in the list.
     * @param position Position in the list.
     * @return Item identifier at the position in the list.
     */
    @Override
    public long getItemId(int position) {

        return position;
    }

    /**
     * Gets the view for the item in the position in the list.
     * @param position Position in the list.
     * @param convertView View of the item in the list.
     * @param parent Parent view of the item in the list.
     * @return Updated view of the item in the list.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Create the view if necessary.
        if (convertView == null) {
            convertView = new NoteListItemView(context);
        }

        // Get the item and update the view.
        final NoteModel model = this.getItem(position);
        ((NoteListItemView)convertView).updateFromModel(model, position);

        // Add the checked event.
        final CheckBox viewCheckBox = ((NoteListItemView) convertView).getCheckBox();
        viewCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Update the model and notify the listener.
                model.setIsSelected(viewCheckBox.isChecked());
                listItemSelectedListener.listItemSelected(model, viewCheckBox.isChecked());

                // Update the style.
                if (viewCheckBox.isChecked())
                    viewCheckBox.setTextColor(context.getResources().getColor(R.color.text_color_green));
                else viewCheckBox.setTextColor(context.getResources().getColor(R.color.text_color_gray_dark));
            }
        });

        return convertView;
    }

    /**
     * Reloads the data.
     */
    public void reloadData() {

        // Set the data set.
        this.setDataSet(ApplicationManager.getInstance().getDataSetNotes());
    }

    /**
     * Gets the data set.
     * @return List of data.
     */
    public List<NoteModel> getDataSet() {

        return this.dataSet;
    }

    /**
     * Sets the data set.
     * @param dataSet List of data.
     */
    private void setDataSet(List<NoteModel> dataSet) {

        // Set the data set.
        this.dataSet = dataSet;

        // Notify that the data set has changed.
        this.notifyDataSetChanged();
    }
}
