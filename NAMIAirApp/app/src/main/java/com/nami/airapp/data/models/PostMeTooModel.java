package com.nami.airapp.data.models;

import com.parse.ParseObject;
import com.parse.ParseClassName;
import com.parse.ParseUser;

import com.nami.airapp.data.constants.PostMeTooModelConstants;
import com.nami.airapp.data.models.PostModel;

/**
 * Contains data about a Post me too.
 */
@ParseClassName("PostMeToo")
public class PostMeTooModel extends ParseObject {

    /**
     * Gets the post model.
     * @return Post model.
     */
    public PostModel getPostModel() { return (PostModel)get(PostMeTooModelConstants.FieldPost); }

    /**
     * Sets the post model.
     * @param postModel Post model.
     */
    public void setPostModel(PostModel postModel) { put(PostMeTooModelConstants.FieldPost, postModel); }

    /**
     * Gets the post user.
     * @return ParseUser
     */
    public ParseUser getPostUser() { return (ParseUser)get(PostMeTooModelConstants.FieldUser); }

    /**
     * Sets the post user.
     * @param postUser ParseUser
     */
    public void setPostUser(ParseUser postUser) { put(PostMeTooModelConstants.FieldUser, postUser); }
}
