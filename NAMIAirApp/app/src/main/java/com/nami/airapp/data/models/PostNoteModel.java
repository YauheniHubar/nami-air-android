package com.nami.airapp.data.models;

import java.util.ArrayList;
import java.util.Date;

import com.parse.ParseObject;
import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import com.nami.airapp.data.constants.BaseModelConstants;
import com.nami.airapp.data.constants.PostNoteModelConstants;
import com.nami.airapp.data.models.PostModel;

/**
 * Contains data about a Post note.
 */
@ParseClassName("PostNote")
public class PostNoteModel extends ParseObject {

    // Private Members.
    private Date noteDate = null;

    /**
     * Gets the post model.
     * @return Post model.
     */
    public PostModel getPostModel() { return (PostModel)get(PostNoteModelConstants.FieldPost); }

    /**
     * Sets the post model.
     * @param postModel Post model.
     */
    public void setPostModel(PostModel postModel) { put(PostNoteModelConstants.FieldPost, postModel); }

    /**
     * Gets the post user.
     * @return ParseUser
     */
    public ParseUser getPostUser() { return (ParseUser)get(PostNoteModelConstants.FieldUser); }

    /**
     * Sets the post user.
     * @param postUser ParseUser
     */
    public void setPostUser(ParseUser postUser) { put(PostNoteModelConstants.FieldUser, postUser); }

    /**
     * Gets the post notes.
     * @return Post notes.
     */
    public ArrayList<String> getPostNotes() { return (ArrayList<String>)get(PostNoteModelConstants.FieldNotes); }

    /**
     * Sets the post notes.
     * @param value Post notes.
     */
    public void setPostNotes(ArrayList<String> value) { put(PostNoteModelConstants.FieldNotes, value); }

    /**
     * Gets the note date.
     * @return Note date.
     */
    public Date getNoteDate() {
        if (this.noteDate == null)
            this.noteDate = this.getCreatedAt();
        return this.noteDate;
    }

    /**
     * Sets the note date.
     * @param value Note date.
     */
    public void setNoteDate(Date value) {
        this.noteDate = value;
    }

    /**
     * Creates a query adapter to fetch the notes.
     * @param postModel The Post model.
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<PostNoteModel> createPostNoteAdapterQuery(PostModel postModel) {

        // Declare the filters.
        final PostModel filterPostModel = postModel;

        return new ParseQueryAdapter.QueryFactory<PostNoteModel>() {

            @Override
            public ParseQuery<PostNoteModel> create() {

                // Create the query.
                ParseQuery<PostNoteModel> query = new ParseQuery<PostNoteModel>(PostNoteModel.class);

                // Set the include.
                query.include(PostNoteModelConstants.FieldNotes);

                // Set the filters.
                query.whereEqualTo(PostNoteModelConstants.FieldPost, filterPostModel);

                // Set the sort.
                query.orderByDescending(BaseModelConstants.FieldCreatedAt);

                return query;
            }
        };
    }
}
