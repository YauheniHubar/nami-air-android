package com.nami.airapp.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.HashMap;

import com.nami.airapp.R;
import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.AppFragmentManager;
import com.nami.airapp.utils.KeyboardManager;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

/**
 * Post Detail Fragment.
 */
public class SettingsFragment extends Fragment implements OnClickListener {


    // Private Members.
    private SettingsViewContainer viewContainer = null;

    /**
     * Default Constructor.
     */
    public SettingsFragment() {}

    /**
     * Creates a new instance of a PostDetailFragment.
     * @return New instance of a PostDetailFragment.
     */
    public static SettingsFragment newInstance() {

        // Create the new instance.
        SettingsFragment instance = new SettingsFragment();

        return instance;
    }


    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set the action bar.
        MainActivity mainActivity = (MainActivity)getActivity();
        mainActivity.setActionBarNoTabs();

        // Set options menu.
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    AppFragmentManager.RemoveFromStack(that, getActivity());
                    return true;
                }

                return false;
            }
        });
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, getActivity());
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (view == this.viewContainer.buttonExport) {
            this.handleExport();
        } else if (view == this.viewContainer.buttonEditAccount) {
            this.showEditAccount();
        } else if (view == this.viewContainer.buttonDeleteAccount) {
            this.showDeleteAccount();
        } else if (view == this.viewContainer.buttonLogout) {
            this.handleLogout();
        } else if (view == this.viewContainer.buttonTerms) {
            this.showTermsOfUse();
        } else if (view == this.viewContainer.buttonFAQ) {
            this.showFAQ();
        } else if (view == this.viewContainer.buttonPrivacy) {
            this.showPrivacyPolicy();
        } else if (view == this.viewContainer.buttonVersion) {
            this.showVersion();
        } else if (view == this.viewContainer.buttonCredits) {
            this.showCredits();
        } else if (view == this.viewContainer.buttonFeedback) {
            this.handleFeedback();
        }
    }

    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(getActivity(), actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.settings_title));
    }

    /**
     * Handles the export.
     */
    private void handleExport() {

        // Attempt to export the user data.
        this.attemptUserExportData();
    }

    /**
     * Shows the edit account.
     */
    private void showEditAccount() {

        // Create the fragment.
        EditAccountFragment editAccountFragment = EditAccountFragment.newInstance();

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, editAccountFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Shows the delete account.
     */
    private void showDeleteAccount() {

        // Create the fragment.
        DeleteAccountFragment deleteAccountFragment = DeleteAccountFragment.newInstance();

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, deleteAccountFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Handles the logout.
     */
    private void handleLogout() {

        // Create the dialog.
        final Dialog userLogoutDialog = new Dialog(getActivity());
        userLogoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        userLogoutDialog.setContentView(R.layout.dialog_user_logout);

        // Get the views.
        Button userLogoutLogout = (Button)userLogoutDialog.findViewById(R.id.dialog_user_logout_logout);
        Button userLogoutCancel = (Button)userLogoutDialog.findViewById(R.id.dialog_user_logout_cancel);

        // Add the listeners.
        userLogoutLogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                // Dismiss the dialog.
                userLogoutDialog.dismiss();

                // Get the activity.
                MainActivity mainActivity = (MainActivity)getActivity();

                // Perform the logout.
                mainActivity.handleLogout();
            }
        });
        userLogoutCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                // Dismiss the dialog.
                userLogoutDialog.dismiss();
            }
        });

        // Show the dialog.
        userLogoutDialog.show();

    }

    /**
     * Shows the terms of use.
     */
    private void showTermsOfUse() {

        // Create the fragment.
        TermsFragment termsFragment = TermsFragment.newInstance();

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, termsFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Shows the FAQ
     */
    private void showFAQ() {

        // Create the fragment.
        FaqMainFragment faqFragment = FaqMainFragment.newInstance();

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, faqFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Shows the privacy policy.
     */
    private void showPrivacyPolicy() {

        // Create the fragment.
        PrivacyFragment privacyFragment = PrivacyFragment.newInstance();

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, privacyFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Shows the version.
     */
    private void showVersion() {

        // Create the fragment.
        VersionFragment versionFragment = VersionFragment.newInstance();

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, versionFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Shows the credits.
     */
    private void showCredits() {

        // Create the fragment.
        CreditsFragment creditsFragment = CreditsFragment.newInstance();

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, creditsFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Handles the feedback.
     */
    private void handleFeedback() {

        // Get the activity.
        MainActivity mainActivity = (MainActivity)getActivity();

        // Create the email.
        mainActivity.sendFeedbackEmail();
    }

    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = SettingsViewContainer.with(view);

        // Set the click events.
        this.viewContainer.buttonExport.setOnClickListener(this);
        this.viewContainer.buttonEditAccount.setOnClickListener(this);
        this.viewContainer.buttonDeleteAccount.setOnClickListener(this);
        this.viewContainer.buttonLogout.setOnClickListener(this);
        this.viewContainer.buttonTerms.setOnClickListener(this);
        this.viewContainer.buttonFAQ.setOnClickListener(this);
        this.viewContainer.buttonPrivacy.setOnClickListener(this);
        this.viewContainer.buttonVersion.setOnClickListener(this);
        this.viewContainer.buttonCredits.setOnClickListener(this);
        this.viewContainer.buttonFeedback.setOnClickListener(this);
    }

    /**
     * Attempts to export the user data.
     */
    private void attemptUserExportData() {

        // Create the progress dialog.
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.settings_export_dialog_title));
        progressDialog.show();

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("exportData", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Dismiss the dialog
                progressDialog.dismiss();

                // Check the exception.
                if (e != null) {
                    Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    return;
                }

                // Send the data to an email.
                sendExportedDataToEmail((String)o);
            }
        });
    }




    /**
     * Sends the exported data to an email.
     */
    private void sendExportedDataToEmail(String data) {

        // Get the activity.
        MainActivity mainActivity = (MainActivity)getActivity();

        // Send the data to email.
        mainActivity.sendExportedDataToEmail(data);
    }

    /**
     * Holds the view for the Settings.
     */
    private static class SettingsViewContainer {

        // Private Members.
        private RelativeLayout buttonExport = null;
        private RelativeLayout buttonEditAccount = null;
        private RelativeLayout buttonDeleteAccount = null;
        private RelativeLayout buttonLogout = null;
        private RelativeLayout buttonTerms = null;
        private RelativeLayout buttonFAQ = null;
        private RelativeLayout buttonPrivacy = null;
        private RelativeLayout buttonVersion = null;
        private RelativeLayout buttonCredits = null;
        private RelativeLayout buttonFeedback = null;

        // Private Constructor.
        private SettingsViewContainer() {}

        // Static Constructor.
        public static SettingsViewContainer with(View view) {

            // Create the instance
            SettingsViewContainer instance = new SettingsViewContainer();

            // Get the items.
            instance.buttonExport = (RelativeLayout)view.findViewById(R.id.settings_account_button_export);
            instance.buttonEditAccount = (RelativeLayout)view.findViewById(R.id.settings_account_button_edit_account);
            instance.buttonDeleteAccount = (RelativeLayout)view.findViewById(R.id.settings_account_button_delete_account);
            instance.buttonLogout = (RelativeLayout)view.findViewById(R.id.settings_account_button_logout);
            instance.buttonTerms = (RelativeLayout)view.findViewById(R.id.settings_legal_button_terms);
            instance.buttonFAQ = (RelativeLayout)view.findViewById(R.id.settings_legal_button_faq);
            instance.buttonPrivacy = (RelativeLayout)view.findViewById(R.id.settings_legal_button_privacy);
            instance.buttonVersion = (RelativeLayout)view.findViewById(R.id.settings_about_button_version);
            instance.buttonCredits = (RelativeLayout)view.findViewById(R.id.settings_about_button_credits);
            instance.buttonFeedback = (RelativeLayout)view.findViewById(R.id.settings_about_button_feedback);


            return instance;
        }
    }
}
