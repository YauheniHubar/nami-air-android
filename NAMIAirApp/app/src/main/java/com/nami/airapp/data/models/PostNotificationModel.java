package com.nami.airapp.data.models;

import android.widget.Toast;
import android.content.Context;
import java.util.HashMap;
import java.util.Date;
import java.lang.Comparable;
import android.os.Parcel;
import android.os.Parcelable;

import com.nami.airapp.data.singletons.ApplicationManager;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;


import com.nami.airapp.data.constants.BaseModelConstants;
import com.nami.airapp.data.constants.PostNotificationModelConstants;
import com.nami.airapp.data.enums.NotificationSubType;
import com.nami.airapp.data.enums.NotificationType;


/**
 * Contains data about a Post like.
 */
@ParseClassName("PostNotification")
public class PostNotificationModel extends ParseObject implements Parcelable, Comparable<PostNotificationModel> {

    /**
     * Gets the post model.
     * @return Post model.
     */
    public PostModel getPostModel() {

        Object post = get(PostNotificationModelConstants.FieldPost);
        if (post != null && post.getClass() == PostModel.class)
            return (PostModel)post;

        return null;
    }

    /**
     * Sets the post model.
     * @param postModel Post model.
     */
    public void setPostModel(PostModel postModel) { put(PostNotificationModelConstants.FieldPost, postModel); }

    /**
     * Gets the notification type.
     * @return NotificationSubType.
     */
    public NotificationType getNotificationType() { return NotificationType.fromValue(getInt(PostNotificationModelConstants.FieldNotificationType)); }

    /**
     * Sets the notification type.
     * @param notificationType NotificationType
     */
    public void setNotificationType(NotificationType notificationType) { put(PostNotificationModelConstants.FieldNotificationType, notificationType.getValue()); }

    /**
     * Gets the number of likes.
     * @return Like Count.
     */
    public int getLikeCount() { return getInt(PostNotificationModelConstants.FieldLikeCount); }

    /**
     * Sets the number of likes.
     * @param value Like Count.
     */
    public void setLikeCount(int value) { put(PostNotificationModelConstants.FieldLikeCount, value); }
	
	/**
     * Gets the number of hugs.
     * @return Hug Count.
     */
    public int getHugCount() { return getInt(PostNotificationModelConstants.FieldHugCount); }

    /**
     * Sets the number of hugs.
     * @param value Hug Count.
     */
    public void setHugCount(int value) { put(PostNotificationModelConstants.FieldHugCount, value); }
	
	/**
     * Gets the number of meToos.
     * @return MeToo Count.
     */
    public int getMeTooCount() { return getInt(PostNotificationModelConstants.FieldMeTooCount); }

    /**
     * Sets the number of meToos.
     * @param value MeToo Count.
     */
    public void setMeTooCount(int value) { put(PostNotificationModelConstants.FieldMeTooCount, value); }
	
	/**
     * Gets the number of notes.
     * @return Note Count.
     */
    public int getNoteCount() { return getInt(PostNotificationModelConstants.FieldNoteCount); }

    /**
     * Sets the number of notes.
     * @param value Note Count.
     */
    public void setNoteCount(int value) { put(PostNotificationModelConstants.FieldNoteCount, value); }
	
	
	/**
     * Gets the number of inspiredBys.
     * @return InspiredBy Count.
     */
    public int getInspiredByCount() { return getInt(PostNotificationModelConstants.FieldInspiredByCount); }

    /**
     * Sets the number of inspiredBys.
     * @param value InspiredBy Count.
     */
    public void setInspiredByCount(int value) { put(PostNotificationModelConstants.FieldInspiredByCount, value); }

    /**
     * Gets whether the notification has been read.
     * @return Whether the notification has been read.
     */
    public boolean getRead() { return getBoolean(PostNotificationModelConstants.FieldRead); }

    /**
     * Sets whether the notification has been read.
     * @param isRead Whether the notification has been read.
     */
    public void setRead(boolean isRead) { put(PostNotificationModelConstants.FieldRead, isRead); }

    /**
     * Gets the notification message.
     * @return String.
     */
    public String getMessage() { return getString(PostNotificationModelConstants.FieldMessage); }

    /**
     * Sets the notification message.
     * @param value Message.
     */
    public void setMessage(String value) { put(PostNotificationModelConstants.FieldMessage, value); }

    /**
     * Gets the modified at date.
     * @return Date.
     */
    public Date getModifiedAt() { return getDate(PostNotificationModelConstants.FieldModifiedAt); }

    /**
     * Sets the modified at date.
     * @param value Date.
     */
    public void setModifiedAt(Date value) { put(PostNotificationModelConstants.FieldModifiedAt, value); }


    /**
     * Implements Comparable.
     * @param model The model to compare to.
     * @return Negative if less than, zero if equal, greater than zero if greater.
     */
    public int compareTo(PostNotificationModel model) {

        // Declare the order value.
        int order = 0;

        // Test for admin.
        if (this.getNotificationType() == NotificationType.DIRECT &&
            model.getNotificationType() == NotificationType.DIRECT) {

            if (this.getRead() == true && model.getRead() == true) {
                order = this.getModifiedAt().compareTo(model.getModifiedAt());
            } else if (this.getRead() == true && model.getRead() == false) {
                order = -1;
            } else order = 1;

        } else if (this.getNotificationType() == NotificationType.DIRECT &&
                   model.getNotificationType() != NotificationType.DIRECT) {

            if (this.getRead() == false)
                order = 1;
            else order = this.getModifiedAt().compareTo(model.getModifiedAt());

        } else if (this.getNotificationType() != NotificationType.DIRECT &&
                   model.getNotificationType() == NotificationType.DIRECT) {

            if (model.getRead() == false)
                order = -1;
            else order = this.getModifiedAt().compareTo(model.getModifiedAt());

        } else {

            order = this.getModifiedAt().compareTo(model.getModifiedAt());
        }


        return order;
    }

    /**
     * Describes the contents of the parcel.
     * @return Hash code of the parcel.
     */
    @Override
    public int describeContents() {

        return super.hashCode();
    }

    /**
     * Writes the contents of the post to a parcel.
     * @param parcel Parcel to write the contents to.
     * @param flags Parcel flags.
     */
    @Override
    public void writeToParcel(Parcel parcel, int flags) {

        // Write the post contents.
        parcel.writeString(getObjectId());
        parcel.writeParcelable(getPostModel(), flags);
        parcel.writeInt(getNotificationType().getValue());
        parcel.writeInt(getLikeCount());
        parcel.writeInt(getHugCount());
        parcel.writeInt(getMeTooCount());
        parcel.writeInt(getNoteCount());
        parcel.writeInt(getInspiredByCount());
        parcel.writeByte((byte) (getRead() ? 1 : 0));
        parcel.writeString(getMessage());
        parcel.writeLong(getModifiedAt().getTime());
    }

    /**
     * Creates the parcel.
     */
    public static Creator<PostNotificationModel> CREATOR = new Creator<PostNotificationModel>() {

        @Override
        public PostNotificationModel createFromParcel(Parcel parcel) {

            // Create the post.
            PostNotificationModel model = new PostNotificationModel();

            // Get the post contents.
            model.setObjectId(parcel.readString());
            model.setPostModel((PostModel)parcel.readParcelable(PostModel.class.getClassLoader()));
            model.setNotificationType(NotificationType.fromValue(parcel.readInt()));
            model.setLikeCount(parcel.readInt());
            model.setHugCount(parcel.readInt());
            model.setMeTooCount(parcel.readInt());
            model.setNoteCount(parcel.readInt());
            model.setInspiredByCount(parcel.readInt());
            model.setRead(parcel.readByte() != 0);
            model.setMessage(parcel.readString());
            model.setModifiedAt(new Date(parcel.readLong()));

            return model;
        }


        @Override
        public PostNotificationModel[] newArray(int size) {
            return new PostNotificationModel[size];
        }
    };

    /**
     * Creates a query adapter to fetch the posts notification.
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<PostNotificationModel> createPostNotificationFeedAdapterQuery() {


        return new ParseQueryAdapter.QueryFactory<PostNotificationModel>() {

            @Override
            public ParseQuery<PostNotificationModel> create() {

                // Create the query.
                ParseQuery<PostNotificationModel> query = new ParseQuery<PostNotificationModel>(
                        PostNotificationModel.class);

                // Add the filters.
                query.whereEqualTo(PostNotificationModelConstants.FieldUser, ParseUser.getCurrentUser());

                // Add the includes.
                query.include(PostNotificationModelConstants.FieldPost);

                // Add the sort.
                query.orderByDescending(PostNotificationModelConstants.FieldModifiedAt);

                return query;
            }
        };
    }

    /**
     * Creates a query adapter to fetch the moderator message notification
     * @param messageId The message id.
     * @return Newly created parse adapter.
     */
    public static ParseQuery<PostNotificationModel> createPostNotificationModeratorMessageQuery(final String messageId) {

        // Create the query.
        ParseQuery<PostNotificationModel> query = new ParseQuery<PostNotificationModel>(
                PostNotificationModel.class);

        // Add the filters.
        query.whereEqualTo(BaseModelConstants.FieldObjectId, messageId);

        // Add the includes.
        query.include(PostNotificationModelConstants.FieldPost);

        return query;
    }



    /**
     * Creates a query adapter to fetch the moderator message notification
     * @return Newly created parse adapter.
     */
    public static ParseQuery<PostNotificationModel> createPostNotificationUnreadModeratorMessageQuery() {

        // Create the query.
        ParseQuery<PostNotificationModel> query = new ParseQuery<PostNotificationModel>(
                PostNotificationModel.class);

        // Add the filters.
        query.whereEqualTo(PostNotificationModelConstants.FieldUser, ParseUser.getCurrentUser());
        query.whereEqualTo(PostNotificationModelConstants.FieldNotificationType, NotificationType.DIRECT.getValue());
        query.whereEqualTo(PostNotificationModelConstants.FieldRead, false);

        return query;
    }


    /**
     * Creates a query adapter to fetch the moderator message notification
     * @return Newly created parse adapter.
     */
    public static ParseQuery<PostNotificationModel> createPostNotificationUnreadMessageQuery() {

        // Create the query.
        ParseQuery<PostNotificationModel> query = new ParseQuery<PostNotificationModel>(
                PostNotificationModel.class);

        // Add the filters.
        query.whereEqualTo(PostNotificationModelConstants.FieldUser, ParseUser.getCurrentUser());
        query.whereEqualTo(PostNotificationModelConstants.FieldRead, false);

        return query;
    }

    /**
     * Creates a post notification.
     * @param context The context.
     * @param postModel The post to create the notification for.
     * @param notificationSubType The type of notification to create.
     */
    public static void CreateSocialNotification(final Context context, PostModel postModel, NotificationSubType notificationSubType) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postId", postModel.getObjectId());
        params.put("notificationType", NotificationType.SOCIAL.getValue());
        params.put("notificationSubType", notificationSubType.getValue());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("notificationCreate", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }

    /**
     * Creates a post notification.
     * @param context The context.
     * @param postModel The post to create the notification for.
     */
    public static void CreatePendingNotification(final Context context, PostModel postModel) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postId", postModel.getObjectId());
        params.put("notificationType", NotificationType.PENDING.getValue());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("notificationCreate", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }

    /**
     * Creates a post notification.
     * @param context The context.
     * @param postModel The post to create the notification for.
     */
    public static void CreateInspiredNotification(final Context context, PostModel postModel) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postId", postModel.getObjectId());
        params.put("notificationType", NotificationType.INSPIRED.getValue());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("notificationCreate", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }

    /**
     * Creates a post notification.
     * @param context The context.
     * @param postModel The post to create the notification for.
     */
    public static void CreateNoteNotification(final Context context, PostModel postModel) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postId", postModel.getObjectId());
        params.put("notificationType", NotificationType.NOTES.getValue());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("notificationCreate", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }

    /**
     * Creates a changed group notification.
     * @param context
     */
    public static void CreateChangeGroupNotification(final Context context) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("notificationType", NotificationType.CHANGE_GROUP.getValue());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("notificationCreateChangeGroup", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }

    /**
     * Deletes a post notification.
     * @param context The context.
     * @param postModel The post to create the notification for.
     * @param notificationSubType The type of notification to delete.
     */
    public static void DeleteSocialNotification(final Context context, PostModel postModel, NotificationSubType notificationSubType) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("postId", postModel.getObjectId());
        params.put("notificationType", NotificationType.SOCIAL.getValue());
        params.put("notificationSubType", notificationSubType.getValue());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("notificationDelete", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }

    /**
     * Sets a notification as read.
     * @param context
     * @param postNotificationModel
     */
    public static void SetNotifcationRead(final Context context, PostNotificationModel postNotificationModel) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(context) == false) {
            return;
        }

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("notificationId", postNotificationModel.getObjectId());

        // Call the cloud code.
        ParseCloud.callFunctionInBackground("notificationSetRead", params, new FunctionCallback<Object>() {

            @Override
            public void done(Object o, ParseException e) {

                // Check the exception.
                if (e != null) {
                    ApplicationManager.getInstance().showConnectionError(context);
                }
            }
        });
    }
}
