package com.nami.airapp.interfaces;

/**
 * Created by Brian on 12/8/2014.
 */
public interface PostHideListener {


    public void postHidden(String objectId);
}
