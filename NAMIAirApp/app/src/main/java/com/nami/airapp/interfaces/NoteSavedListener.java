package com.nami.airapp.interfaces;

import com.nami.airapp.data.models.FeelingModel;

import java.util.List;

/**
 * Created by Brian on 12/3/2014.
 */
public interface NoteSavedListener {

    public void noteSaved();
}
