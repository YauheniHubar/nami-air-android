package com.nami.airapp.interfaces;

/**
 * Created by Brian on 12/3/2014.
 */
public interface ShowPrivacyPolicyListener {

    public void showPrivacyPolicy();
}
