package com.nami.airapp.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.LinearLayout;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

import com.nami.airapp.R;
import com.nami.airapp.fragments.NoteFeedFragment;
import com.nami.airapp.fragments.SearchFeedFragment;

/**
 * Created by Brian on 11/26/2014.
 */
public class PostManager {

    /**
     * Sets the post style.
     * @param context Context
     * @param text Post text.
     */
    public static void setPostStyle(Context context, SpannableString text) {

        // Create the styled span.
        TextAppearanceSpan styledSpan = new TextAppearanceSpan(context, R.style.post_item);

        // Create the type face span.
        CalligraphyTypefaceSpan typeFaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(context.getAssets(), "fonts/proxima_nova_webfont.ttf"));

        // Set the spans.
        text.setSpan(typeFaceSpan, 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(styledSpan, 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

    }

    /**
     * Sets the first sentence style.
     * @param context Context
     * @param text Post text.
     */
    public static void setPostFirstSentenceStyle(Context context, SpannableString text) {

        // Find the first period and set the style.
        int periodPosition = TextUtils.indexOf(text, '.');
        if (periodPosition < 0) periodPosition = text.length();

        // Create the type face span.
        CalligraphyTypefaceSpan typeFaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(context.getAssets(), "fonts/museo_slab_500_webfont.ttf"));

        // Create the styled span.
        TextAppearanceSpan styledSpan = new TextAppearanceSpan(context, R.style.post_item_first_sentence);

        // Set the spans.
        text.setSpan(typeFaceSpan, 0 , periodPosition, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(styledSpan, 0 , periodPosition, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }


    /**
     * Sets the hash tag style.
     * @param context Context
     * @param fragment The fragment containing the post.
     * @param text Post text.
     */
    public static void setPostHashTagsStyle(Context context, final Fragment fragment, SpannableString text) {

        // Find all the hash tags and set the style.
        int hashTagPosition = TextUtils.indexOf(text, '#');
        while (hashTagPosition >= 0) {

            // Find the end of the hash tag.
            int hashTagPositionEnd = TextUtils.indexOf(text, ' ', hashTagPosition + 1);
            if (hashTagPositionEnd < 0) hashTagPositionEnd = text.length();

            // Extract the hash tag.
            final String hashTag = TextUtils.substring(text, hashTagPosition, hashTagPositionEnd)
                    .replace(".","")
                    .replace("!", "")
                    .replace(",", "")
                    .replace(" ", "");

            // Create the hash tag appearance span.
            TextAppearanceSpan hashTagAppearanceSpan = new TextAppearanceSpan(context, R.style.post_item_hash_tag);

            // Create the clickable span.
            ClickableSpan clickableSpan = new ClickableSpan() {

                @Override
                public void onClick(View view) {

                    // Create the fragment.
                    SearchFeedFragment searchFeedFragment = SearchFeedFragment.newInstance(hashTag);

                    // Create the transaction.
                    FragmentTransaction transaction = fragment.getFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                    transaction.replace(R.id.main_container, searchFeedFragment);
                    transaction.addToBackStack(null);

                    // Commit the transaction.
                    transaction.commit();
                }

                public void updateDrawState(TextPaint ds) {

                }
            };

            // Set the spans.
            text.setSpan(hashTagAppearanceSpan, hashTagPosition, hashTagPositionEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            text.setSpan(clickableSpan, hashTagPosition, hashTagPositionEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            // Find the next hash tag.
            hashTagPosition = TextUtils.indexOf(text, '#', hashTagPositionEnd + 1);
        }
    }

    /**
     * Sets the feelings style.
     * @param context Context
     * @param text Post text.
     */
    public static void setPostFeelingsStyle(Context context, SpannableString text) {

        // Create the feeling text.
        String feelingText = String.format(" %s ", context.getString(R.string.post_compose_feeling));

        // Find the feelings indicator.
        int feelingsPosition = TextUtils.indexOf(text, feelingText);
        if (feelingsPosition < 0) return;

        // Loop to find the last feelings indicator.
        while(TextUtils.indexOf(text, context.getString(R.string.post_compose_feeling), feelingsPosition + 1) >= 0) {
            feelingsPosition = TextUtils.indexOf(text, context.getString(R.string.post_compose_feeling), feelingsPosition + 1);
        }

        // Create the type face span.
        CalligraphyTypefaceSpan typeFaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(context.getAssets(), "fonts/proxima_nova_bold.ttf"));

        // Create the styled span.
        TextAppearanceSpan styledSpan = new TextAppearanceSpan(context, R.style.post_item_feelings);

        // Set the spans.
        text.setSpan(typeFaceSpan, feelingsPosition , text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(styledSpan, feelingsPosition , text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    /**
     * Sets the post padding and corners.
     * @param context Context
     * @param layoutContainer Layout
     * @param layoutItem Item
     * @param position Position of the item in the dataset.
     */
    public static void setPostPaddingCorners(Context context, LinearLayout layoutContainer, LinearLayout layoutItem, int position) {

        // Get the top padding values.
        int topPadding = layoutContainer.getPaddingTop();
        int sidePadding = topPadding;

        // Determine the side.
        if (position % 2 == 0) {
            layoutContainer.setPadding(0, topPadding, sidePadding, 0);
            layoutItem.setBackgroundResource(R.drawable.post_item_container_left);
        } else {
            layoutContainer.setPadding(sidePadding, topPadding, 0, 0);
            layoutItem.setBackgroundResource(R.drawable.post_item_container_right);
        }
    }

    /**
     * Sets the hash tag style.
     * @param context Context
     * @param fragment The fragment containing the post.
     * @param text Post text.
     */
    public static void setPostHashNotesStyle(Context context, final Fragment fragment, SpannableString text) {

        // Find all the hash tags and set the style.
        int hashTagPosition = TextUtils.indexOf(text, '#');
        while (hashTagPosition >= 0) {

            // Find the end of the hash tag.
            int hashTagPositionEnd = TextUtils.indexOf(text, ' ', hashTagPosition + 1);
            if (hashTagPositionEnd < 0) hashTagPositionEnd = text.length();

            // Extract the hash tag.
            final String hashTag = TextUtils.substring(text, hashTagPosition, hashTagPositionEnd)
                    .replace(".","")
                    .replace("!", "")
                    .replace(",", "")
                    .replace(" ", "");

            // Create the hash tag appearance span.
            TextAppearanceSpan hashTagAppearanceSpan = new TextAppearanceSpan(context, R.style.post_detail_note);

            // Create the clickable span.
            ClickableSpan clickableSpan = new ClickableSpan() {

                @Override
                public void onClick(View view) {

                    // Create the fragment.
                    SearchFeedFragment searchFeedFragment = SearchFeedFragment.newInstance(hashTag);

                    // Create the transaction.
                    FragmentTransaction transaction = fragment.getFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                    transaction.replace(R.id.main_container, searchFeedFragment);
                    transaction.addToBackStack(null);

                    // Commit the transaction.
                    transaction.commit();

                    // Remove the view.
                    ((android.view.ViewManager)view.getParent()).removeView(view);
                }

                public void updateDrawState(TextPaint ds) {

                }
            };

            // Set the spans.
            text.setSpan(hashTagAppearanceSpan, hashTagPosition, hashTagPositionEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            text.setSpan(clickableSpan, hashTagPosition, hashTagPositionEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            // Find the next hash tag.
            hashTagPosition = TextUtils.indexOf(text, '#', hashTagPositionEnd + 1);
        }
    }
}
