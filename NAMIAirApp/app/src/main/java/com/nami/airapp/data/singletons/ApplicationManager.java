package com.nami.airapp.data.singletons;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.text.TextUtils;

import java.security.acl.Group;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import com.nami.airapp.R;
import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.models.PostHugModel;
import com.nami.airapp.data.models.PostLikeModel;
import com.nami.airapp.data.models.PostMeTooModel;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.interfaces.PostRemovedListener;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import com.nami.airapp.data.enums.UserType;
import com.nami.airapp.data.models.FeelingModel;
import com.nami.airapp.data.models.NoteModel;
import com.nami.airapp.data.models.PostFlagModel;
import com.nami.airapp.data.models.RedFlagKeywordModel;

import com.nami.airapp.interfaces.PostHideListener;
import com.nami.airapp.interfaces.AuthorBlockListener;
import com.nami.airapp.interfaces.GroupChangeListener;


/**
 * Contains data related to the application.
 */
public class ApplicationManager
{

    // Private Static Members.
    private static final String KEY_APP_NAME = "com.nami.airapp";
    private static final String KEY_PREFERENCES_SHOWN_GUIDED_TOUR = "KEY_PREFERENCES_SHOWN_GUIDED_TOUR";

    // Private Members.
    private List<NoteModel> dataSetNotes = new ArrayList<NoteModel>();
    private List<FeelingModel> dataSetFeelings = new ArrayList<FeelingModel>();
    private HashMap<String, PostFlagModel> dataSetUserFlaggedPosts = new HashMap<String, PostFlagModel>();
    private HashMap<String, PostFlagModel> dataSetModeratorFlaggedPosts = new HashMap<String, PostFlagModel>();
    private List<RedFlagKeywordModel> dataSetRedFlagKeywords = new ArrayList<RedFlagKeywordModel>();
    private HashMap<String, String> dataSetUpdatedPosts = new HashMap<String, String>();
    private HashMap<String, PostLikeModel> dataSetPostLikes = new HashMap<String, PostLikeModel>();
    private HashMap<String, PostHugModel> dataSetPostHugs = new HashMap<String, PostHugModel>();
    private HashMap<String, PostMeTooModel> dataSetPostMeToos = new HashMap<String, PostMeTooModel>();

    private AlertDialog connectionDialog = null;
    private Boolean isConnected = null;
    private boolean acceptedTermsOfUse = false;
    private boolean cancelledTermsOfUse = false;
    private boolean shownWelcomeAnimation = false;
    private String moderatorMessageId;
    private int notificationBadgeCount = 0;
    private MainActivity mainActivity = null;

    private HashMap<PostHideListener, PostHideListener> postHideListeners = new HashMap<PostHideListener, PostHideListener>();
    private HashMap<AuthorBlockListener, AuthorBlockListener> authorBlockListeners = new HashMap<AuthorBlockListener, AuthorBlockListener>();
    private HashMap<GroupChangeListener, GroupChangeListener> groupChangeListeners = new HashMap<GroupChangeListener, GroupChangeListener>();
    private HashMap<PostRemovedListener, PostRemovedListener> postRemovedListeners = new HashMap<PostRemovedListener, PostRemovedListener>();

    private HashMap<String, String> popupData = new HashMap<String, String>();

    private BroadcastReceiver connectionReceiver = null;


    // Private Static Members.
    private static ApplicationManager _instance;

    /**
     * Private Constructor.
     */
    private ApplicationManager() { }

    /**
     * Gets/Creates the ApplicationManager instance.
     * @return ApplicationManager instance.
     */
    public synchronized static ApplicationManager getInstance()
    {
        if (_instance == null)
        {
            _instance = new ApplicationManager();
        }
        return _instance;
    }

    /**
     * Sets the popup data.
     * @param popupTitle
     * @param linkTitle
     * @param linkUrl
     */
    public void setPopupData(String popupTitle, String linkTitle, String linkUrl) {

        this.popupData.put("popupTitle", popupTitle);
        this.popupData.put("linkTitle", linkTitle);
        this.popupData.put("linkUrl", linkUrl);
    }

    /**
     * Determines if there is popup data.
     * @return
     */
    public Boolean hasPopupData() {

        if (this.popupData.containsKey("popupTitle")) {
            if (this.popupData.get("popupTitle").length() > 0)
                return true;
        }

        return false;
    }

    /**
     * Gets the popup data.
     * @return
     */
    public HashMap<String, String> getPopupData() {
        return this.popupData;
    }

    public void clearPopupData() {
        this.popupData.put("popupTitle", "");
    }

    /**
     * Determines if there is a network connection.
     * @param context
     * @return
     */
    public boolean hasNetworkConnection(Context context) {

        // Check if the network has been checked.
        if (this.isConnected == null) {

            // Create the connectivity manager.
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            this.isConnected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        }

        return this.isConnected;
    }

    /**
     * Checks the network connection and displays the error
     * @param context
     * @return
     */
    public boolean checkNetworkConnection(Context context) {

        if (this.isConnected == false) {
            this.showConnectionError(context);
        }

        return this.isConnected;
    }

    /**
     * Configures the connection receiver.
     */
    public void configureConnectionReceiver(Context context) {

        // Test the broadcast receiver.
        if (this.connectionReceiver == null) {

            // Create the receiver.
            this.connectionReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {

                    boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
                    String reason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);
                    boolean isFailover = intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false);

                    NetworkInfo currentNetworkInfo = (NetworkInfo)intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);

                    // Set the connection status.
                    isConnected = !noConnectivity;
                }
            };

            // Register the receiver.
            context.registerReceiver(this.connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    /**
     * Fetches the note list.
     */
    public void fetchNoteList() {

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<NoteModel> queryFactory = NoteModel.createNoteAdapterQuery();

        // Create and execute the query.
        ParseQuery<NoteModel> query = queryFactory.create();
        new NoteParseQueryTask().execute(query);
    }

    /**
     * Fetches the feelings list.
     */
    public void fetchFeelingsList() {

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<FeelingModel> queryFactory = FeelingModel.createFeelingAdapterQuery();

        // Create and execute the query.
        ParseQuery<FeelingModel> query = queryFactory.create();
        new FeelingParseQueryTask().execute(query);
    }

    /**
     * Fetches the red flag keyword list.
     */
    public void fetchRedFlagKeywordList() {

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<RedFlagKeywordModel> queryFactory = RedFlagKeywordModel.createRedFlagKeywordAdapterQuery();

        // Create and execute the query.
        ParseQuery<RedFlagKeywordModel> query = queryFactory.create();
        new RedFlagKeywordParseQueryTask().execute(query);
    }

    /**
     * Fetches the user flagged posts.
     */
    public void fetchUserFlaggedPosts() {

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<PostFlagModel> queryFactory = PostFlagModel.createPostFlagAdapterQuery();

        // Create and execute the query.
        try {

            ParseQuery<PostFlagModel> query = queryFactory.create();
            List<PostFlagModel> flaggedPosts = query.find();

            // Add the flagged posts to the hash set.
            this.dataSetUserFlaggedPosts = new HashMap<String, PostFlagModel>();
            for (PostFlagModel flaggedPost : flaggedPosts) {
                if (flaggedPost.getPostModel() != null) {
                    this.dataSetUserFlaggedPosts.put(flaggedPost.getPostModel().getObjectId(), flaggedPost);
                }
            }
        }
        catch (ParseException e) {

            this.dataSetUserFlaggedPosts = new HashMap<String, PostFlagModel>();
        }
    }

    /**
     * Fetches the moderator flagged posts.
     */
    public void fetchModeratorFlaggedPosts() {

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<PostFlagModel> queryFactory = PostFlagModel.createModeratorPostFlagAdapterQuery();

        // Create and execute the query.
        try {

            ParseQuery<PostFlagModel> query = queryFactory.create();
            List<PostFlagModel> flaggedPosts = query.find();

            // Add the flagged posts to the hash set.
            this.dataSetModeratorFlaggedPosts = new HashMap<String, PostFlagModel>();
            for (PostFlagModel flaggedPost : flaggedPosts) {
                this.dataSetModeratorFlaggedPosts.put(flaggedPost.getPostModel().getObjectId(), flaggedPost);
            }
        }
        catch (ParseException e) {

            this.dataSetModeratorFlaggedPosts = new HashMap<String, PostFlagModel>();
        }
    }


    /**
     * Gets the note list.
     * @return Note list.
     */
    public List<NoteModel> getDataSetNotes() {

        return this.dataSetNotes;
    }

    /**
     * Gets the feeling list.
     * @return Feeling list.
     */
    public List<FeelingModel> getDataSetFeelings() {

        return this.dataSetFeelings;
    }

    /**
     * Gets the red flag keyword list.
     * @return Red Flag Keyword list.
     */
    public List<RedFlagKeywordModel> getDataSetRedFlagKeywords() {

        return this.dataSetRedFlagKeywords;
    }

    /**
     * Finds the post flag model for a post.
     * @param postModel The post to find the flag model for.
     * @return PostFlagModel
     */
    public PostFlagModel findPostFlaggedStatus(PostModel postModel) {

        // Determine if the hash map contains the post.
        if (this.dataSetUserFlaggedPosts.containsKey(postModel.getObjectId())) {
            return this.dataSetUserFlaggedPosts.get(postModel.getObjectId());
        }

        return null;
    }

    /**
     * Finds the post flag model for a post.
     * @param postModel The post to find the flag model for.
     * @return PostFlagModel
     */
    public PostFlagModel findModeratorPostFlaggedStatus(PostModel postModel) {

        // Determine if the hash map contains the post.
        if (this.dataSetModeratorFlaggedPosts.containsKey(postModel.getObjectId())) {
            return this.dataSetModeratorFlaggedPosts.get(postModel.getObjectId());
        }

        return null;
    }

    /**
     * Gets whether the user has accepted the terms of use.
     * @return Boolean
     */
    public boolean getAcceptedTermsOfUse() {

        return this.acceptedTermsOfUse;
    }

    /**
     * Sets whether the user has accepted the terms of use.
     * @param value
     */
    public void setAcceptedTermsOfUse(boolean value) {

        this.acceptedTermsOfUse = value;
    }

    /**
     * Gets whether the user cancelled the terms of use.
     * @return Boolean
     */
    public boolean getCancelledTermsOfUse() {

        boolean cancelled = this.cancelledTermsOfUse;
        this.cancelledTermsOfUse = false;
        return cancelled;
    }

    /**
     * Sets whether the user cancelled the terms of user.
     * @param value Boolean
     */
    public void setCancelledTermsOfUse(boolean value) {

        this.cancelledTermsOfUse = value;
    }

    /**
     * Gets whether the welcome animation has been shown.
     * @return
     */
    public boolean getShownWelcomeAnimation() {

        return this.shownWelcomeAnimation;
    }

    /**
     * Sets whether the welcome animation has been shown.
     * @param value
     */
    public void setShownWelcomeAnimation(boolean value) {

        this.shownWelcomeAnimation = value;
    }

    /**
     * Gets the moderator message id if any.
     * @return Moderator message Id.
     */
    public String getModeratorMessageId() {

        return this.moderatorMessageId;
    }

    /**
     * Sets the moderator message id.
     * @param value Moderator message id.
     */
    public void setModeratorMessageId(String value) {

        this.moderatorMessageId = value;
    }

    /**
     * Adds a PostHideListener.
     * @param postHideListener
     */
    public void addPostHideListener(PostHideListener postHideListener) {

        this.postHideListeners.put(postHideListener, postHideListener);
    }

    /**
     * Adds an AuthorBlockListener
     * @param authorBlockListener
     */
    public void addAuthorBlockListener(AuthorBlockListener authorBlockListener) {

        this.authorBlockListeners.put(authorBlockListener, authorBlockListener);
    }

    /**
     * Adds a GroupChangeListener.
     * @param groupChangeListener
     */
    public void addGroupChangeListener(GroupChangeListener groupChangeListener) {

        this.groupChangeListeners.put(groupChangeListener, groupChangeListener);
    }

    /**
     * Adds a PostRemovedListener.
     * @param postRemovedListener
     */
    public void addPostRemovedListener(PostRemovedListener postRemovedListener) {

        this.postRemovedListeners.put(postRemovedListener, postRemovedListener);
    }

    /**
     * Notifies the PostHideListeners
     * @param objectId
     */
    public void notifyPostHideListeners(String objectId) {

        // Loop through the listeners.
        for (PostHideListener postHideListener : this.postHideListeners.keySet()) {
            postHideListener.postHidden(objectId);
        }
    }

    /**
     * Notifies the AuthorBlockListeners
     * @param objectId
     */
    public void notifyAuthorBlockListeners(String objectId) {

        // Loop through the listeners.
        for (AuthorBlockListener authorBlockListener : this.authorBlockListeners.keySet()) {
            authorBlockListener.authorBlocked(objectId);
        }
    }

    /**
     * Notifies the GroupChangeListeners
     * @param userType
     */
    public void notifyGroupChangeListeners(UserType userType) {

        // Loop through the listeners.
        for (GroupChangeListener groupChangeListener : this.groupChangeListeners.keySet()) {
            groupChangeListener.groupChanged(userType);
        }
    }


    public void notifyPostRemoveListeners(PostModel postModel) {

        // Loop through the listeners.
        for (PostRemovedListener postRemovedListener : this.postRemovedListeners.keySet()) {
            postRemovedListener.PostRemoved(postModel);
        }
    }

    /**
     * Gets the notification badge count.
     * @return
     */
    public int getNotificationBadgeCount() {

        return this.notificationBadgeCount;
    }

    /**
     * Sets badge count.
     * @param mainActivity
     * @param badgeCount
     */
    public void setNotificationBadgeCount(MainActivity mainActivity, int badgeCount) {

        // Set the members.
        this.mainActivity = mainActivity;
        this.notificationBadgeCount = badgeCount;

        // Update the tab.
        mainActivity.setNotificationBadgeCount(this.notificationBadgeCount);
    }

    /**
     * Decreases the badge count.
     */
    public void decreaseNotificationBadgeCount() {

        // Decrease the badge count.
        this.notificationBadgeCount--;

        // Update the tab.
        mainActivity.setNotificationBadgeCount(this.notificationBadgeCount);
    }

    /**
     * Adds a post as updated and needs to be refreshed.
     * @param objectId
     */
    public void addUpdatedPost(String objectId) {

        // Add it to the map.
        if (this.dataSetUpdatedPosts.containsValue(objectId) == false) {
            this.dataSetUpdatedPosts.put(objectId, objectId);
        }
    }

    /**
     * Returns true if the post needs updating.
     * @param objectId
     * @return
     */
    public boolean postNeedsUpdating(String objectId) {

        // Declare the return value.
        boolean postNeedsUpdating = false;

        // Check the data set.
        if (this.dataSetUpdatedPosts.containsValue(objectId)) {
            postNeedsUpdating = true;
            this.dataSetUpdatedPosts.remove(objectId);
        }

        return postNeedsUpdating;
    }

    /**
     * Adds the post likes to the hash map.
     * @param postLikes
     */
    public void addPostLikes(ArrayList<PostLikeModel> postLikes) {
        
        if (postLikes != null) {
            for (PostLikeModel postLike : postLikes) {
                this.dataSetPostLikes.put(postLike.getPostModel().getObjectId(), postLike);
            }
        }
    }

    /**
     * Adds the post Hugs to the hash map.
     * @param postHugs
     */
    public void addPostHugs(ArrayList<PostHugModel> postHugs) {

        if (postHugs != null) {
            for (PostHugModel postHug : postHugs) {
                this.dataSetPostHugs.put(postHug.getPostModel().getObjectId(), postHug);
            }
        }
    }

    /**
     * Adds the post MeToos to the hash map.
     * @param postMeToos
     */
    public void addPostMeToos(ArrayList<PostMeTooModel> postMeToos) {

        if (postMeToos != null) {
            for (PostMeTooModel postMeToo : postMeToos) {
                this.dataSetPostMeToos.put(postMeToo.getPostModel().getObjectId(), postMeToo);
            }
        }
    }

    /**
     * Removes the post like from the hash map.
     * @param objectId
     */
    public void removePostLike(String objectId) {
        
        this.dataSetPostLikes.remove(objectId);
    }

    /**
     * Removes the post Hug from the hash map.
     * @param objectId
     */
    public void removePostHug(String objectId) {

        this.dataSetPostHugs.remove(objectId);
    }

    /**
     * Removes the post MeToo from the hash map.
     * @param objectId
     */
    public void removePostMeToo(String objectId) {

        this.dataSetPostMeToos.remove(objectId);
    }

    /**
     * Determines if the post like is in the hash map.
     * @param objectId
     * @return
     */
    public PostLikeModel hasPostLike(String objectId) {
        
        return this.dataSetPostLikes.get(objectId);
    }

    /**
     * Determines if the post Hug is in the hash map.
     * @param objectId
     * @return
     */
    public PostHugModel hasPostHug(String objectId) {

        return this.dataSetPostHugs.get(objectId);
    }

    /**
     * Determines if the post MeToo is in the hash map.
     * @param objectId
     * @return
     */
    public PostMeTooModel hasPostMeToo(String objectId) {

        return this.dataSetPostMeToos.get(objectId);
    }

    /**
     * Determines if the user has been shown the guided tour.
     * @param context
     * @return
     */
    public boolean hasShownGuidedTour(Context context) {

        // Get the preferences.
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_APP_NAME, Context.MODE_PRIVATE);

        // Attempt to get the key.
        return sharedPreferences.getBoolean(KEY_PREFERENCES_SHOWN_GUIDED_TOUR, false);
    }

    /**
     * Sets that the user has been shown the guided tour.
     * @param context
     */
    public void setShownGuidedTour(Context context) {

        // Get the preferences.
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_APP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();

        // Set the key.
        sharedPreferencesEditor.putBoolean(KEY_PREFERENCES_SHOWN_GUIDED_TOUR, true);
        sharedPreferencesEditor.apply();
    }

    /**
     * Shows the connection error.
     */
    public void showConnectionError(Context context) {

        // Check the dialog.
        if (this.connectionDialog != null && this.connectionDialog.isShowing())
            return;

        // Create the dialog.
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(context.getString(R.string.connection_error_title));
        dialogBuilder.setMessage(context.getString(R.string.connection_error_message));
        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
//        if(!((Activity) context).isFinishing())
//        {
//            //show dialog
//        }
        // Show the dialog.
        this.connectionDialog = dialogBuilder.create();
        this.connectionDialog.show();

    }

    /**
     * Queries parse.
     */
    private class NoteParseQueryTask extends AsyncTask<ParseQuery<NoteModel>, Void, List<NoteModel>> {

        @Override
        protected void onPreExecute() {

            // Create the posts.
            dataSetNotes = new ArrayList<NoteModel>();
        }

        @Override
        protected List<NoteModel> doInBackground(ParseQuery<NoteModel>... queries) {

            // Declare the return value.
            List<NoteModel> data = null;

            // Get the query.
            ParseQuery<NoteModel> query = queries[0];

            // Attempt to execute the query.
            try {
                data = query.find();
            }
            catch (ParseException e) {
                data = new ArrayList<NoteModel>();
            }

            return data;
        }

        @Override
        protected void onPostExecute(List<NoteModel> notesFound) {

            // Set the data set.
            dataSetNotes = notesFound;
        }
    }

    /**
     * Queries parse.
     */
    private class FeelingParseQueryTask extends AsyncTask<ParseQuery<FeelingModel>, Void, List<FeelingModel>> {

        @Override
        protected void onPreExecute() {

            // Create the posts.
            dataSetFeelings = new ArrayList<FeelingModel>();
        }

        @Override
        protected List<FeelingModel> doInBackground(ParseQuery<FeelingModel>... queries) {

            // Declare the return value.
            List<FeelingModel> data = null;

            // Get the query.
            ParseQuery<FeelingModel> query = queries[0];

            // Attempt to execute the query.
            try {
                data = query.find();
            }
            catch (ParseException e) {
                data = new ArrayList<FeelingModel>();
            }

            return data;
        }

        @Override
        protected void onPostExecute(List<FeelingModel> feelingsFound) {

            // Set the data set.
            dataSetFeelings = feelingsFound;
        }
    }
	
	/**
     * Queries parse.
     */
    private class RedFlagKeywordParseQueryTask extends AsyncTask<ParseQuery<RedFlagKeywordModel>, Void, List<RedFlagKeywordModel>> {

        @Override
        protected void onPreExecute() {

            // Create the posts.
            dataSetRedFlagKeywords = new ArrayList<RedFlagKeywordModel>();
        }

        @Override
        protected List<RedFlagKeywordModel> doInBackground(ParseQuery<RedFlagKeywordModel>... queries) {

            // Declare the return value.
            List<RedFlagKeywordModel> data = null;

            // Get the query.
            ParseQuery<RedFlagKeywordModel> query = queries[0];

            // Attempt to execute the query.
            try {
                data = query.find();
            }
            catch (ParseException e) {
                data = new ArrayList<RedFlagKeywordModel>();
            }

            return data;
        }

        @Override
        protected void onPostExecute(List<RedFlagKeywordModel> redFlagKeywordsFound) {

            // Set the data set.
            dataSetRedFlagKeywords = redFlagKeywordsFound;
        }
    }



}