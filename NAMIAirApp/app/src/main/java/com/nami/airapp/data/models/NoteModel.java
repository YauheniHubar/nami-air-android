package com.nami.airapp.data.models;

import com.parse.ParseObject;
import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import com.nami.airapp.data.constants.NoteModelConstants;
/**
 * Contains data about a Note.
 */
@ParseClassName("Note")
public class NoteModel extends ParseObject {

    // Private Members.
    private boolean isSelected;

    /**
     * Gets the note name.
     * @return Note name.
     */
    public String getName() { return getString(NoteModelConstants.FieldName); }

    /**
     * Sets the note name.
     * @param value Note name.
     */
    public void setName(String value) { put(NoteModelConstants.FieldName, value); }

    /**
     * Gets the selected value.
     * @return True if selected, otherwise false;
     */
    public boolean getIsSelected() { return this.isSelected; }

    /**
     * Sets the selected value.
     * @param value Selected value.
     */
    public void setIsSelected(boolean value) { this.isSelected = value; }
	
	/**
     * Creates a query adapter to fetch the notes.
     * @return Newly created parse adapter.
     */
    public static ParseQueryAdapter.QueryFactory<NoteModel> createNoteAdapterQuery() {


        return new ParseQueryAdapter.QueryFactory<NoteModel>() {

            @Override
            public ParseQuery<NoteModel> create() {

                // Create the query.
                ParseQuery<NoteModel> query = new ParseQuery<NoteModel>(
                        NoteModel.class);                

                // Set the sort.
                query.orderByAscending(NoteModelConstants.FieldName);

                return query;
            }
        };
    }
}
