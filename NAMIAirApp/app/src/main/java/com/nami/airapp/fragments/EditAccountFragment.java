package com.nami.airapp.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nami.airapp.R;
import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.enums.UserType;
import com.nami.airapp.data.models.PostNotificationModel;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.AppFragmentManager;
import com.nami.airapp.utils.KeyboardManager;
import com.nami.airapp.utils.ModelValidator;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import org.w3c.dom.Text;

/**
 * Post Detail Fragment.
 */
public class EditAccountFragment extends Fragment implements OnClickListener {


    // Private Members.
    private Context context = null;
    private EditAccountViewContainer viewContainer = null;
    private boolean accountSwitchedGroups = false;

    /**
     * Default Constructor.
     */
    public EditAccountFragment() {}

    /**
     * Creates a new instance of a PostDetailFragment.
     * @return New instance of a PostDetailFragment.
     */
    public static EditAccountFragment newInstance() {

        // Create the new instance.
        EditAccountFragment instance = new EditAccountFragment();

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set the action bar.
        MainActivity mainActivity = (MainActivity)context;
        mainActivity.setActionBarNoTabs();

        // Set options menu.
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_account, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        /*
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    AppFragmentManager.RemoveFromStack(that, context);
                    return true;
                }

                return false;
            }
        });
        */
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, context);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (view == this.viewContainer.buttonSave) {
            this.attemptUserEdit();
        }
    }

    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.edit_account_title));
    }

    /**
     * Attempts to edit a user.
     */
    private void attemptUserEdit() {

        // Hide the keyboard.
        KeyboardManager.HideActivityKeyboard(context);


        // Validate the user edit.
        ModelValidator<ParseUser> validator = this.validateUserEdit();
        if (!validator.isValid()) {

            // Set the error message.
            this.viewContainer.textViewError.setText(validator.getMessage());

            // Show the error message.
            this.viewContainer.layoutError.setVisibility(View.VISIBLE);

        } else {

            // Hide the error message.
            this.viewContainer.layoutError.setVisibility(View.GONE);

            // Get the current user.
            ParseUser user = ParseUser.getCurrentUser();

            // Start the Edit Account Task.
            new EditAccountTask().execute(user);
        }
    }


    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = EditAccountViewContainer.with(view);

        // Set the click events.
        this.viewContainer.buttonSave.setOnClickListener(this);

        // Set the username.
        this.viewContainer.editTextEmail.setText(ParseUser.getCurrentUser().getUsername());

        // Set the appropriate user type.
        UserType userType = UserType.fromValue(ParseUser.getCurrentUser().getInt(UserModelConstants.FieldUserType));
        if (userType == UserType.USER)
            this.viewContainer.radioUser.setChecked(true);
        else this.viewContainer.radioCaregiver.setChecked(true);
    }



    /**
     * Attempts to validate the user edit.
     * @return ModelValidator.
     */
    private ModelValidator<ParseUser> validateUserEdit() {

        // Get the values.
        String emailAddress = this.viewContainer.editTextEmail.getText().toString();
        String password = this.viewContainer.editTextPassword.getText().toString();
        String passwordConfirm = this.viewContainer.editTextPasswordConfirm.getText().toString();
        boolean isUser = this.viewContainer.radioUser.isChecked();
        boolean isCaregiver = this.viewContainer.radioCaregiver.isChecked();

        // Ensure there is a password.
        if (TextUtils.isEmpty(password)) {
            return new ModelValidator<ParseUser>(null, false, getString(R.string.edit_account_validation_password_missing));
        }

        // Ensure there is a password confirm.
        if (TextUtils.isEmpty(passwordConfirm)) {
            return new ModelValidator<ParseUser>(null, false, getString(R.string.edit_account_validation_password_confirm_missing));
        }

        // Check the passwords.
        if (!TextUtils.equals(password, passwordConfirm)) {
            return new ModelValidator<ParseUser>(null, false, getString(R.string.edit_account_validation_password_mismatch));
        }

        // Return success.
        return new ModelValidator<ParseUser>(null, true, null);
    }

    /**
     * Shows the choose group dialog.
     */
    private void showChangeGroupDialog() {

        // Create the dialog.
        final Dialog changeGroupDialog = new Dialog(context);
        changeGroupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        changeGroupDialog.setContentView(R.layout.dialog_change_group);

        // Get the button.
        Button changeGroupDialogOk = (Button)changeGroupDialog.findViewById(R.id.dialog_change_group_ok);
        changeGroupDialogOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                changeGroupDialog.dismiss();
            }
        });

        // Show the dialog.
        changeGroupDialog.show();

        // Get the current user and notify the listeners.
        ParseUser user = ParseUser.getCurrentUser();
        ApplicationManager.getInstance().notifyGroupChangeListeners(UserType.fromValue(user.getInt(UserModelConstants.FieldUserType)));
    }

    /**
     * Task to Edit Account
     */
    private class EditAccountTask extends AsyncTask<ParseUser, Void, ParseException> {

        // Private Members.
        private ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {

            // Create the progress dialog.
            this.progressDialog = new ProgressDialog(context);
            this.progressDialog.setTitle(getString(R.string.edit_account_progress_title));
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        @Override
        protected ParseException doInBackground(ParseUser... users) {

            // Get the user.
            ParseUser user = users[0];

            // Attempt to create account.
            try
            {
                // Attempt to login with the current user.
                String username = user.getUsername();
                String password = viewContainer.editTextPassword.getText().toString();
                ParseUser.logIn(username, password);


                // Determine if the user switched groups.
                boolean userSelected = viewContainer.radioUser.isChecked();
                boolean careGiverSelected = viewContainer.radioCaregiver.isChecked();
                UserType userType = UserType.fromValue(ParseUser.getCurrentUser().getInt(UserModelConstants.FieldUserType));
                if (userType == UserType.USER && userSelected == false)
                    accountSwitchedGroups = true;
                else if (userType == UserType.CAREGIVER && careGiverSelected == false)
                    accountSwitchedGroups = true;
                else accountSwitchedGroups = false;

                // Set the user type.
                if (userSelected == true)
                    user.put(UserModelConstants.FieldUserType, UserType.USER.getValue());
                else user.put(UserModelConstants.FieldUserType, UserType.CAREGIVER.getValue());

                // Set the username.
                if (TextUtils.isEmpty(viewContainer.editTextEmail.getText().toString()) == false) {
                    user.setEmail(viewContainer.editTextEmail.getText().toString());
                    user.setUsername(viewContainer.editTextEmail.getText().toString());
                }

                // Set the password.
                if (TextUtils.isEmpty(viewContainer.editTextPassword.getText().toString()) == false) {
                    user.setPassword(viewContainer.editTextPassword.getText().toString());
                }

                // Save the user.
                user.save();

                // Login again to update user information.
                user.logIn(username, password);
            }
            catch(ParseException e) {

                return e;
            }

            return null;
        }

        @Override
        protected void onPostExecute(ParseException e) {

            // Dismiss the progress dialog.
            this.progressDialog.dismiss();

            // Check for an exception.
            if (e != null) {

                // Set the error message.
                viewContainer.textViewError.setText(e.getLocalizedMessage());

                // Show the error message.
                viewContainer.layoutError.setVisibility(View.VISIBLE);

            } else {


                // Show the changed group dialog if necessary.
                if (accountSwitchedGroups == true) {
                    showChangeGroupDialog();

                    // Create the notification.
                    PostNotificationModel.CreateChangeGroupNotification(context);
                }
            }
        }
    }

    /**
     * Holds the view for the Edit Account
     */
    private static class EditAccountViewContainer {

        // Private Members.
        private EditText editTextEmail = null;
        private EditText editTextPassword = null;
        private EditText editTextPasswordConfirm = null;
        private RadioButton radioUser = null;
        private RadioButton radioCaregiver = null;
        private LinearLayout layoutError = null;
        private TextView textViewError = null;
        private Button buttonSave = null;

        // Private Constructor.
        private EditAccountViewContainer() {}

        // Static Constructor.
        public static EditAccountViewContainer with(View view) {

            // Create the instance
            EditAccountViewContainer instance = new EditAccountViewContainer();

            // Get the items.
            instance.editTextEmail = (EditText)view.findViewById(R.id.edit_account_edit_text_email);
            instance.editTextPassword = (EditText)view.findViewById(R.id.edit_account_edit_text_password);
            instance.editTextPasswordConfirm = (EditText)view.findViewById(R.id.edit_account_edit_text_password_confirm);
            instance.radioUser = (RadioButton)view.findViewById(R.id.edit_account_radio_user);
            instance.radioCaregiver = (RadioButton)view.findViewById(R.id.edit_account_radio_caregiver);
            instance.buttonSave = (Button)view.findViewById(R.id.edit_account_button_save);
            instance.layoutError = (LinearLayout)view.findViewById(R.id.edit_account_layout_error);
            instance.textViewError = (TextView)view.findViewById(R.id.edit_account_text_view_error);

            return instance;
        }
    }
}
