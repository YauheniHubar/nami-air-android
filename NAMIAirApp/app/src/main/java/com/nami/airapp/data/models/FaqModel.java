package com.nami.airapp.data.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.nami.airapp.data.enums.ApprovedStatus;
import com.nami.airapp.data.enums.FlaggedStatus;
import com.nami.airapp.data.enums.UserType;

import java.util.Date;

public class FaqModel implements Parcelable {

    public String Title;

    public String Answer;

    /**
     * Describes the contents of the parcel.
     * @return Hash code of the parcel.
     */
    @Override
    public int describeContents() {

        return super.hashCode();
    }

    /**
     * Writes the contents of the post to a parcel.
     * @param parcel Parcel to write the contents to.
     * @param flags Parcel flags.
     */
    @Override
    public void writeToParcel(Parcel parcel, int flags) {

        // Write the post contents.
        parcel.writeString(Title);
        parcel.writeString(Answer);
    }

    /**
     * Creates the parcel.
     */
    public static Creator<FaqModel> CREATOR = new Creator<FaqModel>() {

        @Override
        public FaqModel createFromParcel(Parcel parcel) {

            // Create the post.
            FaqModel model = new FaqModel();

            // Get the post contents.
            model.Title = parcel.readString();
            model.Answer = parcel.readString();
            return model;
        }


        @Override
        public FaqModel[] newArray(int size) {
            return new FaqModel[size];
        }
    };

}
