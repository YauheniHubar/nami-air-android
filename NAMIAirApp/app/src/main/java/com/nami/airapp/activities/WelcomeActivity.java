package com.nami.airapp.activities;

import android.app.Application;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.utils.DisplayManager;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.view.ViewHelper;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import com.nami.airapp.utils.KeyboardManager;
import com.nami.airapp.utils.ModelValidator;
import com.nami.airapp.R;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.animation.ObjectAnimator;


/**
 * Welcome Activity.
 */
public class WelcomeActivity extends ActionBarActivity implements OnClickListener {

    // Private Members.
    private WelcomeActivityViewContainer viewContainer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the requested orientation.
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Initialize the activity and action bar.
        this.initializeActivity();
        this.initializeActionBar();

        // Set the content view.
        setContentView(R.layout.activity_welcome);

        // Initialize the layout content.
        this.initializeLayoutContent();


    }

    @Override
    protected void onResume() {
        super.onResume();

        // Start the animation.
        if (ApplicationManager.getInstance().getShownWelcomeAnimation() == false)
            this.startAnimation();
        else {

            this.viewContainer.layoutBubbles.setVisibility(View.GONE);
            this.viewContainer.layoutWelcomeMain.setVisibility(View.VISIBLE);
            this.viewContainer.imageViewLogoAnon.setVisibility(View.VISIBLE);
            this.viewContainer.imageViewLogoSupport.setVisibility(View.VISIBLE);
            this.viewContainer.imageViewLogoNami.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(new CalligraphyContextWrapper(newBase));
    }


    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (view == this.viewContainer.textViewLogin) {
            this.showLoginLayout();
        } else if (view == this.viewContainer.textViewSignUp) {
            this.showCreateAccountLayout();
        } else if (view == this.viewContainer.buttonLogin) {
            this.attemptUserLogin();
        } else if (view == this.viewContainer.buttonCreateAccount) {
            this.showCreateAccount();
        } else if (view == this.viewContainer.textViewTerms) {
            this.showTermsOfUse();
        } else if (view == this.viewContainer.textViewPrivacyPolicy) {
            this.showPrivacyPolicy();
        } else if (view == this.viewContainer.textViewForgotPassword) {
            this.showResetPassword();
        }

    }

    /**
     Initializes the activity.
     */
    private void initializeActivity() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Hide the action bar.
        getSupportActionBar().hide();
    }

    /**
     * Initializes the layout content.
     */
    private void initializeLayoutContent() {

        // Create the view container.
        this.viewContainer = WelcomeActivityViewContainer.with(this);

        // Add the click events.
        this.viewContainer.buttonCreateAccount.setOnClickListener(this);
        this.viewContainer.buttonLogin.setOnClickListener(this);
        this.viewContainer.textViewSignUp.setOnClickListener(this);
        this.viewContainer.textViewLogin.setOnClickListener(this);
        this.viewContainer.textViewForgotPassword.setOnClickListener(this);
        this.viewContainer.textViewPrivacyPolicy.setOnClickListener(this);
        this.viewContainer.textViewTerms.setOnClickListener(this);
    }

    /**
     * Starts the animation.
     */
    private void startAnimation() {

        // Get the screen size.
        final int height = DisplayManager.getScreenHeight(this);
        final int width = DisplayManager.getScreenWidth(this);

        // Calculate the heights.
        int logoHeight = (int)DisplayManager.convertDpToPixel(190, this);
        int bubbleAnimateHeight1 = (height / 2) + (logoHeight / 2);

        // Slide the bubbles up.
        positionOffScreenBottom(this.viewContainer.layoutBubbles);
        positionOffScreenBottom(this.viewContainer.layoutLogo);


        // Create the bubble and logo animation set.
        AnimatorSet bubbleLogoAnimationSet = new AnimatorSet();
        bubbleLogoAnimationSet.playTogether(
                ObjectAnimator.ofFloat(this.viewContainer.layoutBubbles, "translationY", -bubbleAnimateHeight1),
                ObjectAnimator.ofFloat(this.viewContainer.layoutLogo, "translationY", -bubbleAnimateHeight1)
        );
        bubbleLogoAnimationSet.setInterpolator(new LinearInterpolator());
        bubbleLogoAnimationSet.setDuration((long)(5 * 1000)).start();

        // Create the bubble finish animation.
        ObjectAnimator bubbleFinishAnimator = ObjectAnimator.ofFloat(this.viewContainer.layoutBubbles, "translationY", -height);
        bubbleFinishAnimator.setInterpolator(new LinearInterpolator());
        bubbleFinishAnimator.setDuration((long)(5/4 * 1000));
        bubbleFinishAnimator.setStartDelay((long)(5 * 1000));
        bubbleFinishAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {

                animateLogoViews();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        bubbleFinishAnimator.start();
    }

    /**
     * Animates the logo views.
     */
    private void animateLogoViews() {

        // Position anonymous off the screen.
        positionOffScreenLeft(this.viewContainer.imageViewLogoAnon);
        this.viewContainer.imageViewLogoAnon.setVisibility(View.VISIBLE);

        // Animate onto the screen.
        ObjectAnimator anonymousAnimator = ObjectAnimator.ofFloat(this.viewContainer.imageViewLogoAnon, "translationX", 0);
        anonymousAnimator.setDuration(1500);
        anonymousAnimator.start();

        // Set the alphas for the other images.
        this.viewContainer.imageViewLogoNami.setVisibility(View.VISIBLE);
        this.viewContainer.imageViewLogoSupport.setVisibility(View.VISIBLE);
        ViewHelper.setAlpha(this.viewContainer.imageViewLogoNami, 0.0f);
        ViewHelper.setAlpha(this.viewContainer.imageViewLogoSupport, 0.0f);

        AnimatorSet alphaSet = new AnimatorSet();
        alphaSet.playTogether(
            ObjectAnimator.ofFloat(this.viewContainer.imageViewLogoNami, "alpha", 0.0f, 1.0f),
            ObjectAnimator.ofFloat(this.viewContainer.imageViewLogoSupport, "alpha", 0.0f, 1.0f)
        );
        alphaSet.setDuration(2000);
        alphaSet.setStartDelay(1500);
        alphaSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {

                finishAnimation();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        alphaSet.start();

    }

    /**
     * Finishes the animation.
     */
    private void finishAnimation() {

        // Position view off the screen.
        this.viewContainer.layoutBubbles.setVisibility(View.GONE);
        this.viewContainer.layoutWelcomeMain.setVisibility(View.VISIBLE);
        positionOffScreenBottom(this.viewContainer.layoutWelcomeMain);

        // Get the position of the logo.
        int position[] = new int[2];
        this.viewContainer.layoutLogo.getLocationOnScreen(position);

        // Get the padding.
        int padding = (int)DisplayManager.convertDpToPixel(20, this);

        // Create the animation set.
        AnimatorSet finalSet = new AnimatorSet();
        finalSet.playTogether(
                ObjectAnimator.ofFloat(this.viewContainer.layoutLogo, "translationY", position[1] - padding * 2, 0),
                ObjectAnimator.ofFloat(this.viewContainer.layoutWelcomeMain, "translationY",0)
        );
        finalSet.setDuration(1500);
        finalSet.start();

        // Set shown the welcome animation.
        ApplicationManager.getInstance().setShownWelcomeAnimation(true);
    }



    /**
     * Positions the view off screen at bottom.
     * @param view
     */
    private void positionOffScreenBottom(View view) {

        // Get the screen size.
        int height = DisplayManager.getScreenHeight(this);

        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "translationY", height);
        animator.setDuration(0);
        animator.start();
    }

    /**
     * Positions the view off screen at bottom.
     * @param view
     */
    private void positionOffScreenLeft(View view) {

        // Get the screen size.
        int width = DisplayManager.getScreenHeight(this);

        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "translationX", -width);
        animator.setDuration(0);
        animator.start();
    }

    /**
     * Shows the login layout.
     */
    private void showLoginLayout() {

        // Set the visibilities.
        this.viewContainer.layoutCreateAccount.setVisibility(View.GONE);
        this.viewContainer.layoutLogin.setVisibility(View.VISIBLE);
    }

    /**
     * Shows the terms of use.
     */
    private void showTermsOfUse() {

        // Create and start the intent.
        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * Shows the privacy policy.
     */
    private void showPrivacyPolicy() {

        // Create and start the intent.
        Intent intent = new Intent(this, PrivacyPolicyActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * Shows the reset password dialog.
     */
    private void showResetPassword() {

        // Create the dialog
        final Dialog resetPasswordDialog = new Dialog(this);
        resetPasswordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        resetPasswordDialog.setContentView(R.layout.dialog_reset_password);

        // Get the views.
        final EditText resetPasswordDialogEmailAddress = (EditText)resetPasswordDialog.findViewById(R.id.dialog_reset_password_edit_text_email);
        Button resetPasswordDialogCancelButton = (Button)resetPasswordDialog.findViewById(R.id.dialog_reset_password_button_cancel);
        Button resetPasswordDialogSubmitButton = (Button)resetPasswordDialog.findViewById(R.id.dialog_reset_password_button_submit);

        // Add the listeners.
        resetPasswordDialogCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                resetPasswordDialog.dismiss();
            }
        });
        resetPasswordDialogSubmitButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                // Check for an email address.
                String emailAddress = resetPasswordDialogEmailAddress.getText().toString();
                if (TextUtils.isEmpty(emailAddress)) {

                    // Display the message.
                    Toast.makeText(WelcomeActivity.this, getString(R.string.dialog_reset_password_validate_email), Toast.LENGTH_LONG).show();
                    return;
                } else {

                    // Attempt to reset the password.
                    ParseUser.requestPasswordResetInBackground(emailAddress, new RequestPasswordResetCallback() {
                        @Override
                        public void done(ParseException e) {

                            // Check for an exception.
                            if (e != null) {

                                // Display the message.
                                Toast.makeText(WelcomeActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                                return;
                            }

                            // Dismiss.
                            resetPasswordDialog.dismiss();

                            // Display the message.
                            Toast.makeText(WelcomeActivity.this, getString(R.string.dialog_reset_password_complete), Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
        });

        // Show the dialog.
        resetPasswordDialog.show();

    }

    /**
     * Shows the create account layout.
     */
    private void showCreateAccountLayout() {

        // Set the visibilities.
        this.viewContainer.layoutLogin.setVisibility(View.GONE);
        this.viewContainer.layoutCreateAccount.setVisibility(View.VISIBLE);
    }

    /**
     * Shows the create account.
     */
    private void showCreateAccount() {

        // Create and start the intent.
        Intent intent = new Intent(this, CreateAccountActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.pull_up_from_bottom, R.anim.no_animation);
    }

    /**
     * Starts the main application activity.
     */
    private void startMainApplicationActivity() {

        // Determine if the guided tour has been show.
        if (ApplicationManager.getInstance().hasShownGuidedTour(this)) {

            // Create and start the activity.
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();

        } else {

            // Create and start the activity.
            Intent intent = new Intent(this, GuidedTourActivity.class);
            startActivity(intent);
            finish();

        }


    }

    /**
     * Attempts to login the user.
     */
    private void attemptUserLogin() {

        // Hide the keyboard.
        KeyboardManager.HideActivityKeyboard(this);

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this) == false) {
            return;
        }

        // Validate the user login.
        ModelValidator<ParseUser> validator = this.validateUserLogin();
        if (!validator.isValid()) {

            // Set the error message.
            this.viewContainer.textViewError.setText(validator.getMessage());

            // Show the error container.
            this.viewContainer.layoutError.setVisibility(View.VISIBLE);
            viewContainer.layoutWelcomeMain.setPadding(0, 0, 0, 0);

        } else {

            // Create the progress dialog.
            final ProgressDialog progressDialog = new ProgressDialog(WelcomeActivity.this);
            progressDialog.setTitle(getString(R.string.welcome_login_progress_title));
            progressDialog.setMessage(getString(R.string.welcome_login_progress_message));
            progressDialog.setCancelable(false);
            progressDialog.show();

            // Get the user name and password.
            String username = this.viewContainer.editTextEmail.getText().toString();
            String password = this.viewContainer.editTextPassword.getText().toString();

            // Attempt to login.
            final WelcomeActivity that = this;
            ParseUser.logInInBackground(username, password, new LogInCallback() {

                @Override
                public void done(ParseUser parseUser, ParseException e) {

                    // Dismiss the dialog.
                    progressDialog.dismiss();

                    // Check for an exception.
                    if (e != null) {

                        if (TextUtils.indexOf(e.getMessage(), "invalid login credentials") >= 0) {

                            // Set the error message.
                            viewContainer.textViewError.setText(e.getLocalizedMessage());

                            // Show the error container.
                            viewContainer.layoutError.setVisibility(View.VISIBLE);
                            viewContainer.layoutWelcomeMain.setPadding(0, 0, 0, 0);
                        } else {

                            ApplicationManager.getInstance().showConnectionError(that);
                        }

                    } else {

                        // Check for disabled.
                        if (parseUser.getBoolean(UserModelConstants.FieldDisabled) == true) {

                            // Set the error message.
                            viewContainer.textViewError.setText("Your account has been disabled.");

                            // Show the error container.
                            viewContainer.layoutError.setVisibility(View.VISIBLE);
                            viewContainer.layoutWelcomeMain.setPadding(0, 0, 0, 0);

                            // Clear the edit text fields.
                            viewContainer.editTextEmail.setText("");
                            viewContainer.editTextPassword.setText("");

                            // Log the user out.
                            ParseUser.logOut();

                            return;
                        }

                        // Save the current installation.
                        ParseInstallation.getCurrentInstallation().saveInBackground();

                        // Start the main application activity.
                        startMainApplicationActivity();
                    }
                }
            });
        }
    }

    /**
     * Attempts to validate the user login.
     * @return ModelValidator.
     */
    private ModelValidator<ParseUser> validateUserLogin() {

        // Create the user.
        ParseUser model = new ParseUser();

        // Get the values.
        String emailAddress = this.viewContainer.editTextEmail.getText().toString();
        String password = this.viewContainer.editTextPassword.getText().toString();

        // Validate the email.
        if (TextUtils.isEmpty(emailAddress)) {
            return new ModelValidator<ParseUser>(model, false, getString(R.string.welcome_login_validation_missing_email));
        }

        // Validate the password.
        if (TextUtils.isEmpty(password)) {
            return new ModelValidator<ParseUser>(model, false, getString(R.string.welcome_login_validation_missing_password));
        }

        // Return success.
        return new ModelValidator<ParseUser>(model, true, null);
    }

    /**
     * Holds the view for the Welcome Activity
     */
    private static class WelcomeActivityViewContainer {

        // Private Members
        private Button buttonCreateAccount = null;
        private Button buttonLogin = null;
        private EditText editTextEmail = null;
        private EditText editTextPassword = null;
        private RelativeLayout layoutCreateAccount = null;
        private LinearLayout layoutLogin = null;
        private LinearLayout layoutWelcomeMain = null;
        private RelativeLayout layoutLogo = null;
        private LinearLayout layoutError = null;
        private TextView textViewLogin = null;
        private TextView textViewForgotPassword = null;
        private TextView textViewSignUp = null;
        private TextView textViewPrivacyPolicy = null;
        private TextView textViewTerms = null;
        private TextView textViewError = null;
        private ImageView imageViewLogo = null;
        private ImageView imageViewLogoAnon = null;
        private ImageView imageViewLogoSupport = null;
        private ImageView imageViewLogoNami = null;
        private RelativeLayout layoutBubbles = null;


        /**
         * Private Constructor
         */
        private WelcomeActivityViewContainer() {}

        /**
         * Public Static Constructor
         * @param activity Activity to create the view from
         * @return WelcomeActivityViewContainer
         */
        public static WelcomeActivityViewContainer with(Activity activity) {

            // Create the instance.
            WelcomeActivityViewContainer instance = new WelcomeActivityViewContainer();

            // Get the items.
            instance.buttonCreateAccount = (Button)activity.findViewById(R.id.welcome_create_account_button_create_account);
            instance.buttonLogin = (Button)activity.findViewById(R.id.welcome_login_button_login);
            instance.editTextEmail = (EditText)activity.findViewById(R.id.welcome_login_edit_text_email);
            instance.editTextPassword = (EditText)activity.findViewById(R.id.welcome_login_edit_text_password);
            instance.layoutCreateAccount = (RelativeLayout)activity.findViewById(R.id.welcome_create_account);
            instance.layoutLogin = (LinearLayout)activity.findViewById(R.id.welcome_login);
            instance.layoutLogo = (RelativeLayout)activity.findViewById(R.id.welcome_logo_container);
            instance.layoutError = (LinearLayout)activity.findViewById(R.id.welcome_layout_error);
            instance.textViewLogin = (TextView)activity.findViewById(R.id.welcome_create_account_login_text_view_login_here);
            instance.textViewForgotPassword = (TextView)activity.findViewById(R.id.welcome_login_text_view_forgot_password);
            instance.textViewSignUp = (TextView)activity.findViewById(R.id.welcome_login_sign_up_text_view_sign_up);
            instance.textViewPrivacyPolicy = (TextView)activity.findViewById(R.id.welcome_footer_text_view_privacy_policy);
            instance.textViewTerms = (TextView)activity.findViewById(R.id.welcome_footer_text_view_terms);
            instance.textViewError = (TextView)activity.findViewById(R.id.welcome_text_view_error);
            instance.imageViewLogo = (ImageView)activity.findViewById(R.id.welcome_logo);
            instance.imageViewLogoAnon = (ImageView)activity.findViewById(R.id.welcome_logo_anon);
            instance.imageViewLogoSupport = (ImageView)activity.findViewById(R.id.welcome_logo_support);
            instance.imageViewLogoNami = (ImageView)activity.findViewById(R.id.welcome_logo_nami);
            instance.layoutWelcomeMain = (LinearLayout)activity.findViewById(R.id.welcome_main);
            instance.layoutBubbles = (RelativeLayout)activity.findViewById(R.id.welcome_bubbles);


            return instance;
        }
    }
}
