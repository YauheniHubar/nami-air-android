package com.nami.airapp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.pm.ActivityInfo;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.nami.airapp.R;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.utils.ActionBarManager;
import com.parse.ParseUser;

import java.io.InputStream;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TermsActivity extends ActionBarActivity implements OnClickListener {

    // Private Members.
    private String terms = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the requested orientation.
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Load the terms.
        this.loadTerms();

        // Initialize the activity and action bar.
        this.initializeActivity();
        this.initializeActionBar();

        // Set the content view.
        setContentView(R.layout.activity_terms);

        // Initialize the layout content.
        this.initializeLayoutContent();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(new CalligraphyContextWrapper(newBase));
    }


    /**
     Initializes the activity.
     */
    private void initializeActivity() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(this, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.terms_title));
    }

    /**
     * Initializes the layout content.
     */
    private void initializeLayoutContent() {

        // Set the terms.
        //TextView textViewTerms = (TextView)this.findViewById(R.id.create_account_terms_text);
        //textViewTerms.setText(Html.fromHtml(this.terms));

        WebView webViewTerms = (WebView)this.findViewById(R.id.create_account_terms_text);
        webViewTerms.setBackgroundColor(0x00000000);
        webViewTerms.loadDataWithBaseURL(null, this.terms, "text/html", "utf-8", null);
        webViewTerms.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return true;
            }
        });
        webViewTerms.setLongClickable(false);
        webViewTerms.setClickable(false);
        webViewTerms.setOnTouchListener(new View.OnTouchListener(){
            public boolean onTouch(View v, MotionEvent event) {
                WebView.HitTestResult hr = ((WebView)v).getHitTestResult();

                if (event.getAction() == MotionEvent.ACTION_UP &&
                        (hr.getType() == WebView.HitTestResult.ANCHOR_TYPE || hr.getType() == WebView.HitTestResult.SRC_ANCHOR_TYPE)) {

                    // Check the extra.
                    if (hr.getExtra().indexOf("#_pp") >= 0) {

                        // Show the privacy policy.
                        showPrivacyPolicy();;
                    }

                }

                return false;
            }
        });
    }

    /**
     * Shows the privacy policy.
     */
    private void showPrivacyPolicy() {

        // Create the intent.
        Intent intent = new Intent(this, PrivacyPolicyActivity.class);
        startActivity(intent);
    }

    /**
     * Loads the terms from the assets.
     */
    private void loadTerms() {

        // Get the asset manager.
        AssetManager assetManager = this.getAssets();

        try {

            // Create the input stream.
            InputStream inputStream = assetManager.open("copy/terms.txt");

            // Read the input stream.
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);

            // Close the stream.
            inputStream.close();

            // Create the text.
            this.terms = new String(buffer);


        }
        catch (Exception e) {

            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
