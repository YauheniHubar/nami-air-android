package com.nami.airapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.nami.airapp.R;
import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.adapters.FaqAdapter;
import com.nami.airapp.data.models.FaqSectionModel;
import com.nami.airapp.data.models.FaqModel;
import com.nami.airapp.handlers.PostXmlHandler;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.AppFragmentManager;
import com.nami.airapp.utils.KeyboardManager;
import com.nami.airapp.views.FaqListItemView;
import com.nami.airapp.handlers.FaqXmlHandler;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Post Detail Fragment.
 */
public class FaqFragment extends Fragment {

    // Private Members
    private Context context = null;
    private FaqViewContainer viewContainer = null;
    private FaqAdapter adapter = null;
    private FaqSectionModel frequentlyAskedQuestions = null;

    // Private Static Members.
    private static final String KEY_FRAGMENT_FAQ_SECTION = "KEY_FRAGMENT_FAQ_SECTION";

    /**
     * Default Constructor.
     */
    public FaqFragment() {}

    /**
     * Creates a new instance of a PostDetailFragment.
     * @return New instance of a PostDetailFragment.
     */
    public static FaqFragment newInstance(FaqSectionModel sectionModel) {

        // Create the new instance.
        FaqFragment instance = new FaqFragment();

        // Add the post model to the bundle.
        Bundle args = new Bundle();
        args.putParcelable(KEY_FRAGMENT_FAQ_SECTION, sectionModel);
        instance.setArguments(args);

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            this.frequentlyAskedQuestions = args.getParcelable(KEY_FRAGMENT_FAQ_SECTION);
        }
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set the action bar.
        MainActivity mainActivity = (MainActivity)getActivity();
        mainActivity.setActionBarNoTabs();

        // Set options menu.
        setHasOptionsMenu(true);

        // Create the adapter.
        this.adapter = new FaqAdapter(getActivity(), this.frequentlyAskedQuestions);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_faq, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    AppFragmentManager.RemoveFromStack(that, getActivity());
                    return true;
                }

                return false;
            }
        });
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, getActivity());
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {


    }

    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(getActivity(), actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, this.frequentlyAskedQuestions.Question    );
    }



    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = FaqViewContainer.with(view);

        // Set the expandable view properties.
        this.viewContainer.listViewFaq.setGroupIndicator(null);
        this.viewContainer.listViewFaq.setChildIndicator(null);
        this.viewContainer.listViewFaq.setDivider(getResources().getDrawable(android.R.color.transparent));
        this.viewContainer.listViewFaq.setDividerHeight(1);


        // Set the adapter.
        this.viewContainer.listViewFaq.setAdapter(this.adapter);

        // Add the listeners.
        this.viewContainer.listViewFaq.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            // Private Members.
            FaqListItemView previousGroupView = null;

            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {

                // Get the view.
                FaqListItemView groupView = (FaqListItemView)view;

                // Determine if it is expanded.
                if (viewContainer.listViewFaq.isGroupExpanded(i) == false) {

                    // Set to expanded.
                    groupView.showExpanded();

                    // Collapse the previous.
                    if (previousGroupView != null && groupView != previousGroupView) {
                        previousGroupView.showCollapsed();
                    }

                } else {

                    // Set to collapsed.
                    groupView.showCollapsed();
                }

                // Set the previous group view.
                previousGroupView = groupView;

                return false;
            }
        });
        this.viewContainer.listViewFaq.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            // Private Members.
            int previousGroupViewId = -1;

            @Override
            public void onGroupExpand(int i) {

                if (i != previousGroupViewId)
                    viewContainer.listViewFaq.collapseGroup(previousGroupViewId);
                previousGroupViewId = i;
            }
        });
    }


    /**
     * Holds the view for the FAQ
     */
    private static class FaqViewContainer {

        // Private Members.
        private ExpandableListView listViewFaq;

        // Private Constructor.
        private FaqViewContainer() {}

        // Static Constructor.
        public static FaqViewContainer with(View view) {

            // Create the instance
            FaqViewContainer instance = new FaqViewContainer();

            // Get the items.
            instance.listViewFaq = (ExpandableListView)view.findViewById(R.id.faq_list_view);

            return instance;
        }
    }
}
