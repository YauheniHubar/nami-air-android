package com.nami.airapp.views;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nami.airapp.R;
import com.nami.airapp.data.models.FaqModel;
import com.nami.airapp.data.models.FaqSectionModel;

public class FaqSectionItemView extends LinearLayout {

    // Private Members.
    private FaqSectionItemViewContainer viewContainer = null;
    private Context context;

    /**
     * Default Constructor.
     * @param context Context which the view runs in.
     */
    public FaqSectionItemView(Context context) {
        super(context);

        // Create the view.
        LayoutInflater.from(context).inflate(R.layout.view_faq_section_item, this);

        // Set the context.
        this.context = context;

        // Create the view container.
        this.viewContainer = FaqSectionItemViewContainer.with(this);
    }

    /**
     * Updates the model.
     * @param model The model to update.
     * @param position The position of the model in the dataset.
     */
    public void updateFromModel(FaqSectionModel model, int position) {

        // Set the detail.
        this.viewContainer.textViewSectionName.setText(model.Question);
    }


    /**
     * Holds the view for the FAQ List Item Child View
     */
    private static class FaqSectionItemViewContainer {

        // Private Members.
        private TextView textViewSectionName;

        // Private Constructor.
        private FaqSectionItemViewContainer() {}

        // Static Constructor.
        public static FaqSectionItemViewContainer with(View view) {

            // Create the instance
            FaqSectionItemViewContainer instance = new FaqSectionItemViewContainer();

            // Get the items.
            instance.textViewSectionName = (TextView)view.findViewById(R.id.faq_section_item_text);

            return instance;
        }
    }

}
