package com.nami.airapp.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.nami.airapp.R;
import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.adapters.BaseParseAdapter.OnQueryRunListener;
import com.nami.airapp.data.adapters.PostFeedAdapter;
import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.enums.FeedType;
import com.nami.airapp.data.enums.UserType;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.KeyboardManager;
import com.nami.airapp.interfaces.PostRemovedListener;
import com.parse.ParseUser;

public class MyPostFeedFragment extends ListFragment implements OnQueryTextListener, OnRefreshListener, OnQueryRunListener, PostRemovedListener {

    // Private Members.
    private Context context = null;
    private FeedType feedType = null;
    private UserType userType = null;
    private PostFeedAdapter adapter = null;
    private SwipeRefreshLayout swipeRefreshLayout = null;
    private int previousLastViewItem = -1;
    private PostFeedViewContainer viewContainer = null;

    // Private Static Members
    private static final String KEY_FRAGMENT_MY_POST_FEED_LIST = "KEY_FRAGMENT_MY_POST_FEED_LIST";
    private static final int MENU_SEARCH = Menu.FIRST;
    private static final int MENU_SETTINGS = MENU_SEARCH + 1;

    // Public Static Members.
    public static final int TAB = 1;
    public static final String TAG = "MyPostFeedFragmentTag";

    /**
     * Default Constructor.
     */
    public MyPostFeedFragment() {}

    /**
     * Creates a new instance of a PostFeedFragment.
     * @return New instance of a PostFeedFragment.
     */
    public static MyPostFeedFragment newInstance() {

        // Create and return the new instance.
        MyPostFeedFragment instance = new MyPostFeedFragment();

        // Create the bundle.
        instance.setArguments(new Bundle());

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set options menu.
        setHasOptionsMenu(true);

        // Get the current user type and set the feed type.
        this.userType = UserType.fromValue(ParseUser.getCurrentUser().getInt(UserModelConstants.FieldUserType));
        this.feedType = FeedType.MY_POSTS;

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_feed, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Add the listeners.
        ApplicationManager.getInstance().addPostRemovedListener(this);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // Create the search menu item.
        MenuItem search = menu.add(Menu.NONE, MENU_SEARCH, Menu.NONE, getString(R.string.options_menu_search_title));
        MenuItemCompat.setShowAsAction(search, MenuItemCompat.SHOW_AS_ACTION_ALWAYS  | MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        search.setIcon(R.drawable.ic_menu_search);

        // Create the settings menu item.
        MenuItem settings = menu.add(Menu.NONE, MENU_SETTINGS, Menu.NONE, getString(R.string.options_menu_settings));
        MenuItemCompat.setShowAsAction(settings, MenuItemCompat.SHOW_AS_ACTION_ALWAYS | MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        settings.setIcon(R.drawable.ic_menu_settings);

        // Create the search view.
        SearchView searchView = new SearchView(context);
        searchView.setOnQueryTextListener(this);
        searchView.setSubmitButtonEnabled(true);
        MenuItemCompat.setActionView(search, searchView);
    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case MENU_SETTINGS:
                this.showSettings();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when the fragment is paused.
     */
    @Override
    public void onPause() {
        super.onPause();

        // Put the adapter list into the bundle.
        if (this.adapter.getDataSet() != null && this.adapter.getDataSet().size() > 0)
            getArguments().putParcelableArrayList(KEY_FRAGMENT_MY_POST_FEED_LIST, (ArrayList<PostModel>)this.adapter.getDataSet());
    }

    /**
     * Called when the activity is created.
     * @param savedInstanceState The saved instance state.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



    }

    @Override
    public void onResume() {
        super.onResume();

        // Check the bundle.
        if (getArguments() != null && getArguments().containsKey(KEY_FRAGMENT_MY_POST_FEED_LIST)) {
            List<PostModel> dataSet = getArguments().getParcelableArrayList(KEY_FRAGMENT_MY_POST_FEED_LIST);

            // Create the adapter.
            this.adapter = new PostFeedAdapter(this, context, this.feedType, UserType.fromValue(ParseUser.getCurrentUser().getInt(UserModelConstants.FieldUserType)), false);

            // Set the data set.
            this.adapter.setDataSet(dataSet);
        } else {

            // Create the adapter.
            this.adapter = new PostFeedAdapter(this, context, this.feedType, UserType.fromValue(ParseUser.getCurrentUser().getInt(UserModelConstants.FieldUserType)), true);
        }

        // Set the adapter.
        this.adapter.setOnQueryRunListener(this);
        setListAdapter(this.adapter);
    }

    /**
     * Called when a list view item is clicked.
     * @param listView List view containing the clicked item.
     * @param view The view.
     * @param position The position of the item.
     * @param id The identifier of the item.
     */
    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {

        // Get the post model.
        PostModel selectedPostModel = (PostModel)getListView().getItemAtPosition(position);
        this.showPostModelDetail(selectedPostModel);
    }

    /**
     * Called when the search text changes.
     * @param text The new text.
     * @return boolean
     */
    public boolean onQueryTextChange(String text) {
        return true;
    }

    /**
     * Called when the search text is submitted.
     * @param query The query that was submitted.
     * @return boolean
     */
    public boolean onQueryTextSubmit(String query) {

        // Hide the keyboard.
        KeyboardManager.HideActivityKeyboard(context);

        // Create the fragment.
        SearchFeedFragment searchFeedFragment = SearchFeedFragment.newInstance(query);

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, searchFeedFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();


        return true;
    }

    /**
     * Implements the PostRemovedListener.
     * @param postModel
     */
    public void PostRemoved(PostModel postModel) {

        // Check the bundle.
        if (getArguments() != null && getArguments().containsKey(KEY_FRAGMENT_MY_POST_FEED_LIST)) {
            getArguments().remove(KEY_FRAGMENT_MY_POST_FEED_LIST);
        }
    }


    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.main_tab_text_my_posts));
    }

    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = PostFeedViewContainer.with(view);

        // Get the swipe layout.
        this.swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.post_feed_swipe_refresh_layout);

        // Set the swipe layout properties.
        this.swipeRefreshLayout.setOnRefreshListener(this);

        // Find the list view.
        ListView listView = (ListView)view.findViewById(android.R.id.list);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {


            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                // Determine if loading.
                if (adapter.isLoading == true)
                    return;

                if (viewContainer.progress.getVisibility() == View.VISIBLE)
                    return;

                // Determine if it is at the top.
                int topVerticalPosition = (viewContainer.listView == null || viewContainer.listView.getChildCount() == 0) ?
                        0 : viewContainer.listView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topVerticalPosition >= 0);

                // Determine if the last item is visible.
                final int lastItem = firstVisibleItem + visibleItemCount;
                if (totalItemCount > 0 &&lastItem == totalItemCount && previousLastViewItem != lastItem) {
                    previousLastViewItem = lastItem;

                    // Load more data.
                    adapter.loadMoreData();
                }
            }
        });
    }

    @Override
    public void onRefresh() {

        // Reload the data.
        this.adapter.reloadData(this.swipeRefreshLayout);
    }

    /**
     * OnQueryRunListener Implementation
     */
    public void onLoaded() {

        if (!isAdded()) return;

        // Show / Hide the appropriate controls.
        if (this.adapter.getDataSet().size() > 0) {
            this.viewContainer.progress.setVisibility(View.GONE);
            this.viewContainer.listView.setVisibility(View.VISIBLE);
            this.viewContainer.textViewNoResults.setVisibility(View.GONE);
        } else {
            this.viewContainer.progress.setVisibility(View.GONE);
            this.viewContainer.listView.setVisibility(View.GONE);
            this.viewContainer.textViewNoResults.setVisibility(View.VISIBLE);
            this.viewContainer.textViewNoResults.setText(getString(R.string.post_feed_my_posts_no_results));
        }
    }

    /**
     * OnQueryRunListener Implementation
     */
    public void onLoading() {

        // Show / Hide the appropriate controls.
        this.viewContainer.progress.setVisibility(View.VISIBLE);
    }

    /**
     * Shows the settings.
     */
    private void showSettings() {

        // Create the fragment.
        SettingsFragment settingsFragment = SettingsFragment.newInstance();

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, settingsFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Shows all the posts.
     */
    private void showAllPosts() {

        if (this.feedType != FeedType.ALL_POSTS) {
            this.feedType = FeedType.ALL_POSTS;
            this.adapter.setFeedType(this.feedType);
        }
    }

    /**
     * Shows only my posts.
     */
    private void showMyPosts() {

        if (this.feedType != FeedType.MY_POSTS) {
            this.feedType = FeedType.MY_POSTS;
            this.adapter.setFeedType(this.feedType);
        }
    }

    /**
     * Shows the post model detail.
     * @param postModel The post model.
     */
    private void showPostModelDetail(PostModel postModel) {

        // Determine the post owner.
        ParseUser postOwnerUser = postModel.getUser();
        if (TextUtils.equals(postOwnerUser.getObjectId(), ParseUser.getCurrentUser().getObjectId())) {

            // Create the fragment.
            MyPostDetailFragment postDetailFragment = MyPostDetailFragment.newInstance(postModel);

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, postDetailFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();

        } else {

            // Create the fragment.
            PostDetailFragment postDetailFragment = PostDetailFragment.newInstance(postModel);

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, postDetailFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();
        }
    }

    /**
     * Holds the view for the Post Feed Item.
     */
    private static class PostFeedViewContainer {

        // Private Members.
        private LinearLayout progress;
        private ListView listView;
        private TextView textViewNoResults;


        // Private Constructor.
        private PostFeedViewContainer() {}

        // Static Constructor.
        public static PostFeedViewContainer with(View view) {

            // Create the instance
            PostFeedViewContainer instance = new PostFeedViewContainer();

            // Get the items.
            instance.listView = (ListView)view.findViewById(android.R.id.list);
            instance.progress = (LinearLayout)view.findViewById(R.id.progress);
            instance.textViewNoResults = (TextView)view.findViewById(R.id.no_results);

            return instance;
        }
    }
}
