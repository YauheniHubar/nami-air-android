package com.nami.airapp.data.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.SwipeRefreshLayout;
import java.util.Date;

import com.nami.airapp.data.constants.PostModelConstants;
import com.nami.airapp.data.enums.ApprovedStatus;
import com.nami.airapp.data.models.PostHugModel;
import com.nami.airapp.data.models.PostLikeModel;
import com.nami.airapp.data.models.PostMeTooModel;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import com.nami.airapp.data.enums.FeedType;
import com.nami.airapp.data.enums.UserType;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.interfaces.PostHideListener;
import com.nami.airapp.interfaces.AuthorBlockListener;
import com.nami.airapp.interfaces.GroupChangeListener;
import com.nami.airapp.views.PostFeedItemView;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.parse.ParseUser;

public class PostFeedAdapter extends BaseParseAdapter implements PostHideListener, AuthorBlockListener, GroupChangeListener {

    // Private Members
    private Fragment fragment;
    private FeedType feedType = null;
    private UserType userType = null;
    private List<PostModel> dataSet = null;
    private SwipeRefreshLayout swipeRefreshLayout = null;
    private Date latestDate = null;
    public Date earliestDate = null;

    /**
     * Default Constructor.
     * @param context Context which the adapter runs in.
     * @param feedType The feed type (All Posts, My Posts).
     * @param userType The user type (User, Caregiver).
     * @param reload Whether to reload the data.
     */
    public PostFeedAdapter(Fragment fragment, Context context, FeedType feedType, UserType userType, boolean reload) {
        super(context);

        // Set the members.
        this.fragment = fragment;
        this.feedType = feedType;
        this.userType = userType;

        // Add the listeners.
        ApplicationManager.getInstance().addPostHideListener(this);
        ApplicationManager.getInstance().addAuthorBlockListener(this);
        ApplicationManager.getInstance().addGroupChangeListener(this);

        // Load / Reload the data.
        if (reload)
            this.reloadData();
    }

    /**
     * Gets the number of items in the list.
     * @return Number of items in the list.
     */
    @Override
    public int getCount() {

        return this.dataSet.size();
    }

    /**
     * Gets the item at the position in the list.
     * @param position Position in the list.
     * @return Item at the position in the list.
     */
    @Override
    public PostModel getItem(int position) {

        return this.dataSet.get(position);
    }

    /**
     * Gets the item identifier at the position in the list.
     * @param position Position in the list.
     * @return Item identifier at the position in the list.
     */
    @Override
    public long getItemId(int position) {

        return position;
    }

    /**
     * Gets the view for the item in the position in the list.
     * @param position Position in the list.
     * @param convertView View of the item in the list.
     * @param parent Parent view of the item in the list.
     * @return Updated view of the item in the list.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Create the view if necessary.
        if (convertView == null) {
            convertView = new PostFeedItemView(context);
        }

        // Get the item and update the view.
        PostModel model = this.getItem(position);
        ((PostFeedItemView)convertView).updateFromModel(fragment, model, position);

        return convertView;
    }

    /**
     * Reloads the data.
     */
    public void reloadData() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            this.dataSet = new ArrayList<PostModel>();
            return;
        }

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<PostModel> queryFactory = PostModel.createPostFeedAdapterQuery(this.feedType, this.userType);

        // Notify the listener.
        setLoading(true);

        // Create and execute the query.
        ParseQuery<PostModel> query = queryFactory.create();
        new PostFeedParseQueryTask(this.context, this).execute(query);
    }

    /**
     * Reloads the data with newer posts.
     * @param swipeRefreshLayout Swipe Layout
     */
    public void reloadData(SwipeRefreshLayout swipeRefreshLayout) {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Set the swipe layout.
        this.swipeRefreshLayout = swipeRefreshLayout;

        // Notify the listener.
        setLoading(true);

        // Set a default latest date if there is not one.
        if (this.latestDate == null)
            this.latestDate = new Date(1);


        // Create the query factory.
        ParseQueryAdapter.QueryFactory<PostModel> queryFactory = PostModel.createPostFeedNewItemsAdapterQuery(this.feedType, this.userType, this.latestDate, this.earliestDate);

        // Create and execute the query.
        ParseQuery<PostModel> query = queryFactory.create();
        new PostFeedNewItemsParseQueryTask(this.context, this).execute(query);
    }

    /**
     * Loads more data.
     */
    public void loadMoreData() {

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Notify the listener.
        setLoading(true);

        // Get the earliest date in the data set.
        Date earliestDate = null;
        if (this.dataSet != null && this.dataSet.size() > 0) {
            PostModel earliestPostModel = this.dataSet.get(this.dataSet.size() - 1);
            earliestDate = earliestPostModel.getCreatedAt();
        }

        // Set a default latest date if there is not one.
        if (earliestDate == null)
            earliestDate = new Date(1);

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<PostModel> queryFactory = PostModel.createPostFeedEarlierAdapterQuery(this.feedType, this.userType, earliestDate);

        // Create and execute the query.
        ParseQuery<PostModel> query = queryFactory.create();
        new PostFeedEarlierItemsParseQueryTask(this.context, this).execute(query);
    }

    /**
     * Sets the feed type for the adapter.
     * @param feedType The feed type (All Posts, My Posts).
     */
    public void setFeedType(FeedType feedType) {

        this.feedType = feedType;
        this.reloadData();
    }

    /**
     * Sets the user type for the adpater.
     * @param userType The user type.
     * @param userType
     */
    public void setUserType(UserType userType) {

        this.userType = userType;
    }

    /**
     * Gets the data set.
     * @return List of data.
     */
    public List<PostModel> getDataSet() {

        return this.dataSet;
    }

    /**
     * Sets the data set.
     * @param dataSet List of data.
     */
    public void setDataSet(List<PostModel> dataSet) {

        // Set the data set.
        this.dataSet = dataSet;

        // Notify the listener.
        setLoading(false);

        // Notify that the data set has changed.
        this.notifyDataSetChanged();

        // Set the latest date.
        this.setDatasetDates();
    }

    /**
     * Prepends the new data set to the beginning of the current data set.
     * @param newDataSet The new data set to prepend.
     */
    public void setDataSetPrepend(List<PostModel> newDataSet) {

        // Set the data set.
        this.dataSet = newDataSet;

        // Notify the listener.
        setLoading(false);

        // Notify that the data set has changed.
        this.notifyDataSetChanged();

        // Set to not refreshing.
        if (this.swipeRefreshLayout != null) {
            this.swipeRefreshLayout.setRefreshing(false);
            this.swipeRefreshLayout = null;
        }

        // Set the latest date.
        this.setDatasetDates();
    }

    /**
     * Appends the new data set to the end of the current data set.
     * @param newDataSet The new data set to prepend.
     */
    public void setDataSetAppend(List<PostModel> newDataSet) {

        // Notify the listener.
        setLoading(false);

        // Return if there is no new data.
        if (newDataSet.size() == 0)
            return;

        // Set the data set.
        if (this.dataSet == null)
            this.dataSet = new ArrayList<PostModel>();
        this.dataSet.addAll(newDataSet);

        // Notify that the data set has changed.
        this.notifyDataSetChanged();

        // Set the latest date.
        this.setDatasetDates();
    }

    /**
     * Implements PostHideListener
     * @param objectId
     */
    public void postHidden(String objectId) {

        // Declare the changed.
        boolean changed = false;

        // Loop through all the posts.
        for (int index = 0; index < this.dataSet.size(); index++) {

            // Get the post.
            PostModel postModel = this.dataSet.get(index);

            // Check the post id.
            if (TextUtils.equals(postModel.getObjectId(), objectId)) {
                changed = true;
                this.dataSet.remove(index);
                break;
            }
        }

        // Notify that the data set has changed.
        if (changed)
            this.notifyDataSetChanged();
    }

    /**
     * Sets the earliest and latest dates
     */
    private void setDatasetDates() {

        // Get the latest date in the data set.
        this.latestDate = null;
        this.earliestDate = null;
        for (PostModel postModel : this.dataSet) {

            // Check for existing date values.
            if (postModel.getUpdatedAt() == null)
                break;

            // Check for null.
            if (this.latestDate == null)
                latestDate = postModel.getUpdatedAt();

            if (this.earliestDate == null)
                this.earliestDate = postModel.getUpdatedAt();

            // Check the time.
            if (postModel.getUpdatedAt().getTime() > latestDate.getTime())
                latestDate = postModel.getUpdatedAt();
            if (postModel.getUpdatedAt().getTime() < earliestDate.getTime())
                earliestDate = postModel.getUpdatedAt();

        }

        // Check for dates.
        if (this.earliestDate == null)
            this.earliestDate = new Date();
    }

    /**
     * Implements AuthorBlockListener
     * @param objectId
     */
    public void authorBlocked(String objectId) {

        // Declare the changed.
        boolean changed = false;

        // Loop through all the posts.
        for (int index = 0; index < this.dataSet.size(); index++) {

            // Get the post and user.
            PostModel postModel = this.dataSet.get(index);
            ParseUser user = postModel.getUser();

            // Check the user id.
            if (TextUtils.equals(user.getObjectId(), objectId)) {
                changed = true;
                this.dataSet.remove(index);
                index--;
                continue;
            }
        }

        // Notify that the data set has changed.
        if (changed)
            this.notifyDataSetChanged();
    }

    /**
     * Implements GroupChangeListener
     * @param userType
     */
    public void groupChanged(UserType userType) {

        // Set the user type.
        this.userType = userType;

        // Reload the data.
        //this.reloadData();
    }

    /**
     * Queries parse.
     */
    private class PostFeedParseQueryTask extends AsyncTask<ParseQuery<PostModel>, Void, List<PostModel>> {

        private Context context;
        private PostFeedAdapter adapter;
        private ParseException parseException;

        public PostFeedParseQueryTask(Context context, PostFeedAdapter adapter) {
            this.context = context;
            this.adapter = adapter;
        }

        @Override
        protected void onPreExecute() {

            // Create the posts.
            dataSet = new ArrayList<PostModel>();
        }

        @Override
        protected List<PostModel> doInBackground(ParseQuery<PostModel>... queries) {

            // Declare the return value.
            List<PostModel> data = null;

            // Get the query.
            ParseQuery<PostModel> query = queries[0];

            // Set the number of items to load.
            query.setLimit(loadLimit);

            // Attempt to execute the query.
            try {

                // Query.
                data = query.find();

                // Create the params.
                HashMap<String, Object> params = new HashMap<String, Object>();
                ArrayList<String> postPointers = new ArrayList<String>();
                for (PostModel post : data) { postPointers.add(post.getObjectId()); }
                params.put("posts", postPointers);

                // Call the cloud function.
                HashMap<String, Object> result = ParseCloud.callFunction("queryPostsSocialNotifications", params);

                // Add the social notifications.
                ApplicationManager.getInstance().addPostLikes((ArrayList<PostLikeModel>)result.get("postLikes"));
                ApplicationManager.getInstance().addPostHugs((ArrayList<PostHugModel>) result.get("postHugs"));
                ApplicationManager.getInstance().addPostMeToos((ArrayList<PostMeTooModel>) result.get("postMeToos"));
            }
            catch (ParseException e) {
                data = new ArrayList<PostModel>();
                this.parseException = e;
            }

            return data;
        }

        @Override
        protected void onPostExecute(List<PostModel> postsFound) {

            if (this.parseException != null) {

                if (this.adapter.swipeRefreshLayout != null) {
                    this.adapter.swipeRefreshLayout.setRefreshing(false);
                    this.adapter.swipeRefreshLayout = null;
                }

                ApplicationManager.getInstance().showConnectionError(this.context);

                return;
            }

            // Set the data set.
            setDataSet(postsFound);
        }
    }

    /**
     * Queries parse.
     */
    private class PostFeedNewItemsParseQueryTask extends AsyncTask<ParseQuery<PostModel>, Void, List<PostModel>> {

        private Context context;
        private PostFeedAdapter adapter;
        private ParseException parseException;

        public PostFeedNewItemsParseQueryTask(Context context, PostFeedAdapter adapter) {
            this.context = context;
            this.adapter = adapter;
        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected List<PostModel> doInBackground(ParseQuery<PostModel>... queries) {


            // Declare the return value.
            List<PostModel> data = null;

            // Get the query.
            ParseQuery<PostModel> query = queries[0];

            // Attempt to execute the query.
            try {
                data = query.find();

                // Create the params.
                HashMap<String, Object> params = new HashMap<String, Object>();
                ArrayList<String> postPointers = new ArrayList<String>();
                for (PostModel post : data) { postPointers.add(post.getObjectId()); }
                params.put("posts", postPointers);

                // Call the cloud function.
                HashMap<String, Object> result = ParseCloud.callFunction("queryPostsSocialNotifications", params);

                // Add the social notifications.
                ApplicationManager.getInstance().addPostLikes((ArrayList<PostLikeModel>)result.get("postLikes"));
                ApplicationManager.getInstance().addPostHugs((ArrayList<PostHugModel>) result.get("postHugs"));
                ApplicationManager.getInstance().addPostMeToos((ArrayList<PostMeTooModel>) result.get("postMeToos"));
            }
            catch (ParseException e) {
                data = new ArrayList<PostModel>();
                this.parseException = e;
            }

            return data;
        }

        @Override
        protected void onPostExecute(List<PostModel> postsFound) {

            if (this.parseException != null) {

                if (this.adapter.swipeRefreshLayout != null) {
                    this.adapter.swipeRefreshLayout.setRefreshing(false);
                    this.adapter.swipeRefreshLayout = null;
                }

                ApplicationManager.getInstance().showConnectionError(this.context);
                return;
            }

            // Set the data set.
            setDataSetPrepend(postsFound);
        }
    }

    /**
     * Queries parse.
     */
    private class PostFeedEarlierItemsParseQueryTask extends AsyncTask<ParseQuery<PostModel>, Void, List<PostModel>> {

        private Context context;
        private PostFeedAdapter adapter;
        private ParseException parseException;

        public PostFeedEarlierItemsParseQueryTask(Context context, PostFeedAdapter adapter) {
            this.context = context;
            this.adapter = adapter;
        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected List<PostModel> doInBackground(ParseQuery<PostModel>... queries) {

            // Declare the return value.
            List<PostModel> data = null;

            // Get the query.
            ParseQuery<PostModel> query = queries[0];

            // Set the number of items to load.
            query.setLimit(loadLimit);

            // Attempt to execute the query.
            try {
                data = query.find();

                // Create the params.
                HashMap<String, Object> params = new HashMap<String, Object>();
                ArrayList<String> postPointers = new ArrayList<String>();
                for (PostModel post : data) { postPointers.add(post.getObjectId()); }
                params.put("posts", postPointers);

                // Call the cloud function.
                HashMap<String, Object> result = ParseCloud.callFunction("queryPostsSocialNotifications", params);

                // Add the social notifications.
                ApplicationManager.getInstance().addPostLikes((ArrayList<PostLikeModel>)result.get("postLikes"));
                ApplicationManager.getInstance().addPostHugs((ArrayList<PostHugModel>) result.get("postHugs"));
                ApplicationManager.getInstance().addPostMeToos((ArrayList<PostMeTooModel>) result.get("postMeToos"));
            }
            catch (ParseException e) {
                data = new ArrayList<PostModel>();
                this.parseException = e;
            }

            return data;
        }

        @Override
        protected void onPostExecute(List<PostModel> postsFound) {

            if (this.parseException != null) {

                if (this.adapter.swipeRefreshLayout != null) {
                    this.adapter.swipeRefreshLayout.setRefreshing(false);
                    this.adapter.swipeRefreshLayout = null;
                }

                ApplicationManager.getInstance().showConnectionError(this.context);
                return;
            }

            // Set the data set.
            setDataSetAppend(postsFound);
        }
    }
}
