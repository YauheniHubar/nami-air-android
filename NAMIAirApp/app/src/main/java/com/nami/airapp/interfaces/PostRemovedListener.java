package com.nami.airapp.interfaces;

import com.nami.airapp.data.models.PostModel;

/**
 * Created by Brian on 12/23/2014.
 */
public interface PostRemovedListener {

    public void PostRemoved(PostModel postModel);
}
