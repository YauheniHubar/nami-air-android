package com.nami.airapp.data.adapters;

import android.content.Context;
import android.widget.BaseAdapter;

/**
 * Base Adapter used for all Parse Adapters.
 */
public abstract class BaseParseAdapter extends BaseAdapter {

    // Protected Members
    protected Context context;
    private OnQueryRunListener listener;
    public boolean isLoading;
    protected int loadLimit = 20;

    public interface OnQueryRunListener {
        public void onLoaded();
        public void onLoading();
    }

    /**
     * Default constructor.
     * @param context Context which the adapter runs in.
     */
    public BaseParseAdapter(Context context) {

        this.context = context;
    }

    public void setOnQueryRunListener(OnQueryRunListener listener) {
        this.listener = listener;
        notifyQueryRunListener();
    }

    protected void setLoading(boolean value) {
        isLoading = value;
        notifyQueryRunListener();
    }

    private void notifyQueryRunListener() {
        if (listener != null) {
            if (isLoading) {
                listener.onLoading();
            }
            else {
                listener.onLoaded();
            }
        }
    }
}
