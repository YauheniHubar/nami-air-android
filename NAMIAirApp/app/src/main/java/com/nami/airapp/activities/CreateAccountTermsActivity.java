package com.nami.airapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.content.pm.ActivityInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.nami.airapp.R;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.utils.ActionBarManager;

import java.io.InputStream;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CreateAccountTermsActivity extends ActionBarActivity implements OnClickListener {

    // Private Members.
    private CreateAccountTermsActivityViewContainer viewContainer = null;
    private String terms = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the requested orientation.
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Load the terms.
        this.loadTerms();

        // Initialize the activity and action bar.
        this.initializeActivity();
        this.initializeActionBar();

        // Set the content view.
        setContentView(R.layout.activity_create_account_terms);

        // Initialize the layout content.
        this.initializeLayoutContent();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                ApplicationManager.getInstance().setCancelledTermsOfUse(true);
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (view == this.viewContainer.buttonContinue) {
            this.attemptSetTerms();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(new CalligraphyContextWrapper(newBase));
    }

    /**
     Initializes the activity.
     */
    private void initializeActivity() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(this, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.create_account_title));
    }

    /**
     * Initializes the layout content.
     */
    private void initializeLayoutContent() {

        // Create the view container.
        this.viewContainer = CreateAccountTermsActivityViewContainer.with(this);

        // Add the click events.
        this.viewContainer.buttonContinue.setOnClickListener(this);

        // Check if already agreed to the terms.
        if (ApplicationManager.getInstance().getAcceptedTermsOfUse() == false) {
            this.viewContainer.buttonContinue.setEnabled(false);
            this.viewContainer.radioAccept.setChecked(false);
        } else {
            this.viewContainer.buttonContinue.setEnabled(true);
            this.viewContainer.radioAccept.setChecked(true);
        }

        // Add the radio event.
        this.viewContainer.radioAccept.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                // Test if checked.
                if (viewContainer.radioAccept.isChecked()) {
                    viewContainer.buttonContinue.setEnabled(true);
                }
            }
        });

        // Set the terms.
        TextView textViewTerms = (TextView)this.findViewById(R.id.create_account_terms_text);
        textViewTerms.setText(Html.fromHtml(this.terms));
    }

    /**
     * Attempts to set that the terms were accepted.
     */
    private void attemptSetTerms() {

        // Set that the terms were accepted.
        ApplicationManager.getInstance().setAcceptedTermsOfUse(true);

        // Finish the intent.
        finish();
    }

    /**
     * Loads the terms from the assets.
     */
    private void loadTerms() {

        // Get the asset manager.
        AssetManager assetManager = this.getAssets();

        try {

            // Create the input stream.
            InputStream inputStream = assetManager.open("copy/terms.txt");

            // Read the input stream.
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);

            // Close the stream.
            inputStream.close();

            // Create the text.
            this.terms = new String(buffer);


        }
        catch (Exception e) {

            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Holds the view for the Create Account Terms Activity.
     */
    private static class CreateAccountTermsActivityViewContainer {

        // Private Members
        private Button buttonContinue = null;
        private RadioButton radioAccept = null;

        /**
         * Private Constructor
         */
        private CreateAccountTermsActivityViewContainer() {}

        /**
         * Public Static Constructor
         * @param activity Activity to create the view from
         * @return CreateAccountActivityViewContainer
         */
        public static CreateAccountTermsActivityViewContainer with(Activity activity) {

            // Create the instance.
            CreateAccountTermsActivityViewContainer instance = new CreateAccountTermsActivityViewContainer();

            // Get the items.
            instance.buttonContinue = (Button)activity.findViewById(R.id.create_account_terms_continue);
            instance.radioAccept = (RadioButton)activity.findViewById(R.id.create_account_terms_accept);

            return instance;
        }
    }
}
