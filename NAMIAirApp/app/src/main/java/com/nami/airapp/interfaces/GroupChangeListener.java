package com.nami.airapp.interfaces;

import com.nami.airapp.data.enums.UserType;

/**
 * Created by Brian on 12/8/2014.
 */
public interface GroupChangeListener {


    public void groupChanged(UserType userType);
}
