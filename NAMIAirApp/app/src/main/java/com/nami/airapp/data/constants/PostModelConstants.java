package com.nami.airapp.data.constants;

/**
 * Holds constants for the Post model.
 */
public class PostModelConstants {

    public static final String FieldUser = "user";
    public static final String FieldUserType = "userType";
    public static final String FieldApprovedStatus = "approvedStatus";
    public static final String FieldFlaggedStatus = "flaggedStatus";
    public static final String FieldText = "text";
    public static final String FieldIsPrivate = "isPrivate";
    public static final String FieldInspiredByPost = "inspiredByPost";
    public static final String FieldLikeCount = "likeCount";
    public static final String FieldHugCount = "hugCount";
    public static final String FieldMeTooCount = "meTooCount";
    public static final String FieldMeTooStoryCount = "meTooStoryCount";
    public static final String FieldNoteCount = "noteCount";
    public static final String FieldHashTags = "hashtags";
    public static final String FieldHashNotes = "hashnotes";
    public static final String FieldIsHidden = "isHidden";
}
