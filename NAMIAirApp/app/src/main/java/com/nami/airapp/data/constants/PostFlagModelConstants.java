package com.nami.airapp.data.constants;

/**
 * Holds constants for the PostLike model.
 */
public class PostFlagModelConstants {

    public static final String FieldPost = "post";
    public static final String FieldUser = "user";
    public static final String FieldFlaggedStatus = "flaggedStatus";
}
