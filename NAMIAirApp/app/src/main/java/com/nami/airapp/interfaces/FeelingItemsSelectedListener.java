package com.nami.airapp.interfaces;

import java.util.List;

import com.nami.airapp.data.models.FeelingModel;

/**
 * Created by Brian on 12/3/2014.
 */
public interface FeelingItemsSelectedListener {

    public void feelingItemsSelected(List<FeelingModel> feelings);
}
