package com.nami.airapp.views;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nami.airapp.R;
import com.nami.airapp.data.models.FaqModel;

public class FaqListItemView extends LinearLayout {

    // Private Members.
    private FaqListItemChildContainer viewContainer = null;
    private Context context;

    /**
     * Default Constructor.
     * @param context Context which the view runs in.
     */
    public FaqListItemView(Context context) {
        super(context);

        // Create the view.
        LayoutInflater.from(context).inflate(R.layout.view_faq_list_item, this);

        // Set the context.
        this.context = context;

        // Create the view container.
        this.viewContainer = FaqListItemChildContainer.with(this);
    }

    /**
     * Updates the model.
     * @param model The model to update.
     * @param position The position of the model in the dataset.
     */
    public void updateFromModel(FaqModel model, int position) {

        // Set the name.
        this.viewContainer.textViewDetail.setText(model.Title);
    }


    /**
     * Shows the group as collapsed.
     */
    public void showCollapsed() {

        // Set to collapsed state.
        this.viewContainer.viewSeparator.setVisibility(View.VISIBLE);
        this.viewContainer.imageViewIndicator.setImageResource(R.drawable.ic_faq_closed);
    }

    /**
     * Shows the group as expanded.
     */
    public void showExpanded() {

        // Set to expanded state.
        this.viewContainer.viewSeparator.setVisibility(View.GONE);
        this.viewContainer.imageViewIndicator.setImageResource(R.drawable.ic_faq_open);
    }

    /**
     * Holds the view for the FAQ List Item Child View
     */
    private static class FaqListItemChildContainer {

        // Private Members.
        private TextView textViewDetail;
        private ImageView imageViewIndicator;
        private View viewSeparator;

        // Private Constructor.
        private FaqListItemChildContainer() {}

        // Static Constructor.
        public static FaqListItemChildContainer with(View view) {

            // Create the instance
            FaqListItemChildContainer instance = new FaqListItemChildContainer();

            // Get the items.
            instance.textViewDetail = (TextView)view.findViewById(R.id.faq_group);
            instance.imageViewIndicator = (ImageView)view.findViewById(R.id.faq_group_indicator);
            instance.viewSeparator = (View)view.findViewById(R.id.faq_group_separator);

            return instance;
        }
    }

}
