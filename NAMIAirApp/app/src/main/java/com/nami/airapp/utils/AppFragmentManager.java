package com.nami.airapp.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.BackStackEntry;
import android.support.v4.app.FragmentTransaction;

import com.nami.airapp.R;
import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.fragments.GetHelpFragment;
import com.nami.airapp.fragments.MyPostFeedFragment;
import com.nami.airapp.fragments.PostFeedFragment;
import com.nami.airapp.fragments.PostNotificationFeedFragment;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

/**
 * Handles Fragment Requests.
 */
public class AppFragmentManager {

    /**
     * Removes the fragment from the stack.
     * @param fragment The Fragment to remove from the stack.
     * @param context The Fragment's context.
     */
    public static void RemoveFromStack(Fragment fragment, Context context) {

        // Create the transaction.
        FragmentManager manager = ((FragmentActivity)context).getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.remove(fragment);

        // Commit the transaction and pop the stack.
        transaction.commit();
        manager.popBackStack();

        // Get the top fragment.
        if (manager.getBackStackEntryCount() == 0) return;
        Fragment stackFragment = manager.getFragments().get(manager.getBackStackEntryCount() - 1);

        // Determine the fragment type.
        if (stackFragment instanceof PostFeedFragment) {

            // Set the correct tab.
            MainActivity mainActivity = (MainActivity)context;
            mainActivity.setActionBarTabs(PostFeedFragment.TAB);

        } else if (stackFragment instanceof MyPostFeedFragment) {

            // Set the correct tab.
            MainActivity mainActivity = (MainActivity)context;
            mainActivity.setActionBarTabs(MyPostFeedFragment.TAB);

        } else if (stackFragment instanceof PostNotificationFeedFragment) {

            // Set the correct tab.
            MainActivity mainActivity = (MainActivity)context;
            mainActivity.setActionBarTabs(PostNotificationFeedFragment.TAB);

        } else if (stackFragment instanceof GetHelpFragment) {

            // Set the correct tab.
            MainActivity mainActivity = (MainActivity)context;
            mainActivity.setActionBarTabs(GetHelpFragment.TAB);
        }

    }
}
