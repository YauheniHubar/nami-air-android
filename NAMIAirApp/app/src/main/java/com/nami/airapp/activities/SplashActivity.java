package com.nami.airapp.activities;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.nami.airapp.data.singletons.ApplicationManager;
import com.parse.ParseUser;
import com.parse.ParsePush;
import com.parse.ParsePushBroadcastReceiver;

import com.flurry.android.FlurryAgent;

import com.nami.airapp.R;


/**
 * Splash Activity.
 */
public class SplashActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize the activity and action bar.
        this.initializeActivity();
        this.initializeActionBar();

        // Set the content view.
        setContentView(R.layout.activity_splash);

        // Start the initial activity.
        this.startInitialActivity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    /**
        Initializes the activity.
     */
    private void initializeActivity() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Hide the action bar.
        getSupportActionBar().hide();
    }


    /**
     * Starts the correct initial activity.
     */
    private void startInitialActivity() {

        // Declare the intent.
        Intent intent = null;

        // Create the correct intent based on the user status.
        if (ParseUser.getCurrentUser() != null) {
            intent = new Intent(this, MainActivity.class);

            // Set as having shown the animation.
            ApplicationManager.getInstance().setShownWelcomeAnimation(true);
        }
        else intent = new Intent(this, WelcomeActivity.class);

        // Start the intent.
        startActivity(intent);
        finish();
    }
}
