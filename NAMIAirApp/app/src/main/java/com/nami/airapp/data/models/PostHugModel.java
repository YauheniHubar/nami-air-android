package com.nami.airapp.data.models;

import com.parse.ParseObject;
import com.parse.ParseClassName;
import com.parse.ParseUser;

import com.nami.airapp.data.constants.PostHugModelConstants;
import com.nami.airapp.data.models.PostModel;

/**
 * Contains data about a Post hug.
 */
@ParseClassName("PostHug")
public class PostHugModel extends ParseObject {

    /**
     * Gets the post model.
     * @return Post model.
     */
    public PostModel getPostModel() { return (PostModel)get(PostHugModelConstants.FieldPost); }

    /**
     * Sets the post model.
     * @param postModel Post model.
     */
    public void setPostModel(PostModel postModel) { put(PostHugModelConstants.FieldPost, postModel); }

    /**
     * Gets the post user.
     * @return ParseUser
     */
    public ParseUser getPostUser() { return (ParseUser)get(PostHugModelConstants.FieldUser); }

    /**
     * Sets the post user.
     * @param postUser ParseUser
     */
    public void setPostUser(ParseUser postUser) { put(PostHugModelConstants.FieldUser, postUser); }
}
