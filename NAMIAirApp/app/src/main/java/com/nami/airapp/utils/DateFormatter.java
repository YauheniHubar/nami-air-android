package com.nami.airapp.utils;

import android.content.Context;
import android.text.TextUtils;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;

import com.nami.airapp.R;

/**
 * Formats dates in the app.
 */
public class DateFormatter {


    /**
     * Formats the date that appears on a post.
     * @param postDate The date of the post.
     * @param context The running context.
     * @return Formatted date.
     */
    public static String FormatPostDate(Date postDate, Context context) {

        // Declare and create the return value.
        String time = new PrettyTime().format(postDate);

        // Test for moments.
        if (TextUtils.indexOf(time, context.getString(R.string.date_utils_moment)) >= 0) {
            time = context.getString(R.string.date_utils_just_now);
        }

        return time;
    }
}
