package com.nami.airapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.nami.airapp.R;
import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.adapters.FaqAdapter;
import com.nami.airapp.data.adapters.FaqSectionAdapter;
import com.nami.airapp.data.models.FaqSectionModel;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.handlers.FaqXmlHandler;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.AppFragmentManager;
import com.nami.airapp.views.FaqListItemView;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Post Detail Fragment.
 */
public class FaqMainFragment extends ListFragment {

    // Private Members
    private Context context = null;
    private FaqSectionAdapter adapter = null;
    private List<FaqSectionModel> frequentlyAskedQuestions = null;

    /**
     * Default Constructor.
     */
    public FaqMainFragment() {
    }

    /**
     * Creates a new instance of a PostDetailFragment.
     *
     * @return New instance of a PostDetailFragment.
     */
    public static FaqMainFragment newInstance() {

        // Create the new instance.
        FaqMainFragment instance = new FaqMainFragment();

        return instance;
    }

    /**
     * Called on attach.
     *
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     * @param inflater           The inflater.
     * @param container          The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set the action bar.
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setActionBarNoTabs();

        // Set options menu.
        setHasOptionsMenu(true);

        // Load the frequently asked questions.
        this.loadFrequentlyAskedQuestions();

        // Create the adapter.
        this.adapter = new FaqSectionAdapter(this.context, this.frequentlyAskedQuestions);

        // Set the adapter.
        setListAdapter(adapter);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_faq_main, container, false);
    }

    /**
     * Called when the view is created.
     *
     * @param view               Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    AppFragmentManager.RemoveFromStack(that, getActivity());
                    return true;
                }

                return false;
            }
        });
    }

    /**
     * Called when the menu is created.
     *
     * @param menu     The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    /**
     * Called when a menu item is selected.
     *
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, getActivity());
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     *
     * @param view The view that has a click event.
     */
    public void onClick(View view) {


    }

    /**
     * Called when a list view item is clicked.
     * @param listView List view containing the clicked item.
     * @param view The view.
     * @param position The position of the item.
     * @param id The identifier of the item.
     */
    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {

        if (position == 3) {

            // Launch the site.
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(getString(R.string.faq_nami_about)));
            startActivity(intent);

        } else {

            // Get the post model.
            FaqSectionModel selectedFaqSection = (FaqSectionModel)getListView().getItemAtPosition(position);

            // Create the fragment.
            FaqFragment faqFragment = FaqFragment.newInstance(selectedFaqSection);

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, faqFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();
        }


    }

    /**
     * Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(getActivity(), actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.faq_title));
    }

    /**
     * Loads the frequently asked questions.
     */
    private void loadFrequentlyAskedQuestions() {

        // Get the asset manager.
        AssetManager assetManager = this.context.getAssets();

        try {
            // Create the input stream and reader.
            InputStream inputStream = assetManager.open("xml/frequentlyAskedQuestions.xml");
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser parser = parserFactory.newSAXParser();
            XMLReader reader = parser.getXMLReader();

            // Create the handler and parse the contents.
            FaqXmlHandler xmlHandler = new FaqXmlHandler();
            reader.setContentHandler(xmlHandler);
            InputSource inputSource = new InputSource(inputStream);
            reader.parse(inputSource);

            // Set the items.
            this.frequentlyAskedQuestions = xmlHandler.getItems();

            // Close the stream.
            inputStream.close();

        } catch (Exception e) {
            Toast.makeText(this.context, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Initializes the layout content.
     *
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

    }

}
