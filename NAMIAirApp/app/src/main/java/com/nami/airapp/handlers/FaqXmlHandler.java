package com.nami.airapp.handlers;


import com.nami.airapp.data.models.FaqModel;
import com.nami.airapp.data.models.FaqSectionModel;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

/**
 * Created by Brian on 12/16/2014.
 */
public class FaqXmlHandler extends DefaultHandler {

    // Private Members.
    private ArrayList<FaqSectionModel> items = null;
    private FaqSectionModel section = null;
    private FaqModel item = null;
    private boolean currentElement = false;
    private String currentValue = "";

    public ArrayList<FaqSectionModel> getItems() { return this.items; }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        currentElement = true;
        if (qName.equals("Sections"))
            this.items = new ArrayList<FaqSectionModel>();
        else if (qName.equals("Section"))
            this.section = new FaqSectionModel();
        else if (qName.equals("Questions"))
            this.section.Questions = new ArrayList<FaqModel>();
        else if (qName.equals("Question"))
            this.item = new FaqModel();

    }

    public void endElement(String uri, String localName, String qName) throws SAXException {

        currentElement = false;

        if (qName.equalsIgnoreCase("Title"))
            this.item.Title = currentValue.trim();
        else if (qName.equalsIgnoreCase("Answer"))
            this.item.Answer = currentValue.trim();
        else if (qName.equalsIgnoreCase("Question"))
            this.section.Questions.add(this.item);
        else if (qName.equalsIgnoreCase("Questions"))
            this.items.add(this.section);
        else if (qName.equals("SectionQuestion"))
            this.section.Question = currentValue.trim();


        currentValue = "";
    }

    public void characters(char[] ch, int start, int length)
            throws SAXException {

        if (currentElement) {
            currentValue = currentValue + new String(ch, start, length);
        }

    }

}
