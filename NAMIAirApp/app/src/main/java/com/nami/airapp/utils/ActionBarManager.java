package com.nami.airapp.utils;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.text.Layout;
import android.text.Spannable;
import android.text.style.TextAppearanceSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.graphics.drawable.ColorDrawable;
import android.text.SpannableString;

import com.nami.airapp.R;
import com.nami.airapp.widgets.TextDrawable;

import org.w3c.dom.Text;

/**
 * Manages the action bar.
 */
public class ActionBarManager {

    /**
     * Sets the action bar to use the custom view.
     * @param actionBar Action bar.
     */
    public static void SetCustomView(Context context, ActionBar actionBar) {

        // Clear the title.
        actionBar.setTitle("");

        // Set the background color.
        actionBar.setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.background_color_light)));

        // Remove the icon.
        actionBar.setIcon(new ColorDrawable(context.getResources().getColor(android.R.color.transparent)));

        // Create the custom view.
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View actionBarView = layoutInflater.inflate(R.layout.actionbar, null);

        // Set the custom view.
        actionBar.setDisplayOptions(actionBar.getDisplayOptions() | ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(actionBarView, new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.MATCH_PARENT, Gravity.CENTER));

        // Check for home as up.
        if ((actionBar.getDisplayOptions() & ActionBar.DISPLAY_HOME_AS_UP) == ActionBar.DISPLAY_HOME_AS_UP) {
            actionBar.setHomeAsUpIndicator(R.drawable.actionbar_up_indicator);
        }

    }

    /**
     * Sets the action bar title.
     * @param actionBar Action bar
     * @param title Title
     */
    public static void SetActionBarTitle(ActionBar actionBar, String title) {

        // Set the title.
        TextView actionBarTitle = (TextView)actionBar.getCustomView().findViewById(R.id.actionbar_title);
        actionBarTitle.setText(title);
    }


    /**
     * Sets the action bar home as up as text.
     * @param actionBar Action bar
     * @param homeAsUpText Text
     * @param styleResId Style id.
     */
    public static void SetActionBarHomeAsUpText(Context context, ActionBar actionBar, String homeAsUpText, int styleResId) {

        // Create the spannable string and set the style.
        SpannableString spannableHomeAsUpText = new SpannableString(homeAsUpText);
        if (styleResId != 0) {
            spannableHomeAsUpText.setSpan(new TextAppearanceSpan(context, styleResId), 0 , spannableHomeAsUpText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        // Create the drawable.
        TextDrawable homeAsUpDrawable = new TextDrawable(context);
        homeAsUpDrawable.setText(spannableHomeAsUpText);

        // Set the home as up indicator.
        actionBar.setHomeAsUpIndicator(homeAsUpDrawable);
    }
}
