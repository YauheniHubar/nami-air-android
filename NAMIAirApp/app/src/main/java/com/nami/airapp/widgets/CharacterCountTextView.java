package com.nami.airapp.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.text.Layout;
import android.text.Layout.Alignment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.text.style.TextAppearanceSpan;
import android.util.AttributeSet;

import com.nami.airapp.R;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

/**
 * Class to properly implement ellipsis on text overflow.
 */
public class CharacterCountTextView extends LinkifiedTextView {

	// Private Members
	private CharSequence fullText = null;
	private SpannableStringBuilder textBuilder = null;
	private boolean isEllipsized;
    private boolean isStale;
    private boolean programmaticChange;
	private int maxLines = -1;
    private int maxCharacters = -1;
    private float lineSpacingMultiplier = 1.0f;
    private float lineAdditionalVerticalPadding = 0.0f;
    private SpannableString ellipsisText;
    private int ellipsisTextStyle = -1;
    private boolean preserveQuote = false;

    /**
     * Constructor.
     * @param context Context in which the text view runs.
     */
	public CharacterCountTextView(Context context) {
        super(context);

    }

    /**
     * Constructor.
     * @param context Context in which the text view runs.
     * @param attrs Attribute set.
     */
    public CharacterCountTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Initialize based on the attributes.
        this.initializeAttributes(context, attrs);
    }

    /**
     * Constructor.
     * @param context Context in which the text view runs.
     * @param attrs Attribute set.
     * @param defStyle Default style.
     */
    public CharacterCountTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        
        // Initialize based on the attributes.
        this.initializeAttributes(context, attrs);
    }

    /**
     * Sets the max number of lines before ellipsis.
     * @param maxLines Max number of lines before ellipsis.
     */
    @Override
    public void setMaxLines(int maxLines) {
        super.setMaxLines(maxLines);
        this.maxLines = maxLines;
        isStale = true;
    }

    /**
     * Sets the line spacing.
     * @param add Additional vertical padding.
     * @param mult Line spacing multiplier.
     */
    @Override
    public void setLineSpacing(float add, float mult) {
        this.lineAdditionalVerticalPadding = add;
        this.lineSpacingMultiplier = mult;
        super.setLineSpacing(add, mult);
    }

    /**
     * Called when the text changes.
     * @param text The new text.
     * @param start The start position of the new text.
     * @param before The position before the new text.
     * @param after The position after the new text.
     */
    @Override
    protected void onTextChanged(CharSequence text, int start, int before, int after) {
        super.onTextChanged(text, start, before, after);
        if (!programmaticChange) {
            fullText = text;
            isStale = true;
        }
    }

    /**
     * Called when needed to draw.
     * @param canvas The canvas where it is drawn.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        if (isStale) {
            super.setEllipsize(null);
            this.layoutText();
        }
        super.onDraw(canvas);
    }

    /**
     * Override to make sure the default text view method is not called.
     * @param where Where to set the ellipsis at.
     */
    @Override
    public void setEllipsize(TruncateAt where) {
        // Ellipsize settings are not respected
    }

    /**
     * Gets the maximum number of lines.
     * @return Maximum number of lines.
     */
    public int getMaxLines() {
    	
    	return this.maxLines;
    }

    /**
     * Lays out the text.
     */
    private void layoutText() {
    	
        // Create the initial state.
        int maxLines = getMaxLines();
        CharSequence workingText = fullText;
        boolean ellipsized = false;

        if (isInEditMode())
            return;

        // Ensure it is not set to allow all text.
        if (maxLines != -1 || maxCharacters != -1) {

            // Create the string builder.
            this.textBuilder = new SpannableStringBuilder(workingText);

            // Create the layout.
            Layout layout = createWorkingLayout(this.textBuilder);

            // Check if the layout is greater than the max number of lines.
            if ((maxLines != -1 && (layout.getLineCount() > maxLines)) ||
                (maxCharacters != -1 && (workingText.length() > maxCharacters))) {

                // Get the line offset if it is supposed to use max characters.
                if (maxCharacters != -1 && workingText.length() > maxCharacters) {
                    maxLines = layout.getLineForOffset(maxCharacters);
                }

                // Get all but the last line.
                workingText = fullText.subSequence(0, layout.getLineEnd(maxLines - 1));

                // Create the string builder.
                this.textBuilder = new SpannableStringBuilder(workingText);
                this.textBuilder.append(" ").append(this.ellipsisText);

                // Loop until the layout fits.
                while (createWorkingLayout(this.textBuilder).getLineCount() > maxLines) {
                    int lastSpace = TextUtils.lastIndexOf(workingText, ' ');
                    if (lastSpace == -1) {
                        break;
                    }

                    // Update the text.
                    workingText = workingText.subSequence(0, lastSpace);
                    this.textBuilder = new SpannableStringBuilder(workingText);
                    if (this.preserveQuote) {
                        this.textBuilder.append("...\"");
                    }
                    this.textBuilder.append(" ").append(this.ellipsisText);
                }

                // Set the final text.
                this.textBuilder = new SpannableStringBuilder(workingText);
                if (this.preserveQuote) {
                    this.textBuilder.append("...\"");
                }
                this.textBuilder.append(" ").append(this.ellipsisText);
                ellipsized = true;
            }
        }

        // Determine if the text has changed.
        if (!this.textBuilder.toString().equals(getText())) {
            programmaticChange = true;
            try {
                setText(this.textBuilder, BufferType.SPANNABLE);
            } finally {
                programmaticChange = false;
            }
        }

        // Set the final state.
        isStale = false;
        if (ellipsized != isEllipsized) {
            isEllipsized = ellipsized;
        }

    }


    /**
     * Creates a layout based on the text.
     * @param workingText The text used to create the layout.
     * @return Newly constructed layout that fits the text.
     */
    private Layout createWorkingLayout(CharSequence workingText) {
        return new StaticLayout(workingText, getPaint(), getWidth() - getPaddingLeft() - getPaddingRight(),
                Alignment.ALIGN_NORMAL, lineSpacingMultiplier, lineAdditionalVerticalPadding, false);
    }

    /**
     * Initializes the control based on the attributes.
     * @param context Context in which the text view runs.
     * @param attrs Attribute set.
     */
    private void initializeAttributes(Context context, AttributeSet attrs) {
    	
    	// Get the max number of lines.
    	TypedArray maxLinesArray = context.obtainStyledAttributes(attrs, new int[] { android.R.attr.maxLines });
        int maxLines = maxLinesArray.getInt(0, -1);
        if (maxLines > 0) {
            this.setMaxLines(maxLines);
        }
        maxLinesArray.recycle();

        // Get the ellipsis text and style.
        TypedArray ellipsisTextArray = context.obtainStyledAttributes(attrs, R.styleable.EllipsisTextView);
        final int count = ellipsisTextArray.getIndexCount();
        for (int index = 0; index < count; index++) {
        	int attr = ellipsisTextArray.getIndex(index);
        	switch(attr) {
        	
        		case R.styleable.EllipsisTextView_ellipsisText:
        			this.ellipsisText = new SpannableString(ellipsisTextArray.getString(attr));
        			break;
        		case R.styleable.EllipsisTextView_ellipsisTextStyle:        			
        			this.ellipsisTextStyle = ellipsisTextArray.getResourceId(attr, -1);
        			break;
                case R.styleable.EllipsisTextView_ellipsisPreserveQuote:
                    this.preserveQuote = ellipsisTextArray.getBoolean(attr, false);
                    break;
                case R.styleable.EllipsisTextView_ellipsisMaxCharacters:
                    this.maxCharacters = ellipsisTextArray.getInteger(attr, -1);

        	};
        }
        ellipsisTextArray.recycle(); 
        
        // Style the ellipsis text.
        this.styleEllipsisText();
    }

    /**
     * Sets the style for the ellipsis text.
     */
    private void styleEllipsisText() {
    	
    	// Check for a style.
    	if (this.ellipsisTextStyle > 0) {

            // Create the styled span.
            TextAppearanceSpan styledSpan = new TextAppearanceSpan(getContext(), this.ellipsisTextStyle);

            // Create the type face span.
            CalligraphyTypefaceSpan typeFaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(getContext().getAssets(), "fonts/proxima_nova_webfont.ttf"));

            // Set the spans.
    		this.ellipsisText.setSpan(typeFaceSpan, 0, this.ellipsisText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            this.ellipsisText.setSpan(styledSpan, 0, this.ellipsisText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    	}
    }
}
