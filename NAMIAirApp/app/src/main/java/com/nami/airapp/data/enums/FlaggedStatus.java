package com.nami.airapp.data.enums;

/**
 * Flagged Status enum.
 */
public enum FlaggedStatus {


    // Values
    NOT_FLAGGED (0),
    INAPPROPRIATE (1),
    SELF_HARM (2),
    THREATENING (3),
    BULLYING (4),
    SPAM (5);


    // Private Members.
    private final int id;

    /**
     * Constructor.
     * @param id Flagged Status value.
     */
    FlaggedStatus(int id) {

        // Set the members.
        this.id = id;
    }

    /**
     * Gets the Flagged Status value.
     * @return Flagged Status value.
     */
    public int getValue() {

        return this.id;
    }

    /**
     * Creates a FlaggedStatus enum from a value.
     * @param id Flagged Status value.
     * @return FlaggedStatus enum.
     */
    public static FlaggedStatus fromValue(int id) {

        // Determine the id.
        switch (id) {

            case 0 : { return NOT_FLAGGED; }
            case 1 : { return INAPPROPRIATE; }
            case 2 : { return SELF_HARM; }
            case 3 : { return THREATENING; }
            case 4 : { return BULLYING; }
            case 5 : { return SPAM; }
        };

        return null;
    }
}
