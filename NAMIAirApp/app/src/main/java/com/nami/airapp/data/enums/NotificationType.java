package com.nami.airapp.data.enums;

/**
 * Notification Type enum
 */
public enum NotificationType {

    // Values
    SOCIAL (1),
	NOTES (2),
    INSPIRED (3),
    PENDING (4),
	APPROVED (5),
    DISAPPROVED (6),
	DIRECT(7),
    CHANGE_GROUP(8);

    // Private Members.
    private final int id;

    /**
     * Constructor.
     * @param id Notification Type value.
     */
    NotificationType(int id) {

        // Set the members.
        this.id = id;
    }

    /**
     * Gets the Notification Type value.
     * @return Notification Type value.
     */
    public int getValue() {

        return this.id;
    }

    /**
     * Creates a NotificationType enum from a value.
     * @param id Notification Type value.
     * @return NotificationType enum.
     */
    public static NotificationType fromValue(int id) {

        // Determine the id.
        switch (id) {

            case 1 : { return SOCIAL; }
			case 2 : { return NOTES; }
            case 3 : { return INSPIRED; }
            case 4 : { return PENDING; }
			case 5 : { return APPROVED; }
            case 6 : { return DISAPPROVED; }
			case 7 : { return DIRECT; }
            case 8 : { return CHANGE_GROUP; }
        };

        return null;
    }
}
