package com.nami.airapp.data.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.nami.airapp.R;
import com.nami.airapp.data.models.FaqSectionModel;
import com.nami.airapp.data.models.FeelingModel;
import com.nami.airapp.views.FaqSectionItemView;
import com.nami.airapp.views.FeelingListItemView;

import java.util.List;


public class FaqSectionAdapter extends BaseParseAdapter {


    // Private Members.
    private Context context;
    private List<FaqSectionModel> dataSet;

    public FaqSectionAdapter(Context context, List<FaqSectionModel> frequentlyAskedQuestions) {
        super(context);

        this.context = context;
        this.dataSet = frequentlyAskedQuestions;

        this.reloadData();
    }

    /**
     * Gets the number of items in the list.
     * @return Number of items in the list.
     */
    @Override
    public int getCount() {

        return this.dataSet.size();
    }

    /**
     * Gets the item at the position in the list.
     * @param position Position in the list.
     * @return Item at the position in the list.
     */
    @Override
    public FaqSectionModel getItem(int position) {

        return this.dataSet.get(position);
    }

    /**
     * Gets the item identifier at the position in the list.
     * @param position Position in the list.
     * @return Item identifier at the position in the list.
     */
    @Override
    public long getItemId(int position) {

        return position;
    }

    /**
     * Gets the view for the item in the position in the list.
     * @param position Position in the list.
     * @param convertView View of the item in the list.
     * @param parent Parent view of the item in the list.
     * @return Updated view of the item in the list.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Create the view if necessary.
        if (convertView == null) {
            convertView = new FaqSectionItemView(context);
        }

        // Get the item and update the view.
        final FaqSectionModel model = this.getItem(position);
        ((FaqSectionItemView)convertView).updateFromModel(model, position);

        return convertView;
    }

    /**
     * Reloads the data.
     */
    public void reloadData() {

        // Notify that the data set has changed.
        this.notifyDataSetChanged();
    }


}
