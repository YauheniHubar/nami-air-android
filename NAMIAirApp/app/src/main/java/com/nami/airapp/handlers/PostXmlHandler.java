package com.nami.airapp.handlers;


import java.util.ArrayList;
import java.util.Date;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.nami.airapp.data.enums.ApprovedStatus;
import com.nami.airapp.data.models.PostModel;

/**
 * Created by Brian on 12/16/2014.
 */
public class PostXmlHandler extends DefaultHandler {

    // Private Members.
    private ArrayList<PostModel> items = null;
    private PostModel item = null;
    private boolean currentElement = false;
    private String currentValue = "";

    public ArrayList<PostModel> getItems() { return this.items; }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        currentElement = true;
        if (qName.equals("Posts"))
            this.items = new ArrayList<PostModel>();
        else if (qName.equals("Post")) {
            this.item = new PostModel();
            this.item.setApprovedStatus(ApprovedStatus.APPROVED);
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {

        currentElement = false;

        if (qName.equalsIgnoreCase("PostText"))
            this.item.setPostText(currentValue.trim());
        else if (qName.equalsIgnoreCase("PostLikeCount"))
            this.item.setLikeCount(Integer.valueOf(currentValue.trim()));
        else if (qName.equalsIgnoreCase("PostHugCount"))
            this.item.setHugCount(Integer.valueOf(currentValue.trim()));
        else if (qName.equalsIgnoreCase("PostMeTooCount"))
            this.item.setMeTooCount(Integer.valueOf(currentValue.trim()));
        else if (qName.equalsIgnoreCase("PostNoteCount"))
            this.item.setNoteCount(Integer.valueOf(currentValue.trim()));
        else if (qName.equalsIgnoreCase("PostInspiredByCount"))
            this.item.setMeTooStoryCount(Integer.valueOf(currentValue.trim()));
        else if (qName.equalsIgnoreCase("PostDate")) {

            int days = Integer.valueOf(currentValue.trim());
            Date date = new Date();
            this.item.setPostDate(new Date(date.getTime() - (1000 * 60 * 60 * 24 * days)));

        }
        else if (qName.equalsIgnoreCase("Post"))
            this.items.add(item);

        currentValue = "";

    }

    public void characters(char[] ch, int start, int length)
            throws SAXException {

        if (currentElement) {
            currentValue = currentValue + new String(ch, start, length);
        }

    }

}
