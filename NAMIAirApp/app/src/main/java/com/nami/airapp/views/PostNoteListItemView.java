package com.nami.airapp.views;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.*;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.*;
import android.text.TextUtils.*;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import java.util.Date;
import java.util.Collections;

import com.nami.airapp.R;
import com.nami.airapp.data.models.*;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.utils.DateFormatter;
import com.nami.airapp.utils.PostManager;

/**
 * Class containing logic for the view in the Note List
 */
public class PostNoteListItemView extends LinearLayout {

	// Private Members.
	private PostNoteListItemViewContainer viewContainer = null;
    private Context context;

    /**
     * Default Constructor.
     * @param context Context which the view runs in.
     */
	public PostNoteListItemView(Context context) {
		super(context);
		
		// Create the view.
		LayoutInflater.from(context).inflate(R.layout.view_post_note_list_item, this);

        // Set the context.
        this.context = context;
		
		// Create the view container.
		this.viewContainer = PostNoteListItemViewContainer.with(this);
	}

    /**
     * Updates the model.
     * @param fragment The fragment.
     * @param model The model to update.
     */
	public void updateFromModel(Fragment fragment, PostNoteModel model) {

        // Set the note date.
        String date = DateFormatter.FormatPostDate(model.getNoteDate(), this.context);
        this.viewContainer.textViewNoteDate.setText(date);

        // Build the note text.
        String noteText = "";
        for (String note : model.getPostNotes()) {

            noteText += note + " ";
        }

        // Create the Spannable string.
        SpannableString noteTextSpannable = new SpannableString(noteText);

        // Update the text.
        PostManager.setPostHashNotesStyle(context, fragment, noteTextSpannable);

        // Set the note text.
        this.viewContainer.textViewNoteText.setText(noteTextSpannable, BufferType.SPANNABLE);
        this.viewContainer.textViewNoteText.setMovementMethod(LinkMovementMethod.getInstance());
	}

   
    /**
     * Holds the view for the Post Feed Item.
     */
	private static class PostNoteListItemViewContainer {

		// Private Members.
        private TextView textViewNoteDate;
        private TextView textViewNoteText;

		// Private Constructor.
		private PostNoteListItemViewContainer() {}

		// Static Constructor.
		public static PostNoteListItemViewContainer with(View view) {

			// Create the instance
			PostNoteListItemViewContainer instance = new PostNoteListItemViewContainer();

            // Get the items.
            instance.textViewNoteDate = (TextView)view.findViewById(R.id.post_note_list_text_view_date);
            instance.textViewNoteText = (TextView)view.findViewById(R.id.post_note_list_text_view_text);

			return instance;
		}
	}
	
}
