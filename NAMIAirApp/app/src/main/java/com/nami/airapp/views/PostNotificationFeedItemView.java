package com.nami.airapp.views;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;

import com.nami.airapp.R;
import com.nami.airapp.data.models.PostNotificationModel;
import com.nami.airapp.utils.DateFormatter;

/**
 * Class containing logic for the view in the Post Feed.
 */
public class PostNotificationFeedItemView extends LinearLayout {

	// Private Members.
    private PostNotificationFeedItemViewContainer viewContainer = null;
    private Context context;

    /**
     * Default Constructor.
     * @param context Context which the view runs in.
     */
	public PostNotificationFeedItemView(Context context) {
		super(context);
		
		// Create the view.
		LayoutInflater.from(context).inflate(R.layout.view_post_notification_feed_item, this);

        // Set the context.
        this.context = context;
		
		// Create the view container.
		this.viewContainer = PostNotificationFeedItemViewContainer.with(this);
	}

    /**
     * Updates the model.
     * @param model The model to update.
     * @param position The position of the model in the dataset.
     */
	public void updateFromModel(PostNotificationModel model, int position) {

        // Toggle the view containers.
        this.toggleViewContainers(model);
		
        // Create the appropriate text.
        switch (model.getNotificationType()) {
            case SOCIAL:
                this.updateSocialText(model);
                break;
            case INSPIRED:
                this.updateInspiredText(model);
                break;
            case NOTES:
                this.updateNoteText(model);
                break;
            case PENDING:
                this.updatePendingText(model);
                break;
            case APPROVED:
                this.updateApprovedText(model);
                break;
            case DISAPPROVED:
                this.updateDisapprovedText(model);
                break;
            case DIRECT:
                this.updateDirectText(model);
                break;
            case CHANGE_GROUP:
                this.updateChangeGroupText(model);
                break;
        }
	}

    /**
     * Displays the appropriate container.
     * @param model
     */
    private void toggleViewContainers(PostNotificationModel model) {

        if (model.getRead()) {

            switch (model.getNotificationType()) {
                case DIRECT:
                    this.viewContainer.layoutPostNotificationFeedItemAdminReadContainer.setVisibility(View.VISIBLE);
                    this.viewContainer.layoutPostNotificationFeedItemAdminNotReadContainer.setVisibility(View.GONE);
                    this.viewContainer.layoutPostNotificationFeedItemReadContainer.setVisibility(View.GONE);
                    this.viewContainer.layoutPostNotificationFeedItemNotReadContainer.setVisibility(View.GONE);
                    break;
                default:
                    this.viewContainer.layoutPostNotificationFeedItemAdminReadContainer.setVisibility(View.GONE);
                    this.viewContainer.layoutPostNotificationFeedItemAdminNotReadContainer.setVisibility(View.GONE);
                    this.viewContainer.layoutPostNotificationFeedItemReadContainer.setVisibility(View.VISIBLE);
                    this.viewContainer.layoutPostNotificationFeedItemNotReadContainer.setVisibility(View.GONE);
                    break;
            }

        } else {

            switch (model.getNotificationType()) {
                case DIRECT:
                    this.viewContainer.layoutPostNotificationFeedItemAdminReadContainer.setVisibility(View.GONE);
                    this.viewContainer.layoutPostNotificationFeedItemAdminNotReadContainer.setVisibility(View.VISIBLE);
                    this.viewContainer.layoutPostNotificationFeedItemReadContainer.setVisibility(View.GONE);
                    this.viewContainer.layoutPostNotificationFeedItemNotReadContainer.setVisibility(View.GONE);
                    break;
                default:
                    this.viewContainer.layoutPostNotificationFeedItemAdminReadContainer.setVisibility(View.GONE);
                    this.viewContainer.layoutPostNotificationFeedItemAdminNotReadContainer.setVisibility(View.GONE);
                    this.viewContainer.layoutPostNotificationFeedItemReadContainer.setVisibility(View.GONE);
                    this.viewContainer.layoutPostNotificationFeedItemNotReadContainer.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }

    /**
     * Updates the social text.
     * @param model Post Notification model
     */
    private void updateSocialText(PostNotificationModel model) {

        // Declare the social strings.
        ArrayList<String> socialStrings = new ArrayList<String>();
        String likeText = null;
        String hugText = null;
        String meTooText = null;

        // Get the social counts.
        int likeCount = model.getLikeCount();
        int hugCount = model.getHugCount();
        int meTooCount = model.getMeTooCount();

        // Create the appropriate social strings.
        if (likeCount > 0) {
            if (likeCount > 1)
                likeText = String.format(context.getString(R.string.post_notification_social_likes), likeCount);
            else likeText = String.format(context.getString(R.string.post_notification_social_like), likeCount);
            socialStrings.add(likeText);
        }
        if (hugCount > 0) {
            if (hugCount > 1)
                hugText = String.format(context.getString(R.string.post_notification_social_hugs), hugCount);
            else hugText = String.format(context.getString(R.string.post_notification_social_hug), hugCount);
            socialStrings.add(hugText);
        }
        if (meTooCount > 0) {
            if (meTooCount > 1)
                meTooText = String.format(context.getString(R.string.post_notification_social_metoos), meTooCount);
            else meTooText = String.format(context.getString(R.string.post_notification_social_metoo), meTooCount);
            socialStrings.add(meTooText);
        }

        // Create the notification text.
        String notificationText = "";
        for (int index = 0; index < socialStrings.size(); index++) {
            notificationText += socialStrings.get(index);
            if (index < socialStrings.size() - 1) {
                if (index == 0 && socialStrings.size() == 2)
                    notificationText += " and ";
                else if (index == 0 && socialStrings.size() == 3)
                    notificationText += ", ";
                else if (index == 1 && socialStrings.size() == 3)
                    notificationText += ", and ";
            }
        }

        // Create the spannable notification text.
        String yourPostText = context.getString(R.string.post_notification_your_post) + " ";
        notificationText = yourPostText + notificationText + ".   ";
        SpannableString notificationTextSpannable = new SpannableString(notificationText);
        int postPosition = TextUtils.indexOf(notificationTextSpannable, "post");
        notificationTextSpannable.setSpan(new TextAppearanceSpan(getContext(), R.style.post_notification_text_post), postPosition, postPosition + 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Create the date.
        SpannableString dateSpannable = createDateString(model);

        // Create the combined text.
        CharSequence combinedText = TextUtils.concat(notificationTextSpannable, dateSpannable);

        // Set the notification text.
        if (model.getRead())
            this.viewContainer.textViewPostNotificationFeedItemReadText.setText(combinedText);
        else this.viewContainer.textViewPostNotificationFeedItemNotReadText.setText(combinedText);
    }

    /**
     * Updates the note text.
     * @param model Post Notification model
     */
    private void updateNoteText(PostNotificationModel model) {

        // Declare the note string.
        String noteText = null;

        // Get the note counts.
        int noteCount = model.getNoteCount();

        // Create the appropriate note strings.
        if (noteCount > 1)
            noteText = String.format(context.getString(R.string.post_notification_notes), noteCount);
        else noteText = String.format(context.getString(R.string.post_notification_note), noteCount);

        // Create the notification text.
        int textAppended = 0;
        String notificationText = "";
        if (noteText != null) {
            notificationText += noteText;
        }

        // Create the spannable notification text.
        String yourPostText = context.getString(R.string.post_notification_your_post) + " ";
        notificationText = yourPostText + notificationText + ".   ";
        SpannableString notificationTextSpannable = new SpannableString(notificationText);
        int postPosition = TextUtils.indexOf(notificationTextSpannable, "post");
        notificationTextSpannable.setSpan(new TextAppearanceSpan(getContext(), R.style.post_notification_text_post), postPosition, postPosition + 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Create the date.
        SpannableString dateSpannable = createDateString(model);

        // Create the combined text.
        CharSequence combinedText = TextUtils.concat(notificationTextSpannable, dateSpannable);

        // Set the notification text.
        if (model.getRead())
            this.viewContainer.textViewPostNotificationFeedItemReadText.setText(combinedText);
        else this.viewContainer.textViewPostNotificationFeedItemNotReadText.setText(combinedText);
    }

    /**
     * Updates the pending text.
     * @param model Post Notification model
     */
    private void updatePendingText(PostNotificationModel model) {

        // Get the notification text.
        String notificationText = context.getString(R.string.post_notification_pending) + "    ";

        // Create the spannable notification text.
        SpannableString notificationTextSpannable = new SpannableString(notificationText);
        int postPosition = TextUtils.indexOf(notificationTextSpannable, "post");
        notificationTextSpannable.setSpan(new TextAppearanceSpan(getContext(), R.style.post_notification_text_post), postPosition, postPosition + 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Create the date.
        SpannableString dateSpannable = createDateString(model);

        // Create the combined text.
        CharSequence combinedText = TextUtils.concat(notificationTextSpannable, dateSpannable);

        // Set the notification text.
        if (model.getRead())
            this.viewContainer.textViewPostNotificationFeedItemReadText.setText(combinedText);
        else this.viewContainer.textViewPostNotificationFeedItemNotReadText.setText(combinedText);
    }

    /**
     * Updates the approved text.
     * @param model Post Notification model
     */
    private void updateApprovedText(PostNotificationModel model) {

        // Get the notification text.
        String notificationText = context.getString(R.string.post_notification_approved) + "    ";

        // Create the spannable notification text.
        SpannableString notificationTextSpannable = new SpannableString(notificationText);
        int postPosition = TextUtils.indexOf(notificationTextSpannable, "post");
        notificationTextSpannable.setSpan(new TextAppearanceSpan(getContext(), R.style.post_notification_text_post), postPosition, postPosition + 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Create the date.
        SpannableString dateSpannable = createDateString(model);

        // Create the combined text.
        CharSequence combinedText = TextUtils.concat(notificationTextSpannable, dateSpannable);

        // Set the notification text.
        if (model.getRead())
            this.viewContainer.textViewPostNotificationFeedItemReadText.setText(combinedText);
        else this.viewContainer.textViewPostNotificationFeedItemNotReadText.setText(combinedText);
    }

    /**
     * Updates the disapproved text.
     * @param model Post Notification model
     */
    private void updateDisapprovedText(PostNotificationModel model) {

        // Get the notification text.
        String notificationText = context.getString(R.string.post_notification_disapproved) + "    ";

        // Create the spannable notification text.
        SpannableString notificationTextSpannable = new SpannableString(notificationText);
        int postPosition = TextUtils.indexOf(notificationTextSpannable, "post");
        notificationTextSpannable.setSpan(new TextAppearanceSpan(getContext(), R.style.post_notification_text_post), postPosition, postPosition + 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Create the date.
        SpannableString dateSpannable = createDateString(model);

        // Create the combined text.
        CharSequence combinedText = TextUtils.concat(notificationTextSpannable, dateSpannable);

        // Set the notification text.
        if (model.getRead())
            this.viewContainer.textViewPostNotificationFeedItemReadText.setText(combinedText);
        else this.viewContainer.textViewPostNotificationFeedItemNotReadText.setText(combinedText);
    }

    /**
     * Updates the changed group text.
     * @param model Post Notification model
     */
    private void updateChangeGroupText(PostNotificationModel model) {

        // Get the notification text.
        String notificationText = context.getString(R.string.post_notification_change_group) + "    ";

        // Create the spannable notification text.
        SpannableString notificationTextSpannable = new SpannableString(notificationText);

        // Create the date.
        SpannableString dateSpannable = createDateString(model);

        // Create the combined text.
        CharSequence combinedText = TextUtils.concat(notificationTextSpannable, dateSpannable);

        // Set the notification text.
        if (model.getRead())
            this.viewContainer.textViewPostNotificationFeedItemReadText.setText(combinedText);
        else this.viewContainer.textViewPostNotificationFeedItemNotReadText.setText(combinedText);
    }


    /**
     * Updates the inspired text.
     * @param model
     */
    private void updateInspiredText(PostNotificationModel model) {

        // Declare the inspired string.
        String inspiredText = null;

        // Get the inspired counts.
        int inspiredCount = model.getInspiredByCount();

        // Create the appropriate note strings.
        if (inspiredCount > 1)
            inspiredText = String.format(context.getString(R.string.post_notification_inspired_posts), inspiredCount);
        else inspiredText = String.format(context.getString(R.string.post_notification_inspired_post), inspiredCount);

        // Create the notification text.
        int textAppended = 0;
        String notificationText = "";
        if (inspiredText != null) {
            notificationText += inspiredText;
        }

        // Create the spannable notification text.
        String yourPostText = context.getString(R.string.post_notification_your_post) + " ";
        notificationText = yourPostText + notificationText + ".   ";
        SpannableString notificationTextSpannable = new SpannableString(notificationText);
        int postPosition = TextUtils.indexOf(notificationTextSpannable, "post");
        notificationTextSpannable.setSpan(new TextAppearanceSpan(getContext(), R.style.post_notification_text_post), postPosition, postPosition + 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Create the date.
        SpannableString dateSpannable = createDateString(model);

        // Create the combined text.
        CharSequence combinedText = TextUtils.concat(notificationTextSpannable, dateSpannable);

        // Set the notification text.
        if (model.getRead())
            this.viewContainer.textViewPostNotificationFeedItemReadText.setText(combinedText);
        else this.viewContainer.textViewPostNotificationFeedItemNotReadText.setText(combinedText);
    }

    /**
     * Updates the direct text.
     * @param model Post Notification model
     */
    private void updateDirectText(PostNotificationModel model) {

        // Get the notification text.
        String notificationText = context.getString(R.string.post_notification_moderator) + "    ";

        // Create the spannable notification text.
        SpannableString notificationTextSpannable = new SpannableString(notificationText);

        // Create the date.
        SpannableString dateSpannable = createDateString(model);

        // Create the combined text.
        CharSequence combinedText = TextUtils.concat(notificationTextSpannable, dateSpannable);

        // Set the notification text.
        if (model.getRead())
            this.viewContainer.textViewPostNotificationFeedItemAdminReadText.setText(combinedText);
        else this.viewContainer.textViewPostNotificationFeedItemAdminNotReadText.setText(combinedText);
    }

    /**
     * Creates the date string for the post notification.
     * @param model Post Notification model
     * @return Spannable string
     */
    private SpannableString createDateString(PostNotificationModel model) {

        // Get the date.
        String notificationDate = DateFormatter.FormatPostDate(model.getModifiedAt(), this.context);

        // Create the spannable string.
        SpannableString dateSpannable = new SpannableString(notificationDate);

        // Set the style.
        dateSpannable.setSpan(new TextAppearanceSpan(getContext(), R.style.post_notification_date), 0, notificationDate.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return dateSpannable;
    }

    /**
     * Holds the view for the Post Notification Feed Item.
     */
	private static class PostNotificationFeedItemViewContainer {

		// Private Members.
        private LinearLayout layoutPostNotificationFeedItemNotReadContainer = null;
        private LinearLayout layoutPostNotificationFeedItemReadContainer = null;
        private LinearLayout layoutPostNotificationFeedItemAdminNotReadContainer = null;
        private LinearLayout layoutPostNotificationFeedItemAdminReadContainer = null;
        private TextView textViewPostNotificationFeedItemNotReadText = null;
        private TextView textViewPostNotificationFeedItemReadText = null;
        private TextView textViewPostNotificationFeedItemAdminNotReadText = null;
        private TextView textViewPostNotificationFeedItemAdminReadText = null;

		// Private Constructor.
		private PostNotificationFeedItemViewContainer() {}

		// Static Constructor.
		public static PostNotificationFeedItemViewContainer with(View view) {

			// Create the instance
            PostNotificationFeedItemViewContainer instance = new PostNotificationFeedItemViewContainer();

            // Get the items.
            instance.layoutPostNotificationFeedItemNotReadContainer = (LinearLayout)view.findViewById(R.id.post_notification_feed_item_not_read_container);
            instance.layoutPostNotificationFeedItemReadContainer = (LinearLayout)view.findViewById(R.id.post_notification_feed_item_read_container);
            instance.layoutPostNotificationFeedItemAdminNotReadContainer = (LinearLayout)view.findViewById(R.id.post_notification_feed_item_admin_not_read_container);
            instance.layoutPostNotificationFeedItemAdminReadContainer = (LinearLayout)view.findViewById(R.id.post_notification_feed_item_admin_read_container);
            instance.textViewPostNotificationFeedItemNotReadText = (TextView)view.findViewById(R.id.post_notification_feed_item_not_read_text_view_text);
            instance.textViewPostNotificationFeedItemReadText = (TextView)view.findViewById(R.id.post_notification_feed_item_read_text_view_text);
            instance.textViewPostNotificationFeedItemAdminNotReadText = (TextView)view.findViewById(R.id.post_notification_feed_item_admin_not_read_text_view_text);
            instance.textViewPostNotificationFeedItemAdminReadText = (TextView)view.findViewById(R.id.post_notification_feed_item_admin_read_text_view_text);

			return instance;
		}
	}
	
}
