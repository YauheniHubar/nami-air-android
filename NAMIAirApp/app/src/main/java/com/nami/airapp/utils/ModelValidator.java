package com.nami.airapp.utils;

/**
 * Validates a model.
 * @param <T> Object type to validate.
 */
public class ModelValidator<T extends Object> {

    // Private Members.
    private T model;
    private boolean valid;
    private String message;

    /**
     * Public Constructor
     * @param model Model to validate.
     * @param valid If the model is valid.
     * @param message Message to display if not valid.
     */
    public ModelValidator(T model, boolean valid, String message) {

        // Set the members.
        this.model = model;
        this.valid = valid;
        this.message = message;
    }

    /**
     * Gets the model that was validated.
     * @return Model that was validated.
     */
    public T getModel() {

        return this.model;
    }

    /**
     * Gets the validation status.
     * @return True if validation was successful, false if not.
     */
    public boolean isValid() {

        return this.valid;
    }

    /**
     * Gets the validation message.
     * @return Message to display if validation was not successful.
     */
    public String getMessage() {

        return this.message;
    }
}
