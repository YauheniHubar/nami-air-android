package com.nami.airapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.nami.airapp.R;
import com.nami.airapp.utils.ActionBarManager;

/**
 * Post Compose Fragment
 */
public class GetHelpFragment extends Fragment implements OnClickListener {

    // Private Members.
    private Context context = null;
    private GetHelpViewContainer viewContainer = null;

    // Public Static Members.
    public static final int TAB = 4;
    public static final String TAG = "GetHelpFragmentTag";

    /**
     * Default Constructor.
     */
    public GetHelpFragment() {}


    /**
     * Creates a new instance of a PostComposeFragment.
     * @return New instance of a PostComposeFragment.
     */
    public static GetHelpFragment newInstance() {

        // Create and return the new instance.
        GetHelpFragment instance = new GetHelpFragment();
        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set options menu.
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_get_help, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {


    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (this.viewContainer.layoutContactSomeone == view) {
            this.handleContactSomeone();
        } else if (this.viewContainer.layoutFindSupport == view) {
            this.handleFindSupport();
        } else if (this.viewContainer.layoutLearnMore == view) {
            this.handleLearnMore();
        } else if (this.viewContainer.layoutHelpSomeone == view) {
            this.handleHelpSomeone();
        } else if (this.viewContainer.layoutAbout == view) {
            this.handleAbout();
        }
    }

    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.main_tab_text_help));
    }

    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = GetHelpViewContainer.with(view);

        // Add the events.
        this.viewContainer.layoutContactSomeone.setOnClickListener(this);
        this.viewContainer.layoutFindSupport.setOnClickListener(this);
        this.viewContainer.layoutLearnMore.setOnClickListener(this);
        this.viewContainer.layoutHelpSomeone.setOnClickListener(this);
        this.viewContainer.layoutAbout.setOnClickListener(this);
    }

    /**
     * Handles the contact someone
     */
    private void handleContactSomeone() {

        // Create the fragment.
        GetHelpNowFragment helpNowFragment = GetHelpNowFragment.newInstance();

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, helpNowFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Handles the find support
     */
    private void handleFindSupport() {

        // Create the fragment.
        GetHelpSupportFragment helpSupportFragment = GetHelpSupportFragment.newInstance();

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, helpSupportFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Handles the learn more
     */
    private void handleLearnMore() {

        // Launch the site.
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.get_help_main_learn_more_link)));
        startActivity(intent);
    }

    /**
     * Handles the help someone
     */
    private void handleHelpSomeone() {

        // Launch the site.
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.get_help_main_help_someone_link)));
        startActivity(intent);
    }

    /**
     * Handles the about
     */
    private void handleAbout() {

        // Create the fragment.
        GetHelpAboutFragment helpAboutFragment = GetHelpAboutFragment.newInstance();

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, helpAboutFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Holds the view for the Get Help
     */
    private static class GetHelpViewContainer {

        // Private Members.
        private RelativeLayout layoutContactSomeone = null;
        private RelativeLayout layoutFindSupport = null;
        private RelativeLayout layoutLearnMore = null;
        private RelativeLayout layoutHelpSomeone = null;
        private RelativeLayout layoutAbout = null;

        // Private Constructor.
        private GetHelpViewContainer() {}

        // Static Constructor.
        public static GetHelpViewContainer with(View view) {

            // Create the instance
            GetHelpViewContainer instance = new GetHelpViewContainer();

            // Get the items.
            instance.layoutContactSomeone = (RelativeLayout)view.findViewById(R.id.get_help_main_contact_someone);
            instance.layoutFindSupport = (RelativeLayout)view.findViewById(R.id.get_help_main_find_support);
            instance.layoutLearnMore = (RelativeLayout)view.findViewById(R.id.get_help_main_learn_more);
            instance.layoutHelpSomeone = (RelativeLayout)view.findViewById(R.id.get_help_main_help_someone);
            instance.layoutAbout = (RelativeLayout)view.findViewById(R.id.get_help_main_about);

            return instance;
        }
    }
}
