package com.nami.airapp.data.enums;

/**
 * Approved Status enum.
 */
public enum ApprovedStatus {

    // Values
    NOTAPPROVED (0),
    APPROVED (1),
    DISAPPROVED (2);

    // Private Members.
    private final int id;

    /**
     * Constructor.
     * @param id Approved Status value.
     */
    ApprovedStatus(int id) {

        // Set the members.
        this.id = id;
    }

    /**
     * Gets the Approved Status value.
     * @return Approved Status value.
     */
    public int getValue() {

        return this.id;
    }

    /**
     * Creates a ApprovedStatus enum from a value.
     * @param id Approved Status value.
     * @return ApprovedStatus enum.
     */
    public static ApprovedStatus fromValue(int id) {

        // Determine the id.
        switch (id) {

            case 0 : { return NOTAPPROVED; }
            case 1 : { return APPROVED; }
            case 2 : { return DISAPPROVED; }
        };

        return null;
    }
}
