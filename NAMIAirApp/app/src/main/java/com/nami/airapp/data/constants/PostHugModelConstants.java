package com.nami.airapp.data.constants;

/**
 * Holds constants for the PostHug model.
 */
public class PostHugModelConstants {

    public static final String FieldPost = "post";
    public static final String FieldUser = "user";
}
