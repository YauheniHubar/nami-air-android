package com.nami.airapp.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Toast;
import android.widget.TextView;
import android.content.pm.ActivityInfo;

import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.fragments.CreateAccountTermsFragment;
import com.nami.airapp.interfaces.ShowPrivacyPolicyListener;
import com.nami.airapp.interfaces.TermsAcceptedListener;
import com.nami.airapp.utils.ActionBarManager;
import com.parse.ParseException;
import com.parse.ParseUser;

import com.nami.airapp.data.enums.UserType;
import com.nami.airapp.utils.KeyboardManager;
import com.nami.airapp.utils.ModelValidator;
import com.nami.airapp.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class CreateAccountActivity extends ActionBarActivity implements OnClickListener, TermsAcceptedListener, ShowPrivacyPolicyListener {

    // Private Members.
    CreateAccountActivityViewContainer viewContainer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the requested orientation.
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Initialize the activity and action bar.
        this.initializeActivity();
        this.initializeActionBar();

        // Set the content view.
        setContentView(R.layout.activity_create_account);

        // Initialize the layout content.
        this.initializeLayoutContent();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (view == this.viewContainer.buttonCreateAccount) {
            this.attemptUserCreation();
        } else if (view == this.viewContainer.layoutEmailUsed) {
            this.showEmailUsedDialog();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(new CalligraphyContextWrapper(newBase));
    }

    @Override
    public void onResume() {
        super.onResume();

        // Check if the user cancelled the terms of user.
        if (ApplicationManager.getInstance().getCancelledTermsOfUse()) {
            finish();
            return;
        }
    }

    /**
     Initializes the activity.
     */
    private void initializeActivity() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(this, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.create_account_title));
        ActionBarManager.SetActionBarHomeAsUpText(this, actionBar, getString(R.string.create_account_actionbar_cancel), R.style.actionbar_home_as_up_text);
    }

    /**
     * Initializes the layout content.
     */
    private void initializeLayoutContent() {

        // Create the view container.
        this.viewContainer = CreateAccountActivityViewContainer.with(this);
        this.viewContainer.fragmentTerms = (CreateAccountTermsFragment)getSupportFragmentManager().findFragmentById(R.id.create_account_terms_fragment);
        this.viewContainer.fragmentTerms.termsAcceptedListener = this;
        this.viewContainer.fragmentTerms.showPrivacyPolicyListener = this;

        // Add the click events.
        this.viewContainer.buttonCreateAccount.setOnClickListener(this);
        this.viewContainer.layoutEmailUsed.setOnClickListener(this);

        // Show the terms of use.
        if (ApplicationManager.getInstance().getAcceptedTermsOfUse() == false) {
            this.showTermsOfUse();
        }
    }

    /**
     * Starts the main application activity.
     */
    private void startMainApplicationActivity() {

        // Determine if the guided tour has been show.
        if (ApplicationManager.getInstance().hasShownGuidedTour(this)) {

            // Create and start the activity.
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();

        } else {

            // Create and start the activity.
            Intent intent = new Intent(this, GuidedTourActivity.class);
            startActivity(intent);
            finish();

        }
    }

    /**
     * Shows the privacy policy.
     */
    public void showPrivacyPolicy() {

        // Create the intent.
        Intent intent = new Intent(this, PrivacyPolicyActivity.class);
        startActivity(intent);
    }

    /**
     * Shows the create account.
     */
    private void showTermsOfUse() {

        // Show the terms.
        this.viewContainer.scrollView.setVisibility(View.GONE);
    }

    /**
     * Shows the email used dialog.
     */
    private void showEmailUsedDialog() {

        // Create the dialog.
        final Dialog emailUsedDialog = new Dialog(this) {
            public boolean dispatchTouchEvent(MotionEvent event) {
                dismiss();
                return false;
            }
        };
        emailUsedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        emailUsedDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        emailUsedDialog.setContentView(R.layout.dialog_email_use);
        emailUsedDialog.setCanceledOnTouchOutside(true);

        // Show the dialog.
        emailUsedDialog.show();
    }

    /**
     * Attempts to create a user.
     */
    private void attemptUserCreation() {

        // Hide the keyboard.
        KeyboardManager.HideActivityKeyboard(this);

        // Check if accepted terms of use.
        if (ApplicationManager.getInstance().getAcceptedTermsOfUse() == false) {
            this.showTermsOfUse();
            return;
        }

        // Check the groups before text validation.
        if (this.viewContainer.radioUser.isChecked() == false &&
            this.viewContainer.radioCaregiver.isChecked() == false) {

            // Show the choose group dialog.
            this.showChooseGroupDialog();
            return;
        }

        // Validate the user creation.
        ModelValidator<ParseUser> validator = this.validateUserCreation();
        if (!validator.isValid()) {

            // Hide the email used.
            this.viewContainer.layoutEmailUsed.setVisibility(View.GONE);

            // Set the error message.
            this.viewContainer.textViewError.setText(validator.getMessage());

            // Show the error message.
            this.viewContainer.layoutError.setVisibility(View.VISIBLE);

        } else {

            // Check the connection.
            if (ApplicationManager.getInstance().checkNetworkConnection(this) == false) {
                return;
            }

            // Start the Create Account Task.
            new CreateAccountTask(this).execute((ParseUser)validator.getModel());
        }
    }

    /**
     * Shows the choose group dialog.
     */
    private void showChooseGroupDialog() {

        // Create the dialog.
        final Dialog postFlaggedDialog = new Dialog(this);
        postFlaggedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        postFlaggedDialog.setContentView(R.layout.dialog_choose_group);

        // Show the dialog.
        postFlaggedDialog.show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                // Hide the dialog.
                postFlaggedDialog.dismiss();
            }
        }, 5000);
    }

    /**
     * Shows the error message.
     * @param errorMessage
     */
    public void showError(String errorMessage) {

        // Set the error message.
        this.viewContainer.textViewError.setText(errorMessage);

        // Show the error message.
        this.viewContainer.layoutError.setVisibility(View.VISIBLE);
    }

    /**
     * Implements TermsAcceptedListener
     */
    public void termsAccepted() {

        this.viewContainer.scrollView.setVisibility(View.VISIBLE);
    }

    /**
     * Attempts to validate the user creation.
     * @return ModelValidator.
     */
    private ModelValidator<ParseUser> validateUserCreation() {

        // Create the user.
        ParseUser model = new ParseUser();

        // Get the values.
        String emailAddress = this.viewContainer.editTextEmail.getText().toString();
        String password = this.viewContainer.editTextPassword.getText().toString();
        String passwordConfirm = this.viewContainer.editTextPasswordConfirm.getText().toString();
        boolean isUser = this.viewContainer.radioUser.isChecked();
        boolean isCaregiver = this.viewContainer.radioCaregiver.isChecked();


        // Set the model properties.
        model.setEmail(emailAddress);
        model.setUsername(emailAddress);
        model.setPassword(password);
        if (isUser)
            model.put(UserModelConstants.FieldUserType, UserType.USER.getValue());
        else model.put(UserModelConstants.FieldUserType, UserType.CAREGIVER.getValue());

        // Validate the email.
        if (TextUtils.isEmpty(emailAddress)) {
            return new ModelValidator<ParseUser>(model, false, getString(R.string.create_account_validation_missing_email));
        }

        // Validate the password.
        if (TextUtils.isEmpty(password)) {
            return new ModelValidator<ParseUser>(model, false, getString(R.string.create_account_validation_missing_password));
        }

        // Validate the password confirmation.
        if (TextUtils.isEmpty(passwordConfirm)) {
            return new ModelValidator<ParseUser>(model, false, getString(R.string.create_account_validation_missing_password_confirm));
        }

        // Check the passwords.
        if (!TextUtils.equals(password, passwordConfirm)) {
            return new ModelValidator<ParseUser>(model, false, getString(R.string.create_account_validation_password_mismatch));
        }

        // Return success.
        return new ModelValidator<ParseUser>(model, true, null);
    }

    /**
     * Task to Create Account
     */
    private class CreateAccountTask extends AsyncTask<ParseUser, Void, ParseException> {

        // Private Members.
        private ProgressDialog progressDialog = null;
        private CreateAccountActivity activity;

        public CreateAccountTask(CreateAccountActivity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {

            // Create the progress dialog.
            this.progressDialog = new ProgressDialog(CreateAccountActivity.this);
            this.progressDialog.setTitle(getString(R.string.create_account_progress_title));
            this.progressDialog.setMessage(getString(R.string.create_account_progress_message));
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        @Override
        protected ParseException doInBackground(ParseUser... users) {

            // Get the user.
            ParseUser user = users[0];

            // Attempt to create account.
            try
            {
                user.signUp();
            }
            catch(ParseException e) {

                return e;
            }

            return null;
        }

        @Override
        protected void onPostExecute(ParseException e) {

            // Dismiss the progress dialog.
            this.progressDialog.dismiss();

            // Check for an exception.
            if (e != null) {

                if (TextUtils.indexOf(e.getMessage(), "already taken") >= 0) {

                    // Display the error.
                    activity.showError(e.getLocalizedMessage());
                } else {

                    ApplicationManager.getInstance().showConnectionError(activity);
                }
            } else {

                // Start the main application activity.
                startMainApplicationActivity();
            }
        }
    }


    /**
     * Holds the view for the Create Account Activity.
     */
    private static class CreateAccountActivityViewContainer {

        // Private Members
        private Button buttonCreateAccount = null;
        private EditText editTextEmail = null;
        private EditText editTextPassword = null;
        private EditText editTextPasswordConfirm = null;
        private RadioButton radioUser = null;
        private RadioButton radioCaregiver = null;
        private LinearLayout layoutEmailUsed = null;
        private LinearLayout layoutError = null;
        private TextView textViewError = null;
        private ScrollView scrollView = null;
        private CreateAccountTermsFragment fragmentTerms = null;

        /**
         * Private Constructor
         */
        private CreateAccountActivityViewContainer() {}

        /**
         * Public Static Constructor
         * @param activity Activity to create the view from
         * @return CreateAccountActivityViewContainer
         */
        public static CreateAccountActivityViewContainer with(Activity activity) {

            // Create the instance.
            CreateAccountActivityViewContainer instance = new CreateAccountActivityViewContainer();

            // Get the items.
            instance.buttonCreateAccount = (Button)activity.findViewById(R.id.create_account_button_create_account);
            instance.editTextEmail = (EditText)activity.findViewById(R.id.create_account_edit_text_email);
            instance.editTextPassword = (EditText)activity.findViewById(R.id.create_account_edit_text_password);
            instance.editTextPasswordConfirm = (EditText)activity.findViewById(R.id.create_account_edit_text_password_confirm);
            instance.radioUser = (RadioButton)activity.findViewById(R.id.create_account_radio_user);
            instance.radioCaregiver = (RadioButton)activity.findViewById(R.id.create_account_radio_caregiver);
            instance.layoutEmailUsed = (LinearLayout)activity.findViewById(R.id.create_account_email_used);
            instance.layoutError = (LinearLayout)activity.findViewById(R.id.create_account_layout_error);
            instance.textViewError = (TextView)activity.findViewById(R.id.create_account_text_view_error);
            instance.scrollView = (ScrollView)activity.findViewById(R.id.create_account_scroll_view);

            return instance;
        }
    }
}
