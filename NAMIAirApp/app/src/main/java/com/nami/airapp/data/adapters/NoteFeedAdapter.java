package com.nami.airapp.data.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;

import com.nami.airapp.data.enums.FeedType;
import com.nami.airapp.data.enums.UserType;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.views.PostFeedItemView;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.ArrayList;
import java.util.List;

public class NoteFeedAdapter extends BaseParseAdapter {

    // Private Members
    private Fragment fragment = null;
    private String queryText = null;
    private FeedType feedType = null;
    private UserType userType = null;
    private List<PostModel> dataSet = null;

    /**
     * Default Constructor.
     * @param context Context which the adapter runs in.
     * @param feedType The feed type (All Posts, My Posts).
     * @param userType The user type (User, Caregiver).
     * @param reload Whether to reload the data or not.
     */
    public NoteFeedAdapter(Fragment fragment, Context context, FeedType feedType, UserType userType, String queryText, boolean reload) {
        super(context);

        // Set the members.
        this.fragment = fragment;
        this.feedType = feedType;
        this.userType = userType;
        this.queryText = queryText;

        // Load / Reload the data.
        if (reload)
            this.reloadData();
    }

    /**
     * Gets the number of items in the list.
     * @return Number of items in the list.
     */
    @Override
    public int getCount() {

        return this.dataSet.size();
    }

    /**
     * Gets the item at the position in the list.
     * @param position Position in the list.
     * @return Item at the position in the list.
     */
    @Override
    public PostModel getItem(int position) {

        return this.dataSet.get(position);
    }

    /**
     * Gets the item identifier at the position in the list.
     * @param position Position in the list.
     * @return Item identifier at the position in the list.
     */
    @Override
    public long getItemId(int position) {

        return position;
    }

    /**
     * Gets the view for the item in the position in the list.
     * @param position Position in the list.
     * @param convertView View of the item in the list.
     * @param parent Parent view of the item in the list.
     * @return Updated view of the item in the list.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Create the view if necessary.
        if (convertView == null) {
            convertView = new PostFeedItemView(context);
        }

        // Get the item and update the view.
        PostModel model = this.getItem(position);
        ((PostFeedItemView)convertView).updateFromModel(fragment, model, position);

        return convertView;
    }

    /**
     * Reloads the data.
     */
    public void reloadData() {

        // Set loading.
        setLoading(true);

        // Create the query factory.
        ParseQueryAdapter.QueryFactory<PostModel> queryFactory = PostModel.createNoteFeedAdapterQuery(this.feedType, this.userType, this.queryText);

        // Create and execute the query.
        ParseQuery<PostModel> query = queryFactory.create();
        new PostFeedParseQueryTask().execute(query);
    }

    /**
     * Sets the feed type for the adapter.
     * @param feedType The feed type (All Posts, My Posts).
     */
    public void setFeedType(FeedType feedType) {

        this.feedType = feedType;
        this.reloadData();
    }

    /**
     * Gets the data set.
     * @return List of data.
     */
    public List<PostModel> getDataSet() {

        return this.dataSet;
    }

    /**
     * Sets the data set.
     * @param dataSet List of data.
     */
    public void setDataSet(List<PostModel> dataSet) {

        // Set the data set.
        this.dataSet = dataSet;

        // Set not loading.
        setLoading(false);

        // Notify that the data set has changed.
        this.notifyDataSetChanged();
    }

    /**
     * Queries parse.
     */
    private class PostFeedParseQueryTask extends AsyncTask<ParseQuery<PostModel>, Void, List<PostModel>> {

        @Override
        protected void onPreExecute() {

            // Create the posts.
            dataSet = new ArrayList<PostModel>();
        }

        @Override
        protected List<PostModel> doInBackground(ParseQuery<PostModel>... queries) {

            // Declare the return value.
            List<PostModel> data = null;

            // Get the query.
            ParseQuery<PostModel> query = queries[0];

            // Attempt to execute the query.
            try {
                data = query.find();
            }
            catch (ParseException e) {
                data = new ArrayList<PostModel>();
            }

            return data;
        }

        @Override
        protected void onPostExecute(List<PostModel> postsFound) {

            // Set the data set.
            setDataSet(postsFound);
        }
    }
}
