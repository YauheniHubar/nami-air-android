package com.nami.airapp;

import android.app.Application;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.constants.InstallationModelConstants;
import com.parse.*;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import com.nami.airapp.data.models.FeelingModel;
import com.nami.airapp.data.models.NoteModel;
import com.nami.airapp.data.models.PostFlagModel;
import com.nami.airapp.data.models.PostHugModel;
import com.nami.airapp.data.models.PostLikeModel;
import com.nami.airapp.data.models.PostMeTooModel;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.data.models.PostNoteModel;
import com.nami.airapp.data.models.PostNotificationModel;
import com.nami.airapp.data.models.RedFlagKeywordModel;
import com.nami.airapp.data.singletons.ApplicationManager;

public class AirAppApplication extends Application {


    // Live

    private final static String PARSE_APP_ID = "uJUkecVnvp90fNExwxXQEphTqcdmVEpDVJE5IWaj";
    private final static String PARSE_CLIENT_KEY = "8CVatJmTRaunYpYI5VaAzCMb8uXCf7hfeXjMJ5Jj";

    // New Live (after migration)

//    public static final String PARSE_APP_ID = "C7FNB0zU4q5cvtuyFuLHkPwreKUXhl90BOTSzsZQ";
//    public static final String PARSE_CLIENT_KEY = "pkmqxHEmVXNHP56i9sejgvCKiy5o02lKfokx5fcZ";

    // Staging
//    private final static String PARSE_APP_ID = "C7FNB0zU4q5cvtuyFuLHkPwreKUXhl90BOTSzsZQ";
//    private final static String PARSE_CLIENT_KEY = "V1wn99OnxyMoSrPWVKQT2aChRZ30pG80984Trq9S";



    private final static String FLURRY_APP_ID = "WN7FQJX5249627XQPCR2";

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        // Initialize the parse application.
        this.initializeParseApplication();

        // Fetch the static data sets.
        this.fetchStaticDataSets();

        // Configure the default font.
        this.configureDefaultFont();
    }

    /**
     * Initializes the parse application.
     */
    private void initializeParseApplication() {

        // Initialize the parse models.
        ParseObject.registerSubclass(FeelingModel.class);
        ParseObject.registerSubclass(NoteModel.class);
        ParseObject.registerSubclass(PostFlagModel.class);
        ParseObject.registerSubclass(PostHugModel.class);
        ParseObject.registerSubclass(PostLikeModel.class);
        ParseObject.registerSubclass(PostMeTooModel.class);
        ParseObject.registerSubclass(PostModel.class);
        ParseObject.registerSubclass(PostNoteModel.class);
        ParseObject.registerSubclass(PostNotificationModel.class);
        ParseObject.registerSubclass(RedFlagKeywordModel.class);

        // Initialize the application id and client key.
//        Parse.initialize(this, PARSE_APP_ID, PARSE_CLIENT_KEY);
        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                .applicationId(PARSE_APP_ID)
                .clientKey(PARSE_CLIENT_KEY)
                .server("https://parseapi.back4app.com")

        .build()
        );

        // Set the default push callback.
//        PushService.setDefaultPushCallback(this, MainActivity.class);

        // Save the installation.
        if (ParseUser.getCurrentUser() != null) {
            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
            installation.put(InstallationModelConstants.FieldUser, ParseUser.getCurrentUser());
            installation.addUnique("channels", "receivePopups");
            installation.saveEventually();
        }
    }


    /**
     * Returns the Flurry application key.
     */
    public static String getFlurryApplicationKey() {

        return FLURRY_APP_ID;
    }


    /**
     * Fetches the static data sets.
     */
    private void fetchStaticDataSets() {

        // Configure the connection receiver.
        ApplicationManager.getInstance().configureConnectionReceiver(this);

        // Determine if there is a network connection.
        if (ApplicationManager.getInstance().hasNetworkConnection(this)) {

            // Fetch the note data set.
            ApplicationManager.getInstance().fetchNoteList();

            // Fetch the feeling data set.
            ApplicationManager.getInstance().fetchFeelingsList();

            // Fetch the red flag keyword data set.
            ApplicationManager.getInstance().fetchRedFlagKeywordList();

            // Fetch the user flagged posts.
            ApplicationManager.getInstance().fetchUserFlaggedPosts();

            // Fetch the moderator flagged posts.
            ApplicationManager.getInstance().fetchModeratorFlaggedPosts();
        }
    }

    /**
     * Configures the default font.
     */
    private void configureDefaultFont() {

        CalligraphyConfig.initDefault("fonts/proxima_nova_webfont.ttf", R.attr.fontPath);
    }
}
