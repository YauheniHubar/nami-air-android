package com.nami.airapp.data.models;

import android.widget.Toast;
import android.content.Context;
import java.util.HashMap;
import java.util.Date;
import java.lang.Comparable;
import android.os.Parcel;
import android.os.Parcelable;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseClassName;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;


/**
 * Created by Brian on 12/9/2014.
 */
public class UserModel {


    /**
     * Creates a post notification.
     * @param context The context.
     */
    public static void DeleteAccount(final Context context) {

        // Create the params.
        HashMap<String, Object> params = new HashMap<String, Object>();

        // Call the cloud code.
        try
        {
            ParseCloud.callFunction("deleteAccount", params);
        }
        catch (ParseException e) {}
    }

}
