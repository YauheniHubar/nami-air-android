package com.nami.airapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import java.util.Date;

import com.nami.airapp.R;
import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.data.models.PostNotificationModel;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.AppFragmentManager;
import com.nami.airapp.utils.KeyboardManager;
import com.nami.airapp.widgets.EllipsisTextView;


/**
 * My Post Detail Fragment.
 */
public class PostNotificationAdminFragment extends Fragment implements OnClickListener {

    // Private Static Members.
    private static final String KEY_FRAGMENT_NOTIFICATION_ADMIN_POST = "KEY_FRAGMENT_NOTIFICATION_ADMIN_POST";
    private static final String KEY_FRAGMENT_NOTIFICATION_ADMIN_MESSAGE = "KEY_FRAGMENT_NOTIFICATION_ADMIN_MESSAGE";
    private static final String KEY_FRAGMENT_NOTIFICATION_ADMIN_DATE = "KEY_FRAGMENT_NOTIFICATION_ADMIN_DATE";

    // Private Members.
    private Context context = null;
    private PostNotificationAdminViewContainer viewContainer = null;
    private PostModel postModel;
    private String notificationMessage;
    private Date notificationDate;

    /**
     * Default Constructor.
     */
    public PostNotificationAdminFragment() {}

    /**
     * Creates a new instance of a PostNotificationAdminFragment.
     * @param postNotificationModel The post notification model.
     * @return New instance of a MyPostDetailFragment.
     */
    public static PostNotificationAdminFragment newInstance(PostNotificationModel postNotificationModel) {

        // Create the new instance.
        PostNotificationAdminFragment instance = new PostNotificationAdminFragment();

        // Add the post model to the bundle.
        Bundle args = new Bundle();
        args.putParcelable(KEY_FRAGMENT_NOTIFICATION_ADMIN_POST, postNotificationModel.getPostModel());
        args.putString(KEY_FRAGMENT_NOTIFICATION_ADMIN_MESSAGE, postNotificationModel.getMessage());
        args.putLong(KEY_FRAGMENT_NOTIFICATION_ADMIN_DATE, postNotificationModel.getUpdatedAt().getTime());
        instance.setArguments(args);

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            this.notificationMessage = args.getString(KEY_FRAGMENT_NOTIFICATION_ADMIN_MESSAGE);
            this.notificationDate = new Date(args.getLong(KEY_FRAGMENT_NOTIFICATION_ADMIN_DATE));
            this.postModel = args.getParcelable(KEY_FRAGMENT_NOTIFICATION_ADMIN_POST);
        }
    }

    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set the action bar.
        MainActivity mainActivity = (MainActivity)context;
        mainActivity.setActionBarNoTabs();

        // Set options menu.
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_notification_admin, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    AppFragmentManager.RemoveFromStack(that, context);
                    return true;
                }

                return false;
            }
        });
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.post_feed, menu);        
    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, context);
                return true;            
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (view == this.viewContainer.textViewPostMessage) {
            this.showPostModelDetail(this.postModel);
        }
    }

    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, getString(R.string.post_notification_admin_title));
    }

    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = PostNotificationAdminViewContainer.with(view);

        // Add the click events.
        this.viewContainer.textViewPostMessage.setOnClickListener(this);

        // Set the content.
        this.updateNotificationContent();
    }

    /**
     * Updates the notification content.
     */
    private void updateNotificationContent() {

        // Create the reference post text.
        SpannableString postText = new SpannableString(String.format(getString(R.string.post_notification_admin_reference), this.postModel.getPostText()));
        this.viewContainer.textViewPostMessage.setText(postText, BufferType.SPANNABLE);

        // Set the message text.
        this.viewContainer.textViewAdminMessage.setText(this.notificationMessage);
    }

    /**
     * Shows the post model detail.
     * @param postModel The post model.
     */
    private void showPostModelDetail(PostModel postModel) {

        // Create the fragment.
        MyPostDetailFragment postDetailFragment = MyPostDetailFragment.newInstance(postModel);

        // Create the transaction.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.main_container, postDetailFragment);
        transaction.addToBackStack(null);

        // Commit the transaction.
        transaction.commit();
    }

    /**
     * Holds the view for the Post Notification Item.
     */
    private static class PostNotificationAdminViewContainer {

        // Private Members.
        private EllipsisTextView textViewPostMessage = null;
        private TextView textViewAdminMessage = null;

        // Private Constructor.
        private PostNotificationAdminViewContainer() {}

        // Static Constructor.
        public static PostNotificationAdminViewContainer with(View view) {

            // Create the instance
            PostNotificationAdminViewContainer instance = new PostNotificationAdminViewContainer();

            // Get the items.
            instance.textViewPostMessage = (EllipsisTextView)view.findViewById(R.id.post_notification_admin_text_view_post);
            instance.textViewAdminMessage = (TextView)view.findViewById(R.id.post_notification_admin_text_view_message);

            return instance;
        }
    }
}
