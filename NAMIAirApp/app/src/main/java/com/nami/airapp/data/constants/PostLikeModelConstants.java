package com.nami.airapp.data.constants;

/**
 * Holds constants for the PostLike model.
 */
public class PostLikeModelConstants {

    public static final String FieldPost = "post";
    public static final String FieldUser = "user";
}
