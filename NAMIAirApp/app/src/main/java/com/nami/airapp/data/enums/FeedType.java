package com.nami.airapp.data.enums;

/**
 * Feed Type enum
 */
public enum FeedType {

    // Values
    ALL_POSTS (1),
    MY_POSTS (2);

    // Private Members.
    private final int id;

    /**
     * Constructor.
     * @param id Feed Type value.
     */
    FeedType(int id) {

        // Set the members.
        this.id = id;
    }

    /**
     * Gets the Feed Type value.
     * @return Feed Type value.
     */
    public int getValue() {

        return this.id;
    }

    /**
     * Creates a FeedType enum from a value.
     * @param id Feed Type value.
     * @return FeedType enum.
     */
    public static FeedType fromValue(int id) {

        // Determine the id.
        switch (id) {

            case 1 : { return ALL_POSTS; }
            case 2 : { return MY_POSTS; }
        };

        return null;
    }
}
