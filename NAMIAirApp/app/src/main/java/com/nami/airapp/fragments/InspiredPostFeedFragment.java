package com.nami.airapp.fragments;


import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.ArrayList;

import com.nami.airapp.R;
import com.nami.airapp.activities.MainActivity;
import com.nami.airapp.data.adapters.BaseParseAdapter.OnQueryRunListener;
import com.nami.airapp.data.adapters.InspiredPostFeedAdapter;
import com.nami.airapp.data.constants.UserModelConstants;
import com.nami.airapp.data.enums.FeedType;
import com.nami.airapp.data.enums.UserType;
import com.nami.airapp.data.models.PostModel;
import com.nami.airapp.data.singletons.ApplicationManager;
import com.nami.airapp.utils.ActionBarManager;
import com.nami.airapp.utils.AppFragmentManager;
import com.nami.airapp.utils.KeyboardManager;
import com.nami.airapp.widgets.EllipsisTextView;
import com.parse.ParseException;
import com.parse.ParseUser;

public class InspiredPostFeedFragment extends ListFragment implements View.OnClickListener {

    // Private Static Members
    private static final String KEY_FRAGMENT_INSPIRED_POST = "KEY_FRAGMENT_INSPIRED_POST";
    private static final String KEY_FRAGMENT_INSPIRED_POST_FEED_LIST = "KEY_FRAGMENT_INSPIRED_POST_FEED_LIST";

    // Private Members.
    private Context context = null;
    private InspiredPostFeedAdapter adapter = null;
    private List<PostModel> dataSet = null;
    private PostModel postModel = null;
    private InspiredPostFeedViewContainer viewContainer = null;

    /**
     * Default Constructor.
     */
    public InspiredPostFeedFragment() {}

    /**
     * Creates a new instance of a PostFeedFragment.
     * @return New instance of a PostFeedFragment.
     */
    public static InspiredPostFeedFragment newInstance(PostModel postModel) {

        // Create and return the new instance.
        InspiredPostFeedFragment instance = new InspiredPostFeedFragment();

        // Add the post model to the bundle.
        Bundle args = new Bundle();
        args.putParcelable(KEY_FRAGMENT_INSPIRED_POST, postModel);
        instance.setArguments(args);

        return instance;
    }

    /**
     * Called on attach.
     * @param activity The activity.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Set the context.
        this.context = activity;
    }

    /**
     *
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            this.postModel = args.getParcelable(KEY_FRAGMENT_INSPIRED_POST);
        }
    }


    /**
     *
     * @param inflater The inflater.
     * @param container The view container.
     * @param savedInstanceState Saved instance state.
     * @return Newly created view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Set the action bar.
        MainActivity mainActivity = (MainActivity)context;
        mainActivity.setActionBarNoTabs();

        // Set options menu.
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inspired_post_feed, container, false);
    }

    /**
     * Called when the view is created.
     * @param view Instance of the view.
     * @param savedInstanceState Saved instance state.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize the fragment and action bar.
        this.initializeFragment();
        this.initializeActionBar();

        // Initialize the layout content.
        this.initializeLayoutContent(view);

        // Handle the back button appropriately.
        final Fragment that = this;
        this.getView().setFocusableInTouchMode(true);
        this.getView().requestFocus();
        this.getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    AppFragmentManager.RemoveFromStack(that, context);
                    return true;
                }

                return false;
            }
        });

        // Check the connection.
        if (ApplicationManager.getInstance().checkNetworkConnection(this.context) == false) {
            return;
        }

        // Initialize the data.
        new InitializePageAsyncTask(this.context).execute();
    }

    /**
     * Called when the menu is created.
     * @param menu The menu.
     * @param inflater The inflater.
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.post_feed, menu);

    }

    /**
     * Called when a menu item is selected.
     * @param item The menu item that is selected.
     * @return True if handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                AppFragmentManager.RemoveFromStack(this, context);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when the fragment is paused.
     */
    @Override
    public void onPause() {
        super.onPause();

        // Put the adapter list into the bundle.
        getArguments().putParcelableArrayList(KEY_FRAGMENT_INSPIRED_POST_FEED_LIST, (ArrayList<PostModel>)this.adapter.getDataSet());
    }

    /**
     * Called when the activity is created.
     * @param savedInstanceState The saved instance state.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check the bundle.
        if (getArguments() != null && getArguments().containsKey(KEY_FRAGMENT_INSPIRED_POST_FEED_LIST)) {
            List<PostModel> dataSet = getArguments().getParcelableArrayList(KEY_FRAGMENT_INSPIRED_POST_FEED_LIST);


            // Create the adapter.
            this.adapter = new InspiredPostFeedAdapter(this, context, this.postModel, false);

            // Set the data set.
            this.adapter.setDataSet(dataSet);

        } else {


            // Create and set the adapter.
            this.adapter = new InspiredPostFeedAdapter(this, context, this.postModel, true);
        }

        // Set the adapter.
        setListAdapter(this.adapter);
    }

    /**
     * Called when a list view item is clicked.
     * @param listView List view containing the clicked item.
     * @param view The view.
     * @param position The position of the item.
     * @param id The identifier of the item.
     */
    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {

        // Get the post model.
        PostModel selectedPostModel = (PostModel)getListView().getItemAtPosition(position);
        this.showPostModelDetail(selectedPostModel);
    }

    /**
     * Responds to click events.
     * @param view The view that has a click event.
     */
    public void onClick(View view) {

        // Determine the view.
        if (view == this.viewContainer.textViewInspiredPost) {
            this.showPostModelDetail(this.postModel);
        }
    }

    /**
     * OnQueryRunListener Implementation
     */
    public void onLoaded() {

        // Show / Hide the appropriate controls.
        this.viewContainer.progress.setVisibility(View.GONE);
    }

    /**
     * OnQueryRunListener Implementation
     */
    public void onLoading() {

        // Show / Hide the appropriate controls.
        this.viewContainer.progress.setVisibility(View.VISIBLE);
    }


    /**
     Initializes the fragment.
     */
    private void initializeFragment() {

    }

    /**
     * Initializes the action bar.
     */
    private void initializeActionBar() {

        // Set the action bar properties.
        ActionBar actionBar = ((ActionBarActivity) context).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        ActionBarManager.SetCustomView(context, actionBar);
        ActionBarManager.SetActionBarTitle(actionBar, "Inspired Posts");
    }

    /**
     * Initializes the layout content.
     * @param view The fragment view.
     */
    private void initializeLayoutContent(View view) {

        // Create the view container.
        this.viewContainer = InspiredPostFeedViewContainer.with(view);

        // Set the click events.
        this.viewContainer.textViewInspiredPost.setOnClickListener(this);

        // Hide the appropriate controls.
        this.viewContainer.textViewInspiredPost.setVisibility(View.INVISIBLE);
    }

    /**
     * Updates the post content.
     */
    private void updatePostContent() {

        // Create the reference post text.
        SpannableString postText = new SpannableString(String.format(getString(R.string.post_inspired_feed_reference), this.postModel.getPostText()));
        this.viewContainer.textViewInspiredPost.setText(postText, TextView.BufferType.SPANNABLE);
    }

    /**
     * Shows the post model detail.
     * @param postModel The post model.
     */
    private void showPostModelDetail(PostModel postModel) {

        // Determine the post owner.
        ParseUser postOwnerUser = postModel.getUser();
        if (TextUtils.equals(postOwnerUser.getObjectId(), ParseUser.getCurrentUser().getObjectId())) {

            // Create the fragment.
            MyPostDetailFragment postDetailFragment = MyPostDetailFragment.newInstance(postModel);

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, postDetailFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();

        } else {

            // Create the fragment.
            PostDetailFragment postDetailFragment = PostDetailFragment.newInstance(postModel);

            // Create the transaction.
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            transaction.replace(R.id.main_container, postDetailFragment);
            transaction.addToBackStack(null);

            // Commit the transaction.
            transaction.commit();
        }
    }

    /**
     * Holds the view for the Inspired Post Feed.
     */
    private static class InspiredPostFeedViewContainer {

        // Private Members.
        EllipsisTextView textViewInspiredPost = null;
        private LinearLayout progress;

        // Private Constructor.
        private InspiredPostFeedViewContainer() {}

        // Static Constructor.
        public static InspiredPostFeedViewContainer with(View view) {

            // Create the instance
            InspiredPostFeedViewContainer instance = new InspiredPostFeedViewContainer();

            // Get the items.
            instance.textViewInspiredPost = (EllipsisTextView)view.findViewById(R.id.post_inspired_feed_text_view);
            instance.progress = (LinearLayout)view.findViewById(R.id.progress);

            return instance;
        }

    }

    /**
     * Initializes the page with data.
     */
    private class InitializePageAsyncTask extends AsyncTask<Void, Void, Void> {

        private Context context = null;
        private ParseException parseException = null;


        public InitializePageAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Fetch the model if needed.
            try
            {
                if (postModel.isDataAvailable() == false)
                    postModel.fetchIfNeeded();
                else if (ApplicationManager.getInstance().postNeedsUpdating(postModel.getObjectId()))
                    postModel.fetch();

            }
            catch (ParseException e)
            {

                this.parseException = e;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void voids) {

            if (this.parseException != null) {
                ApplicationManager.getInstance().showConnectionError(context);
            }

            // Set the content.
            updatePostContent();

            // Show the appropriate controls.
            viewContainer.textViewInspiredPost.setVisibility(View.VISIBLE);
        }
    }
}
