package com.nami.airapp.data.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import java.util.List;

import com.nami.airapp.data.models.FaqModel;
import com.nami.airapp.data.models.FaqSectionModel;
import com.nami.airapp.views.FaqListItemChildView;
import com.nami.airapp.views.FaqListItemView;


public class FaqAdapter extends BaseExpandableListAdapter {


    // Private Members.
    private Context context;
    private FaqSectionModel frequentlyAskedQuestions;

    public FaqAdapter(Context context, FaqSectionModel frequentlyAskedQuestions) {
        this.context = context;
        this.frequentlyAskedQuestions = frequentlyAskedQuestions;
    }

    public Object getChild(int groupPosition, int childPosition) {
        return frequentlyAskedQuestions.Questions.get(groupPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = new FaqListItemChildView(context);
        }

        // Get the item and update the view.
        final FaqModel model = this.frequentlyAskedQuestions.Questions.get(groupPosition);
        ((FaqListItemChildView)convertView).updateFromModel(model, 0);

        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    public Object getGroup(int groupPosition) {
        return frequentlyAskedQuestions.Questions.get(groupPosition);
    }

    public int getGroupCount() {
        return frequentlyAskedQuestions.Questions.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = new FaqListItemView(context);
        }

        // Get the item and update the view.
        final FaqModel model = this.frequentlyAskedQuestions.Questions.get(groupPosition);
        ((FaqListItemView)convertView).updateFromModel(model, 0);

        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
